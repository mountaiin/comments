 const StaticmanAPI = require('staticman/lib/Staticman');
   const staticman = new StaticmanAPI();

exports.handler = async (event, context, callback) => {
    try {
               const response = await staticman.processEntry(event);
        callback(null, {
                     statusCode: 200,
                     body: JSON.stringify(response)

        });

    } catch (e) {
        callback(null, {
                     statusCode: e.status || 500,
            body: JSON.stringify({
                           message: e.message

            })

        });

    }

};
