---
tags: explaining
---
**Dateiorganisation und Strukturierung in Obsidian:**

1. **Ordnerstruktur erstellen**: Ordne deine Notizen in einer sinnvollen Ordnerstruktur, z.B. thematisch oder nach Projekten. Du könntest zum Beispiel nach Hauptthemenbereichen wie "Arbeit", "Persönliche Entwicklung" und "Freizeit" organisieren.
2. **Dateinamen**: Verwende aussagekräftige Dateinamen, die den Inhalt der Notizen widerspiegeln. Du könntest zum Beispiel eine Namenskonvention verwenden, wie etwa ein Präfix mit dem Thema oder Projekt, gefolgt vom Haupttitel der Notiz.
3. **Tags**: Nutze Tags, um Notizen mit ähnlichen Themen oder Konzepten einfach wiederzufinden. Du kannst Tags erstellen, indem du ein "#"-Symbol gefolgt von dem gewünschten Tag-Wort eingibst.
4. **Verknüpfungen**: Verwende interne Links und Verweise, um verwandte Notizen miteinander zu verbinden. Das hilft dir, Zusammenhänge und Verbindungen zwischen verschiedenen Themen und Ideen herzustellen und zu entdecken.
5. **Index- oder Startseite**: Erstelle eine Startseite oder ein Inhaltsverzeichnis, das eine Übersicht deiner wichtigsten Notizen und Themenbereiche bietet und schnellen Zugriff auf alle ermöglicht.

**Arbeiten mit Templates in Obsidian:**

1. **Templates erstellen**: Um ein neues Template zu erstellen, lege einfach eine neue Markdown-Datei (*.md) in einem Ordner deiner Wahl an, der nur Template-Dateien enthält. Schreibe dann die gewünschten Inhalte und Formatierungen in die Datei.
2. **Templates verwenden**: Um dein erstelltes Template zu verwenden, kannst du `{% raw %}{{templater_template:TemplateName}}{% endraw %}` in einer neuen oder bestehenden Notiz eingeben und durch den tatsächlichen Namen deines Templates ersetzen. Sobald du die Templater-Erweiterung in Obsidian installiert und konfiguriert hast, wird dein Template automatisch eingesetzt.
3. **Templater-Plugin**: Wenn du das Templater-Plugin verwendest, stelle sicher, dass du es korrekt konfiguriert hast. Gehe zu Einstellungen ➜ Plugin-Optionen ➜ Templater und wähle den Ordner aus, in dem du deine Templates gespeichert hast.
4. **Dynamische Templates**: Du kannst auch dynamische Templates erstellen, um z.B. aktuelles Datum und Uhrzeit einzufügen. Für das aktuelle Datum könntest du {% raw %}`{{date:YYYY-MM-DD}}`{% endraw %} in dein Template einfügen. Schaue in die Templater-Dokumentation für weitere dynamische Funktionen und Anwendungsbeispiele.