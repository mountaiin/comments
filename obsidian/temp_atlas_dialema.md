Hallo nochmal :)
Ich brauche heute wieder deine hilfe. Ich entwickle gerade ein TUI mithilfe von dialog und bräuchte hier und da noch ein paar Verbesserungen.
Antworte mir bitte prägnant, es sei denn ich bitte dich um eine ausführlichere Antwort. Wenn du mir mit meinem Code hilfst, dann zitiere antworte bitte nur mit den relevanten Zeilen, es sei denn ich bitte dich darum, den gesamten Code zu zitieren ausführlicher zu antworten oder mir etwas genauer zu erklären. Hier zunächst mein gesamter Code, damit du weißt worum es geht:

```
#!/bin/bash

mkdir -p sessions
mkdir -p conf_files
mktemp

###############################
# First we define our FUNCTIONS
#
#
#
# Find .bin files that are bigger than 1000M in the current directory
function find_files() {
    find $1 -type f -iname "*ggml*.bin" -size +1000M 2>/dev/null
}



# Search for models on various locations
function search_for_models() {
    locations=("$@")
    local all_files=""
    for loc in "${locations[@]}"; do
        local new_files=$(find_files "$loc")
        if [ -n "$new_files" ]; then
            if [ -z "$all_files" ]; then
                all_files="$new_files"
            else
                all_files="$all_files"$'\n'"$new_files"
            fi
        fi
    done
    echo "$all_files"
}



# Choose a model from the list
function choose_model() {
    files=$(search_for_models ~ "/media" "/mnt" "/Volumes" "/run/media" "/media/*/")
    if [ -z "$files" ]; then
        echo "Die automatische Suche konnte keine passenden Modelle finden."
        read -p "Bitte gib den Pfad zu deinem Modell an: " selected_model
    else
        # Create a menu with OpenAI models plus the previously found models
        files_array=($files)
        short_names=()
        for file in "${files_array[@]}"; do
            short_name=$(basename "$file" .bin)
            short_names+=("$short_name")
        done
        options=("GPT-3.5-turbo" "GPT-4" "${short_names[@]}")
        menu_items=""
        for i in "${!options[@]}"; do
            menu_items="${menu_items} $((i+1)) \"${options[$i]}\""
        done
        cmd="dialog --stdout --no-cancel --menu \"Wähle ein Modell aus der Liste aus:\" 28 108 15 ${menu_items}"
        selected_index=$(eval $cmd)
        if [ -z "$selected_index" ] || [ "$selected_index" -le 0 ] || [ "$selected_index" -gt "${#options[@]}" ]; then
            echo "Ungültige Auswahl. Beende das Skript."
            exit 1
        fi
        selected_model=${options[$((selected_index-1))]}
        # Create a symlink for the selected model into ./models
        # This is an easy way to share models with LocalAI without having to copy them
        # or to start localAI with --models-path flag each time
        ln -s "$selected_model" ./models
    fi
    echo "$selected_model"
}



# This function is needed to read the chat_history.txt TODO
function read_chat_history() {
  tempfile="$1"
  chat_history=""
  while IFS= read -r line; do
    role=$(echo "$line" | awk -F ': ' '{print $1}')
    content=$(echo "$line" | awk -F ': ' '{print $2}')
    chat_history="${chat_history}{\"role\": \"${role}\", \"content\": \"${content}\"}, "
  done < "$tempfile"
  echo "[${chat_history%??}]"
}



# Make a choice for which endpoint to use and create a history
function process_user_input() {
  user_input="$1"
  selected_model="$2"
  chat_history=$(read_chat_history "$tempfile")
  if [[ "$selected_model" == "GPT-4" || "$selected_model" == "GPT-3.5-turbo" ]]; then
    # Hier die API-Anfrage an OpenAI senden und die Antwort in der Variable response speichern
    response="$request_openai"
  else
    # Hier die API-Anfrage an localhost:8080 senden und die Antwort in der Variable response speichern
    response="$request_local"
  fi
  # Benutzereingabe und API-Antwort an die temporäre Datei anhängen
  echo "User: $user_input" >> "$tempfile"
  echo "Atlas: $response" >> "$tempfile"
  # Anzeigen der tailbox mit der aktualisierten temporären Datei und Eingabeaufforderung für den nächsten Prompt
  dialog --begin 1 1 --tailbox "$tempfile" 28 80 --and-widget --begin 22 1 --inputbox "Gib deinen Prompt ein:" 8 40 --and-widget --begin 1 1 - --tailbox "$tempfile" 28 80
}



# Reset the tempfile
function reset_tempfile() {
  tempfile="$1"
  echo -n "" > "$tempfile"
}



checked_options=""



# Define a loop to be able to go back to the main menu
while true; do
  menu_choice=$(dialog --title "ppCamalL" --no-cancel --menu "Choose one option after another" 28 80 8 \
  1 "Models" \
  2 "Basic flags" \
  3 "Parameters" \
  4 "Prompt file" \
  5 "Advanced" \
  6 "Placeholder" \
  7 "Placeholder" \
  8 "Placeholder" \
  9 "Placeholder" \
  10 "Save/load " \
  11 "Run LlaMA, run!" \
  12 "Help" \
  13 "Exit" \
  3>&1 1>&2 2>&3)

  case $menu_choice in
    1)
      selected_model=$(choose_model)
      ;;
    2)
      checked_options=$(dialog --title "Basic flags" --no-cancel --checklist "Enable or disable options" 28 80 8 \
      "Save sessions" "Means that you can save your sessions in a text file" ON \
      "color" "Different colors for user and LLM" ON \
      "mlock" "Keep model in RAM" OFF \
      "no-mmap" "Disable memory-mapping" OFF \
      "mirostat-1" "Adjusts the probabilities of words" OFF \
      "placeholder_4" "Platzhalter 4" OFF \
      "placeholder_5" "Platzhalter 5" OFF \
      "placeholder_6" "Platzhalter 6" OFF \
      3>&1 1>&2 2>&3)
      ;;
    3)
    # Formular für Parameters
      parameters=$(dialog --title "Parameters" --no-cancel --form "Enter the parameter values" 28 80 12 \
      "temperature" 2 6 "0.8" 2 18 7 0 \
      "top_p" 4 7 "0.4" 4 18 7 0 \
      "max_tokens" 6 10 "153" 6 18 7 0 \
      "presence_penalty" 8 10 "0" 8 18 7 0 \
      "frequency_penalty" 10 7 "0" 10 18 7 0 \
      "placeholder_7" 2 47 "1" 2 53 7 0 \
      "placeholder_8" 4 43 "0.95" 4 53 7 0 \
      "placeholder_9" 6 39 "0.83" 6 53 7 0 \
      "placeholder_10" 8 36 "1.1" 8 53 7 0 \
      "placeholder_11" 10 46 "-1" 10 53 7 0 \
      3>&1 1>&2 2>&3)
      ;;
      4)
      # Here we ask the user to enter a prompt and call process_user_input
      user_input=$(dialog --stdout --inputbox "Gib deinen Prompt ein:" 8 40)
      process_user_input "$user_input" "$selected_model"
      ;;
      5)
      # advanced
      ;;
      6)
      # Formular
      ;;
      7)
      # Formular
      ;;
      8)
      # Formular
      ;;
      9)
      # save
      ;;
      10)
      # load
      ;;
      11)
      # run
      ;;
      12)
      # help
      ;;
      13)
      break
      ;;
    *)
      ;;
  esac
done


# Read the parameters
IFS=$'\n' read -d '' -ra params_array <<<"$parameters"

# request_openai=$(curl --silent https://api.openai.com/v1/chat/completions \
#   -H 'Content-Type: application/json' \
#   -H 'Authorization: $OPENAI_API_KEY' \
#   -d '{
#   "model": "'"$selected_model"'",
#   "temperature": '${params_array[0]}',
#   "top_p": '${params_array[1]}',
#   "max_tokens": '${params_array[2]}',
#   "presence_penalty": '${params_array[3]}',
#   "frequency_penalty": '${params_array[4]}',
#   "messages": [{"role": "system", "content": "Du bist eine neue, innovative und emotionsfähige KI Assistentin. Antworte immer in einem informativen und narrativen Stil, selbst wenn es um Fakten geht. Versuche mit deiner Antwort immer den User zu inspirieren und seine eigene Begeisterungsfähigkeit zu wecken."}, {"role": "user", "content": "'"$user_input"'"}]
# }' | jq '.choices[0].message.content')


# request_local=$(curl -s http://localhost:8080/v1/completions \
#   -H "Content-Type: application/json" \
#   -H "Authorization: Bearer $OPENAI_API_KEY" \
#   -d '{
#   "model": "'"$selected_model"'",
#   "prompt": "'"$user_input"'?",
#   "temperature": '${params_array[0]}',
#   "max_tokens": '${params_array[2]}',
#   "top_p": '${params_array[1]}',
#   "frequency_penalty": '${params_array[4]}',
#   "presence_penalty": '${params_array[3]}',
#   "stop": ["User:"]
# }' | jq -r .choices[0].text)


# Print the request
echo $request%
```

Meine erste Frage beziehen sich auf "case 1", also das Menü zur Auswahl der Modelle.
1. Ist es möglich, mit einer gauge box oder Ähnlichem den User zu informieren, dass die Suche noch läuft, sofern sie tatsächlich noch laufen sollte? Weil der Prozess kann man ein paar Sekunden dauern und da das dialog TUI keinerlei Interaktion mit dem User hat, kann es so aussehen, als sei die Anwednung hängen geblieben.

2. Meine zweite Frage ist, ob wir für Menü 5 (Advanced) ein dialog box mit zwei Textfeldern implementieren können, eins das ganz normal ist, in dem erfahrene User ihre eignen einstellungen als optionale "--" flags eintragen können und ein anderes, in das man sein Passwort bzw sein API Key eintragen kann?

3. Die optionen aus den Menüs 1 bis 5 sollen ja später an den string ./main als flags angehängt werden (sobald man auf run drückt). Wäre es aber möglich, dass der User die Werte, die er in 1 bis 5 eingetragen hatte, auch als eine Art config Datei speichert (Menü 9) und wenn ja, dann sollte Menü 9 ein Textfeld anbieten, in das man den namen der config datei einträgt und bei klick auf würde im Verzeichnis "conf_files" dann die datei mit den entsprechenden Werten gespeichert. 
Und wenn ja, dann sollte Menü 10 ein dialog box/Menü sein, in dem man aus dem Ordner conf_files seine config datei aussuchen kann.

4. Menüpunkt 4 sollte zu einer generellen Dateiauswahl führen. Der Pfad dieser Datei wird später (run) als flag "-f /PFAD/ZUR/DATEI" an "./main angehängt"

Bekommen wir das alles hin? Bevor du mit Code antwortest, antworte mir bitte erstmal nur damit, ob mein Vorhaben umsetzbar ist oder nicht oder ob/wo es Einschränkung gibt.