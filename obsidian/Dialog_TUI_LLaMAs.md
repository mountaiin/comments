Ich möchte eine TUI mit dialog erstellen. Ich habe dialog bereits installiert. Könntest du mein Skript, das gleich weiter unten folgt, für mich folgenderweise anpassen?
Mein Skript soll es usern erleichtern, lokale Sprachmodelle wie gpt-2 und neo-gpt-x usw auszuwählen und die dazugehörigen hyperparameter und prompts einzustellen. Mit dialog soll es so angepasst werden:
- als aller erstes soll der user gefragt werden, ob er eine "schnelle standard einstellung" oder eine "fortgeschrittenere einstellung" (bitte finde hierfür auch passendere wörter) oder eine bereits gespeicherte profil-/config-datei laden möchte.
- wenn der user schnell gewählt hat, soll  im nächsten die find function laufen (siehe weiter unten) und der user soll aus einer liste auswählen. als hyperparameter werden hier die einstellungen aus meinem skript übernommen (also alles nach "./main" kommt, siehe unten im skript) und ./main gestartet werden.
- wenn der user fortgeschritten auswählt, dann soll auch erstmal die find function ausgeführt werden und der user soll aus einer liste auswählen
- nach auswahl des modells soll der fortgeschrittene user gefragt werden, ob er ohne prompt fortfahren möchte oder mit manuellem prompt oder eine text-datei laden möchte, die den prompt enthält
- text-datei bietet ein textfeld, in dem der user den pfad zur datei eingeben soll (würde später als options-flag -f /pfad/zur/datei übernommen werden).
  manuelle prompt eingabe bietet dem user ebenfalls ein textfeld, in dem er allerdings einfach seinen prompt eingeben soll (dieses würde später als string als options-flag -p $PROMPT übernommen werden).
  wenn ohne prompt ausgewählt wurde, springen wir einfach zum nächsten punkt
- nach auswahl des prompts soll der fortgeschrittene user gefragt werden, welche hyperparameter er einstellen möchte. dann sollen schieberegler erscheinen, um jeweils temp, top_k und top_p einstellen zu können.  für die options-flags (--instruct und --color) wären kontrollkästchen.
- der fortgeschrittene user soll dann gefragt werden, ob er mit der inference beginnen möchte, oder ob er seine einstellungen erst speichern und dann mit der inference fortfahren möchte.
- sollte er speichern wählen, werden einfach alle dinge, die er ausgewählt hat (also model und parameter) in eine textdatei erstellt. und zwar in so einem format, dass es später gelesen und geladen werden könnte.

Hier kommt mein skript:
```
#!/bin/bash

function find_files() {
    find $1 -type f -iname "*ggml*.bin" -size +1000M 2>/dev/null
}

function search_for_models() {
    locations=("$@")
    local all_files=""
    for loc in "${locations[@]}"; do
        local new_files=$(find_files "$loc")
        if [ -n "$new_files" ]; then
            if [ -z "$all_files" ]; then
                all_files="$new_files"
            else
                all_files="$all_files"$'\n'"$new_files"
            fi
        fi
    done
    echo "$all_files"
}

files=$(search_for_models ~ "/media" "/mnt" "/Volumes" "/run/media" "/media/*/")

if [ -z "$files" ]; then
    echo "Die automatische Suche konnte keine passenden Modelle finden."
    read -p "Bitte gib den Pfad zu deinem Modell an: " selected_model
else
    echo "Wähle ein Modell aus der Liste aus:"
    options=($files)
    for i in "${!options[@]}"; do
        echo "$((i+1)). ${options[$i]}"
    done

    read -p "Gib die Nummer des gewünschten Modells ein: " input

    if [ -z "$input" ] || [ "$input" -le 0 ] || [ "$input" -gt "${#options[@]}" ]; then
        echo "Ungültige Auswahl. Beende das Skript."
        exit 1
    fi

    selected_model=${options[$((input-1))]}
fi

#~ echo "Du hast das Modell '${selected_model}' ausgewählt."

#~ sleep 2
#-f ~/Deepbrain/prompts/Erlesene_Prompts/programmer.txt

./main      -m $selected_model          	    \
            --color         --instruct          \
            -r "User:"      --threads 4         \
            --keep -1       --batch_size 8      \
            --temp 0.33      --top_p 1        \
            --top_k 8      --n_predict 358     \
            --repeat_penalty 1.0                \
            --ctx_size 512
            #--repeat_last_n 64

```

Denkst du, du bekommst das so hin, oder möchtest du vorher noch etwas wissen?

Achja! bevor du mit dem skript beginnst, sag mir bitte erstmal, ob dialog für mein vorhaben gut geeignet ist oder ob du etwas anderes empfehlen würdest.