---
config:
 append:
  bodyParams: true
  reqParams: true
 context: "prompt"
bodyParams:
 model: gpt-3.5-turbo-0301
 messages=
 [
        {" role": "system", "content": "Du bist eine hilfsbereite Assistentin." },
        {"role": "user", "content": "Wer hat die World Series im Jahr 2020 gewonnen?"},
        {"role": "assistant", "content": "Die L.A. Dodgers haben 2020 die World Series gewonnen."},
        {"role": "user", "content": "Vielen Dank. Ich habe noch eine Frage."},
        {"role": "assistant", "content": "Natürlich! Was willst du wissen?"}
]
reqParams:
 url: "https://api.openai.com/v1/chat/completions"
---

user: Wo wurde es eigentlich gespielt?
