---
PromptInfo:
 promptId: modDetailed 
 name: 🔬
 description: photo, with more precise details 
 tags: photo, dalle-2,modifier
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "512x512"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---
{{selection}}, Detailed