---
PromptInfo:
 promptId: 256px 
 name: 📷
 description: 256x256 picture 
 tags: photo,dalle-2,quality,lens
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "256x256"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---

{{selection}}, {{tags}}
