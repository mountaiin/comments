#!/bin/bash
for file in *.md; do
  sed -i.bak 's/🖼️ //g' "$file"
  sed -i.bak '/^[[:space:]]*description/d' "$file"
  sed -i.bak 's/author:\ Prompt\ Engineering\ Guide//g' "$file"
  sed -i.bak 's/version: 0\.0\.1//g' "$file"
  sed -i.bak 's/name/description/g' "$file"
  sed -i.bak 's/1024x1024/512x512/g' "$file"
  
  # Füge "name: " zwischen Zeile 3 und 4 ein.
  sed -i.bak '3a\
 name:\
' "$file"

  
  # Entferne leere Zeilen.
  sed -i.bak '/^[[:space:]]*$/d' "$file"
  
  rm "$file.bak"
done
echo "Fertig!"