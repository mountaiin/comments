---
PromptInfo:
 promptId: qua4K
 name: 4️⃣
 description: 4K/8K photo 
 tags: photo, dalle-2,quality
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "512x512"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---
{{selection}}, 4K/8K