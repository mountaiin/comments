#!/bin/bash
for file in *.md; do
  sed -i.bak 's/\ Generate\ a//g' "$file"

  # Entferne leere Zeilen.
  #sed -i.bak '/^[[:space:]]*$/d' "$file"
  
  rm "$file.bak"
done
echo "Fertig!"