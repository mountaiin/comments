---
PromptInfo:
 promptId: ligAmbient
 name: 🏮
 description: Ambient Lighting Foto
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "515x512"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---

{{selection}}, Ambient Lighting