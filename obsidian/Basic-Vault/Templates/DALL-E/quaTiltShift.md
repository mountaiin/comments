---
PromptInfo:
 promptId: quaTiltShift
 name: 🏙️
 description: Tilt-shift photo 
 tags: photo, dalle-2,quality
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "512x512"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---
{{selection}}, Tilt-shift photography