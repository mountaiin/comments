- - -
### Bilder Generierung
```shell
curl https://api.openai.com/v1/images/generations \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
    "prompt": "a white siamese cat",
    "n": 1,
    "size": "512x512"
  }'
  ```
---
### Variationen
```shell
curl https://api.openai.com/v1/images/variations \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -F image='@corgi_and_cat_paw.png' \
  -F n=1 \
  -F size="512x512"
```
---
### Bilder bearbeiten
```shell
curl https://api.openai.com/v1/images/edits \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -F image="@sunlit_lounge.png" \
  -F mask="@mask.png" \
  -F prompt="A sunlit indoor lounge area with a pool containing a flamingo" \
  -F n=1 \
  -F size="512x512"
```
---

#### Consider Visual Prompt Builder
https://tools.saxifrage.xyz/prompt