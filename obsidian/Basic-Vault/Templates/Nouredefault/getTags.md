---
PromptInfo:
 Name: Erhalte Tags für deinen Inhalt
 Beschreibung: Wähle einen Inhalt und erhalte Vorschläge für Tags dazu
 Tags: Schreiben, Lernen
bodyParams:
 max_tokens: 30
---
Inhalt:
{{Kontext}}
Aufforderung:
Tags für den Inhalt im Markdown-Format vorschlagen
Tags:
