---
PromptInfo:
 Name: Blog-Titel abrufen
 Beschreibung: Wähle einen Inhalt aus und es wird eine Liste von Blogtiteln erstellt
 Tags: Schreiben
---
Inhalt:
{{Kontext}}
Aufforderung:
Schlage 10 attraktive Blogtitel zu diesem Inhalt vor: