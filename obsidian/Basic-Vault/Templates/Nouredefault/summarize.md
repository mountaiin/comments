---
PromptInfo:
 Name: Zusammenfassen
 Beschreibung: Wähle einen Inhalt aus und er wird zusammengefasst.
 Tags: Schreiben, Denken, Lernen
---
Inhalt:
{{Kontext}}
Aufforderung:
fasse den Inhalt zusammen