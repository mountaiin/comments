---
PromptInfo:
 Name: Umschreiben, Paraphrase
 description: Wähle einen Inhalt aus und er wird umgeschrieben.
 Tags: Schreiben
---
Inhalt:
{{Kontext}}
Aufforderung:
schreibe den Inhalt um, um ihn attraktiver zu machen
