---
PromptInfo:
 Name: Absatz schreiben
 Beschreibung: Wähle ein Element aus, es wird ein Absatz erstellt.
 Tags: Schreiben, Titel, Gliederung
---
title:
{{Titel}}
Gliederung:
{{Gliederung}}

# {{context}}
