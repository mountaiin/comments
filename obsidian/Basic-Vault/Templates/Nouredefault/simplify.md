---
PromptInfo:
 Name: Vereinfachen
 Beschreibung: Wähle einen Inhalt aus und er wird vereinfacht.
 Tags: Denken, Schreiben
---
Inhalt:
{{Kontext}}
Aufforderung:
Mache den Inhalt sehr klar und einfach zu verstehen
