---
PromptInfo:
 Name: Auf E-Mail negativ antworten 😡
 Beschreibun: Wähle den Inhalt der E-Mail aus und es wird eine negative Antwort erzeugt
 Tags: Kommunikation, E-Mail
---
Aufforderung:
Beantworte diese E-Mail negativ auf professionelle Weise.
E-Mail:
{{Kontext}}
Antwort:
