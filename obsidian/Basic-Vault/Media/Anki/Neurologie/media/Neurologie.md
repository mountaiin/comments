
# der klinischen Untersuchung der 33-jährig
### Bei der klinischen Untersuchung der 33-jährigen Patientin in der Notfallaufnahme zeigt sich eine Paraparese mit spastischer Tonuserhöhung und gesteigertem PSR und ASR beidseits. Welche Untersuchung ist angezeigt, um die Feststellung der zentralen Parese zu sichern?
#card #neurology #anki
Pyramidenzeichen
Parese der rechten OE mit Kraftgrad 4 und normaler Trophik, Veränderung des Tonus nicht sicher erhebbar; mit welcher Untersuchung kann man die zentralen Parese sichern? Sehnenreflexe 2011-09 etc.

# einer Patientin besteht eine Syringomyeli
### Bei einer Patientin besteht eine Syringomyelie mit dissozierter Sensibilitätsstörung C6 bis Th5 rechts. Dabei zeigt sich im Status in den betroffenen radikulären Versorgungsgebieten ein Ausfall von
#card #neurology #anki
Schmerz und Temperatur

# Verteilung der Häufigkeit der verschieden
### Die Verteilung der Häufigkeit der verschiedenen Demenzformen kann so beschrieben werden:
#card #neurology #anki
Alzheimer   vaskuläre Demenz   Lewy-Körperchen Demenz

# 73-jähriger Patient kommt in Ihre Ordinat
### Ein 73-jähriger Patient kommt in Ihre Ordination und meint, er leide unter einer Demenz. Welchen Screeningtest setzen Sie neben der Patientenbeobachtung zur Beurteilung einer möglichen Demenz ein?
#card #neurology #anki
Mini Mental State Examination

# 62-jährige Patientin kommt wegen Vergess
### Eine 62-jährige Patientin kommt wegen Vergesslichkeit und Apathie zur Abklärung einer möglichen Demenz in die Ordination. Im Mini Mental State Examination (MMSE) ist sie auffällig, der Neurostatus ist normal. Die Patientin berichtet über einen Leistungsknick vor 2 Monaten, Freudlosigkeit und einer Durchschlafstörung. Ihre Vermutungsdiagnose lautet:
#card #neurology #anki
Depression
Therapie: Serotoninwiederaufnahmehemmer 2010-07

# 56-jährige Patientin mit multiplen Insul
### Eine 56-jährige Patientin mit multiplen Insulten leidet seit 15 Jahren an schlecht eingestellter Hypertonie und raucht 30 Zigaretten pro Tag. Bei der Durchuntersuchung finden sich verschiedene auffällige Ergebnisse. Welches davon weist auf einen zusätzlichen Schlaganfall-Risikofaktor hin?
#card #neurology #anki
HbA1c erhöht

# einem 64-jährigen Patienten wurde ein beg
### Bei einem 64-jährigen Patienten wurde ein beginnendes links betontes Parkinsonsyndrom vom Rigor-Akinese Dominanztyp festgestellt. Er berichtet, dass er vor der Diagnosestellung
#card #neurology #anki
längere Zeit wegen Rückenschmerzen behandelt wurde.

# Assistenzarzt berichtet der Primaria über
### Ein Assistenzarzt berichtet der Primaria über die Symptome eines 60-jährigen Parkinsonpatienten (Äquivalenztyp) in fortgeschritenem Stadium der Erkrankung (Hoen und Yahr III). Neben typischem Rigor, Tremor und Akinese sind ihm weitere Symptome aufgefallen. Welches Symptom davon ist nicht typisch für den M. Parkinson?
Welches davon wird von der Primaria als nicht Parkinson-typisch angezweifelt? 2007-07
#card #neurology #anki
Faszikulationen

# wichtigsten Ergebnisse für die Diagnose e
### Die wichtigsten Ergebnisse für die Diagnose einer Multiplen Sklerose neben der Anamnese und der Klinik erbringen folgende Hilfsbefunde:
#card #neurology #anki
Liquoruntersuchung und MRI

# 25-jähriger Patient wurde bei einem epile
### Ein 25-jähriger Patient wurde bei einem epileptischen Anfall beobachtet. Die Außenanamnese beschreibt, dass der Patient auf Ansprache nicht reagiert habe. Um zwischen einem partiellen Anfall mit komplexer Symptomatik und einem Grand Mal Anfall unterscheiden zu können, fragen Sie weiter nach:
#card #neurology #anki
Motorischen Symptomen

# 58-jähriger Patient kommt wegen seit 3 Wo
### Ein 58-jähriger Patient kommt wegen seit 3 Wochen zunehmender Kopfschmerzen. In den letzten Tagen wacht der Patient schon mit Kopfschmerzen und Übelkeit auf. Im Status finden sich links vor rechts gesteigerte Sehnenreflexe, eine angedeutete Hemiparese links, kein Meningismus. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Tumor

# 35-jähriger Patient wurde wegen des Verda
### Ein 35-jähriger Patient wurde wegen des Verdachts auf Subarachnoidalblutung lumbalpunktiert. Zellzahl 7/3, Eiweiß und Zellbild unauffällig. Der Patient klagt weiterhin über starke Kopfschmerzen bei allen körperlichen Aktivitäten, die nur beim Liegen im Bett vollständig verschwinden. Was ist Ihre Verdachtsdiagnose?
#card #neurology #anki
Postpunktioneller Kopfschmerz

# einem 38-jährigen Patienten mit chronisch
### Bei einem 38-jährigen Patienten mit chronischer Lumbago in der Anamnese traten vor 7 Tagen wieder heftige Rückenschmerzen auf. Seit heute Morgen strahlen diese in das linke Bein aus. Es besteht auch eine Sensibilitätsstörung am hinteren Unterschenkel bis zur Sohle. Der Patient kann links auch nicht auf den Zehenspitzen stehen. Was ist Ihre Verdachtsdiagnose?
#card #neurology #anki
Radikuläres Syndrom S1 li

# 43-jährige Patientin hat seit 3 Wochen h
### Eine 43-jährige Patientin hat seit 3 Wochen heftige Schmerzen in der rechten Hand, eine Hypaesthesie im Endglied des Zeigefingers und eine Schwäche für die Opposition des Daumens. Sie vermuten ein Karpaltunnelsyndrom und benützen zur Diagnosesicherung aus der Nervenleitgeschwindigkeitsuntersuchung das Ergebnis der
#card #neurology #anki
distalen Latenz.

# 64-jähriger Patient kommt wegen der Trias
### Ein 64-jähriger Patient kommt wegen der Trias Gangstörung, kognitiver Abbau und Blasenstörung in die Ordination. Ihre Verdachtsdiagnose lautet:
#card #neurology #anki
Normaldruckhydrocephalus

# 43-jähriger Patient kommt wegen Zittern b
### Ein 43-jähriger Patient kommt wegen Zittern beider Hände in Ihre Ordination. Sie denken an einen essentiellen Tremor und fragen nach
#card #neurology #anki
der Familienanamnese.

# 60-jähriger Patient kommt mit einem beinb
### Ein 60-jähriger Patient kommt mit einem beinbetonten zentralen Hemisyndrom links zur Aufnahme. Anamnestisch berichtet er über einen schleichenden Verlauf der Beschwerden, das Akut CT zeigt eine kreisrunde homogen Kontrastmittel-aufnehmende Struktur rechts an der Falx. Ihre Diagnose lautet:
#card #neurology #anki
Meningeom

# 38-jährige Patientin kommt wegen Doppelbi
### Ein 38-jährige Patientin kommt wegen Doppelbildern und einer Schluckstörung in Ihre Ordination. Die Symptomatik nimmt bei Anstrengung und am Abend zu. Sie vermuten eine Myasthenia gravis. Was ist der nächste diagnostische Schritt?
#card #neurology #anki
Bestimmung von Antikörpern gegen Acetylcholinrezeptoren

# 34-jähriger soporöser Patient mit Meningi
### Ein 34-jähriger soporöser Patient mit Meningismus und starken Kopfschmerzen wird akut mit Verdacht auf SAB zum CT gebracht. Nach kurzer Zeit kommt von dort ein Anruf auf die Station mit der Mitteilung, der Bewusstseinszustand des Patienten habe sich verschlechtert. Die richtige Aussage lautet: Der Patient ist nunmehr
#card #neurology #anki
komatös

# hem Gefäßversorgungsgebiet ist ein Insult
### Welchem Gefäßversorgungsgebiet ist ein Insult mit beinbetonter Hemiparese rechts und Blasenstörung zuzuordnen?
#card #neurology #anki
Linke Arteria cerebri anterior

# 75-jährige Patientin wird wegen zunehmen
### Eine 75-jährige Patientin wird wegen zunehmender Vergesslichkeit zur Abklärung einer möglichen Demenz stationär aufgenommen. Im Neurostatus zeigt sich eine geringe Hemiparese rechts, sowie eine geringe rechts betonte Tetraspastik und beidseits positive Pyramidenzeichen. Sie vermuten am ehesten das Vorliegen einer
#card #neurology #anki
vaskulären Demenz.

# Eintreffen des Notarzthubschraubers war 
### Beim Eintreffen des Notarzthubschraubers war der 25-jährige Patient ansprechbar, danach war er ca. eine Stunde bewusstlos. Der initiale GCS (Glasgow Coma Scale) war 12. Die posttraumatische Amnesie erstreckt sich über sechs Stunden. Das ergibt die Beurteilung:
#card #neurology #anki
Mittelschweres Schädelhirntrauma

# 82-jähriger Patient mit Parkinsonsyndrom 
### Ein 82-jähriger Patient mit Parkinsonsyndrom seit 15 Jahren leidet unter einem Dopa-Langzeitsyndrom. Dabei kommt es zum Wirkungsverlust der Dopa-Therapie (wearing off) und Dyskinesien. Eine mögliche Maßnahme zur Verbesserung der Symptome ist es,
#card #neurology #anki
L-Dopa öfter am Tag zu geben.
Wirkungsverlust der Dopa-Therapie (wearing off) und Psychose, was noch? Dyskinesie 2021-09
Wirkungsverlust der Dopa-Therapie (wearing off) und Dyskinesien, was noch? Psychose 2009-06 etc.

# einem 35-jährigen Mann tritt ohne Provoka
### Bei einem 35-jährigen Mann tritt ohne Provokation ein Grand-Mal-Anfall auf. Anamnestisch findet sich vor 5 Jahren ein unklarer Bewusstseinsverlust. Der Status ist unauffällig. Zur weiteren Diagnostik
#card #neurology #anki
ist neben dem EEG eine CT- oder MRI-Untersuchung notwendig.

# der Untersuchung einer Parese wird der Kr
### Bei der Untersuchung einer Parese wird der Kraftgrad angegeben. KG 2 bedeutet
#card #neurology #anki
Bewegung nach Aufhebung der Schwerkraft möglich.

# 65-jähriger Patient berichtet über eine s
### Ein 65-jähriger Patient berichtet über eine seit einigen Tagen bestehende geringgradige Lähmung der linken Hand. Welche anamnestische Frage hilft in der Vermutungsdiagnose zur Ätiologie in der Entscheidung zwischen Schlaganfall und Tumor weiter?
#card #neurology #anki
War der Beginn der Symptomatik plötzlich oder schleichend?

# 62 jähriger Patient berichtet über eine S
### Ein 62 jähriger Patient berichtet über eine Sehstörung auf dem linken Auge, die 10 Minuten andauerte und sich dann vollständig zurückbildete. Wegen der Möglichkeit einer transistorisch ischämische Attacke (TIA) als ätiologischen Hintergrund fragen Sie den Patienten:
#card #neurology #anki
Traten zur gleichen Zeit Schwäche und Lähmung der rechten Hand auf?

# 63-jähriger Patient wird wegen zunehmende
### Ein 63-jähriger Patient wird wegen zunehmender Gedächtnisstörung, kognitiven Leistungseinbußen und Persönlichkeitsveränderungen in der Ambulanz vorgestellt. Angehörige berichten über affektive Störungen. Es zeigt sich eine Sprachstörung mit Perseverationen und hochgradig positive frontale Zeichen. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Pick’sche Demenz

# welcher der folgenden Symptomkombinatione
### Bei welcher der folgenden Symptomkombinationen denken Sie sofort an ein cerebrales vaskuläres Geschehen? (2)
#card #neurology #anki
Wiederkehrendes Einschlafgefühl und Ungeschicklichkeit einer Hand //Plötzliche Schwäche und Gefühlsstörung einer Halbseite

# einer 46-jährigen Patientin zeigen sich s
### Bei einer 46-jährigen Patientin zeigen sich seit 5 Stunden zunehmende Hirnstammzeichen, die Patientin ist hochgradig quadruparetisch und die Bewusstseinslage verschlechtert sich dramatisch. Die MR-Angiographie zeigt einen fast vollständigen Verschluss der Arteria basilaris. Was soll therapeutisch geschehen?
#card #neurology #anki
i.A. Lyse

# 28-jährige Patientin erlitt einen Autoun
### Eine 28-jährige Patientin erlitt einen Autounfall und hat eine Rissquetschwunde rechts parietal davongetragen. Sie kann sich an den Unfallhergang nicht erinnern und hat diffuse Kopfschmerzen. Im Status finden sich diskrete Halbseitenzeichen links. Sie ordnen folgende Untersuchung an:
#card #neurology #anki
CT zum Ausschluss eines SDH (Subduralhämatoms)

# neu diagnostizierte Parkinsonpatientin f
### Eine neu diagnostizierte Parkinsonpatientin fragt Sie nach der Ursache ihrer Krankheit. Sie antworten, dass beim Parkinsonsyndrom
#card #neurology #anki
Nervenzellen der nigro-striatalen Bahn zu Grunde gehen.

# 65-jähriger Patient erleidet aus voller G
### Ein 65-jähriger Patient erleidet aus voller Gesundheit einen rechtshirnigen Mediainsult mit Hemiplegie links und Bewusstseinsstörung. Bei der Einlieferung wird ein Grand Mal beobachtet, später auf der Station ein weiterer Anfall. Die Epilepsieerkrankung des Patienten wird klassifiziert als
#card #neurology #anki
symptomatisch

# 20-jähriger Patient leidet seit mehreren 
### Ein 20-jähriger Patient leidet seit mehreren Jahren an einer mesialen Temporallappenepilepsie mit fokal komplexen Temporallappenanfällen. Nach typischen epigastrischen Auren treten dabei folgende Symptome auf:
#card #neurology #anki
Bewusstseinsstörung, Kauen, Schmatzen

# 35-jähriger Patient, der vor 2 Monaten ei
### Ein 35-jähriger Patient, der vor 2 Monaten ein Schädelhirntrauma mit intraventrikulärer Blutung erlitten klagt seit einigen Tagen über zunehmende Kopfschmerzen, seit gestern verbunden mit Übelkeit und Erbrechen. Bei der Untersuchung in der Ordination ist er somnolent, die Sehnenreflexe bds. sehr lebhaft, Pyramidenzeichen positiv. Sie veranlassen
#card #neurology #anki
die sofortige stationäre Aufnahme zum Akut-CT wegen Verdacht auf Hirndruck.

# einer 50-jährigen Patientin traten vor 4 
### Bei einer 50-jährigen Patientin traten vor 4 Tagen heftige Rückenschmerzen auf, seit gestern strahlen diese in das linke Bein aus. Es besteht auch Sensibilitätsstörung am lateralen Unterschenkel bis zur Großzehe und eine Großzehenheberschwäche links. Sie haben die Verdachtsdiagnose:
#card #neurology #anki
Radikuläres Syndrom L5 li

# 55-jähriger Patient ist auf Marcoumar ein
### Ein 55-jähriger Patient ist auf Marcoumar eingestellt. Er erwachte heute mit massiven Schmerzen im Bereich der rechten Leiste und des rechten Oberschenkels. Es fällt eine Schwäche für das Hüftbeugen und Kniestrecken auf, gleichzeitig findet sich eine Hypaesthesie am ventralen Oberschenkel und am medialen Unterschenkel. Ihre Diagnose lautet: Läsion des/der
#card #neurology #anki
N. femoralis

# 19-jährige übergewichtige Patientin komm
### Eine 19-jährige übergewichtige Patientin kommt wegen seit Tagen zunehmenden Kopfschmerzen mit Übelkeit und Erbrechen und Sehstörungen zur Aufnahme. Die CT-Untersuchung ist ebenso unauffällig wie der neurologische Status mit Ausnahme der Gesichtsfelduntersuchung. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Pseudotumor cerebri

# 64-jährige Patientin kommt wegen Zittern
### Eine 64-jährige Patientin kommt wegen Zitterns beider Hände in Ihre Ordination. Bei der klinischen Untersuchung finden sich kein Rigor und keine Hypokinese. Zur Differentialdiagnose weisen Sie sie zur folgenden Laboruntersuchung zu:
#card #neurology #anki
Schilddrüsenhormone 

# 66-jähriger Patient kommt mit der Anamnes
### Ein 66-jähriger Patient kommt mit der Anamnese zu Ihnen in die Ordination, dass vor 4 Wochen eine Ungeschicklichkeit der rechten Hand schleichend begonnen und sich seither verschlechtert habe. Seit einigen Tagen seien Kopfschmerzen und auch eine expressive Sprachstörung hinzugekommen. Bei der klinischen Untersuchung zeigt sich ein spastisches Hemisyndrom rechts. Ihre Vermutungsdiagnose eines malignen Tumors erhärtet sich in der CT-Untersuchung, die eine links-hirnige Raumforderung mit perifokalem Ödem ergibt. Welche Tumoren sind in dieser Konstellation am häufigsten?
#card #neurology #anki
Metastasen und Gliome

# 53-jähriger Cellist kommt wegen dystonen 
### Ein 53-jähriger Cellist kommt wegen dystonen Muskelspasmen, die in letzter Zeit immer häufiger sowohl beim Üben wie auch beim Auftreten bei schnellen Musikstücken im Bereich der Finger der linken Hand auftreten in Ihre Ordination. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Lokalisierte Extremitätendystonie

# Patient mit Hemiparese links hat auch ein
### Ein Patient mit Hemiparese links hat auch ein schiefes Gesicht. Sie vermuten eine zentrale Fazialisparese links und finden passend im Status Auffälligkeiten beim:
#card #neurology #anki
Augenschließen, Zähnezeigen, Pfeifen
periphere Fazialisparese - Auffälligkeiten beim: Augenschließen, Stirnrunzeln, Zähnezeigen 2011-07 etc.

# Patient mit Hemiparese rechts hat Schwier
### Ein Patient mit Hemiparese rechts hat Schwierigkeiten beim Sprechen. Sie versuchen eine Zuordnung zu einer Aphasieform. Dazu beurteilen Sie:
#card #neurology #anki
Spontansprache und Sprachverständnis

# 28-jährige Patientin kommt wegen heftige
### Eine 28-jährige Patientin kommt wegen heftiger Kopfschmerzen und Parästhesien in der linken Hand in die Notfallaufnahme. Welche anamnestische Frage hilft Ihnen in der ätiologischen Zuordnung zwischen einer Gehirnblutung und erstmaligem erweiterten Migräneanfall weiter?
#card #neurology #anki
Haben Sie vor dem Einsetzen der Kopfschmerzen flimmernde Zacken gesehen?

# 72-jähriger Patient berichtet über eine S
### Ein 72-jähriger Patient berichtet über eine Schwäche und Gefühlsstörung der rechten Hand, die 10 Minuten andauerte und sich dann vollständig rückbildete. Wegen der Möglichkeit einer TIA fragen Sie den Patienten:
#card #neurology #anki
Traten zur gleichen Zeit eine Sehstörung auf dem linken Auge auf?

# der Demenz vom Alzheimertyp ist ein Neuro
### Bei der Demenz vom Alzheimertyp ist ein Neurotransmittersystem besonders betroffen. Die Gehirne dieser Patienten weisen einen um 30-75% verminderten Gehalt an diesem Transmitter auf, dessen medikamentöse Erhöhung zur Verbesserung der Neurotransmission verwendet wird. Es handelt sich dabei um:
#card #neurology #anki
Acetylcholin

# einer 72-jährigen Patientin mit rechtshir
### Bei einer 72-jährigen Patientin mit rechtshirnigem Mediainsult wurden Makroangiopathie und Mikroangiopathie als Ätiologie mit extra- und intrakraniellem Doppler/Duplex und MR-Perfusionsmessung ausgeschlossen. Welche Ätiologie sollte zusätzlich ausgeschlossen werden?
#card #neurology #anki
Embolie bei intermittierendem Vorhofflimmern

# Patient, der möglicherweise intravenös ly
### Der Patient, der möglicherweise intravenös lysiert werden soll, ist 70 Jahre alt und hat seit 2 Stunden 30 min eine geringe Hemiparese rechts und motorische Aphasie, das CT ist unauffällig, die Laborwerte sind alle im Normbereich, der Blutdruck 120/70. Sie treffen folgende Entscheidung:
#card #neurology #anki
Sofortiger Beginn der Lyse

# 40-jährige Patientin wird nach einem Rei
### Eine 40-jährige Patientin wird nach einem Reitunfall in der Unfallambulanz betreut. Sie war nicht bewusstlos, der Unfallhergang ist ihr aber nicht vollständig erinnerlich. Das Schädelröntgen ist unauffällig, der Neurostatus normal. Was soll angeordnet werden?
#card #neurology #anki
Stationäre Aufnahme zur 24-stündigen Observanz

# der Kontrolluntersuchung berichtet eine 6
### Bei der Kontrolluntersuchung berichtet eine 60-jährige Patientin, die seit 5 Jahren unter einem linksseitigen Hemiparkinsonsyndrom leidet, dass nun auch rechtsseitig eine Bradykinese aufgetreten sei, was auch die klinische Untersuchung gemeinsam mit einem neu aufgetretenen Rigor rechts bestätigt. Ihre Einschätzung lautet, dass:
#card #neurology #anki
Hemiparkinson immer in die bilaterale Form übergeht

# einem 56-jährigen Patienten wird klinisch
### Bei einem 56-jährigen Patienten wird klinisch ein bilaterales Parkinsonsyndrom vom Rigor/Akinese Dominanztyp festgestellt. Die Diagnose kann bestätigt werden durch:
#card #neurology #anki
Beta-CIT SPECT

# immunmodulative Therapie mit Betainterfer
### Die immunmodulative Therapie mit Betainterferon oder Glatiramerazetat erreichen / erzielt 2017-09 bei Multiple Sklerose-Patienten eine:
#card #neurology #anki
Reduzierung der Schubrate
immunmodulative Therapie mit Betainterferon oder Natalizumab: Reduzierung der Schubrate 2021-09

# 26-jähriger Patient klagt seit Tagen über
### Ein 26-jähriger Patient klagt seit Tagen über Schmerzattacken im Gesicht links, etwa im Bereich der Wange, ausstrahlend zum Kieferwinkel. Die Schmerzen seien von ziehendem Charakter, zeigten keine Begleitsymptome und würden meist mehrere Stunden andauern. Ihre Verdachtsdiagnose:
#card #neurology #anki
Zahnschmerzen

# einer 46-jährigen Patientin traten vor 2 
### Bei einer 46-jährigen Patientin traten vor 2 Tagen heftige Rückenschmerzen auf, seit heute strahlen diese in das rechte Bein aus. Es besteht auch eine Sensibilitätsstörung über dem rechten Knie und eine rechtsseitige Schwäche beim Hinaufsteigen auf einen Sessel. Sie haben die Verdachtsdiagnose:
#card #neurology #anki
Radikuläres Syndrom L4 re

# 45-jähriger Patient klagt seit den Abends
### Ein 45-jähriger Patient klagt seit den Abendstunden über plötzlich aufgetretene Kopfschmerzen und Nackensteife. Neben einem Meningismus in der klinischen Untersuchung findet sich im CT Blut in den basalen Zysternen. Wie ist Ihre Einschätzung?
#card #neurology #anki
Es handelt sich eindeutig um eine Subarachnoidalblutung.

# 71-jährige Patientin wurde wegen aufstei
### Eine 71-jährige Patientin wurde wegen aufsteigender schlaffer Tetraparese unter der Verdachtsdiagnose Polyradiculitis aufgenommen. Den nachfragenden Angehörigen erklären Sie zum Krankheitsgeschehen,
#card #neurology #anki
dass es sich um eine Autoimmunkrankheit handelt und ein Zusammenhang mit abgelaufenen Infektionen bestehen kann.

# 44-jährige Patientin kommt wegen abnorme
### Eine 44-jährige Patientin kommt wegen abnormer Ermüdbarkeit der Muskulatur der oberen Extremitäten sowie einer Schluck- und Sprechstörung in Ihre Ordination. Die Symptomatik nimmt bei wiederholten Bewegungen und Daueranstrengung zu. Die Abklärung ergibt die Diagnose einer Myasthenia gravis. Welche Therapieoption zur Reduktion der Bildung von Antikörpern gegen Azetylcholinrezeptoren steht zur Verfügung?
#card #neurology #anki
Kortikoide

# 29-jährige Patientin mit problemloser Sc
### Eine 29-jährige Patientin mit problemloser Schwangerschaft ohne Risikofaktoren klagt wenige Tage vor dem Geburtstermin über zunehmende Kopfschmerzen, im Status findet sich eine diskrete Hemiparese links. Im Akut-CT zeigen sich mehrere kleine Einblutungsherde rechts parieto-temporal. Ihre Verdachtsdiagnose lautet:
#card #neurology #anki
Sinusvenenthrombose

# 48-jähriger Patient kommt wegen seit eine
### Ein 48-jähriger Patient kommt wegen seit einer Woche zunehmender Kopfschmerzen in die Notfallaufnahme. Welche anamnestische Frage hilft Ihnen in der ätiologischen Zuordnung zwischen Spannungskopfschmerz und Hirndruck weiter?
#card #neurology #anki
„Stellte sich mit den Kopfschmerzen Übelkeit und Erbrechen ein?“

# 28-jährige Patientin kommt wegen Ungesch
### Eine 28-jährige Patientin kommt wegen Ungeschicklichkeit und einer geringen Schwäche des rechten Arms in die Ordination. Die Beschwerden haben vor drei Tagen akut begonnen. Für die vollständige Anamnese bezüglich einer möglichen Multiple Sklerose (MS) Erkrankung fragen Sie wonach? Nach:
#card #neurology #anki
Vorangegangenen Episoden von Sehstörungen oder Paresen

# Patient klagt über lumbalen Schmerz mit A
### Ein Patient klagt über lumbalen Schmerz mit Ausstrahlung in das rechte Bein. Bei einer radikulären Läsion S1 erwarten Sie im Status neben einem positiven Lasègue-Zeichen rechtsseitig:
#card #neurology #anki
Plantarflexionsschwäche im Sprunggelenk

# der Aufnahme berichtet die 64-jährige Pat
### Bei der Aufnahme berichtet die 64-jährige Patientin mit multiplen vaskulären Risikofaktoren über eine Sehstörung auf der rechten Seite. Sie vermuten eine Läsion im Bereich des Versorgungsgebiets der Art. cerebri posterior links. Diese Annahme bestätigt sich im Status durch:
#card #neurology #anki
Homonyme Hemianopsie in der Konfrontationsperimetrie

# 67-jährige Patientin wird wegen Zeichen 
### Eine 67-jährige Patientin wird wegen Zeichen einer Demenz mit zunehmender Gedächtnisstörung und kognitiven Leistungseinbußen stationär aufgenommen. Angehörige berichten über ((kurz dauernde Bewusstseinsstörungen und)) psychotische Episoden mit Wahnbildung. Ihre Vermutungsdiagnose lautet:
#card #neurology #anki
Lewy-Körperchen Demenz

# Patient mit akutem rechtshirnigen ischämi
### Ein Patient mit akutem rechtshirnigen ischämischen Insult wird durchuntersucht und eine sofortige Vollantikoagulation wird erwogen. Welche der folgenden Untersuchungen kann einen Befund ergeben, der diese Maßnahme nötig macht?
#card #neurology #anki
Transösophageales Herzecho

# rt nach dem Eintreffen einer 65-jährigen 
### Sofort nach dem Eintreffen einer 65-jährigen Patientin mit akuten sensomotorischen Halbseitenzeichen rechts und Aphasie wird nach dem Schlaganfallschema eine Blutabnahme durchgeführt. Der wichtigste Wert für die Entscheidung, ob eine Lyse durchgeführt werden kann, ist das Ergebnis
#card #neurology #anki
der Gerinnung.

# neu diagnostizierte Multiple Sklerose-Pa
### Eine neu diagnostizierte Multiple Sklerose-Patientin möchte von Ihnen eine Erklärung dafür, wie die Erkrankung entsteht. Sie antworten, dass sowohl genetische wie auch Umweltfaktoren eine maßgebliche Rolle spielen dürften. Außerdem ist bekannt, dass
#card #neurology #anki
aktivierte T-Lymphozyten und Autoantikörper zur Zerstörung von Myelinscheiden führen

# Patientin mit Verdachtsdiagnose einer sc
### Eine Patientin mit Verdachtsdiagnose einer schubförmig verlaufenden Multiplen Sklerose wird durchuntersucht, um die Diagnose zu bestätigen. Diese Bestätigung findet sich durch:
#card #neurology #anki
Oligoklonale Banden und periventrikuläre MR-Herde

# einer de novo Parkinsonpatientin zeigt si
### Bei einer de novo Parkinsonpatientin zeigt sich in der klinischen Untersuchung rechts betont an den oberen Extremitäten ein Rigor bei kontralateraler Bewegungsaktivierung. Wodurch wird die Diagnose unterstützt?
#card #neurology #anki
Vermindertes Mitschwingen der Arme beim Gehen

# 82-jähriger Patient mit bekanntem Parkins
### Ein 82-jähriger Patient mit bekanntem Parkinsonsyndrom seit 15 Jahren leidet unter einem Dopa-Langzeitsyndrom. Dabei kommt es zu Wirkungsverlust der Dopa-Therapie (wearing off) und Dyskinesien. Nach welchem weiteren Zeichen werden Sie fahnden?
#card #neurology #anki
Psychose
Wirkungsverlust der Dopa-Therapie (wearing off) und Psychose, was noch? Dyskinesie 2021-09
Maßnahme zur Verbesserung der Symptome? L-Dopa öfter am Tag zu geben. 2007-09 etc.

# einer 36-jährigen Patientin mit hemikrane
### Bei einer 36-jährigen Patientin mit hemikranen Kopfschmerzattacken mit Photophobie, Übelkeit und Erbrechen wurde eine Migräne diagnostiziert. Worin besteht die wirksamste Anfallstherapie?
#card #neurology #anki
Gabe von Tryptanen

# 34-jähriger Patient ist beim Radfahren oh
### Ein 34-jähriger Patient ist beim Radfahren ohne Helm gestürzt und hat eine Rissquetschwunde am Hinterkopf davongetragen. Er kann sich an den Unfallhergang erinnern und hat diffuse Kopfschmerzen. Sie ordnen folgende Untersuchung an:
#card #neurology #anki
Schädelröntgen zum Ausschluss einer Schädelfraktur

# 18-jährige Patientin wird nach einem sch
### Eine 18-jährige Patientin wird nach einem schweren Autounfall mit GCS (Glasgow Coma Scale) 5 gefunden, sediert, intubiert und beatmet. Im Aufnahme-CT finden sich eine 3 cm breite konvex-konkav konfigurierte Blutung parietal am Schädelknochen links, Mittellinienshift und ein mäßiges Hirnödem. Die weitere Versorgung besteht in
#card #neurology #anki
sofortiger Trepanation und Blutungsentleerung

# einem 8 [/6 2021
### Bei einem 8 /6 2021-09-jährigen Knaben wurde eine Absencenepilepsie mit typischem Anfallsmuster mit starrem Blick ins Leere für wenige Sekunden diagnostiziert. Sie klären die besorgten Eltern auf, dass bei dieser Form von Epilepsie
#card #neurology #anki
in den meisten Fällen mit Medikamenten Anfallsfreiheit zu erzielen ist

# 24-jähriger Patient mit einer jahrelang m
### Ein 24-jähriger Patient mit einer jahrelang medikamentös intraktablen mesialen Temporallappenepilepsie kommt zur präoperativen Epilepsiediagnostik. Was sind die dabei primär üblichen Methoden neben umfassender neuropsychologischer Testung?
#card #neurology #anki
MRT und Video-EEG-Monitoring

# einem 45-jährigen Patienten mit chronisch
### Bei einem 45-jährigen Patienten mit chronischem Halswirbelsäulenschmerz in der Anamnese traten gestern wieder heftige Schmerzen auf. Es findet sich auch eine Ausstrahlung in die linke Hand. Zugleich besteht auch eine Sensibilitätsstörung dorsal und palmar an den Fingern 2-4. Es besteht keine Parese, allerdings ist der linke Tricepssehnenreflex abgeschwächt. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Radikuläres Syndrom C7 li

# 55-jährige Patientin hat seit einem Aufe
### Eine 55-jährige Patientin hat seit einem Aufenthalt an der Intensivstation Probleme beim Strecken der Hand und der Finger rechts. Beim Überprüfen der Sensibilität finden Sie zusätzlich eine Hypaesthesie dorsal an der Hand und den Fingern 1-3. Ihre Diagnose lautet: Läsion des
#card #neurology #anki
N. radialis

# 53-jährige Patientin erlitt eine Subarac
### Eine 53-jährige Patientin erlitt eine Subarachnoidalblutung mit plötzlichen Kopfschmerzen, Meningismus und blutigem Liquor. In der Katheterangiographie wurde ein Aneurysma der A.communicans anterior gefunden. Nach der erfolgreichen Operation kommt es am Tag 3 zu einer klinischen Verschlechterung mit Halbseitenzeichen rechts. Welche ist die wahrscheinlichste Ursache?
#card #neurology #anki
Gefäßspasmen

# 46-jähriger Patient entwickelte neben Dys
### Ein 46-jähriger Patient entwickelte neben Dysästhesien innerhalb von 2 Tagen eine mäßige schlaffe Tetraparese. Sie behandeln unter der Verdachtsdiagnose der Polyradiculitis mit:
#card #neurology #anki
Immunglobulin i.v. oder Plasmapherese

# 49-jähriger Patient wird wegen eines Hydr
### Ein 49-jähriger Patient wird wegen eines Hydrocephalus nach einer Subarachnoidalblutung aufgenommen. Welche Therapie ist nötig?
#card #neurology #anki
Ventrikuloatrialer Shunt

# 64-jährige Patientin kommt wegen Zittern
### Eine 64-jährige Patientin kommt wegen Zitterns beider Hände in Ihre Ordination. Die einfache Frage nach Zittern in Ruhe oder bei Bewegung führt zu keinem eindeutigen Ergebnis. Welche der folgenden Fragen führt bei positiver Beantwortung zur eindeutigen Diagnose des essentiellen Tremors?
#card #neurology #anki
Verschütten Sie die Suppe, wenn Sie den Löffel zum Mund führen? 

# 55-jährige Patientin wird mit heftigen K
### Eine 55-jährige Patientin wird mit heftigen Kopfschmerzen, hohem Fieber und Übelkeit in die Notfallaufnahme gebracht. Sie hat mehrmals erbrochen und zeigt bei der Erstuntersuchung einen deutlichen Meningismus und eine leichte Bewusstseinseintrübung. Nach einer grob unauffälligen CT-Untersuchung entschließen Sie sich zur Lumbalpunktion. Sie vermuten eine bakterielle Meningitis, da der Liquor
#card #neurology #anki
eine Zellzahl von 3000/3 zeigt

# 66-jähriger Patient kommt mit der Anamnes
### Ein 66-jähriger Patient kommt mit der Anamnese zu Ihnen in die Ordination, dass vor 4 Wochen eine Ungeschicklichkeit der rechten Hand schleichend begonnen und sich seither verschlechtert habe. Seit einigen Tagen sei auch eine expressive Sprachstörung hinzugekommen. Bei der klinischen Untersuchung zeigt sich ein spastisches Hemisyndrom rechts. Ihre erste Vermutungsdiagnose?
#card #neurology #anki
Maligner Tumor

# 43-jährige Patientin kommt wegen Doppelb
### Eine 43-jährige Patientin kommt wegen Doppelbildern und einer wechselnden Schwäche der Arme in Ihre Ordination. Die Symptomatik nimmt bei Anstrengung und am Abend zu. Sie haben die Patientin zur elektrophysiologischen Abklärung zugewiesen. Welche Untersuchung wünschen Sie?
#card #neurology #anki
EMG mit repetitiver Stimulation

# 63-jährige Patientin kommt wegen beidsei
### Eine 63-jährige Patientin kommt wegen beidseitigen unwillkürlichen Augenschlusses mit intermittierender Kontraktion der periorbitalen Muskulatur in Ihre Ordination. Die Patientin ist dabei unfähig, die Lider zu öffnen und damit funktionell blind. Wie lautet Ihre Vermutungsdiagnose?
#card #neurology #anki
Blepharospasmus

# 25-jährige schwangere Patientin mit arte
### Eine 25-jährige schwangere Patientin mit arterieller Hypertonie und Proteinurie klagt über Kopfschmerzen, Augenflimmern, Ohrensausen, Tachykardie und Übelkeit bei zunehmender motorischer Unruhe. Der neurologische Status erscheint normal. Wie lautet Ihre Vermutungsdiagnose?
#card #neurology #anki
Präeklampsie

# 45-jährige Patientin wird wegen Einschla
### Eine 45-jährige Patientin wird wegen Einschlafstörungen an Ihre Ordination überwiesen. Sie berichtet, dass Sie abends im Bett die Beine nicht ruhig halten könne und daher aufstehen und herumgehen müsse. Sie äußern die Verdachtsdiagnose eines Restless Legs Syndroms und wollen die Patientin über ihre Erkrankung informieren. Welche der folgenden Aussagen über das RLS-Syndrom ist nicht richtig? Es
#card #neurology #anki
kann durch Kupfermangel bedingt sein.

# 53-jährige Patientin erlitt eine Subarac
### Eine 53-jährige Patientin erlitt eine Subarachnoidalblutung mit plötzlichen Kopfschmerzen, Meningismus und blutigem Liquor. In der Katheterangiographie wurde ein Aneurysma der A. communicans anterior gefunden. Die Therapieoptionen bestehen in:
#card #neurology #anki
Clipping oder Coiling

# 35-jähriger Patient zeigt 3 Monate nach e
### Ein 35-jähriger Patient zeigt 3 Monate nach einem schweren Schädelhirntrauma eine spontane Verschlechterung seines Zustandsbilds, bestehend aus zunehmender kognitiver Verlangsamung und positiven Pyramidenzeichen. Im Kontroll-CT finden sich leicht vergrößerte innere Liquorräume. Was ist Ihre Vermutungsdiagnose? (2)
#card #neurology #anki
Hydrocephalus malresorptivus Hydrocephalus aresorptivus 2017-06

# 38-jährige Patientin kommt wegen Doppelb
### Eine 38-jährige Patientin kommt wegen Doppelbildern und einer Schluckstörung in Ihre Ordination. Die Symptomatik nimmt bei Anstrengung und am Abend zu. Die Abklärung ergibt die Diagnose einer Myasthenia gravis. Welche symptomatische Therapie wird begonnen?
#card #neurology #anki
Azetylcholinesterasehemmer alle 3 Stunden

# 35-jährige Patientin stellt sich wegen u
### Eine 35-jährige Patientin stellt sich wegen unwillkürlichen Kopfdrehbewegungen durch tonische Spasmen der Hals- und Nackenmuskulatur vor, die schleichend begonnen haben. Ihre Verdachtsdiagnose lautet:
#card #neurology #anki
Torticollis spasmodicus

# 35-jährige Patientin wird mit Kopfschmer
### Eine 35-jährige Patientin wird mit Kopfschmerzen und beginnender Bewusstseinseintrübung in der Notfallaufnahme aufgenommen. Sie zeigt bei der Erstuntersuchung einen mäßigen Meningismus. In der Lumbalpunktion zeigen sich 453/3 Zellen. Sie denken an eine Herpes-Simplex-Meningoencephalitis. Was veranlassen Sie, um die Diagnose abzusichern? (2)
(...) Sie denken an eine Herpes-Simplex-Meningoencephalitis und veranlassen zur Diagnosesicherung eine: 2011-09
#card #neurology #anki
EEG-Untersuchung zum Nachweis von Herden bds. temporal 2009-09 etc.MR-Untersuchung zur Suche von Herden bds. temporal. 2011-09


# 38-jährige Patientin zeigt eine innerhal
### Eine 38-jährige Patientin zeigt eine innerhalb von Stunden aufsteigende schlaffe Paraparese und einen normalen Liquorbefund. Unter der Verdachtsdiagnose einer Polyradiculitis wird sie nach 3 Wochen erneut punktiert. Wie wird in diesem Fall der wahrscheinlichste Liquorbefund sein?
(...) Der wahrscheinlichste Liquorbefund: 2009-09
#card #neurology #anki
Zellzahl normal, Eiweiß exzessiv erhöht

# schwangere 17-jährige Patientin mit arte
### Eine schwangere 17-jährige Patientin mit arterieller Hypertonie und Proteinurie klagt über Kopfschmerzen, Augenflimmern, Ohrensausen, Tachykardie und Übelkeit. In der Ordination kommt es zu einem eine Minute lang dauerndem tonisch-klonischen Anfallsgeschehen. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Eklampsie

# Patient kommt mit einer Vorfußheberschwäc
### Ein Patient kommt mit einer Vorfußheberschwäche, die seit 2 Tagen besteht, in die Ordination. Zur Differenzierung einer möglichen radikulären Läsion von anderen Ursachen eignet sich folgende anamnestische Frage:
#card #neurology #anki
Haben Sie lumbale Schmerzen, die in den Fuß ausstrahlen?

# 56-jähriger Patient berichtet über „wie e
### Ein 56-jähriger Patient berichtet über „wie ein Blitz“ einschießende Schmerzzustände in Bereich des linken Nasenflügels, der linken Wange und Schläfe, die sich nach wenigen Sekunden öfter wiederholen, bis wieder Stunden der Symptomlosigkeit eintreten. Sie diagnostizieren eine Trigeminusneuralgie. Wie gehen Sie weiter vor?
#card #neurology #anki
Gabe von Carbamazepin

# einer 72-jährige Patientin wurde wegen zu
### Bei einer 72-jährige Patientin wurde wegen zunehmender Gedächtnisstörung, einer gleichzeitigen Raumorientierungsstörung und inzipienter Sprachstörung eine Demenz vom Alzheimertyp diagnostiziert. In der Besprechung über mögliche Therapieformen erklären Sie der Patientin und ihren Angehörigen, dass
#card #neurology #anki
eine Heilung nicht möglich ist, aber mit Cholinesterasehemmern eine positive Beeinflussung der kognitiven Funktionen möglich ist.

# Insult im Versorgungsgebiet der Arteria c
### Ein Insult im Versorgungsgebiet der Arteria cerebelli inferior posterior zeigt u.a. folgende Symptome: (2)
#card #neurology #anki
Drehschwindel und dissoziierte Sensibilitätsstörung

# 42-jährige Patientin mit chronischen Kop
### Eine 42-jährige Patientin mit chronischen Kopfschmerzen und normalem Schädel-CT Befund möchte in eine Spezialambulanz für Kopfschmerz einer neurologischen Abteilung zugewiesen werden. Während der Wartezeit auf einen Termin veranlassen Sie:
#card #neurology #anki
Den Ausschluss von Zahn-, HNO- und Augenaffektionen

# jüngeren Parkinsonpatienten werden als Er
### Bei jüngeren Parkinsonpatienten werden als Ersteinstellung Dopamin-Agonisten statt L-Dopa-Präparate gegeben. Der Grund dafür ist, dass
#card #neurology #anki
Studien ein späteres Auftreten von Dystonien und Dyskinesien zeigen.

# 35-jährige Patientin kommt mit einer sei
### Eine 35-jährige Patientin kommt mit einer seit 3 Tagen zunehmenden Hemihypästhesie in die Ordination, im Status finden sich außerdem diskrete cerebelläre Zeichen und eine Reflexdifferenz an den oberen Extremitäten. Sie denken an eine Multiple Sklerose und stellen folgende Frage:
#card #neurology #anki
Litten Sie jemals unter vorübergehender Ungeschicklichkeit oder Sehstörungen?
Was ordnen Sie als erste Maßnahme an? Schädel MR 2021-09

# 25-jähriger Patient wird nach einem schwe
### Ein 25-jähriger Patient wird nach einem schweren Autounfall mit GCS (Glasgow Coma Scale) 3 aufgefunden. Im Aufnahme-CT findet sich ein massives diffuses Hirnödem und eine Kontusionsblutung links temporal. Der Hirndruck ist auch mit Barbituratnarkose, Xylokainbolusbehandlung und Entwässerung nicht kontrollierbar. Am 3. Tag zeigen sich plötzlich weite, lichtstarre Pupillen. Das Eintreten des dissoziierten Hirntods wird vermutet und die Sedierung beendet. Welche klinischen Zeichen erwarten Sie im Einklang mit dieser Diagnose?
#card #neurology #anki
Ausfall der Spontanatmung und der Hirnstammreflexe

# 40-jähriger Patient klagt seit einem Moto
### Ein 40-jähriger Patient klagt seit einem Motorradunfall über eine Schwäche für Fingerspreizen und -schließen sowie Strecken der Endglieder der rechten Hand, gleichzeitig findet sich eine Hypaesthesie an den Fingern 4 und 5. Wie lautet Ihre Diagnose? Läsion des
#card #neurology #anki
N. ulnaris

# Patientin kommt wegen akuter Kopfschmerz
### Eine Patientin kommt wegen akuter Kopfschmerzen in die Notfallambulanz. Um eine Migräneattacke von einem Spannungskopfschmerz unterscheiden zu können, benötigen Sie folgende anamnestische Daten: (6)
#card #neurology #anki
Lokalisation, vegetative Begleiterscheinungen
Differentialdiagnose Migräne bzw. Spannungskopfschmerz - essentielle Fragen: 2013-07 etc.Wo sind die Kopfschmerzen lokalisiert?Sehen Sie vor dem Auftreten der Kopfschmerzen flimmernde Zacken?Verbessert oder verschlechtert körperliche Aktivität den Kopfschmerz?Ähnliche Frage: Stellt sich mit den Kopfschmerzen Übelkeit und Erbrechen ein? 2009-06 etc.
Ähnliche Frage: Lokalisation, Begleiterscheinungen wie Übelkeit, Wirksamkeit von Triptanen 2021-11

# Patientin mit mehreren intracerebralen M
### Eine Patientin mit mehreren intracerebralen Metastasen eines Bronchuskarzinoms erleidet einen Grand Mal Anfall, der nach 2 Minuten in einen andauernden Bewusstseinsverlust mit diskreten Zuckungen der oberen Extremitäten übergeht, die weitere 5 Minuten unverändert bleiben. Sie diagnostizieren:
#card #neurology #anki
Status epilepticus

# einer 67-jährigen Patientin wurde wegen S
### Bei einer 67-jährigen Patientin wurde wegen Schmerzen mit Zentrum im Schläfenbereich bei druckdolenter Arteria temporalis und einer Blutsenkungsgeschwindigkeit von 90/140 die Verdachtsdiagnose einer Arteriitis temporalis gestellt. Das weitere Vorgehen besteht in:
#card #neurology #anki
Bioptische Diagnosesicherung und Gabe von Corticoiden

# Journalist aus Ihrem Bekanntenkreis soll 
### Ein Journalist aus Ihrem Bekanntenkreis soll über die Epilepsieerkrankungen historischer Persönlichkeiten berichten. Er fragt Sie, wie häufig die Epilepsieerkrankung eigentlich ist. Die Prävalenz beträgt:
[(...), wie häufig die Epilepsie in der Allgemeinbevölkerung
eigentlich auftritt. Sie antworten: 2021-09]
#card #neurology #anki
0,7%

# 20-jähriger Patient kommt 4 Monate nach e
### Ein 20-jähriger Patient kommt 4 Monate nach einem Motorradunfall mit schwerem Schädelhirntrauma (SHT) zur Kontrolle in die Ambulanz. Er berichtet über zunehmende Kopfschmerzen, in den letzten Tagen verbunden mit Übelkeit. Sie denken an eine Spätkomplikation nach SHT und ordnen an:
#card #neurology #anki
CT-Kontrolle wegen Hydrocephalus
Welche Spätkomplikationen nach SHT? Hydrocephalus 2010-07 etc.

# einem 45-jährigen Patienten mit chronisch
### Bei einem 45-jährigen Patienten mit chronischem Halswirbelsäulenschmerz in der Anamnese traten gestern wieder heftige Schmerzen auf. Es findet sich auch eine Ausstrahlung in die linke Hand. Zugleich besteht auch eine Sensibilitätsstörung dorsal und palmar an den Fingern 2-4. Es besteht keine Schwäche, allerdings ist der linke TSR abgeschwächt. Sie haben die Verdachtsdiagnose eines radikulären Syndroms C 7 li. Welche Untersuchung im neurologischen Status dürfen Sie nicht vergessen?
#card #neurology #anki
Lange Bahnen

# 28-jährige Patient erlitt einen Autounfal
### Der 28-jährige Patient erlitt einen Autounfall und hat eine Rissquetschwunde rechts parietal davongetragen. Er kann sich an den Unfallhergang erinnern und hat geringe Kopfschmerzen. Der Status ist normal. Welche Untersuchung ordnen Sie an?
#card #neurology #anki
Schädelröntgen zum Ausschluss einer Schädelfraktur

# 45-jähriger Patient mit bekannter Multipl
### Ein 45-jähriger Patient mit bekannter Multipler Sklerose gibt in der Anamnese mehrere Episoden mit vorübergehenden neurologischen Ausfällen wie Hemihypästhesie, Sehstörungen, Para- und Hemiparesen an. Nach welchem häufigen Symptom fahnden Sie nun besonders?
#card #neurology #anki
Internukleäre Ophthalmoplegie

# 48-jähriger Patient kommt wegen seit 3 Wo
### Ein 48-jähriger Patient kommt wegen seit 3 Wochen zunehmender Kopfschmerzen in die Notfallaufnahme. Welche anamnestische Frage hilft Ihnen in der ätiologischen Zuordnung zwischen Migräne und Tumorkopfschmerz weiter?
#card #neurology #anki
„Handelt es sich um Dauerkopfschmerz oder Schmerzattacken?“

# 65-jähriger Patient erleidet aus voller G
### Ein 65-jähriger Patient erleidet aus voller Gesundheit einen einmaligen Grand Mal Anfall, die Durchuntersuchung ergibt keine Auffälligkeiten im MR. Zu diesem Zeitpunkt wird das Krankheitsgeschehen eingeordnet als:
#card #neurology #anki
Singulärer Anfall

# 77-jähriger Patient mit akuter Hemiplegie
### Ein 77-jähriger Patient mit akuter Hemiplegie links, soporöser Bewusstseinsstörung und massivem Neglect wird 70 Minuten nach Symptombeginn in die Notfallaufnahme gebracht. Im Akut-Schädel-CT werden mehrere Auffälligkeiten beschrieben, eine davon stellt eine Kontraindikation gegen Lysebehandlung dar. Welche ist dies? (2)
#card #neurology #anki
Hypodensität von ¾ des Mediastromgebiets rechts 2019-06Hypodensität von ⅓ des Mediastromgebiets rechts

# häufigste Demenzform ist die Alzheimerdem
### Die häufigste Demenzform ist die Alzheimerdemenz. Welches ist die zweithäufigste?
#card #neurology #anki
Vaskuläre Demenz

# einer 37-jährigen Patientin findet sich e
### Bei einer 37-jährigen Patientin findet sich eine Schwäche für das Beugen der Finger 1-3 und Opposition des Daumens. Sie überprüfen die Sensibilität und finden eine Hypästhesie der halben Handfläche inklusive Finger 1-3 und der Endglieder dorsal Finger 2 und 3. Sie diagnostizieren eine Läsion des:
#card #neurology #anki
N. medianus

# einem 46-jährigen Patienten bestehen seit
### Bei einem 46-jährigen Patienten bestehen seit 4 Wochen laufend Rückenschmerzen, seit heute strahlen diese in das linke Bein aus. Es besteht auch eine Sensibilitätsstörung über dem linken Knie und eine linksseitige Schwäche beim Treppensteigen. Sie haben die Verdachtsdiagnose:
#card #neurology #anki
Radikuläres Syndrom L4 li

# 56-jährige Patientin mit multiplen Insul
### Eine 56-jährige Patientin mit multiplen Insulten leidet seit 15 Jahren an schlecht eingestellter Hypertonie, hat einen Diabetes mellitus und raucht 30 Zigaretten pro Tag. Bei welcher Untersuchung kann sich ein zusätzliches Schlaganfallrisiko zeigen?
#card #neurology #anki
EKG

# Patientin kommt wegen unklaren, länger b
### Eine Patientin kommt wegen unklaren, länger bestehenden Kopfschmerzen und einer geringen Schwäche des linken Arms in Ihre Ordination. Sie haben bereits anamnestische aktuelle Beschwerden erhoben und nach allen möglichen aktuellen neurologischen Symptomen nachgefragt. Für die vollständige Anamnese bezüglich der Differentialdiagnose Hirntumor bzw. Insult fragen Sie noch nach:
#card #neurology #anki
Zeitverlauf des Auftretens
Differentialdiagnose Hirntumor bzw. Hirnmetastasen - fragen nach: Bewusstseinsverlusten und Gewichtsverlust 2007-07 etc.

# 68-jähriger Patient kommt wegen seit 2 Wo
### Ein 68-jähriger Patient kommt wegen seit 2 Wochen bestehenden dauernden Kopfschmerzen ohne Übelkeit. Der Schmerz mit lokaler Druckdolenz ist an der rechten Schläfe lokalisiert. Im Status finden sich normale Sehnenreflexe, kein Meningismus. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Arteriitis temporalis

# der Untersuchung eines Patienten, der weg
### Bei der Untersuchung eines Patienten, der wegen eines linksseitigen Kleinhirntumors operiert wurde, findet sich ein pathologischer Finger-Nase-Versuch links mit Ataxie. Welche weiteren Kleinhirnzeichen untersuchen Sie?
#card #neurology #anki
Diadochokinese und Rebound-Phänomen

# 62-jährige Patientin kommt wegen Vergess
### Eine 62-jährige Patientin kommt wegen Vergesslichkeit und Apathie zur Abklärung einer möglichen Demenz in die Ordination. Im Mini-Mental-State-Examination (MMSE) ist sie auffällig, der Neurostatus ist normal. Die Patientin berichtet über einen Leistungsknick vor 2 Monaten, Freudlosigkeit und einer Durchschlafstörung. Ihre Vermutungsdiagnose lautet Depression und Sie verordnen:
#card #neurology #anki
Serotoninwiederaufnahmehemmer
Vermutungsdiagnose: Depression 2007-07

# einem 64-jährigen Patienten mit gering li
### Bei einem 64-jährigen Patienten mit gering links betontem Ruhetremor, Hypokinese und Rigor wurde ein beginnendes Parkinsonsyndrom festgestellt. Um welchen Typ handelt es sich hier?
#card #neurology #anki
Äquivalenztyp

# 60-jähriger Patient berichtet über eine p
### Ein 60-jähriger Patient berichtet über eine plötzlich aufgetretene Schwäche des rechten Beins. Im Status zeigen sich eine Schwäche Kraftgrad 3, spastische Tonuserhöhung und gesteigerte Reflexe der re UE, zusätzlich eine geringe zentrale Parese der re OE. Dieser Status spricht für eine Läsion im Bereich:
#card #neurology #anki
Art. cerebri anterior li.

# neu diagnostizierter Epilepsiepatient sol
### Ein neu diagnostizierter Epilepsiepatient soll auf Carbamazepin eingestellt werden und fragt Sie nach der Chance auf Anfallsfreiheit. Sie antworten, dass:
#card #neurology #anki
70% der Patienten auf das erste Antiepileptikum anfallsfrei werden

# 20-jähriger Patient kommt 4 Monate nach e
### Ein 20-jähriger Patient kommt 4 Monate nach einem Motorradunfall mit schwerem Schädel-Hirn-Trauma (SHT) zur Kontrolle in die Ambulanz. Er berichtet über zunehmende Kopfschmerzen, in den letzten Tagen verbunden mit Übelkeit. Es fällt eine deutliche Verlangsamung auf. Um welche Spätkomplikationen nach SHT handelt es sich?
#card #neurology #anki
Hydrocephalus
Spätkomplikationen nach SHT - welche Untersuchung? CT-Kontrolle wegen Hydrocephalus 2009-09 etc.

# 50-jährige Patientin wird mit heftigen K
### Eine 50-jährige Patientin wird mit heftigen Kopfschmerzen, hohem Fieber und Übelkeit in die Notfallaufnahme gebracht. Sie hat mehrmals erbrochen und zeigt bei der Erstuntersuchung einen deutlichen Meningismus und eine leichte Bewusstseinstrübung. Im Liquor zeigt sich ein granulozytäres Zellbild mit 6000/3 Zellen. Welches weitere Vorgehen ist richtig?
#card #neurology #anki
Anlegen von Blut-, Speichel- und Liquorkulturen sowie sofortige ungezielte Antibiotikatherapie

# 45-jähriger Patient klagt seit den Abends
### Ein 45-jähriger Patient klagt seit den Abendstunden über plötzlich aufgetretene Kopfschmerzen und Nackensteife. Neben einem Meningismus in der klinischen Untersuchung findet sich im CT Blut in den basalen Zysternen. Welche ist die nächste Untersuchung?
#card #neurology #anki
Cerebrale Katheteranangiographie

# 36-jähriger Patient kommt wegen zunehmend
### Ein 36-jähriger Patient kommt wegen zunehmender Kopfschmerzen, Übelkeit und Erbrechen sowie Verlangsamung in die Notfallaufnahme. Im CT zeigen sich stark erweiterte innere Liquorräume mit Ausnahme des 4. Ventrikels. Welchen Verdacht haben Sie?
#card #neurology #anki
Hirndruck bei Aquäduktverschluß
ähnliche Frage: Hydrocephalus occlusus 2011-09 etc.

# 17-jährige Patientin mit arterieller Hyp
### Eine 17-jährige Patientin mit arterieller Hypertonie und Proteinurie in der Schwangerschaft klagt über Kopfschmerzen, Augenflimmern, Ohrensausen, Tachykardie und Übelkeit. In der Ordination kommt es zu einem eine Minute dauerndem tonisch-klonischen Anfallsgeschehen.Wie lautet Ihre Notfalltherapie?
#card #neurology #anki
Magnesium-Sulfat i.v.

# 23-jähriger Mann kommt wegen Unsicherheit
### Ein 23-jähriger Mann kommt wegen Unsicherheit beim Gehen in die Ambulanz. Diese Unsicherheit begann in der Pubertät und hat sich chronisch verschlechtert. Bei der klinischen Untersuchung fallen erloschene Reflexe sowie Muskelatrophien und eine deutliche Hohlfußbildung auf. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Morbus Friedreich

# 38-jährige Patientin zeigt eine innerhal
### Eine 38-jährige Patientin zeigt eine innerhalb von Stunden aufsteigende schlaffe Paraparese und einen normalen Liquorbefund. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Polyradiculitis

# Neurographie ist eine sinnvolle Zusatzunt
### Die Neurographie ist eine sinnvolle Zusatzuntersuchung bei der Abklärung folgender Verdachtsdiagnosen: (3 richtige Antworten)
#card #neurology #anki
Polyneuropathie (PNP)Guillain-Barré-Syndrom (GBS)Amyotrophe Lateralsklerose (ALS)

# 73-jähriger Patient wird wegen eines kurz
### Ein 73-jähriger Patient wird wegen eines kurzfristigen Visusverlusts am rechten Auge und fraglicher neurologischer Beschwerden an Sie überwiesen. Sie möchten herausfinden, ob es sich um eine TIA handelte und fragen den Patienten:
#card #neurology #anki
Hatten Sie eine Gefühlsstörung der linken Hand?

# 79-jährige Patientin wird von den Angehö
### Eine 79-jährige Patientin wird von den Angehörigen wegen zunehmender zerebraler Leistungsschwäche mit Gedächtnisstörungen und Verlangsamung sowie Gangstörung vorgestellt. Sie denken differentialdiagnostisch an einen Morbus Alzheimer und an einen Normaldruckhydrozephalus. Welche anamnestische Frage bringt Sie weiter?
#card #neurology #anki
Leidet die Patientin unter einer Blasenstörung?

# einem Patienten wurde mit 30 Jahren ein M
### Bei einem Patienten wurde mit 30 Jahren ein Morbus Parkinson diagnostiziert. Eine Analyse des Parkingens ergab, dass beide Allele eine Mutation aufweisen. Welcher der folgenden Aussagen über das Parkingen sind richtig?(2 richtige Antworten)
#card #neurology #anki
Mutationen in diesem Gen erklären nur eine geringe Anzahl aller Parkinsonfälle (&lt;5%).Mutationen in diesem Gen erklären eine hohe Anzahl aller früh beginnenden rezessiven Parkinsonfälle (30-50%).

# he Messparameter lassen sich in einer Ele
### Welche Messparameter lassen sich in einer Elektroneurographie bestimmten?(3 richtige Antworten)
#card #neurology #anki
NervenleitgeschwindigkeitDistal motorische LatenzSummenpotentialamplitude

# rankheitsverlauf einer 43-jährigen Patien
### Im Krankheitsverlauf einer 43-jährigen Patientin mit Multipler Sklerose zeigen sich auch unter immunmodulatorischer Therapie mit Interferonen mehrere Schübe jährlich. Die behandelnde Ärztin entscheidet sich zu einer Eskalationstherapie mit:
#card #neurology #anki
Natalizumab

# 28-jährige Patientin kommt wegen häufige
### Eine 28-jährige Patientin kommt wegen häufiger Kopfschmerzen zur Untersuchung. Welche anamnestische Frage hilft Ihnen in der ersten Zuordnung zwischen Spannungskopfschmerz und Migräne weiter? (6)
#card #neurology #anki
Stellt sich mit den Kopfschmerzen Übelkeit und Erbrechen ein? 

Differentialdiagnose Migräne bzw. Spannungskopfschmerz - essentielle Fragen: 2013-07 etc.Wo sind die Kopfschmerzen lokalisiert?Sehen Sie vor dem Auftreten der Kopfschmerzen flimmernde Zacken?Verbessert oder verschlechtert körperliche Aktivität den Kopfschmerz?Differentialdiagnose Migräne bzw. Spannungskopfschmerz - benötigte anamnestische Daten: Lokalisation, vegetative Begleiterscheinungen 2009-09 etc.Ähnliche Frage: Lokalisation, Begleiterscheinungen wie Übelkeit, Wirksamkeit von Triptanen 2021-11

# 73-jähriger Mann mit bekannter KHK und St
### Ein 73-jähriger Mann mit bekannter KHK und St.p. Insult entwickelt plötzlich einen Drehschwindel, Doppelbilder und eine Dysarthrie. Kurz darauf verliert er das Bewusstsein und wird in komatösem Zustand von der Rettung an die NFA gebracht. Im neurologischen Status zeigen sich bilateral positive Pyramidenzeichen. Welche Erkrankung müssen Sie angesichts der Anamnese und des neurologischen Befundes als wahrscheinlichste Differentialdiagnose in Erwägung ziehen?
#card #neurology #anki
Basilaristhrombose

# 25-jährige Frau berichtet, eine halbe St
### Eine 25-jährige Frau berichtet, eine halbe Stunde lang ihren rechten Arm und die rechte Gesichtshälfte nicht richtig gespürt zu haben. Etwa eine Viertelstunde nach dieser Symptomatik sind halbseitige pulsierende Kopfschmerzen aufgetreten und die Frau hat sich einmal übergeben. Was ist die wahrscheinlichste Differentialdiagnose?
#card #neurology #anki
Migräne mit Aura

# 72-jähriger Patient berichtet über eine S
### Ein 72-jähriger Patient berichtet über eine Sehstörung auf dem rechten Auge, die zehn Minuten andauerte und sich dann vollständig rückbildete. Wegen der Möglichkeit einer TIA fragen Sie den Patienten:
#card #neurology #anki
Tragen zur gleichen Zeit eine Schwäche und Gefühlsstörung der linken Hand auf?

# 61-jähriger Mann erleidet 1 Stunde vor de
### Ein 61-jähriger Mann erleidet 1 Stunde vor der Aufnahme in der NFA eine Hemiparese rechts. Im CCT wurde eine intrazerebrale Blutung ausgeschlossen. Wie lange dürfen Sie nach Ausschluss aller Kontraindikationen bei einem akuten ischämischen Insult eine systemische iv-Lyse durchführen?
#card #neurology #anki
Bis 4,5 Stunden nach klinischem Symptombeginn

# 56-jährige Frau leidet seit 1 Woche unte
### Eine 56-jährige Frau leidet seit 1 Woche unter einer rechtsseitigen Lumboischialgie. In der MRT der LWS zeigt sich ein Diskusprolaps auf Höhe L5/S1 rechts. Worauf müssen Sie im Rahmen der neurologischen Untersuchung besonders achten, da diese Symptome ein unverzügliches diagnostisches und therapeutisches Handeln erfordern? (3 richtige Antworten)
#card #neurology #anki
Hochgradige GroßzehenheberschwächeBlasen- und MastdarmstörungReithosenhypästhesie
Rückenschmerzen mit Ausstrahlung ins rechte Bein; wann unbedingt akute weitere Diagnostik? Harnverhalt, Gefühlsstörung im Intimbereich, Parese des rechten Beines 2020-09

# 19-jähriger Patient kommt zur Durchunters
### Ein 19-jähriger Patient kommt zur Durchuntersuchung, weil seit mehreren Jahren eine zunehmende Gang- und Standataxie aufgefallen ist. Anamnestisch finden sich motorische Auffälligkeiten schon seit dem Schulalter, neben cerebellären Zeichen im Status zeigt sich ein Verlust der Tiefensensibilität an beiden unteren Extremitäten. Auffällig ist auch ein Hohlfuß und eine Kyphoskoliose. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Friedreich Ataxie

# 48-jährige Frau wird mit einer Anamnese 
### Eine 48-jährige Frau wird mit einer Anamnese von seit 24 h bestehenden heftigen Kopfschmerzen in der Notfallaufnahme aufgenommen. In den Stunden vor der Aufnahme haben sich eine Somnolenz und eine Hemiparese links entwickelt. Es besteht ein deutlicher Meningismus. Es bestätigt sich der Verdacht der Subarachnoidalblutung und es wird ein Coiling eines Aneurysmas im Bereich der A. cerebri media rechts durchgeführt. In einer Kontroll-CT finden sich Insultareale im Bereich der A. cerebri anterior und media rechts. Worauf sind diese Insultareale zurückzuführen?
#card #neurology #anki
Vasospasmus

# 30-jähriger Patient kommt wegen eines sei
### Ein 30-jähriger Patient kommt wegen eines seit der Jugend bestehenden Tremors zur Durchuntersuchung. Seine Mutter litt ebenfalls an Tremor, im Arm-Vorhalte-Versuch imponiert dieser deutlich an beiden Händen, werden die Arme auf den Oberschenkeln abgelegt sistiert der Tremor. Der Patient kann die Intensität des Tremors bei Vorträgen oder Firmenpräsentationen dadurch reduzieren, dass er kurz davor ein Glas Sekt trinkt. Wie lautet Ihre Diagnose?
#card #neurology #anki
Benigner familiärer essentieller Tremor

# 23-jährige Studentin liegt nach einem SH
### Eine 23-jährige Studentin liegt nach einem SHT bei einem Radunfall im Koma. Die gemessene Hirndruckwerte sind nicht beherrschbar, die Pupillen weit und lichtstarr, die betreuende Intensivstation vermutet das Vorlieren eines dissoziierten Hirntods. Der Freund der jungen Frau spricht sich gegen eine Meldung als Spenderin aus. Wie sind in diesem Fall die rechtlichen Bedingungen für eine Hirntoddiagnostik? (2)
#card #neurology #anki
Es könnte ohne Zustimmung des Freundes mit der Hirntoddiagnostik begonnen werden. //Es kann ohne Zustimmung des Freundes mit der Hirntoddiagnostik begonnen werden.

# einer 46-jährigen Patientin zeigen sich b
### Bei einer 46-jährigen Patientin zeigen sich bei der Aufnahme bei Beginn der Symptomatik vor 6 Stunden Zeichen eines Neglects nach links und eine mittelgradige Hemiparese links. Die MR-Angiographie zeigt einen fast vollständigen Verschluss der Arteria cerebri media rechts. Was soll therapeutisch geschehen?
#card #neurology #anki
Azetylsalizylsäure
Symptomatik Beginn vor 4h: Mechanische Thrombektomie 2020-09

# 35-jährige Patientin erleidet einen epil
### Eine 35-jährige Patientin erleidet einen epileptischen Anfall, bei dem beobachtet wird, dass sie auf Fragen nicht antwortet, an ihrer Kleidung herumnestelt und schmatzend-kauende Mundbewegungen macht. Sie stellen die Verdachtsdiagnose einer:
#card #neurology #anki
Temporallappenepilepsie

# Eintreffen des Rettungswagens am Unfallo
### Beim Eintreffen des Rettungswagens am Unfallort ist der 25-jährige Patient ansprechbar, er war 10 Minuten bewusstlos. Der initiale GCS-Score war 14. Er hat eine posttraumatische Amnesie von 30 Minuten. Es handelt sich um ein/eine:
#card #neurology #anki
Leichtes Schädelhirntrauma

# n besteht die wichtigste Medikation beim 
### Worin besteht die wichtigste Medikation beim Parkinson-Syndrom mit der besten Wirkung auf die Hypo- bzw Akinese?
#card #neurology #anki
L-Dopa mit Decarboxylasehemmer 

# einer 72-jährigen Patientin wurde wegen z
### Bei einer 72-jährigen Patientin wurde wegen zunehmender Gedächtnisstörung, einer gleichzeitigen Raumorientierungsstörung und inzipienter Sprachstörung eine Demenz vom Alzheimertyp diagnostiziert. Welche Therapiemöglichkeit steht Ihnen zur Verfügung?
#card #neurology #anki
Cholinesterasehemmer

# 41-jähriger Patient hat seit heute morgen
### Ein 41-jähriger Patient hat seit heute morgen ein „schiefes Gesicht“. Sie vermuten eine periphere Fazialisparese links und finden passend im Status Auffälligkeiten beim:
#card #neurology #anki
Augenschließen, Stirnrunzeln, Zähnezeigen
zentrale Fazialisparese - Auffälligkeiten beim: Augenschließen, Zähnezeigen, Pfeifen 2009-09 etc.

# Patientin kommt wegen akuter Gesichtssch
### Eine Patientin kommt wegen akuter Gesichtsschmerzen in die Notfallambulanz. Um eine Trigeminusneuralgie von einer Nasennebenhöhlenaffektion unterscheiden zu können, benötigen Sie folgende anamnestische Daten:
#card #neurology #anki
Lokalisation und Schmerzcharakteristik

# 35-jährige Patientin mit Multipler Skler
### Eine 35-jährige Patientin mit Multipler Sklerose (MS) berichtet über Erstsymptome von Parästhesien an der linken unteren Extremität vor 10 Jahren. Eine Retrobulbärneuritis links zeigte sich drei Jahre nach Krankheitsbeginn. Seit nunmehr drei Jahren kommt es regelmäßig jährlich zu zwei Schüben mit einer spastischen Paraparese. Bei dieser Form der MS handelt es sich um ein/e:
#card #neurology #anki
Schubförmig remittierende MS

# 25-jährige Patientin wird wegen rezidivi
### Eine 25-jährige Patientin wird wegen rezidivierender Kopfschmerzattacken in die Ambulanz überwiesen. Sie berichtet über erstmalig vor einem Jahr aufgetretene Kopfschmerzen überwiegend der rechten Kopfseite, häufig mit Übelkeit und Erbrechen. Zu Ihrer Vermutungsdiagnose der Migräne passt folgende Angabe:
#card #neurology #anki
Einfache Schmerzmittel helfen nicht

# Patient kommt mit Schmerzen im Bereich de
### Ein Patient kommt mit Schmerzen im Bereich der Halswirbelsäule (HWS) in die Ambulanz. Seit dem Vortag kommt es zu ausstrahlenden Schmerzen von der HWS zum Ellbogen und von dort ulnar bis zum Kleinfinger ziehend. Bei der Untersuchung zeigt sich keine Schwäche aber eine Abschwächung des Trizepssehnenreflexes, sowie eine geringe Hypästhesie an der ulnaren Handkante. Es handelt sich um ein/e:
#card #neurology #anki
Radikuläres Syndrom C8

# 34-jähriger Patient mit Meningismus und s
### Ein 34-jähriger Patient mit Meningismus und starken Kopfschmerzen wird bei klarem Bewusstsein akut mit Verdacht auf SAB zum CT gebracht. Dort fällt auf, dass der Patient einschläft und auf Ansprechen nur kurz antwortet und gleich wieder wegschläft. Der Bewusstseinszustand des Patienten ist nunmehr:
#card #neurology #anki
Somnolent

# 56-jähriger Patient berichtet über „wie e
### Ein 56-jähriger Patient berichtet über „wie ein Blitz“ einschießende Schmerzzustände im Bereich des linken Nasenflügels, der linken Wange und Schläfe, die sich nach wenigen Sekunden öfter wiederholen, bis wieder Stunden der Symptomlosigkeit eintreten. Wie lautet Ihre Diagnose?
#card #neurology #anki
Trigeminusneuralgie

# hem Gefäßversorgungsgebiet ist ein Insult
### Welchem Gefäßversorgungsgebiet ist ein Insult mit homonymer Hemianopsie nach rechts zuzuordnen? Der:
#card #neurology #anki
Linken Arteria cerebri posterior

# Patientin mit Verdachtsdiagnose einer sc
### Eine Patientin mit Verdachtsdiagnose einer schubförmig verlaufenden Multiplen Sklerose wird untersucht, um die Diagnose zu bestätigen. Welche ist die wesentlichste Untersuchung zur Diagnosesicherung?
#card #neurology #anki
MR-Untersuchung

# 34-jährige Patientin mit bekannter Epile
### Eine 34-jährige Patientin mit bekannter Epilepsie erleidet einen Grand-Mal-Anfall. Zuerst ist eine tonische Phase mit Verkrampfung aller Extremitäten zu beobachten. Nach ½ Minute kommt es zu:
#card #neurology #anki
Einer klonischen Phase, die in eine Bedämmerung übergeht

# 65-jähriger Patient berichtet über eine s
### Ein 65-jähriger Patient berichtet über eine seit einigen Tagen bestehende geringgradige Lähmung der linken Hand. Welche anamnestische Frage hilft in der Vermutungsdiagnose zur Ätiologie in der Entscheidung zwischen Schlaganfall und radikulärer Läsion weiter?
#card #neurology #anki
Haben Sie Schmerzen in der betroffenen Extremität?

# 45-jährige Patientin kommt wegen Einschl
### Eine 45-jährige Patientin kommt wegen Einschlafstörungen in Ihre Ordination. Sie berichtet, dass Sie abends unter Dysästhesien der Beine leide und die halbe Nacht in der Wohnung herumgehe, was die Beschwerden etwas lindere. Diagnose und Therapie sind folgende:
#card #neurology #anki
Restless Legs Syndrom – Therapieversuch mit Dopamin-Agonist

# Mutter kommt mit ihrer 2-jährigen Tochte
### Eine Mutter kommt mit ihrer 2-jährigen Tochter zu Ihnen, da sie bemerkt hat, dass das bisher gesunde Kind beim Laufen Schwierigkeiten hat und eine zunehmende muskuläre Schwäche und Hypertrophie der Wadenmuskulatur bemerkt wird. Sie befürchtet eine Muskeldystrophie vom Duchenne-Typ und damit schwere Lebensbeeinträchtigung und verminderte Lebenserwartung. Sie sagen der Mutter, dass:
#card #neurology #anki
Mädchen keine klassische Duchenne-Muskeldystrophie bekommen können

# 53-jähriger Patient wird mit einer rasch 
### Ein 53-jähriger Patient wird mit einer rasch progredienten schlaffen Tetraparese, die rasch von distal nach proximal aufsteigt, im Krankenhaus aufgenommen. Es handelt sich um eine Polyradikulitis. Welche der folgenden Symptome können im Verlauf beobachtet werden?(3 richtige Antworten)
#card #neurology #anki
Ausfall sämtlicher MuskeleigenreflexeBilaterale periphere FazialispareseAusfall der Atemmuskulatur mit Beatmungspflicht

# 48-jährige Frau wird mit einer Anamnese 
### Eine 48-jährige Frau wird mit einer Anamnese von seit 24 Stunden bestehenden heftigen Kopfschmerzen in der Notfallaufnahme versorgt. In den Stunden vor der Aufnahme haben sich eine Somnolenz und eine Hemiparese links entwickelt. Es besteht ein deutlicher Meningismus. Welche Untersuchungsmethode wird als erste eingesetzt?
#card #neurology #anki
Computertomographie

# 28-jährige Patientin kommt wegen Fieber 
### Eine 28-jährige Patientin kommt wegen Fieber und Kopfschmerzen mit leichter Übelkeit in die Notfallaufnahme. Sie zeigt einen leichten Meningismus und Sie entschließen sich zur Lumbalpunktion. Sie vermuten eine Virusmeningitis, da der Liquor:
#card #neurology #anki
Eine Zellzahl von 233/3 Zellen zeigt

# 66-jähriger Patient kommt in Ihre Ordinat
### Ein 66-jähriger Patient kommt in Ihre Ordination mit der Anamnese, dass vor 4 Wochen eine Ungeschicklichkeit der rechten Hand schleichend begonnen und sich seither verschlechtert habe. Seit einigen Tagen seien Kopfschmerzen und auch eine expressive Sprachstörung hinzugekommen. Bei der klinischen Untersuchung zeigt sich ein spastisches Hemisyndrom rechts. Ihre Vermutungsdiagnose ist die eines malignen Tumors. Welche klinische Untersuchung kann die Wahrscheinlichkeit Ihrer Diagnose erhöhen?
#card #neurology #anki
Augenhintergrund mit Papille

# 63-jährige Patientin kommt wegen beidsei
### Eine 63-jährige Patientin kommt wegen beidseitigem unwillkürlichen Augenschlusses mit intermittierender Kontraktion der periorbitalen Muskulatur in Ihre Ordination. Die Patientin ist dabei unfähig, die Lider zu öffnen und damit funktionell blind. Die Therapie besteht in:
#card #neurology #anki
Lokalen Botulinumtoxininjektionen

# der klinischen Untersuchung einer 57-jähr
### Bei der klinischen Untersuchung einer 57-jährigen Patientin in der Notfallaufnahme zeigt sich eine Parese der rechten OE mit Kraftgrad 4 und normaler Trophik. Eine Veränderung des Tonus kann nicht sicher erhoben werden. Welche klinische 2018-09 Untersuchung ist angezeigt, um die Feststellung der zentralen Parese zu sichern?
#card #neurology #anki
Sehnenreflexe
Paraparese mit spastischer Tonuserhöhung und gesteigertem PSR und ASR beidseits; mit welcher Untersuchung kann man die zentralen Parese sichern? Pyramidenzeichen 2007-07 etc.

# 48-jährige Patientin wird nach einem sch
### Eine 48-jährige Patientin wird nach einem schweren Autounfall mit GCS 3 aufgefunden. Im Aufnahme-CT findet sich ein massives diffuses Hirnödem. Der Hirndruck ist auch mit Barbituratnarkose nicht kontrollierbar. Am 3. Tag zeigen sich plötzlich weite, lichtstarre Pupillen. Das Eintreten des dissoziierten Hirntods wird vermutet und die Sedierung beendet. Welche klinischen Zeichen erwarten Sie im Einklang mit dieser Diagnose?
#card #neurology #anki
Ausfall von Masseterreflex, Ciliospinalreflex und Kornealreflex

# 73-jähriger Patient kommt in Ihre Ordinat
### Ein 73-jähriger Patient kommt in Ihre Ordination und meint, er leide unter einer Demenz. Welchen Screeningtest setzten Sie neben der Patientenbeobachtung und der Mini-Mental- State-Examination zur Beurteilung einer möglichen Demenz ein?
#card #neurology #anki
Uhrentest

# 37-jährige Multiple-Sklerose-Patientin k
### Eine 37-jährige Multiple-Sklerose-Patientin klagt über eine Blasenstörung. Diese erweist sich typischerweise als:
#card #neurology #anki
Imperativer Harndrang
ähnliche Frage: Urgeinkontinenz 2021-09

# 56-jähringer Mann bemerkt im Bereich der 
### Ein 56-jähringer Mann bemerkt im Bereich der linken Hand ein Zittern, das nur in Ruhe auftritt. 2 Monate nach Beginn der Symptomatik wird er zur neurologischen Untersuchung zugewiesen. Es besteht die Verdachtsdiagnose eines Parkinson-Syndroms. Welche klinischen Aspekte weisen auf ein Parkinson-Syndrom hin?
#card #neurology #anki
Reduziertes Mitschwingen des linken Arms beim Gehen

# 48-jähriger Patient wird nach einem Arbei
### Ein 48-jähriger Patient wird nach einem Arbeitsunfall mit Schädelhirntrauma bewusstlos aufgefunden, vom Notarzt sediert, intubiert und beatmet. Im Aufnahme CT findet sich ein diffuses Hirnödem. Der Patient wird auf der Intensivstation aufgenommen und erhält:
#card #neurology #anki
Hirndrucksonde

# Patient mit bekanntem radikulärem Syndrom
### Ein Patient mit bekanntem radikulärem Syndrom L5 mit typischer Schmerzausstrahlung und geringem sensiblen Ausfall kommt wegen einer Veränderung seiner Symptomatik in die Ambulanz. Der massive Schmerz der vergangenen Tage ist plötzlich verschwunden, allerdings kann er die Großzehe nicht heben und er hat auch kein Gefühl am Vorfuß und an der Großzehe. Welche der folgenden Maßnahmen ist/sind in dieser Situation primär
notwendig?
(...) Folgende Maßnahme/n ist/sind notwendig: 2011-09
#card #neurology #anki
Sofortige neurochirurgische Vorstellung

# 25-jährige Patientin mit Multipler Skler
### Eine 25-jährige Patientin mit Multipler Sklerose nach dem fünften klinischen Schub zeigt im neurologischen Status bilaterale Pyramidenzeichen und eine spastische Tetraparese mit Rechtsbetonung. Es ist auch das Lhermitte-Zeichen auslösbar. Dieses ist dadurch charakterisiert, dass:
#card #neurology #anki
Bei passiven Kopfbewegungen nach vorne ein elektrisierendes Gefühl in beiden Armen verspürt wird.

# 62-jähriger Patient berichtet über eine S
### Ein 62-jähriger Patient berichtet über eine Schwäche und Lähmung der rechten Hand, die 30 Minuten andauerte und sich dann vollständig zurückbildete. Wegen der Möglichkeit einer transistorisch ischämischen Attacke (TIA) als ätiologischen Hintergrund fragen Sie den Patienten:
#card #neurology #anki
Trat zur gleichen Zeit eine Sehstörung auf dem linken Auge auf?

# einer Patientin mit Verdacht auf komplett
### Bei einer Patientin mit Verdacht auf komplette Querschnittsläsion in Höhe Th 8 haben Sie bereits den Ausfall aller Vorderseitenstrangqualitäten und der feinen Berührung unterhalb von Th 8 festgestellt. Um den Ausfall weiterer Hinterstrangqualitäten zu klären untersuchen Sie auch:
#card #neurology #anki
Zahlenerkennen und Vibration

# Insult im Versorgungsgebiet der Arteria c
### Ein Insult im Versorgungsgebiet der Arteria cerebri anterior links zeigt sich in folgender Symptomatik:
#card #neurology #anki
Beinbetonte Hemiparese rechts

# Patient kommt mit einer Schwäche für die 
### Ein Patient kommt mit einer Schwäche für die Plantarflexion des linken Fußes, die seit 2 Tagen besteht, in die Ordination. Zur Differenzierung einer möglichen radikulären Läsion von anderen Ursachen eignet sich welche anamnestische Frage?
#card #neurology #anki
Haben Sie eine Gefühlsstörung im Bereich des Fußes bemerkt?

# 35-jähriger Mann berichtet über Kopfschme
### Ein 35-jähriger Mann berichtet über Kopfschmerzattacken, die nachts aus dem Schlaf heraus beginnen und 20 Minuten bis mehrere Stunden andauern. Der Schmerz ist hemikran links lokalisiert mit Punktum maximum am Auge, dabei Augenröte und Nasenfluss. Nicht steroidale Antirheumatika waren ineffektiv. Wie lautet Ihr Therapievorschlag bei Verdacht auf Clusterkopfschmerz?
#card #neurology #anki
Triptane

# 34-jährige Patient ist beim Radfahren ohn
### Der 34-jährige Patient ist beim Radfahren ohne Helm gestürzt und hat eine Rissquetschwunde am Hinterkopf davongetragen. Er kann sich an den Unfallhergang erinnern, hat diffuse Kopfschmerzen und einen normalen neurologischen Status. Das Schädelröntgen zeigt eine Schädelfraktur. Wie gehen Sie weiter vor?
#card #neurology #anki
Stationäre Aufnahme

# 35-jährige Patientin berichtet über Schm
### Eine 35-jährige Patientin berichtet über Schmerzen von der Schulter über den Ellbogen bis zur Hand ausstrahlend, Kribbeln und Brennen der Fingerspitzen I bis III. Die am meisten hilfreiche Untersuchung, um nach motorischen Symptomen einer Medianusparese zu fahnden, ist die Überprüfung der/des:
#card #neurology #anki
Opposition des Daumens

# 46-jähriger Patient kommt wegen Kopfschme
### Ein 46-jähriger Patient kommt wegen Kopfschmerzen, Schwindel, Übelkeit und Erbrechen zur Aufnahme. Die Symptome haben vor 5 Tagen begonnen und kontinuierlich zugenommen. Im durchgeführten Akut-CT zeigt sich eine deutliche Erweiterung der Seitenventrikel und des III. Ventrikels, der IV. Ventrikel erscheint normal groß. Welche Diagnose ist zu stellen?
#card #neurology #anki
Hydrocephalus occlusus
ähnliche Frage: Hirndruck bei Aquäduktverschluß 2010-07 etc.

# 33-jährige Patientin mit komplikationslo
### Eine 33-jährige Patientin mit komplikationsloser Schwangerschaft entwickelt Parästhesien in beiden Händen sowie heftige Schmerzen bds. von den Fingern I–III bis in die Unterarme ausstrahlend. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Carpaltunnelsyndrom bds.

# 40-jähriger Patient klagt seit den Abends
### Ein 40-jähriger Patient klagt seit den Abendstunden über plötzlich aufgetretene Kopfschmerzen „wie nie“ und Nackensteife. In der neurologischen Untersuchung in der Notfallaufnahme finden Sie einen angedeuteten Meningismus. Sie ordnen an:
#card #neurology #anki
Ein CT, um eine SAB auszuschließen

# 34-jährige Patientin kommt wegen Zittern
### Eine 34-jährige Patientin kommt wegen Zitterns beider Hände in Ihre Ordination. Bei der klinischen Untersuchung finden sich kein Rigor und keine Hypokinese. Zur Differentialdiagnose weisen Sie zur folgenden Laboruntersuchung zu:
#card #neurology #anki
Kupferstoffwechsel

# 63-jährige Patientin liegt nach einem li
### Eine 63-jährige Patientin liegt nach einem links-hirnigen Insult auf der Intermediate Care Station. In Ihrem Nachtdienst werden Sie von der Nachtdienstschwester zur Patientin geholt, da die Patientin mit der Sauerstoffsättigung mehrfach auf unter 80 abgesunken ist. Als Sie zum Bett kommen, atmet die Patientin zuerst normal, dann setzt die Atmung für ca. 10 Sekunden aus und kommt erst wieder bei Sättigungswerten unter 80 in Gang. Was ist Ihre Vermutungsdiagnose?
#card #neurology #anki
Schlafapnoesyndrom

# einem Patienten mit linkshirnigem ischämi
### Bei einem Patienten mit linkshirnigem ischämischen Insult wird im transösophagealen Herzecho ein flottierender Thrombus im Herzohr entdeckt. Welche therapeutische Entscheidung wird daraufhin getroffen?
#card #neurology #anki
Vollheparinisierung

# übergewichtige 37-jährige Frau stellt si
### Eine übergewichtige 37-jährige Frau stellt sich mit Kopfschmerzen vor, die mit einer Gesichtsrötung einhergehen und die vor allem bei Aufregung auftreten. Es gibt kein Tagesmaximum, die Beschwerden halten oft auch über Stunden an, dabei zeigen sich keine vegetativen Begleiterscheinungen. Was ist Ihr nächster Schritt?
#card #neurology #anki
Blutdruck-Messen

# 18-jährige Patientin wird nach einem sch
### Eine 18-jährige Patientin wird nach einem schweren Autounfall mit GCS 5 gefunden. Im Aufnahme-CT finden sich ein diffuses Hirnödem und ein Subduralhämatom rechts, das operativ versorgt wird. Die Hirndruckmessung zeigt über mehrere Tage deutlich erhöhte Werte. Nach Normalisierung dieser Werte wird die Sedierung reduziert, aber die Patientin wacht nicht auf. Es wird die Verdachtsdiagnose „Übergang ins apallische Syndrom“ gestellt. Welche Zeichen für ein „Wachkoma“ würden Sie im Weiteren erwarten?
#card #neurology #anki
Keine Blickfixierung, vegetative Entgleisung

# 28-jährige Patientin kommt wegen Ungesch
### Eine 28-jährige Patientin kommt wegen Ungeschicklichkeit und einer geringen Schwäche des rechten Arms in die Ordination. Die Beschwerden haben vor drei Tagen akut begonnen. Welche der folgenden anamnestischen Angaben macht eine Multiple Sklerose (MS) wahrscheinlich?
#card #neurology #anki
Vorangegangene Episode von Sehstörungen

# einer 31-jährigen Patientin mit Verdachts
### Bei einer 31-jährigen Patientin mit Verdachtsdiagnose Multiple Sklerose wird zusätzlich zur MR-Untersuchung eine Lumbalpunktion durchgeführt. Was zeigt sich typisch im Lumbalpunktat?
#card #neurology #anki
18/3 Zellen und oligoklonale Banden

# 56-jähringer Mann bemerkt im Bereich der 
### Ein 56-jähringer Mann bemerkt im Bereich der linken Hand ein Zittern, das nur in Ruhe auftritt. 2 Monate nach Beginn der Symptomatik wird er zur neurologischen Untersuchung zugewiesen. Es besteht die Verdachtsdiagnose eines Parkinson-Syndroms. Es zeigt sich ein geringer Rigor an der linken oberen Extremität sowie eine geringe Dysdiadochokinese links und ein mangelndes Mitbewegen des linken Armes beim Gehen. Wie lautet die Diagnose?
#card #neurology #anki
Hemiparkinson-Syndrom vom Äquivalenztyp

# Patient mit Diabetes mellitus Typ II wird
### Ein Patient mit Diabetes mellitus Typ II wird Ihnen mit der Frage nach einer möglichen Polyneuropathie vorgestellt. Sie bestätigen die Diagnose und erheben folgende anamnestische Angaben bzw. Befunde: (3 richtige Antworten)
#card #neurology #anki
Reduktion der Vibrationsempfindung an beiden unteren ExtremitätenAbschwächung des ASR (Achillessehnenreflex) beidseitsVerlängerung der distalen Latenzen in der Nervenleitgeschwindigkeitsuntersuchung

# 40-jährige Patientin wird nach einem Rei
### Eine 40-jährige Patientin wird nach einem Reitunfall in der Unfallambulanz betreut. Sie war kurz bewusstlos, der Unfallhergang ist ihr nicht vollständig erinnerlich. Schädelröntgen unauffällig, Neurostatus mit diskreten Halbseitenzeichen. Die Patientin reagiert etwas verlangsamt. Was soll angeordnet werden?
#card #neurology #anki
CT und stationäre Aufnahme

# 9-jähriger Knabe leidet unter einer schwe
### Ein 9-jähriger Knabe leidet unter einer schweren Epilepsie mit verschiedenen Anfallsformen, insbesondere Grand-Mal und tonische Sturzanfälle. Es besteht eine höhergradige Therapieresistenz und der Patient hat einen kognitiven Entwicklungsrückstand. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Lennox-Gastaut Syndrom

# rt nach dem Eintreffen der 65-jährigen Pa
### Sofort nach dem Eintreffen der 65-jährigen Patientin mit akuten sensomotorischen Halbseitenzeichen rechts und Aphasie wird die Durchführung einer systemischen Thrombolyse erwogen. Für die Entscheidung, ob eine Lyse durchgeführt werden soll, ist die wichtigste Information:
#card #neurology #anki
Der genaue Zeitpunkt des Beginns der Symptomatik

# einem akuten Multiple-Sklerose-Schub eine
### Bei einem akuten Multiple-Sklerose-Schub einer 28-jährigen Patientin kommt es zu einer hochgradigen Paraparese. Sie verordnen als Schubtherapie:
#card #neurology #anki
1 g Methylprednisolon i. v. über 5 Tage

# Patient mit Hemiparese rechts hat Schwier
### Ein Patient mit Hemiparese rechts hat Schwierigkeiten beim Sprechen. Sie versuchen eine Zuordnung zu einer Aphasieform. Um eine motorische Aphasie zu erkennen beurteilen Sie zuerst:
#card #neurology #anki
Spontansprache

# 63-jähriger Patient kommt wegen zunehmend
### Ein 63-jähriger Patient kommt wegen zunehmender Gedächtnis und Wortfindungsstörungen ohne Alltagsbeeinträchtigung in die Ambulanz. In der Mini-Mental-State-Untersuchung erreicht er 27 Punkte. Wie lautet Ihre Vermutungsdiagnose?
#card #neurology #anki
Leichte kognitive Störung

# 30-jährige Patientin stellt sich wegen „
### Eine 30-jährige Patientin stellt sich wegen „seit Jahren bestehenden“ chronischen Kopfschmerzen vor. Die Kopfschmerzattacken stellen sich in letzter Zeit im Schnitt 2 mal pro Woche ein. Dabei kommt es am Beginn der Attacke zu Sehstörungen, der Kopfschmerz würde später so schlimm, dass sie sich ins Bett legen müsste. Begleitend tritt eine Übelkeit – auch mit Erbrechen – auf, die Attacken dauern meist einen halben Tag. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Migräne 

# 35-jährige Patientin erleidet einen epil
### Eine 35-jährige Patientin erleidet einen epileptischen Anfall, bei dem beobachtet wird, dass sie auf Fragen nicht antwortet, an ihrer Kleidung herumnestelt und schmatzend kauende Mundbewegungen macht. Es handelt sich um eine Temporallappenepilepsie. Welches Medikament wird zur initialen Behandlung verwendet?
#card #neurology #anki
Carbamazepin
Temporallappenepilepsie, empfohlene Medikation: Monotherapie mit Levetiracetam xx etc.

# 35-jährige Patientin berichtet über Schm
### Eine 35-jährige Patientin berichtet über Schmerzen von der Schulter über den Ellbogen bis zur Hand ausstrahlend, Kribbeln und Brennen der Fingerspitzen I bis III. Mit der Verdachtsdiagnose eines Karpaltunnelsyndroms suchen Sie nach einer sensiblen Reiz- oder Ausfallssymptomatik in welchem Bereich?
#card #neurology #anki
Der Finger I bis III palmar

# Patient klagt über lumbalen Schmerz mit A
### Ein Patient klagt über lumbalen Schmerz mit Ausstrahlung in das rechte Bein. Bei einer radikulären Läsion L5 erwarten Sie im Status neben einem positiven Lasègue-Zeichen rechtsseitig:
#card #neurology #anki
Dorsalflexionsschwäche der Großzehe

# einem Patienten mit bakterieller Meningit
### Bei einem Patienten mit bakterieller Meningitis kommt es nach 14 Tagen antibiotischer Therapie zu einer deutlichen Verbesserung der Beschwerden, die Kopfschmerzen nehmen ab, der Meningismus geht zurück. Nach einer weiteren Woche kommt es wieder zu Kopfschmerzen, einer Verlangsamung, auch Sehstörungen treten auf. Es besteht kein Fieber, die Entzündungswerte sind im Normbereich. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Hydrocephalus malresorptivus

# einer 63-jähringen Patientin wird ein Res
### Bei einer 63-jähringen Patientin wird ein Restless-Legs Syndrom suspiziert. Welche Symptome sprechen dafür? (3 richtige Antworten)
#card #neurology #anki
Bewegungsdrang mit sensiblen MissempfindungenEinschlafstörungen und Verstärkung der Symptome in der NachtLinderung durch Aktivität

# 48-jährige Frau wird mit einer Anamnese 
### Eine 48-jährige Frau wird mit einer Anamnese von seit 24 h bestehenden heftigen Kopfschmerzen in der Notfallaufnahme aufgenommen. In den Stunden vor der Aufnahme haben sich eine Somnolenz und eine Hemiparese links entwickelt. Es besteht ein deutlicher Meningismus. Im CT findet sich Blut im frontalen Interhemispährenspalt und in der digitalen Subtraktions-Katheterangiographie ein Aneurysma der A. communicans anterior. Worin besteht die Therapie?
#card #neurology #anki
Aneurysmacoiling

# 17-jähriger Patient leidet unter der auto
### Ein 17-jähriger Patient leidet unter der autosomal-dominant vererbten Muskelerkrankung einer Myotonen Dystrophie (Curshmann-Steinert). Welche Symptome bzw. Befunde passen zu dieser Erkrankung? (3 richtige Antworten)
#card #neurology #anki
Seit dem 15. Lebensjahr kam es zu einer mäßig progredienten Schwäche der Muskulatur insbesondere an den oberen Extremitäten.Es bestehen eine Katarakt und eine Hodenatrophie.Im Serum findet man eine Erhöhung der Kreatinkinase.

# 63-jähriger Patient kommt wegen Zitterns
### Eine 63-jähriger Patient kommt wegen Zitterns der rechten Hand in Ihre Ordination. Bei der klinischen Untersuchung können Sie die Verdachtsdiagnose Parkinsonsyndrom durch folgenden Befund erhärten:
#card #neurology #anki
Rigor rechts bei kontralateraler Aktivierung

# 48-jährige Frau wird mit einer Anamnese 
### Eine 48-jährige Frau wird mit einer Anamnese von seit 24 h bestehenden heftigen Kopfschmerzen in der Notfallaufnahme aufgenommen. In den Stunden vor der Aufnahme haben sich eine Somnolenz und eine Hemiparese links entwickelt. Es besteht ein deutlicher Meningismus. In der Computertomographie zeigt sich eine Blutung in den Subarachnoidalräumen und Anzeichen eines Verschlusshydrocephalus. Welche Maßnahme ist erforderlich?
#card #neurology #anki
Ventrikeldrainage

# 62-jährige Patientin wird wegen einer au
### Eine 62-jährige Patientin wird wegen einer aufsteigenden schlaffen Paraparese bei einem Liquorbefund mit mäßig erhöhtem Eiweiß aufgenommen. Der Oberarzt besteht darauf, die Patientin auf einem Intermediate Care Bett unterzubringen. Was sollte demnach monitiert werden?
#card #neurology #anki
Pulsoxymetrie und EKG

# 27-jähriger Patient mit der Verdachtsdiag
### Ein 27-jähriger Patient mit der Verdachtsdiagnose einer Multiplen Sklerose zeigt bei der Untersuchung ein spastisch-ataktisches Gangbild, eine rechtsbetonte spastische Paraparese und eine Hinterstrangssensibilitätsstörung, die für einen Herd im unteren Thorakalmark spricht. Er berichtet über eine Retrobulbärneuritis links vor zwei Jahren. Sie halten den Multiple-Sklerose-Verdacht für hochgradig, weil:
#card #neurology #anki
Eine räumliche und zeitliche Dissemination von Herden anzunehmen ist

# 40-jährige Patientin berichtet über „and
### Eine 40-jährige Patientin berichtet über „andauernde“ Kopfschmerzen seit mehreren Monaten mit nur wenigen schmerzfreien Tagen. Häufig komme es dabei zu Übelkeit. Sie habe Medikamente verschrieben bekommen, die aber kaum mehr eine dauernde Wirkung zeigen. Schließlich brauche sie täglich mehrere Tabletten, wobei auch so genannte Triptane immer schlechter wirken würden. Der Status ist normal. Ihre Diagnose und ihr Vorgehen:
#card #neurology #anki
Medikamenten-induzierter Kopfschmerz – Stationäre Aufnahme zum Entzug

# Familie eines Patienten mit Polyradikulit
### Die Familie eines Patienten mit Polyradikulitis (Guillain-Barré) wünscht sich Aufklärung über die Prognose. Sie erklären, dass:
#card #neurology #anki
Bei der überwiegenden Zahl der Patienten Vollremission zu erwarten ist, diese aber Monate dauern kann

# 43-jähriger Patient wird nach einem Front
### Ein 43-jähriger Patient wird nach einem Frontalzusammenstoß bewusstlos aus dem Auto geborgen. Der Notarzt entschließt sich zur sofortigen Sedierung, Intubation und Beatmung mit Reduktion des pCO2. Wozu dient diese Maßnahme?
#card #neurology #anki
Einem Hirnödem entgegenzuwirken.

# 56-jähriger Mann bemerkt im Bereich der l
### Ein 56-jähriger Mann bemerkt im Bereich der linken Hand ein Zittern, das nur in Ruhe auftritt. 2 Monate nach Beginn der Symptomatik wird er zur neurologischen Untersuchung zugewiesen. Es besteht die Verdachtsdiagnose eines Parkinson-Syndroms. Welche initiale Medikation ist dazu vorgesehen?
#card #neurology #anki
Dopaminagonisten

# Patientin mit schubförmiger Multipler Sk
### Eine Patientin mit schubförmiger Multipler Sklerose kann den immunmodulatorischen Therapieversuch mit Interferon Beta wegen massiver Nebenwirkungen (Grippe-ähnliche Symptome und Muskelschmerzen) nicht fortsetzen. An Alternativen stehen folgende zur Verfügung: (3)
(...) Was kann alternativ gegeben werden? 2021-09
#card #neurology #anki
Glatiramerazetat oder Immunglobuline 2008-07 etc.Dimethylfumarat oder Glatiramerazetat 2021-09

# Patientin kommt wegen unklaren, länger b
### Eine Patientin kommt wegen unklaren, länger bestehenden Kopfschmerzen und einer geringen Schwäche des linken Arms in die Ordination. Sie haben bereits anamnestische aktuelle Beschwerden erhoben und nach allen möglichen aktuellen neurologischen Symptomen nachgefragt. Für die vollständige Anamnese bezüglich der Differentialdiagnose Hirntumor bzw. Hirnmetastasen fragen Sie noch nach:
#card #neurology #anki
Bewusstseinsverlusten und Gewichtsverlust
Differentialdiagnose Hirntumor bzw. Insult - fragen nach: Zeitverlauf des Auftretens 2010-07 etc.

# der Aufnahme berichtet die 74-jährige Pat
### Bei der Aufnahme berichtet die 74-jährige Patientin mit multiplen vaskulären Risikofaktorenüber mehrere Schlaganfälle links- und rechtshirnig mit jeweils geringer Symptomatik. Neben bilateralen Pyramidenzeichen findet sich im Status ein positiver Glabellareflex. Dieser gehört zu den:
#card #neurology #anki
Frontalen Zeichen

# einem 74-jährigen Patienten treten seit m
### Bei einem 74-jährigen Patienten treten seit mehr als einem Jahr zunehmend eine Gedächtnisstörung, kognitive Leistungseinbußen und eine Apraxie auf. Der MMSE (Mini Mental State Examination) liegt bei 25; der Patient wird zu einem bildgebenden Verfahren zugewiesen. Mit der Vermutungsdiagnose einer Demenz vom Alzheimertyp ist folgendes Untersuchungsergebnis vereinbar: (2)
(...) Die Vermutungsdiagnose einer Demenz vom Alzheimertyp wurde durch folgendes Untersuchungsergebnis gestützt: 2009-09 etc.
#card #neurology #anki
Gering erweiterte innere und äußere Liquorräume im CT 2012-09Minderperfusion temporo-parietal beidseits im SPECT 2009-09 etc.

Indikatoren für Entwicklung bzw. Vorhandensein einer Alzheimer Demenz: Hippokampusatrophie im Schädel-MRT; reduzierter Glucosestoffwechsel im FDG-PET des Gehirns xx etc.

# 46-jähriger Patient hat ein Schädelhirntr
### Ein 46-jähriger Patient hat ein Schädelhirntrauma mit rechtshirnigem Kontusionsherd erlitten. Drei Monate nach dem Ereignis zeigt sich ein Anfallsgeschehen, bei dem die linke Hand und der linke Arm rhythmisch zucken. Der Patient kann später das Ereignis vollständig beschreiben, da das Bewusstsein erhalten bleibt. Es handelt sich dabei um eine/n:
#card #neurology #anki
Einfach fokalen Anfall

# Patient kommt wegen aufsteigender distal 
### Ein Patient kommt wegen aufsteigender distal betonter Schwäche beider unterer Extremitäten in die Ordination. Sie denken an die mögliche Entwicklung einer Polyneuropathie oder einer Polyradikulitis. Die wesentliche anamnestische Frage zur Differentialdiagnose lautet:
#card #neurology #anki
Wie rasch haben sich die Symptome entwickelt?

# Patient kommt mit der Verdachtsdiagnose e
### Ein Patient kommt mit der Verdachtsdiagnose einer obstruktiven Schlafapnoe in die Ambulanz. Welche anamnestisch erhobenen Symptome passen zu dieser Erkrankung? (3 richtige Antworten)
#card #neurology #anki
Es kommt zu Apnoephasen von mehr als 10 Sekunden.Untertags besteht eine erhöhte Müdigkeit mit Leistungs- und Konzentrationsabfall.Der Familie sind schon seit längerem laute Schnarchgeräusche durch passageren Kollaps der Schlundmuskulatur aufgefallen.

# 25-jährige Patientin kommt wegen abnorme
### Eine 25-jährige Patientin kommt wegen abnormer Müdigkeit, die häufig untertags auftritt, in Ihre Ordination. Sie berichtet auch über mitunter einsetzenden Schlafdrang. So könne sie auch bei alltäglichen Lebenssituationen wie beim Essen und während Vorlesungen das Einschlafen nicht verhindern. Sie wache dann meist nach kurzer Zeit wieder auf. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Narkolepsie

# 59-jähriger Patient wird wegen des Verdac
### Ein 59-jähriger Patient wird wegen des Verdachts auf Wallenberg-Syndrom aufgenommen. Anamnestisch trat in der Nacht plötzlich ein Drehschwindel auf, auch bemerkte der Patient eine Schluckstörung. Welche Zeichen im Status sprechen für ein Wallenberg-Syndrom? (3 richtige Antworten)
#card #neurology #anki
Hornersyndrom auf der Seite der LäsionStörung der Sensibilität für Schmerz- und Temperatur kontralateral zur LäsionAtaxie homolateral zur Läsion

# 35-jährige Patientin stellt sich wegen c
### Eine 35-jährige Patientin stellt sich wegen chronischer Kopfschmerzen vor. Diese bestehen seit mehr als einem halben Jahr und führen zu häufigen Krankenständen. Welche anamnestischen Fragen sind essentiell für die Differentialdiagnose Migräne bzw. Spannungskopfschmerz? (3 richtige Antworten) (6)
#card #neurology #anki
Wo sind die Kopfschmerzen lokalisiert?Sehen Sie vor dem Auftreten der Kopfschmerzen flimmernde Zacken?Verbessert oder verschlechtert körperliche Aktivität den Kopfschmerz?
Ähnliche Frage: Stellt sich mit den Kopfschmerzen Übelkeit und Erbrechen ein? 2009-06 etc.Differentialdiagnose Migräne bzw. Spannungskopfschmerz - benötigte anamnestische Daten: Lokalisation, vegetative Begleiterscheinungen 2009-09 etc.
Ähnliche Frage: Lokalisation, Begleiterscheinungen wie Übelkeit, Wirksamkeit von Triptanen 2021-11

# der Kraftprüfung eines Patienten mit radi
### Bei der Kraftprüfung eines Patienten mit radikulärer Läsion L4 rechts findet sich für das Strecken im Kniegelenk eine herabgesetzte Kraft. Die Streckung im Knie ist gegen Widerstand nicht möglich, sehr wohl aber gegen die Schwerkraft. Es handelt sich um eine Parese mit dem:
#card #neurology #anki
Kraftgrad III

# 46-jähiger Patient kommt wegen heftiger S
### Ein 46-jähiger Patient kommt wegen heftiger Schmerzen lumbal mit Ausstrahlung in die linke aber auch rechte untere Extremität in die Ambulanz. Anamnese und Status sprechen für eine radikuläre Läsion S1 rechts, möglicherweise zusätzlich L5 links. Im Hinblick auf eine mögliche Caudaläsion stellen Sie folgende anamnestische Frage:
#card #neurology #anki
Haben Sie eine Blasenstörung bemerkt?

# einem Patienten besteht eine periphere Pa
### Bei einem Patienten besteht eine periphere Parese distal an der linken unteren Extremität. Diese Parese kann anatomisch gesehen bedingt sein durch eine Läsion im Bereich: (3 richtige Antworten)
#card #neurology #anki
der motorischen Vorderhornzellender Nervenwurzelnder peripheren Nerven

# 60-jähriger Patient berichtet über eine p
### Ein 60-jähriger Patient berichtet über eine plötzlich aufgetretene Ungeschicklichkeit der linken Hand. Im Status zeigt sich eine distale Schwäche KG 4, spastische Tonuserhöhung und gesteigerte Reflexe der li OE, zusätzlich eine zentrale Fazialisparese li. Dieser Status spricht für eine Läsion im Bereich der/des:
#card #neurology #anki
Art. cerebri media re.

# 67-jährige Patientin wird wegen Zeichen 
### Eine 67-jährige Patientin wird wegen Zeichen einer Demenz mit zunehmender Gedächtnisstörung und kognitiven Leistungseinbußen stationär aufgenommen. Angehörige berichten über eine vor wenigen Tagen erfolgte deutliche Verschlechterung, die mit einer Schwäche der linken OE einhergeht. Zur Diagnose einer möglichen vaskulären Demenz ordnen Sie folgende Untersuchungen an:
#card #neurology #anki
CT des Gehirns

# 34-jähriger Patient wird Ihnen wegen schw
### Ein 34-jähriger Patient wird Ihnen wegen schwerer Kopfschmerzen in der Notfallaufnahme zugewiesen. Welche Untersuchungsbefunde und Fakten lassen Sie an die Differentialdiagnose des Cluster-Kopfschmerzes denken? (3 richtige Antworten)
#card #neurology #anki
Der Patient hat seine Kopfschmerzanfälle überwiegend nachts.Es fallen eine Augenrötung und Nasenrinnen auf der betroffenen Seite des Gesichtsschmerzesauf.Der Patient ist voller Bewegungsunruhe.

# 18-jährige Patientin wird nach einem sch
### Eine 18-jährige Patientin wird nach einem schweren Autounfall mit GCS (Glasgow Coma Scale) 5 aufgefunden, sediert, intubiert und beatmet. Im Aufnahme-CT findet sich ein schmales (1/2 cm) Subduralhämatom rechts und ein diffuses Hirnödem. Die weitere Versorgung besteht in:
#card #neurology #anki
Hirndrucksonde, Intensivtherapie.

# 69-jährige Patientin wird nach Auftreten
### Eine 69-jährige Patientin wird nach Auftreten eines akuten rechtseitigen Hemisyndroms mit Parese, Sensibilitätsstörung und Sprachstörung aufgenommen. Bevor mit einer systemischen Lyse mit rt-PA begonnen werden kann, müssen folgende Faktoren ausgeschlossen werden:(2 richtige Antworten)
#card #neurology #anki
Hinweise auf ausgedehnte frühe Infarktzeichen und Blutung im CTSchweres Schädel-Hirn-Trauma, Schlaganfall in den vergangenen drei Monaten

# 56-jähriger Mann bemerkt im Bereich der l
### Ein 56-jähriger Mann bemerkt im Bereich der linken Hand ein Zittern, das nur in Ruhe auftritt. Zwei Monate nach Beginn der Symptomatik wird er zur neurologischen Untersuchung zugewiesen. Es besteht die Verdachtsdiagnose eines Parkinson-Syndroms. Zur Bestätigung der Diagnose wird in der klinischen Untersuchung nach folgendem Symptom gefahndet:
#card #neurology #anki
Rigor der betroffenen Halbseite

# einem 78-jährigen Patienten wurde eine De
### Bei einem 78-jährigen Patienten wurde eine Demenz vom Alzheimertyp diagnostiziert. Neben einer Gedächtnisstörung fanden sich dazu passend im neurologischen Status noch andere Symptome: (3 richtige Antworten) (7)
#card #neurology #anki
Eine SprachstörungEine apraktische StörungHalluzinationen und Wahnbildungen
Charakteristika einer Alzheimer Demenz: schleichend progredienter Krankheitsverlauf; Beeinträchtigung des episodischen Neugedächtnisses; klare Bewusstseinslage 2018-09

# 23-jähriger Patient kommt wegen seltsamer
### Ein 23-jähriger Patient kommt wegen seltsamer Anfälle, unter denen er leidet, in Ihre Ordination. Er berichtet, dass in Situationen, in denen er sich besonders aufregt (z.B. zornig oder beleidigt ist oder lachen muss) plötzlich alle Muskeln erschlaffen würden und er zu Boden sinken oder stürzen würde. Er sei dabei bei vollem Bewusstsein. Was ist Ihre Vermutungsdiagnose?
#card #neurology #anki
Kataplexie bei Narkolepsie

# 54-jährige Patientin verspürt beim Hochh
### Eine 54-jährige Patientin verspürt beim Hochheben eines schweren Korbes plötzlich heftige Kopfschmerzen im Nacken. Nach 12 Stunden stellt sich die Patientin in der Notfallaufnahme vor, die Anteflexion des Kopfes ist schmerzhaft eingeschränkt, der Kopfschmerz höchstgradig beeinträchtigend. Die akut durchgeführte Computertomographie des Schädels zeigt unauffällige Verhältnisse. Eine Lumbalpunktion ergibt als einziges auffälliges Zeichen Erythrozyten im Liquor mit absteigender Rotfärbung des Liquors in der Drei Gläser Probe. Wie lautet Ihre Entscheidung für das weitere Vorgehen?
#card #neurology #anki
Gabe von Schmerzmitteln bei Bedarf – keine Subarachnoidalblutung

# einer 31-jährigen Patientin mit Kauschwäc
### Bei einer 31-jährigen Patientin mit Kauschwäche und Schluckstörung und intermittierenden Doppelbildern wird eine Myasthenia Gravis diagnostiziert. Welche Therapieoptionen stehen zur Verfügung? (3 richtige Antworten)
#card #neurology #anki
ThymektomieAcetylcholinesterase-HemmerKortison

# 36-jähriger Patient liegt nach einem schw
### Ein 36-jähriger Patient liegt nach einem schweren Schädel-Hirn Trauma auf der Intensivstation. Es werden Hirndruckwerte über 90 mmHg gemessen. Die basalen Zisternen erscheinen im CT aufgebraucht. Der Patient hat keine Spontanatmung. Die linke Pupille reagiert nicht, die rechte verzögert auf Licht. Der Hustenreflex ist erloschen. Kann mit der Hirntoddiagnostik begonnen werden?
#card #neurology #anki
Nein, da noch ein Hirnstammreflex vorhanden ist.

# 58-jährige Patientin stellt sich wegen E
### Eine 58-jährige Patientin stellt sich wegen Einschlafstörungen vor, die mit unangenehmen Empfindungen im Bereich der Beine einhergehen und durch Herumgehen gebessert werden. Durch die Gabe des Dopamin-Agonisten Ropinirol kann eine deutliche Besserung erzielt werden. Wie lautet Ihre Diagnose?
#card #neurology #anki
Restless Legs Syndrom

# Aufnahme kommt eine 45-jährige Patientin 
### Zur Aufnahme kommt eine 45-jährige Patientin mit einer Anamnese von Kopfschmerzen über die letzten 4 Tage. Am Tag der Aufnahme kam es zu einer Bewusstseinsstörung mit Somnolenz sowie einem erstmaligen generalisierten epileptischen Anfall. Durch welche Befunde wird die Verdachtsdiagnose der Herpes simplex Meningoenzephalitis erhärtet?(3 richtige Antworten)
#card #neurology #anki
Liquorzellzahl von 750/3Beidseits temporal Theta- Delta Herde im EEGKraniale MR mit beidseits temporobasalem Ödem und Zeichen der Gewebsläsion

# Woche nach einer Gastroenteritis wird ei
### Eine Woche nach einer Gastroenteritis wird eine 42-jährige Patientin wegen innerhalb von Stunden auftretender distal beginnender Paraparese stationär aufgenommen. Wodurch wird die Verdachtsdiagnose der Polyradikulitits erhärtet?(3 richtige Antworten)
#card #neurology #anki
Schlaffe Paresen an UE, später OE mit reduzierten SehnenreflexenErhöhtes Eiweiß in der LumbalpunktionCampylobacter jejuni Infektion als Ursache für die Gastroenteritis

# 63-jähriger Patient mit bekannter Hyperto
### Ein 63-jähriger Patient mit bekannter Hypertonie wird wegen einer rechtshirnigen Blutung mit Halbseitenzeichen links stationär aufgenommen. Am Abend des Aufnahmetags kommt es zu einer Verschlechterung der Situation und einer Vergrößerung der Blutung. Wie würde sich ein erhöhter Hirndruck manifestieren? (3 richtige Antworten) (7)
#card #neurology #anki
Bewusstseinseintrübung (Sopor)Bilaterale spastische Tonuserhöhung und positive PyramidenzeichenBradykardie
neurologische Zeichen bei zunehmendem Hirndruck: Bewusstseinseintrübung, beidseits positive Pyramidenzeichen, Erbrechen 2015-09Notfalltertial: Zeichen des erhöhten Hirndrucks: Anisokorie, Erbrechen, Krampfanfall xx

# he anatomische Struktur kann bei einer ze
### Welche anatomische Struktur kann bei einer zentrale Fazialisparese betroffen sein?
#card #neurology #anki
Tractus corticonuclearis

# 42-jähriger Patient leidet unter Konzentr
### Ein 42-jähriger Patient leidet unter Konzentrationsstörungen und Wesensveränderungen. In der neurologischen Untersuchung fällt ein angedeuteter Rigor beidseits, ein Flapping- Tremor sowie eine Dysarthrie und skandierende Sprache auf. Mit welcher der folgenden Untersuchungen können Sie die vermutete hepatolentikuläre Degeneration (Morbus Wilson) beweisen? (3 richtige Antworten)
#card #neurology #anki
SpaltlampenuntersuchungLeberbiopsieErniedrigte Konzentration von Ceruloplasmin im Serum

# der Untersuchung einer Parese wird der Kr
### Bei der Untersuchung einer Parese wird der Kraftgrad angegeben. KG 3 bedeutet:
#card #neurology #anki
Bewegung gegen die Schwerkraft möglich

# 62-jährige Patientin kommt in Ihre Ordin
### Eine 62-jährige Patientin kommt in Ihre Ordination und berichtet, dass vor 3 Wochen eine Ungeschicklichkeit der linken Hand schleichend begonnen und sich seither verschlechtert habe. Seit letzter Woche habe sie auch zunehmend Kopfschmerzen, zeitweilig mit Übelkeit. Bei der klinischen Untersuchung zeigt sich ein spastisches Hemisyndrom links. Ihre Vermutungsdiagnose eines malignen Tumors erhärtet sich in der MR-Untersuchung, die eine rechts-hirnige Raumforderung mit perifokalem Ödem zeigt. Welche diagnostische Maßnahme ist vor der Einleitung einer Strahlen- oder Chemotherapie erforderlich?
#card #neurology #anki
Histologische Diagnose

# 43-jährige Patientin stellt sich mit hef
### Eine 43-jährige Patientin stellt sich mit heftigen Schmerzen lumbal in der Ambulanz vor. Der Schmerz besteht schon seit 2 Wochen und strahlt seit 3 Tagen in das linke Bein aus und zwar vom hinteren Oberschenkel bis zur Ferse. Gestern Abend sei auch eine Gefühlsstörung im Bereich des Fußes dazu gekommen. Was ist die wahrscheinlichste Diagnose bei dieser Anamnese?
#card #neurology #anki
Radikuläre Läsion S1

# 35-jährige Patientin berichtet über häuf
### Eine 35-jährige Patientin berichtet über häufige Kopfschmerzen, die meist morgens nach dem Erwachen vorhanden sind und im Lauf des Tages an Intensität zunehmen. Körperliche Aktivität führt zu keiner Zunahme der Schmerzen. Es besteht keine Übelkeit. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Spannungkopfschmerz

# einem Patienten mit Feinmotorikstörung de
### Bei einem Patienten mit Feinmotorikstörung der linken oberen Extremität findet sich links an oberer und unterer Extremität ein Rigor. Rechts ist der Tonus normal. Was schließen Sie daraus?
#card #neurology #anki
Es besteht ein Hemiparkinsonsyndrom links.

# 35-jähriger Patient ist seit 4 Jahren an 
### Ein 35-jähriger Patient ist seit 4 Jahren an einer Multiplen Sklerose mit schubförmigem Verlauf und einer Schubfrequenz von 1 Schub pro Jahr erkrankt. Nach den Schüben kam es immer zu Vollremission. Der Patient unterzieht sich einer Operation im Zahnbereich und hat daraufhin Fieber bis 39 °C. Gleichzeitig tritt erneut eine Hemiparese rechts auf. Diese bessert sich nach Abklingen des Fiebers bereits deutlich. Der Patient hat Angst vor einem erneuten Schub. Wie entscheiden Sie sich therapeutisch?
#card #neurology #anki
Keine Therapie da kein Schub

# Angehöriger eines Parkinsonpatienten hat 
### Ein Angehöriger eines Parkinsonpatienten hat in den Medien gehört, dass Parkinson angeblich vererbbar sei. Der Angehörige wendet sich mit der Bitte um Aufklärung an Sie. Welche der folgen Aussagen über die Genetik des Morbus Parkinson sind richtig?(3 richtige Antworten)
#card #neurology #anki
In den Genen LRRK2 und alpha-Synuklein sind Varianten beschrieben, die das Risiko an M. Parkinson zu erkranken erheblich erhöhen (Penetranz über 50%).In den Genen LRRK2 und alpha-Synuklein sind Varianten beschrieben, die das Risiko an M. Parkinson zu erkranken minimal erhöhen (Odds ratio1-2).Patienten mit einem frühen Krankheitsbeginn (&lt;20-30a) zeigen häufig eine rezessive Form der Vererbung.

# he Bedingungen müssen für die Bestätigung
### Welche Bedingungen müssen für die Bestätigung des dissoziierten Hirntods erfüllt sein?(3 richtige Antworten)
#card #neurology #anki
Koma mit fehlenden HirnstammreflexenAusfall der SpontanatmungNull-Linien EEG über 30 Minuten dokumentiert

# 73-jährige Patientin mit rechtshirnigem 
### Eine 73-jährige Patientin mit rechtshirnigem Insult zeigt bei der neurologischen Untersuchung unter anderem einen schweren Neglect nach links. Dieser ist dadurch charakterisiert, dass (2 richtige Antworten)
#card #neurology #anki
eine Vernachlässigung der linken Seite besteht.das Zentrum der Wahrnehmung nach rechts verschoben ist.

# 40-jähriger Mann mit bekannter Epilepsie 
### Ein 40-jähriger Mann mit bekannter Epilepsie erleidet nach Absetzen der antiepileptischen Therapie mehrere generalisierte tonisch-klonische Anfälle. Nach Gabe von 4 mg Lorazepam i.v. treten weiterhin fokal eingeleitete generalisierte Anfälle auf. Welche Substanz stellt beim Status epilepticus nach Versagen der Benzodiazepine die First-Line-Therapie dar?
#card #neurology #anki
Phenytoin i.v.

# he Therapie ist bei Verdacht auf Wernicke
### Welche Therapie ist bei Verdacht auf Wernicke-Encephalopathie umgehend einzuleiten?
#card #neurology #anki
Vitamin B1 i.v.

# Patient wird Ihnen mit Verdacht auf ein G
### Ein Patient wird Ihnen mit Verdacht auf ein Guillain-Barré-Syndrom (GBS) zugewiesen. In der neurographischen Untersuchung finden Sie in vier untersuchten Nerven eine verlangsamte Nervenleitgeschwindigkeit. Welche weiteren Messwerte erhärten die Verdachtsdiagnose? (3 richtige Antworten)
#card #neurology #anki
Deutlich verlängerte F-Wellen in zwei motorischen NervenVerlängerte distale LatenzenInkomplette Leitungsblöcke

# 19-jährige Frau kommt mit 40°C Fieber, K
### Eine 19-jährige Frau kommt mit 40°C Fieber, Kopfschmerzen und deutlichem Menigismus in die NFA. Welchen Liquorbefund erwarten Sie, falls sich Ihre Verdachtsdiagnose einer akuten bakteriellen Meningitis bestätigt? (3)
#card #neurology #anki
Massive Pleozytose, erhöhtes Laktat, erniedrigte Glukose

# 58-jähriger Patient hat Ihnen berichtet, 
### Ein 58-jähriger Patient hat Ihnen berichtet, dass er am Morgen mit einer Ungeschicklichkeit der linken Hand aufgewacht sei. Sie schließen daraus auf ein Problem der Motorik. Was möchten Sie mit Ihrer nächsten Anamnesefrage herausfinden?
#card #neurology #anki
Ob eine Parese oder eine Ataxie vorliegt

# 67-jährige Patientin klagt seit mehr als
### Eine 67-jährige Patientin klagt seit mehr als einem Jahr über zunehmende Vergesslichkeit, Schwierigkeiten Gegenstände wiederzufinden und hatte in letzter Zeit Probleme, von der Chorstunde nach Hause zu finden. Die Minimental State Examination erbringt einen Wert von 25 Punkten. Welche sind die ersten diagnostischen Maßnahmen, die Sie zur Abklärung der Demenz ergreifen? (3 richtige Antworten)
#card #neurology #anki
Neuropsychologische TestungCraniale MRTBestimmung von Blutzucker, Schilddrüsenfunktionswerten und Vitamin B12-Konzentration

# führen bei einer Patientin mit seit einer
### Sie führen bei einer Patientin mit seit einer Woche bestehenden, langsam aufsteigenden Parästhesien an allen 4 Extremitäten eine Lumbalpunktion durch. Welchen Liquorbefund erwarten Sie sich im Falle eines Guillain-Barré-Syndroms?
#card #neurology #anki
Eiweißerhöhung

# 67-jähriger Patient kommt wegen eines Hal
### Ein 67-jähriger Patient kommt wegen eines Haltetremors in die Ambulanz. Der Tremor besteht seit mehr als einem Jahr und führt dazu, dass der Patient die Suppe mit dem Löffel nicht mehr essen kann. Wie lautet Ihre Diagnose?
#card #neurology #anki
Essentieller Tremor

# 38-jähriger Mann berichtet in der NFA, da
### Ein 38-jähriger Mann berichtet in der NFA, dass er vor zwei Stunden beim Aufstehen vom Tisch einen plötzlich einsetzenden occipitalen Kopfschmerz (so stark wie nie zuvor) erlitten hat. Der Kopfschmerz sei nach wie vor vorhanden und spricht nicht auf Analgetika an. Welche potenziell vital bedrohliche Erkrankung kann hier vorliegen?
#card #neurology #anki
Subarachnoidalblutung

# 65-jährige Patientin wird Ihnen vom Haus
### Eine 65-jährige Patientin wird Ihnen vom Hausarzt mit brennenden Schmerzen an beiden Fußsohlen und Unterschenkeln zugewiesen. Es besteht ein langjähriger Diabetes Mellitus. In der von Ihnen durchgeführten Neurographie finden Sie verlangsamte Nervenleitgeschwindigkeiten in 4 untersuchten Nerven bei normaler Summenpotentialamplitude. Außerdem sind die F-Wellen verlängert. Welche Diagnose kommt für Sie primär in Frage?
#card #neurology #anki
Demyelinisierende Polyneuropathie

# schwangere Patientin leidet unter nächtl
### Eine schwangere Patientin leidet unter nächtlichen Schmerzen der rechten Hand, insbesondere der Finger 1 bis 3. Darüber hinaus bestünde am Daumen und Zeigefinger ein Taubheitsgefühl. Sie vermuten ein Karpaltunnelsyndrom. Was bestätigt Ihre Vermutung in der Neurographie? (2 richtige Antworten)
#card #neurology #anki
Eine verlängerte distal motorische Latenz über dem N. medianus rechtsEine verlangsamte sensible Nervenleitgeschwindigkeit über dem N. medianus rechts

# 30-jähriger Patient kommt in die Akutambu
### Ein 30-jähriger Patient kommt in die Akutambulanz, da er am Morgen nach besonders tiefem Schlaf mit einer schlaffen Parese für die Streckung im rechten Handgelenk, Streckung der Finger rechts und einem bamstigen Gefühl am Handrücken rechts zwischen 1. und 2. Finger aufwachte. Was ist die wahrscheinliche Diagnose?
#card #neurology #anki
Läsion des N. radialis

# 58-jährige Patientin kommt wegen Schwind
### Eine 58-jährige Patientin kommt wegen Schwindelbeschwerden in die Ambulanz. Der Schwindel wird als Drehgefühl beschrieben, und tritt beim Umdrehen im Bett, beim Aufstehen aus dem Liegen und bei anderen Kopfbewegungen auf. Welche diagnostischen Maßnahmen verwenden Sie zur Schwindeldifferentialdiagnose? (3 richtige Antworten)
#card #neurology #anki
Frenzelbrillen-UntersuchungDix-Hallpike LagerungsmanöverUnterberger-Tretversuch

# 63-jähriger Mann klagt über brennende Mis
### Ein 63-jähriger Mann klagt über brennende Missempfindungen in den Füßen, die seit ca. 2 Jahren bestehen, eine Unsicherheit beim Gehen und schmerzhafte Muskelkrämpfe. An Vorerkrankungen bestehen eine Hypertonie, eine koronare Herzkrankheit und ein Diabetes mellitus. Im neurologischen Status finden sie keine Paresen, ein vermindertes Vibrationsempfinden an den Füßen, fehlende Achillessehnenreflexe und eine Gangunsicherheit. Woran ist differentialdiagnostisch als erstes zu denken?
#card #neurology #anki
Distal symmetrische sensomotorische Polyneuropathie

# 35-jähriger Mann hat seit 2 Wochen 2-3 Ma
### Ein 35-jähriger Mann hat seit 2 Wochen 2-3 Mal pro Tag immer links temporo-orbital auftretende, unerträglich starke Kopfschmerzen, die etwa 2 Stunden andauern und von einer ipsilateralen Ptosis und Augenrötung begleitet sind. Bereits vor 2 Jahren hatte er gleichartige Kopfschmerzen, die über insgesamt 6 Wochen auftraten, danach war er bis vor 2 Wochen beschwerdefrei. Welche Therapie ist zur Behandlung der Attacken zu empfehlen? (2 richtige Antworten)
#card #neurology #anki
Sumatriptan 6 mg subcutanSauerstoff per Inhalation

# 49-jährige Patientin hat seit 20 Jahren 
### Eine 49-jährige Patientin hat seit 20 Jahren Migräne ohne Aura. Seit 5 Jahren hat die Attackenfrequenz zugenommen, und sie hat nun fast tägliche Kopfschmerzen mittlerer Intensität mit geringer Übelkeit und Lichtempfindlichkeit, die den ganzen Tag andauern. Etwa einmal pro Woche hat sie eine heftige typische Migräneattacke. Sie nimmt 18-20 Mal pro Monat je 1 Tablette Zolmitriptan und zusätzlich auch mehrmals pro Monat eine Tablette mit Acetylsalicylsäure. Welche therapeutische Intervention muss als erste gesetzt werden?
#card #neurology #anki
Entzugstherapie

# 30-jähriger Mann wird nach einem ersten g
### Ein 30-jähriger Mann wird nach einem ersten generalisierten tonisch-klonischen Anfall mit lateralem Zungenbiss zu Ihnen in die Notaufnahme gebracht. Im neurologischen Status zeigt er keine Auffälligkeiten. Eine strukturelle Abklärung mittels MRT des Gehirns sowie ein EEG ergeben ebenfalls keine Auffälligkeiten. Anamnestisch berichtet der Mann, dass er die letzten Tage sehr wenig und zuletzt gar nicht geschlafen habe und außerdem im Rahmen einer Geburtstagsfeier größere Mengen an Alkohol konsumiert hätte. Welche antiepileptische Medikation werden Sie ihm verordnen?
#card #neurology #anki
Keine
Verdachtsdiagnose: Gelegenheitsanfall 2016-07

# 22-jährige Frau hat seit 3 Tagen dumpfe,
### Eine 22-jährige Frau hat seit 3 Tagen dumpfe, drückende Kopfschmerzen, die kurz nachdem sie sich aufsetzt oder nach dem Aufstehen auftreten. Zusätzlich bestehen während der Schmerzen geringe Übelkeit und ein ungerichteter Schwindel. Beim Hinlegen klingen die Schmerzen rasch wieder ab. Vor 5 Tagen erfolgte ambulant eine Lumbalpunktion wegen des Verdachtes auf eine Meningitis, die unauffällig war. Paracetamol brachte bisher keine Besserung der Schmerzen. Der neurologische Status ist unauffällig. Welche Maßnahme ist in jedem Fall zu empfehlen?
#card #neurology #anki
In liegender Position bleiben

# 42-jähriger Patient wurde in der neurolog
### Ein 42-jähriger Patient wurde in der neurologischen Ambulanz wegen einer Polyneuropathie durchuntersucht, die sich durch mehr als 5 Jahre entwickelte. Die Ätiologien Diabetes, Alkoholabusus, Vitamin-B12-Mangel und infektiöse Ursachen wurden ausgeschlossen. Welche weiteren Untersuchungen sollten zur ätiologischen Abklärung noch durchgeführt werden? (3 richtige Antworten)
#card #neurology #anki
Bestimmung der antinukleären AntikörperGenetische UntersuchungUntersuchung des Serumproteins

# 25-jährige Lehrerin wird von ihrem Ehema
### Eine 25-jährige Lehrerin wird von ihrem Ehemann wegen einer „unklaren Bewusstseinsstörung“ in die Notfallaufnahme gebracht. Der Ehemann berichtet, dass seine Frau seit mehreren Monaten wiederholt plötzlich teilnahmslos „Löcher in die Luft“ starre und auf Ansprache nicht reagiere. Nach einigen Sekunden würde sie Kaubewegungen durchführen und (nicht vorhandene) Dinge vom Tisch wischen. Nach einigen Minuten wäre sie kontaktfähig, aber sehr müde und könne sich an das Ereignis nicht erinnern. Außer einem Kontrazeptivum nimmt die Patientin keine Medikamente. Die neurologische Untersuchung ergibt einen unauffälligen Befund. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Komplex-fokale Anfälle

# 35-jähriger Patient Ihrer Epilepsieambula
### Ein 35-jähriger Patient Ihrer Epilepsieambulanz schildert seine Anfälle folgendermaßen: Typischerweise kündige sich ein Anfall durch ein leichtes, aus der Magengegend aufsteigendes Übelkeitsgefühl an; gleichzeitig mit der dann folgenden verminderten Reaktion auf Ansprache käme es zum Auftreten von oro-alimentären Automatismen, einer Dystonie der rechten oberen Extremität sowie Vokalisationen. Danach sei der Patient für einige Minuten desorientiert. Aufgrund von welchem der beschriebenen klinischen Symptome diagnostizieren Sie richtigerweise einen komplex-fokalen Anfall?
#card #neurology #anki
Einschränkung des Bewusstseins

# he Auren werden bei Anfällen aus dem mesi
### Welche Auren werden bei Anfällen aus dem mesialen Temporallappen beobachtet? (3 richtige Antworten)
#card #neurology #anki
Iktale AngstEpigastrische AurenDéjà-vue

# 77-jähriger Patient mit akuter Hemiplegie
### Ein 77-jähriger Patient mit akuter Hemiplegie links, soporöser Bewusstseinsstörung und massivem Neglect wird 70 min nach Symptombeginn in die Notfallaufnahme gebracht. Im Akut-Schädel-CT findet sich keine Blutung. Welcher der folgenden Befunde stellt eine Kontraindikation für die Lysebehandlung dar?
#card #neurology #anki
Thrombozytopenie

# he Therapie ist bei Morbus Wilson zu erwä
### Welche Therapie ist bei Morbus Wilson zu erwägen? (2 richtige Antworten)
#card #neurology #anki
LebertransplantationGabe von D-Penicillamin zur Erhöhung der Kupferausscheidung

# 20-jährige Patientin wurde mit der Rettu
### Eine 20-jährige Patientin wurde mit der Rettung in die Notfallaufnahme gebracht, weil beim Aufstehen vom Schreibtisch plötzlich heftige Kopfschmerzen occipito-nuchal einsetzten und ihren Kollegen ein verlangsamtes Sprechen aufgefallen war. Im neurologischen Status ist die Patientin wach, psychomotorisch gering verlangsamt, gering meningeal, jedoch ohne sensomotorische Herd- oder Halbseitenzeichen. Das Akut-CCT war unauffällig. Welche Untersuchung muss als nächste durchgeführt werden?
#card #neurology #anki
Lumbalpunktion

# 25-jähriger Patient wird wegen eines leic
### Ein 25-jähriger Patient wird wegen eines leichten Schädelhirntraumas zur Beobachtung auf der Unfallstation aufgenommen. Bei der Anamnese stellen Sie fest, dass der Patient mit einer durchschnittlichen Alkoholmenge von 2 Liter Wein am Tag alkoholkrank ist. Das Auftreten welcher Komplikationen müssen Sie bedenken? (3 richtige Antworten)
#card #neurology #anki
Epileptische AnfälleDelirWernicke Encephalopathie

# he Befunde in den Zusatzuntersuchungen si
### Welche Befunde in den Zusatzuntersuchungen sind bei einer akuten Polyradikulitis (Guillain Barré-Syndrom) typischerweise zu erwarten? (2 richtige Antworten)
#card #neurology #anki
Normale Zellzahl und erhöhtes Eiweiß im LiquorVerlangsamte Nervenleitgeschwindigkeit in der Elektroneurografie

# 70-jährige Patientin hat seit 2 Monaten 
### Eine 70-jährige Patientin hat seit 2 Monaten heftige, einschießende Schmerzen im Bereich des linken Oberkiefers, die nur einige Sekunden andauern und 20-100 mal am Tag auftreten. Kauen oder sprechen kann die Attacken auslösen. Der neurologische Status und ein zerebrales MRT sind unauffällig. Welche ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Trigeminusneuralgie

# 70-jähriger Patient wird Ihnen vom Hausar
### Ein 70-jähriger Patient wird Ihnen vom Hausarzt mit brennenden Schmerzen an beiden Fußsohlen zugewiesen. Es bestünde ein langjähriger Alkoholabusus. In der von Ihnen durchgeführten Neurographie finden Sie reduzierte Summenpotentialamplituden in 4 untersuchten Nerven bei normaler Nervenleitgeschwindigkeit. Welche Diagnose kommt für Sie primär in Frage?
#card #neurology #anki
Axonale Polyneuropathie

# Geschwister erkranken mit 25 bzw. 30 Jah
### Zwei Geschwister erkranken mit 25 bzw. 30 Jahren an Morbus Parkinson. Die Eltern der beiden Geschwister sind gesund. Welche Aussagen bezüglich der Genetik sind richtig?(2 richtige Antworten)
#card #neurology #anki
Es handelt sich wahrscheinlich um eine rezessive Form des M. Parkinson.Rezessive Mutationen im Parkingen kommen als mögliche Ursache in Frage.

# eurologischen Status eines Patienten mit 
### Im neurologischen Status eines Patienten mit Verdacht auf Morbus Alzheimer sollen die frontalen Zeichen geprüft werden. Dazu gehören:(3 richtige Antworten)
#card #neurology #anki
der Glabella-Reflexder Palmo-Mental-Reflexder Schnauz-Versuch

# 35-jähriger Patient Ihrer Epilepsieambula
### Ein 35-jähriger Patient Ihrer Epilepsieambulanz leidet seit 4 Jahren an Temporallappenanfällen. Medikamentös habe der Patient zuerst Lamotrigin (in therapeutischer Dosis), danach Levetiracetam (in therapeutischer Dosis) erhalten. Aufgrund unveränderter Anfallsfrequenz erhält der Patient derzeit eine Kombinationstherapie mit Levetiracetam und Carbamazepin, worunter sich die Anfallshäufigkeit ebenfalls nur geringfügig verbessert hat. Wie gehen Sie weiter vor?
#card #neurology #anki
Evaluation der Möglichkeit eines Epilepsie-chirurgischen Eingriffs

# einer 32-jährigen Patientin ihrer Epileps
### Bei einer 32-jährigen Patientin ihrer Epilepsieambulanz haben Sie aufgrund der klinischen Anfallssemiologie die Verdachtsdiagnose einer mesialen Temporallappenepilepsie gestellt. Welche pathologische Veränderung im MRT des Gehirns stellt die häufigste Ursache für eine mesiale Temporallappenepilepsie dar?
#card #neurology #anki
Hippocampussklerose

# 73-jähriger Patient wird nach einem leich
### Ein 73-jähriger Patient wird nach einem leichten Schädelhirntrauma zur Beobachtung aufgenommen. Am Tag nach der Aufnahme bestehen ein starker Tremor und eine ausgeprägte motorische Unruhe. Der Patient erscheint ängstlich und schreckhaft sowie agitiert. Er ist örtlich und zeitlich desorientiert, auffällig auch ein starkes Schwitzen sowie Tachykardie. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Delir

# 30-jähriger Mann sucht die Notfallaufnahm
### Ein 30-jähriger Mann sucht die Notfallaufnahme auf, weil es vor 2 Stunden im Rahmen sexueller Aktivität zu plötzlichen, heftigen, occipito-nuchal beginnenden, dann holokranen Kopfschmerzen kam. Diese halten noch immer an, wenn auch in geringerer Intensität. Der neurologische Status ist unauffällig, der Blutdruck und der Blutbefund sind unauffällig.Welche lebensbedrohende Diagnose ist in der weiteren Abklärung unbedingt auszuschließen?
#card #neurology #anki
Subarachnoidalblutung

# 27-jährige Patientin leidet seit mehrere
### Eine 27-jährige Patientin leidet seit mehreren Wochen unter wechselnden Doppelbildern, einer Ptose rechts sowie einer Schwäche der Kau- und Sprechmuskulatur. Bei der Patientin wurde die Myasthenia gravis elektrophysiologisch und durch Antikörperbestimmung gesichert. Welche Therapieoptionen gibt es? (3 richtige Antworten)
#card #neurology #anki
AcetylcholinesterasehemmerGlucokorticoidgabeThymektomie

# he neurologischen Zeichen sind bei zunehm
### Welche neurologischen Zeichen sind bei zunehmendem Hirndruck zu erwarten? (3 richtige Antworten) (7)
#card #neurology #anki
BewusstseinseintrübungBeidseits positive PyramidenzeichenErbrechen
Zeichen bei erhöhtem Hirndruck: Bewusstseinseintrübung (Sopor), bilaterale spastische Tonuserhöhung und positive Pyramidenzeichen, Bradykardie 2013-07 etc.
Notfalltertial: Zeichen des erhöhten Hirndrucks: Anisokorie, Erbrechen, Krampfanfall xx

# Patient mit einer Parese im Bereich der r
### Ein Patient mit einer Parese im Bereich der rechten oberen Extremität erklärt auf Ihre Anamnesefragen, dass er eine Gefühlsstörung in der rechten Hand, aber keinen Schmerz in HWS oder Schmerzausstrahlung in die rechte Hand verspüre. Was schließen Sie aus dieser Angabe?
#card #neurology #anki
Eine radikuläre Läsion ist ausgeschlossen.

# einem älteren Patienten mit einer axonale
### Bei einem älteren Patienten mit einer axonalen Polyneuropathie finden Sie im EMG des M. tibialis anterior folgende neurogene Veränderungen:(3 richtige Antworten)
#card #neurology #anki
Potentiale motorischer Einheiten mit verlängerten und erhöhten PotentialenEin hohes gelichtetes InterferenzmusterGelegentlich Faszikulationen

# einem 28-jährigen Patienten mit spinalem 
### Bei einem 28-jährigen Patienten mit spinalem Tumor findet sich im neurologischen Status vom Nabel abwärts eine Herabsetzung der oberflächlichen Berührungsempfindung. Vibrationsempfindung und des Lagesinns. Aus neuroanatomischer Sicht spricht diese Befundkonstellation für ein Betroffensein des/der:
#card #neurology #anki
Hinterstrangbahn

# 58-jähriger Patient hat Ihnen über eine U
### Ein 58-jähriger Patient hat Ihnen über eine Ungeschicklichkeit der linken Hand nach dem Erwachen berichtet. Es sei auch zu einer kurzzeitigen Schwäche im Bereich des linken Beines gekommen. Was fragen Sie, um auf das Vorliegen einer kompletten Halbseitensymptomatik schließen zu können?
#card #neurology #anki
Ist eine Funktionsstörung im Bereich des Gesichts aufgefallen?

# einem Patienten mit schweren choreatische
### Bei einem Patienten mit schweren choreatischen Bewegungsstörungen wurde genetisch ein Morbus Huntington gesichert. Sein gesunder Sohn wendet sich an Sie mit der Bitte um eine genetische Beratung. Welche der folgenden Aussagen bezüglich der Genetik über M. Huntington ist richtig? (4)
#card #neurology #anki
M. Huntington zeigt typischerweise das Phänomen der Antizipation. Die Symptome von Mutationsträgern werden im Laufe der Generationen schwerer und treten tendenziell früher auf.
Informationen über M. Huntington: 2020-02 Psychiatrische Symptome zeigen sich häufig früh im Erkrankungsverlauf. Die Erkrankung ist immer durch Mutationen im gleichen Gen charakterisiert.Die Wahrscheinlichkeit, die Erkrankung weiter zu vererben, liegt bei 50%.

# 35-jährige Patientin mit bekannter schub
### Eine 35-jährige Patientin mit bekannter schubförmiger Multipler Sklerose und einer Schub-frequenz von 2 pro Jahr in den letzten 4 Jahren kommt mit einer neu aufgetretenen hochgradigen spastischen Paraparese zur Aufnahme. Die akut durchgeführte spinale MRT Untersuchung zeigt einen großen kontrastmittelaufnehmenden Herd in Höhe Th5. Worin besteht die akute Therapie?
#card #neurology #anki
Methylprednisolon

# der Demenz vom Alzheimertyp finden sich i
### Bei der Demenz vom Alzheimertyp finden sich in den Gehirnen pathologische Ablagerungen. Es handelt sich dabei um:
#card #neurology #anki
Beta-Amyloid

# 56-jähringer Mann bemerkt im Bereich der 
### Ein 56-jähringer Mann bemerkt im Bereich der linken Hand ein Zittern, das nur in Ruhe auftritt. 2 Monate nach Beginn der Symptomatik wird er zur neurologischen Untersuchung zugewiesen. Es besteht die Verdachtsdiagnose eines Parkinson-Syndroms. Neben der klinischen Untersuchung, die einen geringen Rigor und eine Hypokinese rechts zeigt, findet sich:
#card #neurology #anki
im ß-CIT Spect eine Reduktion der dopaminergen Neurone

# Patient, der möglicherweise intravenös ly
### Der Patient, der möglicherweise intravenös lysiert werden soll, ist 70 Jahre alt und hat seit 2 Stunden 30 Minuten eine Hemiplegie rechts und eine motorische Aphasie, das CT zeigt eine Hypodensität im gesamten Bereich des Mediastromgebiets links, die Laborwerte sind alle im Normbereich, der Blutdruck 120/70 mmHg. Sie treffen folgende Entscheidung:
#card #neurology #anki
Keine Lyse bei vorliegender Kontraindikation

# 32-jährige Patientin mit Migräne bericht
### Eine 32-jährige Patientin mit Migräne berichtet über eine Zunahme der Attackenfrequenz auf 1 bis 2 Mal pro Woche. Welche Möglichkeiten zur Migräneprophylaxe haben Sie?(3 richtige Antworten)
#card #neurology #anki
ß-BlockerKalziumantagonistenValproinsäure

# werden als Notarzt/-ärztin zu einer 45-jä
### Sie werden als Notarzt/-ärztin zu einer 45-jährigen Patientin gerufen, bei der im Sitzen plötzlich ein heftiger Drehschwindel aufgetreten ist. Die Patientin erbricht und hat einen deutlichen Spontannystagmus. Welche Erkrankungen schließen Sie differentialdiagnostisch ein? (3 richtige Antworten)
#card #neurology #anki
Morbus MenierePeripherer VestibularisausfallWallenberg-Syndrom

# 46-jähriger Patient stellt sich mit einer
### Ein 46-jähriger Patient stellt sich mit einer radikulären Läsion C7 rechts vor. Sie finden die typische Schwäche des Kennmuskels und den radikulären Sensibilitätsausfall. Was untersuchen Sie, um eine Läsion der ,,langen Bahnen auszuschließen? (3 richtige Antworten)
#card #neurology #anki
Sehnenreflexe an der unteren ExtremitätSensibilität an den unteren ExtremitätenBabinsky Phänomen

# 42-jähriger Patient erleidet bei einem Ra
### Ein 42-jähriger Patient erleidet bei einem Radunfall ein schweres Schädelhirntrauma. Im CT zeigen sich große Kontusionsherde frontal rechts und parietazentral links jeweils mit perifokalem Ödem. Wie schätzen Sie die Prognose des Patienten ein?
#card #neurology #anki
Teilremission mit Sprachstörung und Hemiparese

# 21-jährige bis dato gesunde Patientin ko
### Eine 21-jährige bis dato gesunde Patientin kommt mit 40°C Fieber und Nackensteife in die Notfallaufnahme. Welche Untersuchungsmethode ist im Anschluss an eine CT-Untersuchung geeignet den Verdacht einer Meningitis zu bestätigen/auszuschließen?
#card #neurology #anki
Lumbalpunktion

# 45-jährige Patientin klagt nach einem ga
### Eine 45-jährige Patientin klagt nach einem gastrointestinalen Infekt über seit 2 Tagen langsam aufsteigende Parästhesien an allen 4 Extremitäten. Welche Differentialdiagnose ist am wahrscheinlichsten?
#card #neurology #anki
Guillain-Barre Syndrom

# 72-jähriger Patient wird mit Verdacht auf
### Ein 72-jähriger Patient wird mit Verdacht auf zerebralen Insult aufgenommen. Im Status zeigt sich eine halbseitige Parese rechts. Welche Befunde im Status beweisen Ihnen das Vorliegen einer zentralen Läsion?(3 richtige Antworten)
#card #neurology #anki
Spastische Tonuserhöhung an re OE und re UEGesteigerte Sehnenreflexe an re OE und re UE im SeitenvergleichPositiver Babinski rechts

# 32-jähriger Patient mit Multipler Skleros
### Ein 32-jähriger Patient mit Multipler Sklerose mit Anamnesebeginn vor 8 Jahren und insgesamt mehr als 10 klinischen Schüben wird aus dem Sommerurlaub in die Notfallaufnahme gebracht. Die Patientin zeigte seit gestern eine zunehmende Hemiparese links, dazu Ataxie und eine Bewusstseinseintrübung mit Somnolenz. Die Patientin war in den letzten 1,5 Jahren unter einer Natalizumab-Therapie schubfrei. Welche Ursache dieser klinische Verschlechterung sind zu erwägen? (3 richtige Antworten)
#card #neurology #anki
Erneuter MS-SchubInfekt mit FieberAuftreten einer progressiven multifokalen Leukencephalopathie (PML)

# 67-jähriger Patient wird Ihnen mit der Di
### Ein 67-jähriger Patient wird Ihnen mit der Diagnose Verdacht auf Parkinsonsyndrom zugewiesen. Der Patient berichtet über eine subjektive Verlangsamung der Bewegungen und Ungeschicklichkeit der Feinmotorik auf der rechten Seite. Nach welchen Zeichen suchen Sie um das inzipiente Parkinsonsyndrom im neurologischen Status festzustellen? (3 richtige Antworten) (13)
#card #neurology #anki
Mangelndes Mitbewegen der rechten OE beim GehenGeringer Rigor der rechten OE bei kontralateraler AktivierungVerlangsamung der komplexen Bewegungen (360° Drehung auf der Liege)

idiopathisches Parkinsonsyndrom - Auffälligkeiten in neurolog. Untersuchung: zähen, geschwindigkeits- und winkelunabhängigen Widerstand in der Tonusprüfung; asymmetrische Tonuserhöhung 2020-07
Befunde eines idiopathischen Parkinsonsyndroms: asymmetrischer Beginn; Fehlen sehr früher autonomer Störungen 2018-06
klinisch-neurologischen Befunde eines idiopathischen Parkinson-Syndroms: posturale Instabilität; Bradykinese oder Akinese; Tremor 2018-09
klinisch-neurologischen Befunde eines Morbus Parkinson: rigid-akinetisches (hypokinetisches) Syndrom; Pillendreher-Tremor 2018-11Zeichen in der neurologischen Untersuchung für Parkinsonsyndrom: rigide Tonuserhöhung rechts; mangelndes Mitbewegen der rechten oberen Extremität beim Gehen; angedeutetes Maskengesicht 2021-11

# 43-jährige Patientin, die viel manuell a
### Eine 43-jährige Patientin, die viel manuell arbeitet, hat seit über einem Jahr Kribbelparästhesien im Daumen und Zeigefinger rechts nach manuellen Tätigkeiten, nachts Schmerzen im Handgelenk rechts, in Arm und Schulter ziehend. Sie finden keine Parese und keine Muskelatrophie. Welche Befunde sind in der Neurografie zu erwarten? (2 richtige Antworten)
#card #neurology #anki
Verlangsamte sensible Nervenleitgeschwindigkeit im N. medianusVerlängerte distal motorische Latenz im N. medianus

# 26-jähriger Patient gibt an, dass er 2–4 
### Ein 26-jähriger Patient gibt an, dass er 2–4 Mal pro Monat an Kopfschmerzen leidet. Diese sind bilateral, drückend, von mittlerer Intensität, manchmal begleitet von geringer Übelkeit, jedoch sonst ohne Begleitsymptome. Die Attacken dauern ohne Medikation bis zu 10 Stunden an. Welche Therapie ist erste Wahl zur Behandlung der Attacken?
#card #neurology #anki
Nicht-steroidales Antirheumatikum

# he Untersuchung ist die hilfreichste zur 
### Welche Untersuchung ist die hilfreichste zur differentialdiagnostischen Abgrenzung zwischen einer Läsion des Plexus brachialis und einer radikulären Läsion?
#card #neurology #anki
Elektromyografie

# 40-jährige Frau entwickelte etwa 8 Tage 
### Eine 40-jährige Frau entwickelte etwa 8 Tage nach einem gastrointestinalen Infekt symmetrische, distal betonte Paresen, zuerst an den Beinen, dann an den Armen und distal betonte Parästhesien an Armen und Beinen. In der neurologischen Untersuchung finden sie zusätzlich zur schlaffen Parese fehlende Muskeleigenreflexe. Welche Zusatzuntersuchungen sind akut in die Wege zu leiten? (2 richtige Antworten)
#card #neurology #anki
LumbalpunktionElektroneurografie

# einem 75-jährigen Patienten mit rigid-aki
### Bei einem 75-jährigen Patienten mit rigid-akinetischem Parkinsonsyndrom und L-Dopa Therapie seit 7 Jahren kommt es zu einem Wirkverlust der medikamentösen Therapie. Diese zeigen sich in motorischen Fluktuationen und On-Off Schwankungen. Welches sind weitere mögliche Komplikationen der L-Dopa Langzeittherapie? (2 richtige Antworten)
#card #neurology #anki
DystonienPsychose

# 62-jähriger Mann stellt sich mit seiner F
### Ein 62-jähriger Mann stellt sich mit seiner Frau vor. Die Frau beklagt, dass sich die Persönlichkeit ihres Mannes zuletzt verändert hätte. Er würde neuerdings in der Öffentlichkeit peinliche Dinge tun, wie zum Beispiel Menschen etwas Unangenehmes ins Gesicht zu sagen oder beim Besuch von Lokalen spontan zum Singen von Liedern zu beginnen. Weiters hätte sich sein Gedächtnis verschlechtert. Was ist Ihre Verdachtsdiagnose?
#card #neurology #anki
Frontotemporale Demenz

# 30-jähriger Mann wird nach einem ersten g
### Ein 30-jähriger Mann wird nach einem ersten generalisierten tonisch-klonischen Anfall mit lateralem Zungenbiss zu Ihnen in die Notaufnahme gebracht. Im neurologischen Status zeigt er keine Auffälligkeiten. Eine strukturelle Abklärung mittels MRT des Gehirns sowie ein EEG ergeben ebenfalls keine Auffälligkeiten. Anamnestisch berichtet der Mann, dass er die letzten Tage sehr wenig und zuletzt gar nicht geschlafen habe und außerdem im Rahmen einer Geburtstagsfeier größere Mengen an Alkohol konsumiert hätte. Wie lautet Ihre Verdachtsdiagnose?
#card #neurology #anki
Gelegenheitsanfall
antiepileptische Medikation: keine 2018-09

# 43-jähriger Patient wird nach einem Front
### Ein 43-jähriger Patient wird nach einem Frontalzusammenstoß bewusstlos aus dem Auto geborgen. Der Notarzt entschließt sich, einem Hirnödem entgegenzuwirken. Dazu
#card #neurology #anki
intubiert und beatmet er den Patienten.

# sind Indikatoren in Zusatzuntersuchungen,
### Was sind Indikatoren in Zusatzuntersuchungen, die an das Vorhandensein oder an die Entwicklung einer Alzheimer Demenz denken lassen? (2 richtige Antworten)
#card #neurology #anki
Hippokampusatrophie im Schädel-MRTEin reduzierter Glucosestoffwechsel im FDG-PET des Gehirns
Diagnose Alzheimer Demenz gestützt durch: gering erweiterte innere und äußere Liquorräume im CT; Minderperfusion temporo-parietal beidseits im SPECT 2009-09 etc.

# 27-jährige Patientin leidet seit mehrere
### Eine 27-jährige Patientin leidet seit mehreren Wochen unter wechselnden Doppelbildern, einer Ptose rechts sowie einer Schwäche der Kau- und Sprechmuskulatur. Sie stellen die Verdachtsdiagnose einer Myasthenia gravis. Welche diagnostischen Maßnahmen ergreifen Sie, um Ihren Verdacht zu prüfen?(3 richtige Antworten)
#card #neurology #anki
Repetitive NervenstimulationAntikörper gegen AcetylcholinrezeptorenTensilontest

# 38-jähriger Patient wird nach einer Kopfs
### Ein 38-jähriger Patient wird nach einer Kopfschmerzattacke an Ihrer Ambulanz vorgestellt. Anamnestisch habe es sich um einen donnerschlagartigen Kopfschmerz „wie noch nie“ gehandelt. Derzeit bestünden weiterhin leichte Kopfschmerzen, ansonsten sei der Patient aber beschwerdefrei. Welche der folgenden Untersuchungen setzen sie in der Folge ein, um die vermutete Diagnose einer Subarachnoidalblutung zu bestätigen oder auszuschließen? (3 richtige Antworten)
#card #neurology #anki
CCT/CT-AngiographieDigitale SubtraktionsangiographieLumbalpunktion

# 51-jähriger Patient stellt sich bei Ihnen
### Ein 51-jähriger Patient stellt sich bei Ihnen vor, nachdem es seit einem Jahr zu einer zunehmenden Gangstörung gekommen sei. Des Weiteren würden dem Patienten „Muskelzuckungen“ an der Hand und am Oberschenkel auffallen. Im neurologischen Status fallen Ihnen deutlich gesteigert auslösbare Muskeleigenreflexe auf. Im EMG finden Sie ein neurogenes Schädigungsmuster mit pathologischer Spontanaktivität in der Zunge, der paravertebralen Muskulatur, einem Arm- und einem Beinmuskel. Welches ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Amyotrophe Lateralsklerose

# 50-jähriger Mann hat seit sechs Monaten e
### Ein 50-jähriger Mann hat seit sechs Monaten eine sich seither verschlechternde motorische Schwäche in Armen und Beinen. Im neurologischen Status finden Sie eine distal betonte, schlaffe Tetraparese, fehlende Muskeleigenreflexe und ein vermindertes Vibrationsempfinden. In der elektroneurografischen Untersuchung ist die maximal motorische Nervenleitgeschwindigkeit in drei Nerven verlangsamt, es findet sich ein Leitungsblock in einem Nerven. An welcher Erkrankung leidet der Patient mit hoher Wahrscheinlichkeit?
#card #neurology #anki
Chronisch inflammatorische demyelinisierende Polyneuropathie

# he spezielle neurochirurgische Operations
### Welche spezielle neurochirurgische Operationstechnik kann zur intraoperativen Testung der Sprachfunktion bei resektiven epilepsiechirurgischen Eingriffen angewendet werden?
#card #neurology #anki
Wachoperation

# 45-jähriger Mann hat seit 2 Jahren Kopfsc
### Ein 45-jähriger Mann hat seit 2 Jahren Kopfschmerzattacken, die an 2-7 Tagen pro Woche auftreten, dann jeweils 1-3 Mal pro Tag. Es gab bisher keine schmerzfreien Intervalle, die länger als 3 Wochen andauerten. Die Schmerzen sind immer links orbito-temporal, unerträglich stark, begleitet von einer ipsilateralen Augenrötung und dauern ohne Akuttherapie etwa 2 Stunden an. Welche langfristige Prophylaxe ist dem Patienten zu empfehlen?
#card #neurology #anki
Verapamil

# rch ist eine demyelinisierende Polyneurop
### Wodurch ist eine demyelinisierende Polyneuropathie gekennzeichnet? (3 richtige Antworten)
#card #neurology #anki
Verlängerte minimale F-Wellen Latenz in der NeurografieVerlangsamte motorische Nervenleitgeschwindigkeit in der NeurografieVerlängerte distal motorische Latenz in der Neurografie

# sind derzeit zugelassene Wirkstoffe in de
### Was sind derzeit zugelassene Wirkstoffe in der Behandlung einer Demenz? (2 richtige Antworten)
#card #neurology #anki
RivastigminMemantine

# 25-jährige Patientin hat seit der Jugend
### Eine 25-jährige Patientin hat seit der Jugend Migräne ohne Aura. Seit mehr als einem Jahr hat sie 5–6 Attacken pro Monat, die trotz der Einnahme von Akutmedikamenten 5–20 Stunden andauern können. Sie hat ansonsten keine Vorerkrankungen und nimmt keine anderen Medikamente. Nennen sie die medikamentösen Prophylaxen der ersten Wahl. (3 richtige Antworten)
#card #neurology #anki
MetoprololTopiramatFlunarizin

# 35-jähriger Patient Ihrer Epilepsieambula
### Ein 35-jähriger Patient Ihrer Epilepsieambulanz schildert seine Anfälle folgendermaßen: Typischerweise kündige sich ein Anfall durch ein leichtes, aus der Magengegend aufsteigendes Übelkeitsgefühl an; gleichzeitig mit der dann folgenden verminderten Reaktion auf Ansprache käme es zum Auftreten von oro-alimentären Automatismen, einer Dystonie der rechten oberen Extremität sowie Vokalisationen. Danach sei der Patient für einige Minuten desorientiert. Was ist die wahrscheinlichste Anfallsklassifikation?
#card #neurology #anki
Temporallappenanfall

# 45-jährige Patientin wurde nach aneurysm
### Eine 45-jährige Patientin wurde nach aneurysmatischer Subarachnoidalblutung aus einem Aneurysma der Arteria cerebri media im Stadium Hunt &amp; Hess I vor 8 Tagen mittels Coil-Embolisation neurochirurgisch versorgt. Sie wird seither an der neurochirurgischen Intensivstation betreut, ist wach, ansprechbar und zeigt im klinisch neurologischen Status kein sensomotorisches oder sprachliches Defizit. Plötzlich werden Sie ans Patientenbett gerufen, weil die Patientin mit einer gemischten Aphasie und einer Hemiparese rechts auffällig wird. Sie vermuten als Ursache einen Vasospasmus der linken Arteria cerebri media. Welche diagnostischen Mittel können Sie einsetzen, um Ihren Verdacht zu erhärten? (3 richtige Antworten)
#card #neurology #anki
CT-AngiographieDigitale SubtraktionsangiographieTranscranielle Dopplersonographie

# 38-jähriger Patient wird nach einer Kopfs
### Ein 38-jähriger Patient wird nach einer Kopfschmerzattacke an Ihrer Ambulanz vorgestellt. Anamnestisch habe es sich um einen donnerschlagartigen Kopfschmerz „wie noch nie“ gehandelt. Derzeit bestünden weiterhin leichte Kopfschmerzen, ansonsten sei der Patient aber beschwerdefrei. Welche der folgenden Differentialdiagnosen wird durch donnerschlagartigen Kopfschmerz charakterisiert?
#card #neurology #anki
Subarachnoidalblutung

# 64-jährige Patientin hat seit 3 Wochen S
### Eine 64-jährige Patientin hat seit 3 Wochen Schmerzen in der proximalen Extremitätenmuskulatur, eine proximale Schwäche an Armen und Beinen und druckschmerzhafte Muskeln. Im Blutbefund sind die Kreatinkinase, die Transaminasen und die Aldolase erhöht und die Blutsenkungsgeschwindigkeit ist deutlich beschleunigt. Welche Befunde finden Sie höchstwahrscheinlich in der Elektromyografie? (2 richtige Antworten)
#card #neurology #anki
Pathologische SpontanaktivitätMyogene Potentiale

# 65-jährige Patientin hat seit 8 Wochen D
### Eine 65-jährige Patientin hat seit 8 Wochen Doppelbilder in alle Blickrichtungen, eine rechtsbetonte Ptose und Schluckbeschwerden. Die Symptome nehmen im Lauf des Tages zu, und sie bemerkt auch eine rasche Ermüdbarkeit in den Extremitätenmuskeln. Im neurologischen Status finden Sie neben diesen Symptomen keine Auffälligkeiten. Welche Untersuchungen müssen Sie zur differentialdiagnostischen Abklärung als erste veranlassen? (2 richtige Antworten)
#card #neurology #anki
Repetitive NervenstimulationTensilon Test

# 45-jährige Patientin hat seit der Jugend
### Eine 45-jährige Patientin hat seit der Jugend Kopfschmerzen, initial 3-4 Mal pro Monat, seit ca. 2 Jahren an 15-20 Tagen pro Monat. Die Schmerzen sind stets bilateral, drückend, von leichter bis mittlerer Intensität und ohne vegetative Begleitsymptome. Sie dauern 3-10 Stunden an. Die Patientin nimmt maximal 2 Mal pro Woche ein Schmerzmittel. Welche Prophylaxe ist die erste Wahl?
#card #neurology #anki
Amitriptylin

# 20-jährige Patientin kommt mit seit 14 T
### Eine 20-jährige Patientin kommt mit seit 14 Tagen zunehmenden Kopfschmerzen in die Notfallambulanz. Bei der Patientin kam es bisher nie zu derartigen Kopfschmerzen, der mitgekommenen Angehörigen ist auch eine Wesensveränderung aufgefallen. An Vorerkrankungen ist ein Zustand nach tiefer Beinvenenthrombose vor 2 Jahren bekannt. Die Patientin raucht und nimmt die Pille. Was sind Ihre nächsten diagnostischen Schritte? (2 richtige Antworten)
#card #neurology #anki
Akutlabor mit D-DimerZerebrale Bildgebung (CCT oder MRT) mit venöser Angiographie

# 19-jährige Patientin mit juveniler Absen
### Eine 19-jährige Patientin mit juveniler Absence-Epilepsie und gelegentlichem Auftreten von generalisierten tonisch-klonischen Anfällen kommt zu Ihnen in die Epilepsieambulanz, um sich vor einer möglichen Schwangerschaft beraten zu lassen. Welche Maßnahmen sind idealerweise vor beziehungsweise während einer Schwangerschaft durchzuführen? (3 richtige Antworten)
#card #neurology #anki
Gabe von FolsäureUmstellung auf Retardpräparate, wenn die Möglichkeit bestehtUmstellung auf eine Monotherapie mit einem Antiepileptikum

# Demenz gehört zu den häufigsten Gesundhei
### Die Demenz gehört zu den häufigsten Gesundheitsproblemen im höheren Lebensalter. Für die nächsten Jahre wird mit einem deutlichen Zuwachs an DemenzpatientInnen gerechnet.Welche der Aussagen ist zutreffend?
#card #neurology #anki
Die Prävalenz liegt bei  65-jährigen bei ca. 1% und bei  85-jährigen bei ca. 30%.

# 61-jähriger Patient wird mit Halluzinatio
### Ein 61-jähriger Patient wird mit Halluzinationen von der Polizei an die Psychiatrie gebracht. Im Krankenhaus wird bei dem Patienten eine extrapyramidale Symptomatik festgestellt, welche sich nach einer Therapie mit Risperidon noch deutlich verschlechterte. Was ist Ihre Verdachtsdiagnose?
#card #neurology #anki
Lewy Body Demenz

# 78-jähriger Mann wird mit der Rettung um 
### Ein 78-jähriger Mann wird mit der Rettung um 8 Uhr in die Notfallambulanz gebracht. Seit dem Erwachen besteht eine Sprachstörung, eine Parese der rechten OE und eine Mundast-Schwäche rechts und sowie eine Sensibilitätsstörung der rechten OE. Der Patient gibt aber an, um 3 Uhr auf der Toilette gewesen zu sein und da noch keine Symptome bemerkt zu haben. Als Vortherapie nahm der Patient bisher nur ein Antihypertensivum. Im akut durchgeführten CCT zeigen sich keine sicheren Auffälligkeiten, in der CTA kein proximaler Gefäßverschluss. Welche Akut-Therapie schlagen Sie vor?
#card #neurology #anki
Acetylsalicylsäure (300mg sofort, danach 100mg/Tag)

# he Untersuchungen sind im Rahmen der prä-
### Welche Untersuchungen sind im Rahmen der prä-operativen Abklärung bei geplanter anteriorer Temporallappenresektion obligatorisch durchzuführen?(3 richtige Antworten)
#card #neurology #anki
MagnetresonanztomographieNeuropsychologische EvaluationProlongiertes Video-EEG-Monitoring

# he neuropsychologischen Screeningverfahre
### Welche neuropsychologischen Screeningverfahren finden häufig Anwendung in der Demenzdiagnostik? (2 richtige Antworten)
#card #neurology #anki
UhrentestMini Mental State Examination

# elchen Lebensaltern tritt die Neu-Diagnos
### In welchen Lebensaltern tritt die Neu-Diagnose einer Epilepsie am häufigsten auf?(2 richtige Antworten)
#card #neurology #anki
Im ersten LebensjahrIn höherem Alter (70 plus Jahre)

# 53-jährige Patientin mit Zeichen einer v
### Eine 53-jährige Patientin mit Zeichen einer vor 2 Stunden plötzlich aufgetretenen rechtsseitigen, beinbetonten Hemiparese und einer motorischen Aphasie wird in die Notaufnahme eingeliefert. Welches der folgenden bildgebenden Verfahren setzen Sie sinnvollerweise ein, um bei Verdacht auf einen akuten Schlaganfall möglichst rasch eine intrakranielle Blutung auszuschließen und eine intravenöse Lyse-Therapie einleiten zu können?
#card #neurology #anki
Kraniale Computertomographie

# 50-jähriger Mann mit bekannter Alkoholabh
### Ein 50-jähriger Mann mit bekannter Alkoholabhängigkeit klagt über distal betonte kribbelnde Missempfindungen und schmerzhafte Krämpfe in den Beinen. Im neurologischen Status finden Sie eine sockenförmige Hypästhesie an den Beinen, fehlende Muskeleigenreflexe, eine Atrophie der Wadenmuskulatur und eine diskrete distale Parese in den Beinen.In der Elektroneurografie sind die Summenpotentialamplituden erniedrigt. Was ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Axonale Polyneuropathie

# 45-jährige Patientin wurde nach aneurysm
### Eine 45-jährige Patientin wurde nach aneurysmatischer Subarachnoidalblutung aus einem Aneurysma der Arteria cerebri media im Stadium Hunt &amp; Hess I vor 8 Tagen mittels Coil-Embolisation neurochirurgisch versorgt. Sie wird seither an der neurochirurgischen Intensivstation betreut, ist wach, ansprechbar und zeigt im klinisch-neurologischen Status kein sensomotorisches oder Sprachdefizit. Plötzlich werden Sie ans Patientenbett gerufen, weil die Patientin mit einer gemischten Aphasie und einer Hemiparese rechts auffällig wird.Welche neurochirurgisch-intensivmedizinische Komplikation der aneurysmatischen Subarachnoidalblutung ist in dieser Phase am ehesten für die Symptomatik verantwortlich?
#card #neurology #anki
Vasospasmus

# he Behandlungsmöglichkeiten kommen bei in
### Welche Behandlungsmöglichkeiten kommen bei intracraniellen Aneurysmen zur Anwendung? (2 richtige Antworten)
#card #neurology #anki
Endovaskuläre Coil-EmbolisationMikrochirurgisches Clipping

# 25-jährige Frau wird deutlich psychomoto
### Eine 25-jährige Frau wird deutlich psychomotorisch verlangsamt in die Notaufnahme gebracht. Sie ist zu allen Qualitäten (zeitlich, örtlich, situativ und zur eigenen Person) desorientiert. Zeitweilig fällt sie durch aggressives Verhalten auf, dann wieder wirkt ihr Verhalten durchaus situationsgerecht. Insgesamt fluktuieren vor allem Aufmerksamkeit und Orientierung. Laut Ehemann bestünde dieser Zustand intermittierender Verwirrtheit seit dem Vorabend. An Vorerkrankungen ist bei der Patientin eine Temporallappenepilepsie bekannt. Ihre Medikamente (Lamotrigin) habe sie aber seit einigen Wochen nicht mehr eingenommen, da ihr Ehemann und sie gerne Kinder hätten. Was ist Ihre Verdachtsdiagnose?
#card #neurology #anki
Non-konvulsiver Status epilepticus

# 42-jähriger Patient wird mit einer seit d
### Ein 42-jähriger Patient wird mit einer seit dem Vortag bestehenden Hemiparese und einem Verwirrtheitszustand in die Notfallambulanz gebracht. Es bestanden im Vorfeld schon seit einigen Tagen subfebrile Temperatur, Kopfschmerzen und ein allgemeines Krankheitsgefühl. Vorbekannt ist eine Polytoxikomanie mit i.v. Opiatgebrauch. Im CCT und dann im MRT zeigen sich multiple, bilaterale ischämische Insulte. Welche diagnostischen Schritte streben Sie vorrangig an? (2 richtige Antworten)
#card #neurology #anki
Abnahme von 3 BlutkulturenTransösophageales Herzecho

# 75-jähriger Patient wird mit einem ischäm
### Ein 75-jähriger Patient wird mit einem ischämischen Infarkt in der dorsolateralen Medulla oblongata li (Wallenberg-Syndrom) stationär aufgenommen. Vorbekannt sind ein Diabetes mellitus und eine arterielle Hypertonie. Er klagt über eine Schluckstörung und Drehschwindel. Welche der unten angegebenen klinischen Symptome erwarten Sie zusätzlich? (3 richtige Antworten)
#card #neurology #anki
Hemiataxie ipsilateralHorner Syndrom ipsilatralDissoziierte Sensibilitätsstörung kontralateral

# 20-jährige Patientin kommt mit seit 14 T
### Eine 20-jährige Patientin kommt mit seit 14 Tagen zunehmenden Kopfschmerzen in die Notfallambulanz. Bei der Patientin kam es bisher nie zu derartigen Kopfschmerzen. Der mitgekommenen Angehörigen ist auch eine Wesensveränderung aufgefallen. An Vorerkrankungen ist ein Zustand nach tiefer Beinvenenthrombose vor 2 Jahren bekannt. Die Patientin raucht und nimmt die Pille. Bei der Patientin zeigt sich im MR eine Sinusvenenthrombose. Welche Akuttherapie schlagen Sie vor?
#card #neurology #anki
LMW-Heparintherapie

# welche möglichen postoperativen Komplika
### Über welche möglichen postoperativen Komplikationen müssen Patienten vor der Durchführung einer anterioren Temporallappenresektion aufgeklärt werden?(3 richtige Antworten)
#card #neurology #anki
GedächtnisstörungenGesichtsfelddefektPsychiatrische Komplikationen

# uch ist eine axonale Polyneuropathie geke
### Wodruch ist eine axonale Polyneuropathie gekennzeichnet? (3 richtige Antworten)
#card #neurology #anki
Erniedrigte Summenpotentialamplituden in der NeurografieGelichtetes Interferenzmuster in der MyografieErhöhte Amplituden der Muskelaktionspotentiale in der Myografie

# einem 53-jährigen Patienten hat sich seit
### Bei einem 53-jährigen Patienten hat sich seit einem Jahr eine zunehmende Gefühlsstörung an beiden unteren Extremitäten distal betont entwickelt. Seit mehreren Wochen bemerkt er auch eine Schwäche distal an beiden unteren Extremitäten. Bei der Untersuchung fällt auf, dass der Fersengang nicht möglich ist. Die ASR sind beidseits erloschen. Sie vermuten eine Polyneuropathie. Was sind Ihre diagnostischen Schritte?(3 richtige Antworten)
#card #neurology #anki
Nervenleitgeschwindigkeitsuntersuchung (Nervus peronaeus, suralis)Blutzucker und HbA1cVitamin B12 und Folsäurebestimmung

# 25-jährige Lehrerin wird von ihrem Ehema
### Eine 25-jährige Lehrerin wird von ihrem Ehemann wegen einer „unklaren Bewusstseins- störung“ in die Notfallaufnahme gebracht. Der Ehemann berichtet, dass seine Frau seit mehreren Monaten wiederholt plötzlich teilnahmslos „Löcher in die Luft“ starre und auf Ansprache nicht reagiere. Nach einigen Sekunden würde sie Kaubewegungen durchführen und (nicht vorhandene) Dinge vom Tisch wischen. Nach einigen Minuten wäre sie kontaktfähig, aber sehr müde und könne sich an das Ereignis nicht erinnern. Außer einem Kontrazeptivum nimmt die Patientin keine Medikamente. Der neurologische Status ist normal. Welche Medikation würden Sie empfehlen?
#card #neurology #anki
Monotherapie mit Levetiracetam

# he ist die häufigste fokale Epilepsie?
### Welche ist die häufigste fokale Epilepsie?
#card #neurology #anki
Temporallappenepilepsie

# 25-jährige Frau bemerkte während der Arb
### Eine 25-jährige Frau bemerkte während der Arbeit plötzlich ein Flimmern im Gesichtsfeld links, das sich langsam ausbreitete, anschließend traten Kribbelparästhesien an der linken Hand auf, die sich bis ins Gesicht ausbreiteten. Die Symptome dauerten insgesamt 30 Minuten an. Anschließend bestanden leichte Kopfschmerzen und geringe Übelkeit. Der neurologische Status ist unauffällig. Was ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Migräne mit Aura

# ist die häufigste neurodegenerative Demen
### Was ist die häufigste neurodegenerative Demenzform?
#card #neurology #anki
Alzheimer Demenz

# iner Liquoruntersuchung eines Patienten m
### In einer Liquoruntersuchung eines Patienten mit Demenz vom Alzheimertyp sind welche Biomarker besonders sensitiv für die Diagnose? (2 richtige Antworten)
#card #neurology #anki
Amyloid-BetaTau-Protein

# 37-jähriger Mann war wegen einer Sepsis i
### Ein 37-jähriger Mann war wegen einer Sepsis im Rahmen einer Pneumonie 3 Wochen auf der Intensivstation aufgenommen gewesen. Im Rahmen der Extubation und Beendigung der Sedierung fielen eine schlaffe Tetraparese und eine erschwerte Entwöhnung von der maschinellen Beatmung auf. Im neurologischen Status zeigten sich zudem fehlende Muskeleigenreflexe, aber keine sicheren sensiblen Defizite. In der Elektroneurografie fanden sich überwiegend Zeichen einer axonalen Läsion. Welche ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Critical illness Polyneuropathie

# elchem Zeitfenster kann eine intravenöse 
### In welchem Zeitfenster kann eine intravenöse Thrombolysetherapie mit rtPA bei ischämischem Insult verabreicht werden?
#card #neurology #anki
4,5 h

# Krankheitsbild eines sogenannten mild cog
### Das Krankheitsbild eines sogenannten mild cognitive impairment (leichte kognitive Störung) geht mit folgenden Charakteristika einher: (3 richtige Antworten) insg. 4
#card #neurology #anki
Subjektiv empfundene GedächtnisproblemeKognitive Beeinträchtigung wird in neuropsychologischen Tests objektiviert.Die Basis-Alltagsfunktionen sind ungestört. 2017-09 etc.
Die jährliche Konversionsrate zu einer Alzheimer Demenz beträgt etwa 10%. 2017-06 etc.

# hrer Ordination stellt sich eine 27-jähri
### In Ihrer Ordination stellt sich eine 27-jährige Patientin vor, die Sie seit 5 Jahren wegen einer kryptogenen Epilepsie mit komplex-fokalen und sekundär generalisierten Anfällen betreuen. Ein Jahr zuvor hatten Sie die Monotherapie (Valproinsäure) auf eine Kombinationstherapie (Valproinsäure und Lamotrigin) umgestellt. Die Anfallsfrequenz war danach deutlich rückläufig, der letzte Anfall liegt nunmehr 6 Monate zurück. Die Patientin berichtet Ihnen heute, dass sie schwanger werden möchte. Welche Empfehlungen geben Sie nun dieser Patientin? (2 richtige Antworten)
#card #neurology #anki
Prophylaktische Folsäure-Einnahme (5 mg/d)Umstellung auf eine Monotherapie mit einem Antiepileptikum mit geringem embryotoxischen und teratogenem Potential, z. B. Lamotrigin

# 33-jährige Patientin auf der Durchreise 
### Eine 33-jährige Patientin auf der Durchreise von Bangkok nach Belgrad kollabiert um 6 Uhr früh im Transitbereich des Flughafens Wien, kurz zuvor hatte sie über Doppelbilder geklagt. Bei Eintreffen des Notarztes ist sie tief komatös und tetraplegisch. Bei stabilen Vitalparametern wird sie schutzintubiert. Im Akut-MRT zeigt sich eine zerebrale Ischämie. Welches Gefäßterritorium ist betroffen?
#card #neurology #anki
A. basilaris

# he Grundvoraussetzungen müssen gegeben se
### Welche Grundvoraussetzungen müssen gegeben sein, um Patienten einen therapeutischen epilepsiechirurgischen Eingriff anbieten zu können? (2 richtige Antworten)
#card #neurology #anki
Vorliegen eines Epilepsiesyndroms, das durch diesen Eingriff prinzipiell behandelbar istVorliegen einer medikamentös therapierefraktären Epilepsie

# 72-jährige Patientin beklagt zuletzt ver
### Eine 72-jährige Patientin beklagt zuletzt vermehrt auftretende Probleme beim Erinnern von Gesprächsinhalten sowie dem Inhalt von im Fernsehen gesehenen Nachrichten. Einmalig wäre es nun zu einer Episode gekommen, in der sie in der Stadt Probleme hatte, den richtigen Weg zu finden, obwohl sie diesen schon einmal gegangen war. Was ist Ihre Verdachtsdiagnose?
#card #neurology #anki
Alzheimer Demenz

# he klinisch-neurologischen Untersuchungen
### Welche klinisch-neurologischen Untersuchungen sollten in der präoperativen Abklärung für einen epilepsiechirurgischen Eingriff unbedingt durchgeführt werden? (3 richtige Antworten)
#card #neurology #anki
MRT des SchädelsVideo-EEG-MonitoringNeuropsychologische Testung

# 45-jährige Patienten wird nach aneurysma
### Eine 45-jährige Patienten wird nach aneurysmatischer Subarachnoidalblutung vor etwa 10 Stunden wach und ansprechbar an der neurochirurgischen Intensivstation aufgenommen. Im klinisch-neurologischen Status zeigen sich keine sensomotorischen Defizite, Hunt &amp; Hess I entsprechend. Eine CCT sowie CTA Untersuchung der intracraniellen Gefäße war bereits im zutransferierenden Krankenhaus durchgeführt und ein Aneurysma der Arteria communicans anterior diagnostiziert worden. Dieses soll noch im Laufe des Tages behandelt werden. Nach einer Stunde wird die Patienten zunehmend somnolent und schließlich soporös. Welche Komplikation der Subarachnoidalblutung ist in dieser Phase am ehesten für die Beschwerden verantwortlich?
#card #neurology #anki
Hydrocephalus internus

# hes diagnostische Mittel ist beim Verdach
### Welches diagnostische Mittel ist beim Verdacht auf eine CT-negative Subarachnoidalblutung unbedingt zum Ausschluss einer SAB anzuwenden?
#card #neurology #anki
Lumbalpunktion

# he der folgenden hereditären neurologisch
### Welche der folgenden hereditären neurologischen Erkrankungen sind typischerweise mit einer Hohlfußbildung assoziiert? (2 richtige Antworten)
#card #neurology #anki
Charcot-Marie-Tooth-ErkrankungFriedreich-Ataxie

# stellen bei einer Patientin eine spastisc
### Sie stellen bei einer Patientin eine spastische Parese der rechten oberen und unteren Extremität, rechtsseitig gesteigerte Muskeleigenreflexe und einen positiven Babinski-Reflex rechts fest. Wo vermuten Sie die für diese Symptomatik verantwortliche Läsion?
#card #neurology #anki
Zentral links

# stellen bei einem Patienten an der rechte
### Sie stellen bei einem Patienten an der rechten oberen Extremität eine schlaffe Parese sowie abgeschwächte Muskeleigenreflexe fest. Die Parese besteht seit mindestens einer Woche.Wo vermuten Sie die Läsion am ehesten?
#card #neurology #anki
Zweites Motoneuron

# 65-jährige Patientin berichtet, seit ein
### Eine 65-jährige Patientin berichtet, seit einigen Monaten eine „Bamstigkeit“ und zunehmend auch brennende, manchmal auch elektrisierende Schmerzen und Missempfindungen in den Füßen zu verspüren. Sie habe unterschiedliche nichtsteroidale Analgetika probiert, aber die Schmerzen bestünden unverändert. Bei der Patientin ist seit einigen Jahren ein Typ 2 Diabetes bekannt. Wie lautet angesichts dieser Anamnese die wahrscheinlichste Differenzialdiagnose?
#card #neurology #anki
Polyneuropathie

# he Methodik erlaubt eine nichtinvasive Da
### Welche Methodik erlaubt eine nichtinvasive Darstellung von Hirnaktivitäten (keine Operation, keine Strahlenbelastung)?
#card #neurology #anki
Funktionelle Magnetresonanztomographie

# der Untersuchung eines Patienten stellen 
### Bei der Untersuchung eines Patienten stellen Sie einen ausgeprägten Neglect fest. In welchem Gehirnareal ist die für diese Symptomatik verantwortliche Läsion zu vermuten?
#card #neurology #anki
Parietallappen der nicht-dominanten Hemisphäre

# diagnostizieren bei einem Patienten eine 
### Sie diagnostizieren bei einem Patienten eine homonyme Hemianopsie nach rechts. In welchen der folgenden anatomischen Strukturen kann eine (diese Symptomatik verursachende) Läsion liegen? (3 richtige Antworten)
#card #neurology #anki
Tractus opticusRadiatio opticaVisueller Kortex

# he der folgenden Therapien kommen für die
### Welche der folgenden Therapien kommen für die Schub-Therapie bei der schubförmig verlaufenden Multiplen Sklerose infrage?(2 richtige Antworten)
#card #neurology #anki
KortisonPlasmaseparation

# kennzeichnende Neuropathologie der Alzhei
### Die kennzeichnende Neuropathologie der Alzheimer Demenz ist die progressive Akkumulation von welchen der folgenden Proteine bzw. Proteinfragmente? (2 richtige Antworten)
#card #neurology #anki
Beta-AmyloidTau-Protein

# einem 75-jährigen Patienten besteht seit 
### Bei einem 75-jährigen Patienten besteht seit 2 Stunden eine akut aufgetretene Halbseitensymptomatik rechts und eine Aphasie. CCT inklusive Angiographie sowie Akutlaborbefunde sind unauffällig. Der Patient ist aufgrund einer TIA vor 3 Jahren mit Acetylsalicylsäure 100 mg/Tag behandelt. Der NIHSS (National Institutes of Health Stroke Scale)-Score beträgt 5 Punkte. Welche der folgenden klinisch-therapeutischen Überlegungen sind korrekt? (2 richtige Antworten)
#card #neurology #anki
Der Patient befindet sich im Zeitfenster für eine intravenöse thrombolytische Therapie.Eine endovaskuläre Intervention ist nicht indiziert.

# he der folgenden Befunde sprechen für das
### Welche der folgenden Befunde sprechen für das Vorliegen eines idiopathischen Parkinsonsyndroms („Morbus Parkinson“)? (2 richtige Antworten) (13)
#card #neurology #anki
Asymmetrischer BeginnFehlen sehr früher autonomer Störungen

idiopathisches Parkinsonsyndrom - Auffälligkeiten in neurolog. Untersuchung: zähen, geschwindigkeits- und winkelunabhängigen Widerstand in der Tonusprüfung; asymmetrische Tonuserhöhung 2020-07
klinisch-neurologischen Befunde eines idiopathischen Parkinson-Syndroms: posturale Instabilität; Bradykinese oder Akinese; Tremor 2018-09
klinisch-neurologischen Befunde eines Morbus Parkinson: rigid-akinetisches (hypokinetisches) Syndrom; Pillendreher-Tremor 2018-11
Zeichen des inzipienten Parkinsonsyndroms im neurolog. Status: mangelndes Mitbewegen der rechten OE beim Gehen; geringer Rigor der rechten OE bei kontralateraler Aktivierung; Verlangsamung der komplexen Bewegungen (360° Drehung auf der Liege) 2013-09 etc.Zeichen in der neurologischen Untersuchung für Parkinsonsyndrom: rigide Tonuserhöhung rechts; mangelndes Mitbewegen der rechten oberen Extremität beim Gehen; angedeutetes Maskengesicht 2021-11

# einem 76-jährigen, somatisch sonst gesund
### Bei einem 76-jährigen, somatisch sonst gesunden Patienten sind in der neuropsychologischen Testung deutliche Defizite des episodischen Kurzzeitgedächtnisses objektivierbar. Dies deckt sich auch mit den außenanamnestischen Angaben. Der neurologische Status ist unauffällig. Verhaltensstörungen, psychotische Symptome oder Wesensveränderung bestehen nicht. An welches Demenzsyndrom ist am ehesten zu denken?
#card #neurology #anki
Demenz vom Alzheimer Typ

# elchem Areal des Gehirns entstehen fokale
### In welchem Areal des Gehirns entstehen fokale klonische Anfälle?
#card #neurology #anki
Primär motorischer Kortex des kontralateralen Gyrus präcentralis

# junger Mann wird vom Hausarzt mit dem Zuw
### Ein junger Mann wird vom Hausarzt mit dem Zuweisungsvermerk „hängender Mundwinkel links und inkompletter Lidschluss links“ an die neurologische Ambulanz überwiesen. Sie diagnostizieren eine periphere Facialis-Parese links. Was teilen Sie dem Patienten korrekter Weise mit?(3 richtige Antworten)
#card #neurology #anki
Ihre Symptome sind sehr wahrscheinlich nicht auf eine Gehirn-Erkrankung zurückzuführen.Wenn Ihre Blutwerte keine Entzündungszeichen aufweisen, werden wir Sie mit Kortison behandeln.Ich werde nun noch eine HNO- und augenärztliche Begutachtung veranlassen.

# er Ambulanz wird ein 60-jähriger Mann vor
### In der Ambulanz wird ein 60-jähriger Mann vorstellig. Er berichtet über ein seit 1 Jahr bestehendes „krampfartiges Verziehen“ seines Halses bzw. Kopfes zur rechten Seite. Diese Schiefhaltung ist durch eine „Geste antagoniste“ vorübergehend unterbrechbar. Bis auf die beschriebene Symptomatik ist der neurologische Status unauffällig. Welche weiteren diagnostischen Schritte sind sinnvoll?(2 richtige Antworten)
#card #neurology #anki
MRT-Untersuchung des SchädelsAusschluss eines Morbus Wilson

# 50-jährige Patientin klagt über unangene
### Eine 50-jährige Patientin klagt über unangenehme Gefühle in den Beinen, mit deutlicher Zunahme abends und nachts, sodass sie auch an Einschlafproblemen leide. Bewegung würde die Symptomatik bessern. Welche Erkrankung ist am ehesten zu vermuten?
#card #neurology #anki
Restless Legs

# he der folgenden Zusatzuntersuchungen ver
### Welche der folgenden Zusatzuntersuchungen veranlassen Sie bei einem Patienten, bei dem der hochgradige Verdacht auf eine Narkolepsie besteht?(2 richtige Antworten)
#card #neurology #anki
Multipler Schlaflatenz-TestStationäre Polysomnographie

# untersuchen einen 77-jährigen Patienten, 
### Sie untersuchen einen 77-jährigen Patienten, der mit dem „Verdacht auf eine Parkinson-Demenz“ zugewiesen wurde. In einem bereits vorliegenden CCT zeigen sich erweiterte innere Liquorräume. Im neurologischen Status zeigt sich ein verlangsamtes, „schlurfendes“ Gangbild bei sonst weitgehend unauffälligem Befund. Die Verwandten berichten, dass der Patient in den letzten Monaten starke Konzentrations- und Aufmerksamkeitsdefizite sowie eine Inkontinenz entwickelt habe. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Normaldruckhydrocephalus

# diagnostizieren bei einem Patienten eine 
### Sie diagnostizieren bei einem Patienten eine Acetylcholin-Rezeptor-positive Myasthenia gravis. Welche der folgenden Therapien kommen für diesen Patienten prinzipiell in Frage? (3 richtige Antworten)
#card #neurology #anki
Acetylcholin-Esterase-InhibitorenImmunsuppressivaThymektomie

# 68-jähriger Mann wird aufgrund einer seit
### Ein 68-jähriger Mann wird aufgrund einer seit Monaten progredienten schmerzlosen beidseitigen Schwäche der kleinen Handmuskulatur mit Störung der Feinmotorik vorstellig. In der Untersuchung fallen Ihnen eine Thenaratrophie und einzelne Muskelzuckungen auf. Es bestehen keine sensiblen Störungen. Ein MRT der HWS und eine Messung der Nervenleitgeschwindigkeit waren unauffällig. Was ist die wahrscheinlichste Differentialdiagnose?
#card #neurology #anki
Amyotrophe Lateralsklerose

# eurologischen Status einer 23-jährigen Pa
### Im neurologischen Status einer 23-jährigen Patientin stellen Sie eine spastische Parese der linken unteren Extremität, subklonisch gesteigerte Muskeleigenreflexe und positive Pyramidenbahnzeichen fest. Welche weiterführenden apparativen Untersuchungen veranlassen Sie? (2 richtige Antworten)
#card #neurology #anki
MRT des SchädelsMRT des zervikalen Myelons

# 78-jähriger Patient wurde vom Hausarzt zu
### Ein 78-jähriger Patient wurde vom Hausarzt zur MRT-Untersuchung des Schädels überwiesen, nachdem er über akut aufgetretene „Sehprobleme“ geklagt hatte. In der MRT zeigt sich eine subakute ischämische Läsion im Versorgungsgebiet der linken Arteria cerebri posterior. Welchen Befund erwarten Sie bei diesem Patienten im neurologischen Status?
#card #neurology #anki
Homonyme Hemianopsie nach rechts

# der neurologischen Untersuchung eines Pat
### Bei der neurologischen Untersuchung eines Patienten stellen Sie eine fehlende direkte und indirekte Lichtreaktion sowie eine fehlende Konvergenzreaktion der rechten Pupille fest. Eine Ptose oder Doppelbilder bestehen nicht, auch die Bulbusmotilität ist intakt. Welche Diagnose ist am wahrscheinlichsten?
#card #neurology #anki
Innere N. oculomotorius-Parese

# 40-jährige Patientin ohne relevante Vore
### Eine 40-jährige Patientin ohne relevante Vorerkrankungen berichtet, dass sie heute Vormittag für 10 Minuten „die Worte nicht herausbrachte, jedoch wusste, was sie sagen wollte“. Gleichzeitig kam es, ebenfalls für die Dauer von 10 Minuten, zu einer Sensibilitätsstörung der rechten Gesichtshälfte. Welche Differenzialdiagnosen kommen bei dieser Anamnese in Frage? (3 richtige Antworten)
#card #neurology #anki
Transitorische ischämische AttackeMigräne-AuraFokaler epileptischer Anfall

# he klinischen Befunde sind bei Patienten 
### Welche klinischen Befunde sind bei Patienten mit Läsionen des ersten Motoneurons zu erwarten? (2 richtige Antworten)
#card #neurology #anki
SpastikGesteigerte Muskeleigenreflexe

# untersuchen eine Patientin mit heftigen K
### Sie untersuchen eine Patientin mit heftigen Kopfschmerzen und hohem Fieber. Es besteht ein ausgeprägter Meningismus. Zusätzlich zeigt sich beim passiven Anheben des Kopfes gegen das Sternum eine reflektorische Beugung der Beine in den Hüft- und Kniegelenken. Wie wird dieses Phänomen bezeichnet?
#card #neurology #anki
Positives Brudzinski-Zeichen

# 72-jährige Patientin wird mit dem Zuweis
### Eine 72-jährige Patientin wird mit dem Zuweisungsvermerk „Verdacht auf Parkinson- Syndrom“ vom Hausarzt in die Ambulanz für Bewegungsstörungen überwiesen. Welche der folgenden klinisch-neurologischen Befunde sind mit der Diagnose eines idiopathischen Parkinson-Syndroms vereinbar? (3 richtige Antworten) (13)
#card #neurology #anki
Posturale InstabilitätBradykinese oder AkineseTremor

idiopathisches Parkinsonsyndrom - Auffälligkeiten in neurolog. Untersuchung: zähen, geschwindigkeits- und winkelunabhängigen Widerstand in der Tonusprüfung; asymmetrische Tonuserhöhung 2020-07
Befunde eines idiopathischen Parkinsonsyndroms: asymmetrischer Beginn; Fehlen sehr früher autonomer Störungen 2018-06
klinisch-neurologischen Befunde eines Morbus Parkinson: rigid-akinetisches (hypokinetisches) Syndrom; Pillendreher-Tremor 2018-11
Zeichen des inzipienten Parkinsonsyndroms im neurolog. Status: mangelndes Mitbewegen der rechten OE beim Gehen; geringer Rigor der rechten OE bei kontralateraler Aktivierung; Verlangsamung der komplexen Bewegungen (360° Drehung auf der Liege) 2013-09 etc.Zeichen in der neurologischen Untersuchung für Parkinsonsyndrom: rigide Tonuserhöhung rechts; mangelndes Mitbewegen der rechten oberen Extremität beim Gehen; angedeutetes Maskengesicht 2021-11

# 68-jährige Patientin wird aus einer Unfa
### Eine 68-jährige Patientin wird aus einer Unfallchirurgie zutransferiert, weil es seit den Morgenstunden zu einer Vigilanzminderung gekommen ist. Am Vortag war die Patientin gestürzt (Humerusfraktur, Monokelhämatom). Das initiale Akut-CCT war unauffällig. Die Patientin ist somnolent, ohne sichere Halbseitensymptomatik. Aufgrund eines Vorhofflimmerns ist die Patientin seit Jahren oral antikoaguliert. Welche diagnostische Akut-Maßnahme veranlassen Sie als Erstes?
#card #neurology #anki
Neuerliche CCT-Untersuchung

# he der folgenden Medikamente kommen in de
### Welche der folgenden Medikamente kommen in der Migräne-Prophylaxe zum Einsatz? (3 richtige Antworten)
#card #neurology #anki
BetablockerFlunarizinTopiramat

# he der folgenden Medikamente können bei h
### Welche der folgenden Medikamente können bei hochaktiven Verlaufsformen der schubförmigen Multiplen Sklerose eingesetzt werden? (2 richtige Antworten)
#card #neurology #anki
NatalizumabAlemtuzumab

# der 28-jährigen Frau V. kam es vor 2 Jahr
### Bei der 28-jährigen Frau V. kam es vor 2 Jahren zu einem „Verschwommen-Sehen“ am linken Auge (eine damalige augenärztliche Untersuchung war unauffällig), die nach ca. 2 Wochen spontan remittierte. Vor 3 Wochen kehrte dieselbe Symptomatik, nun am rechten Auge, wieder. In der nun veranlassten Schädel-MRT-Nativ-Untersuchung zeigen sich eine T2-Hyperintensität im rechten Sehnerv sowie zwei weitere Signalalterationen periventrikulär rechts und iuxtacortical links. Welche der folgenden Differenzialdiagnosen ziehen Sie in Betracht? (2 richtige Antworten)
#card #neurology #anki
Schubförmige Multiple SkleroseNeuromyelitis-optica-Spektrum Erkrankung

# Krankheitsbild einer Alzheimer Demenz geh
### Das Krankheitsbild einer Alzheimer Demenz geht mit folgenden Charakteristika einher: (3 richtige Antworten) (7)
#card #neurology #anki
Schleichend progredienter KrankheitsverlaufBeeinträchtigung des episodischen NeugedächtnissesKlare Bewusstseinslage
Symptome einer Alzheimer Demenz: Sprachstörung; apraktische Störung; Halluzinationen und Wahnbildungen 2013-07

# einem 79-jährigen, bislang rüstigen Patie
### Bei einem 79-jährigen, bislang rüstigen Patienten bestehen seit 2 Stunden eine akut aufgetretene Hemiparese rechts und eine Aphasie. Die kraniale MRT mit Angiographie zeigt einen proximalen Gefäßverschluss mit Perfusions-Diffusions-Mismatch im linken Mediastromgebiet. Aufgrund eines paroxysmalen Vorhofflimmerns (bei sonst unauffälliger Anamnese) ist der Patient mit einem Vitamin-K-Antagonisten therapeutisch antikoaguliert (INR: 2,3). Der NIHSS (National Institutes of Health Stroke Scale)-Score beträgt 10 Punkte. Welche therapeutischen überlegungen sind richtig? (2 richtige Antworten)
#card #neurology #anki
Eine intravenöse thrombolytische Therapie ist kontraindiziert. Es sollte eine endovaskuläre Thrombektomie erwogen werden.

# begutachten einen 55-jährigen Patienten m
### Sie begutachten einen 55-jährigen Patienten mit einem rasch progredienten dementiellen Syndrom. Welche der folgenden Zusatzbefunde sprechen für eine Prionenerkrankung? (3 richtige Antworten) (6)
#card #neurology #anki
MyoklonienTriphasische Wellen im EEGErhöhtes 14-3-3-Protein im Liquor
Charakteristika von Creutzfeldt-Jakob: Ataxie, Myoklonien und positive Pyramidenbahnzeichen; positiver Nachweis der 14-3-3-Proteine im Liquor; rasch progrediente dementielle Entwicklung 2019-06

# he Neoplasien des Zentralnervensystems si
### Welche Neoplasien des Zentralnervensystems sind durch ein infiltratives Wachstum gekennzeichnet? (3 richtige Antworten)
#card #neurology #anki
Primäres ZNS-LymphomAnaplastisches AstrozytomGlioblastom

# 39-jährige Patientin mit vorbekannter Mi
### Eine 39-jährige Patientin mit vorbekannter Migräne und chronischem Zervikalsyndrom kommt am Abend in die Notfallambulanz. Mittags sei es beim Joggen zum plötzlichen Auftreten eines donnerschlagsartigen Kopfschmerzes gekommen („so arg wie noch nie in meinem Leben“). Sie habe mehrfach erbrochen. Seither bestehen starke Kopfschmerzen und Übelkeit. Der neurologische Status ist unauffällig (die Meningismus-Prüfung ist bei bekanntem Zervikalsyndrom eingeschränkt beurteilbar). Nach Analgetika-Gabe geht es der Patientin gut. Routinelabor-Befunde und CCT sind unauffällig. Wie gehen Sie weiter vor?
#card #neurology #anki
Lumbalpunktion

# 33-jährige zuvor immer gesunde Frau beri
### Eine 33-jährige zuvor immer gesunde Frau berichtet über eine seit Wochen bestehende zunehmende allgemeine Müdigkeit und Erschöpfbarkeit. Seit einigen Tagen wären ihr auch Doppelbilder, eine Ptose und eine Schluckstörung aufgefallen. Im Armvorhalteversuch sinken beide Arme nach 10 Sekunden ab. Welche weiteren diagnostischen Schritte sind am ehesten zu veranlassen? (2 richtige Antworten)
#card #neurology #anki
Bestimmung der Acetylcholinrezeptor-AutoantikörperRepetitive Stimulation

# hrer Ordination wird eine 64-jährige Frau
### In Ihrer Ordination wird eine 64-jährige Frau vorstellig, die über ein seit ca. 2 Jahren bestehendes „Verziehen“ ihres Halses bzw. Kopfes zur linken Seite berichtet. Sie habe zunehmende Schmerzen in der Halswirbelsäule. Durch Berühren des Kinns mit der Hand könne sie diese Schiefhaltung vorübergehend unterbrechen. Im neurologischen Status findet sich bis auf die beschriebene Symptomatik keine weitere Auffälligkeit. Welche Differentialdiagnose ist die wahrscheinlichste?
#card #neurology #anki
Torticollis

# 60-jährige Patientin wird aufgrund einer
### Eine 60-jährige Patientin wird aufgrund einer progredienten, von distal nach proximal aufsteigenden Schwäche aller 4 Extremitäten vorstellig. Die akute Abklärung führt zur Diagnose eines Guillain-Barré-Syndroms. Welche Therapieoptionen mit gleichwertiger Wirksamkeit stehen Ihnen in der Akutphase zur Auswahl? (2 richtige Antworten)
#card #neurology #anki
PlasmaphereseImmunglobuline i.v.

# 45-jährige Frau berichtet über ein schle
### Eine 45-jährige Frau berichtet über ein schleichend beginnendes, unwillkürliches Zittern beider Hände, welches seit ca. 5 Jahren bestehe und sich allmählich verstärke. Mittlerweile habe sie beim Kochen und Handarbeiten Probleme, da das Zittern vor allem bei Aktivierung der Arme vorkomme. Sie vermuten einen essentiellen Tremor. Welche Fragen haben Sie der Patientin zu stellen, um diese Verdachtsdiagnose weiter zu erhärten? (2 richtige Antworten)
#card #neurology #anki
Bessert sich das Zittern nach Alkoholzufuhr?Zittert in der Blutsverwandtschaft außer Ihnen noch jemand?

# he der folgenden Medikamente kommen in de
### Welche der folgenden Medikamente kommen in der Behandlung des Restless-Legs-Syndroms zum Einsatz? (3 richtige Antworten) 
#card #neurology #anki
DopaminagonistenAlpha2-Delta-LigandenOpiate

# 28-jährige, bislang gesunde Patientin be
### Eine 28-jährige, bislang gesunde Patientin berichtet Folgendes: Bei längerem Föhnen oder Kämmen der Haare bemerke sie seit einiger Zeit eine „Schwere“ und „Schwäche“ der Arme. Abends habe sie oft Doppelbilder und herabhängende Lider und fühle sich kurzatmig. Ein Routinelabor war unauffällig, ebenso ein Schädel-MRT. Welche weiterführenden Untersuchungen sind bei dieser Anamnese sinnvoll? (3 richtige Antworten)
#card #neurology #anki
Thymus-MRT
Acetylcholin-Rezeptor-Autoantikörper-Bestimmung
Tensilon®-Test + repetitive Stimulation

# 51-jährige Frau T. wurde vor 2 Tagen nier
### Die 51-jährige Frau T. wurde vor 2 Tagen nierentransplantiert. Seit der Operation besteht eine mittelgradige Parese für die Hüftbeugung und Kniestreckung rechts. Der Patellarsehen-Reflex ist rechts abgeschwächt. Die Haut an der Vorderseite des rechten Ober- und Unterschenkels wird als hypästhetisch angegeben. Wie lautet die wahrscheinlichste Verdachtsdiagnose?
#card #neurology #anki
Parese des Nervus femoralis

# he der folgenden Symptome treten im Rahme
### Welche der folgenden Symptome treten im Rahmen einer Multiplen Sklerose häufig auf?
(3 richtige Antworten)
#card #neurology #anki
Neuritis N. optici
Fatigue
Urge- und Dranginkontinenz

# 29-jährige Patientin kann seit einem Tag
### Eine 29-jährige Patientin kann seit einem Tag ihr rechtes Auge nicht komplett schließen. Beim Trinken läuft das Wasser aus dem rechten Mundwinkel heraus. Sie stellen einen Lagophthalmus rechts, eine verstrichene Nasolabialfalte rechts sowie einen rechts tiefer stehenden Mundwinkel fest. Wie lautet die wahrscheinlichste Diagnose?
#card #neurology #anki
Periphere Facialis-Parese rechts

# 70-jähriger Patient mit Morbus Parkinson,
### Ein 70-jähriger Patient mit Morbus Parkinson, der seit 10 Jahren unter Levodopa-Therapie steht, zeigt seit kurzem wurmartige Bewegungen, die er zwar kurzfristig unterdrücken kann, die aber „immer wieder von selbst“ kommen würden. Was erklären Sie dem Patienten? (2 richtige Antworten)
#card #neurology #anki
Es handelt sich um Dyskinesien.
Es sind typische Nebenwirkungen einer langjährigen Levodopa-Therapie.

# he der folgenden klinisch-neurologischen 
### Welche der folgenden klinisch-neurologischen Befunde sind mit der Diagnose eines Morbus Parkinson vereinbar? (2 richtige Antworten) (13)
#card #neurology #anki
Rigid-akinetisches (hypokinetisches) Syndrom
Pillendreher-Tremor

Befunde eines idiopathischen Parkinsonsyndroms: asymmetrischer Beginn; Fehlen sehr früher autonomer Störungen 2018-06
klinisch-neurologischen Befunde eines idiopathischen Parkinson-Syndroms: posturale Instabilität; Bradykinese oder Akinese; Tremor 2018-09
klinisch-neurologischen Befunde eines Morbus Parkinson: rigid-akinetisches (hypokinetisches) Syndrom; Pillendreher-Tremor 2018-11
Zeichen des inzipienten Parkinsonsyndroms im neurolog. Status: mangelndes Mitbewegen der rechten OE beim Gehen; geringer Rigor der rechten OE bei kontralateraler Aktivierung; Verlangsamung der komplexen Bewegungen (360° Drehung auf der Liege) 2013-09 etc.Zeichen in der neurologischen Untersuchung für Parkinsonsyndrom: rigide Tonuserhöhung rechts; mangelndes Mitbewegen der rechten oberen Extremität beim Gehen; angedeutetes Maskengesicht 2021-11

# welchen Patienten sollte die Möglichkeit 
### Bei welchen Patienten sollte die Möglichkeit eines epilepsiechirurgischen Eingriffes evaluiert werden?
#card #neurology #anki
Fokale Epilepsie + Versagen von 2 adäquat ausgewählten und ausreichend dosierten Antikonvulsiva

# 80-jährige Patientin wird aufgrund langs
### Eine 80-jährige Patientin wird aufgrund langsam progredienter kognitiver Defizite, die mittlerweile zu massiven Beeinträchtigungen von Alltagsfunktionen führen, von den Angehörigen in die Ambulanz gebracht. Zusätzlich sind rezidivierende optisch-szenische Halluzinationen explorierbar. Im neurologischen Status stellen Sie milde extrapyramidale Symptome fest.
Welche der folgenden Differenzialdiagnosen kommen am ehesten infrage? (2 richtige Antworten)
#card #neurology #anki
Lewy Body Demenz
Demenz bei Morbus Parkinson

# der 49-jährigen Frau H. besteht ein attac
### Bei der 49-jährigen Frau H. besteht ein attackenförmig auftretender Drehschwindel mit Übelkeit und Erbrechen. Der Schwindel sei heute am Morgen im Bett beim raschen Umdrehen erstmals aufgetreten. Die somatische Anamnese ist bis auf eine Contusio capitis in der Vorwoche (CCT unauffällig) bland. Sie vermuten einen benignen paroxysmalen Lagerungsschwindel. Wie sichern Sie diese Diagnose?
#card #neurology #anki
Neurologischer Status mit Provokationsmanöver

# 42-jähriger Patient berichtet über heftig
### Ein 42-jähriger Patient berichtet über heftigste, einseitige Kopfschmerzen im Bereich der Schläfe. Die Schmerzen halten 60 bis 90 Minuten an und treten fast täglich auf. Fallweise habe er auch eine Rötung des Auges auf der Seite des Kopfschmerzes. Während dieser Attacken müsse er sich bewegen, Liegen sei unmöglich. Welche ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Clusterkopfschmerz

# he der folgenden Medikamente kommen in de
### Welche der folgenden Medikamente kommen in der Therapie der akuten Migräneattacke zum Einsatz? (3 richtige Antworten)
#card #neurology #anki
Triptane
Nichtsteroidale Antirheumatika
Antiemetika

# he neurologischen Folgeerscheinungen tret
### Welche neurologischen Folgeerscheinungen treten gehäuft bei PatientInnen mit chronischem Alkoholabusus auf?
(3 richtige Antworten)
#card #neurology #anki
Enzephalopathie
Epileptische Anfälle
Polyneuropathie

# 22-jährige Patientin wird an Ihre Ambula
### Eine 22-jährige Patientin wird an Ihre Ambulanz zugewiesen. Bei der Patientin wurde im 17. Lebensjahr eine primär generalisierte Epilepsie diagnostiziert. Nach zunächst hoher Frequenz generalisierter tonisch-klonischer Anfälle ist die Patientin nun seit 1 ½ Jahren anfallsfrei. Es besteht eine Medikation mit Valproinsäure 2 x 1000 mg/die. Nun möchte die Patientin schwanger werden und „bald mit der Verhütung aufhören“. Was raten Sie der Patientin?
#card #neurology #anki
Umstellung auf eine Monotherapie mit Lamotrigin oder mit Levetiracetam

# 35-jährige Frau berichtet über eine exze
### Eine 35-jährige Frau berichtet über eine exzessive Tagesmüdigkeit, die mit der Geburt ihres ersten Kindes vor 2 Jahren begonnen habe. Sie schlafe abends vor dem Fernseher meistens sofort ein. Einige Mal sei ihr, als sie beim Ansehen einer Comedy-Serie habe lachen müssen, der „Nacken kurz nach vorne gefallen“ (eine Bewusstseinsstörung wird verneint). Beim morgendlichen Aufwachen könne sie sich manchmal für ca. 1 Minute „gar nicht bewegen und auch nicht sprechen“. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Narkolepsie

# Freund von Ihnen (Architekturstudent) möc
### Ein Freund von Ihnen (Architekturstudent) möchte wissen, was eigentlich unter „Hirntod“ verstanden wird. Was erklären Sie ihm korrekter Weise?
#card #neurology #anki
Irreversibel erloschene Gesamtfunktion von Großhirn, Kleinhirn und Hirnstamm

# 45-jähriger Patient berichtet über eine v
### Ein 45-jähriger Patient berichtet über eine verwaschene Sprache und Schluckprobleme, die seit ca. einem halben Jahr langsam zunehmen. Auch hätte er immer wieder feine Muskelzuckungen an Armen und Beinen bemerkt. Welche der folgenden Zusatzbefunde sprechen für das Vorliegen einer Motoneuronerkrankung? (3 richtige Antworten)
#card #neurology #anki
Inadäquate Lachanfälle ohne entsprechenden emotionalen Reiz
Positive Pyramidenbahnzeichen
Faszikulationen/Fibrillationen der Zunge

# he der folgenden Therapieoptionen kommen 
### Welche der folgenden Therapieoptionen kommen in der Behandlung der Myasthenia gravis zum Einsatz? (3 richtige Antworten)
#card #neurology #anki
Acetylcholin-Esterase-Inhibitoren
Immunsuppressiva
Immunadsorption

# stellen bei einer Patientin an der linken
### Sie stellen bei einer Patientin an der linken oberen Extremität eine schlaffe Parese sowie fehlende Muskeleigenreflexe fest. Wo ist die für diese Symptomatik verantwortliche Läsion am ehesten zu vermuten?
#card #neurology #anki
Peripher

# begutachten eine Patientin an der Notfall
### Sie begutachten eine Patientin an der Notfallambulanz, bei der es am Vortag zum akuten Auftreten einer brachiozephal betonten Halbseitensymptomatik rechts und zu einer Aphasie gekommen ist. Die Patientin hat mehrere vaskuläre Risikofaktoren, sodass Sie an einen ischämischen Insult denken. In welchem Stromgebiet ist der Insult bei dieser Symptomatik am ehesten zu vermuten?
#card #neurology #anki
Arteria cerebri media links

# diagnostizieren bei einer Patientin einen
### Sie diagnostizieren bei einer Patientin einen benignen paroxysmalen Lagerungsschwindel. Welche Maßnahmen leiten Sie ein? (2 richtige Antworten)
#card #neurology #anki
Dix-Hallpike-ManöverBefreiungsmanöver nach Epley oder Semont

# F. wird wegen einer „Schwäche in der lin
### Herr F. wird wegen einer „Schwäche in der linken Hand“ vorstellig. Sie erheben folgenden Status an der linken oberen Extremität: Streckung im Ellenbogengelenk KG 5. Streckung im Handgelenk sowie Fingerstreckung KG 3–4. Hypästhesie im Spatium interosseum I. Radiusperiost-Reflex abgeschwächt. Tonus unauffällig. Wo liegt die Läsion?
#card #neurology #anki
Nervus radialis am Oberarm

# dem 27-jährigen, bislang gesunden Herrn M
### Bei dem 27-jährigen, bislang gesunden Herrn M. besteht eine Hypästhesie der rechten oberen Extremität sowie eine spastische Monoparese der linken unteren Extremität. Sie halten eine Multiple Sklerose für wahrscheinlich. Welche der folgenden Untersuchungen sind in diesem Fall zur Diagnosesicherung am zielführendsten? (2 richtige Antworten)
#card #neurology #anki
Magnetresonanztomographie des SchädelsLumbalpunktion

# 74-jähriger Patient mit idiopathischem Pa
### Ein 74-jähriger Patient mit idiopathischem Parkinsonsyndrom beklagt rezidivierende Episoden, in denen er nicht weitergehen könne und sich fühle, als ob er am Boden „kleben bleiben“ würde. Dies trete vor allem auf, wenn er durch enge Türen gehen müsse. Wie nennt man dieses Phänomen?
#card #neurology #anki
Freezing

# he der folgenden Charakteristika sind für
### Welche der folgenden Charakteristika sind für die mesiale Temporallappenepilepsie typisch? (2 richtige Antworten)
#card #neurology #anki
Hippocampusatrophie in der MRTAuren, fokal-komplexe und sekundär generalisierte Anfälle

# Bekannte (Informatikstudentin) möchte vo
### Eine Bekannte (Informatikstudentin) möchte von Ihnen wissen, „wie der Hirntod eines Menschen festgestellt wird“. Was teilen Sie ihr korrekter Weise mit?(2 richtige Antworten)
#card #neurology #anki
Die Hirntod-Diagnostik umfasst eine spezielle klinisch-neurologische Untersuchung und, als wichtigste ergänzende Untersuchung, die Durchführung eines EEGs.Ist die Durchführung eines EEGs nicht möglich, muss der zerebrale Kreislaufstillstand mit alternativen Methoden (transkranieller Dopplersonographie, farbkodierter Duplex-Sonographie, CT-Angiographie) nachgewiesen werden.

# 69-jährige Frau wird vom Hausarzt mit de
### Eine 69-jährige Frau wird vom Hausarzt mit dem Vermerk „seit 6 Monaten häufige Stürze, rascher kognitiver Abbau“ zur weiteren Abklärung zugewiesen. Im neurologischen Status stellen Sie ein akinetisch-rigides Syndrom (ohne Tremor), beeinträchtigte kognitive Leistungen, sakkadierte Blickfolgen sowie eine vertikale Blicklähmung fest. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Atypisches Parkinsonsyndrom

# hen der folgenden neurologischen Erkranku
### Welchen der folgenden neurologischen Erkrankungen liegt eine primär autoimmunologische Pathogenese zugrunde?(2 richtige Antworten)
#card #neurology #anki
Guillain-Barré-SyndromMyasthenia gravis

# 24-jährige Frau plagen seit Wochen zuneh
### Eine 24-jährige Frau plagen seit Wochen zunehmende Schmerzen in der gesamten Muskulatur. Zuletzt wäre auch eine Schwäche des Schulter und Beckengürtels aufgetreten. Der neurologische Status ist unauffällig, allerdings fällt Ihnen ein schmetterlingsförmiges Erythem der Augenlider und des Gesichts auf. Im Labor zeigt sich ein 20-fach erhöhter Wert der Creatinkinase. Was ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Dermatomyositis

# 34-jährige Patientin berichtet über star
### Eine 34-jährige Patientin berichtet über starkes Zittern der Hände, welches sie im alltäglichen Leben deutlich einschränke. Trinken aus einem Glas ist, insbesondere „in stressigen Situationen“, oft nur mit beiden Händen möglich. Nach dem Genuss von Alkohol würde sich die Symptomatik bessern. Welche Erkrankung ist am ehesten zu vermuten?
#card #neurology #anki
Essentieller Tremor

# untersuchen einen Patienten mit hohem Fie
### Sie untersuchen einen Patienten mit hohem Fieber, Photophobie, Übelkeit und heftigsten Kopfschmerzen. Sie stellen einen ausgeprägten Meningismus fest. Auch das Brudzinskizeichen ist positiv. Blutgerinnung und CCT sind unauffällig, die Entzündungsparameter sind erhöht. Sie führen eine Lumbalpunktion durch. Makroskopisch imponiert der Liquor eitrig-trüb. Welche Veränderungen im Liquorbefund sind am ehesten zu erwarten? (2 richtige Antworten)
#card #neurology #anki
Erniedrigter Liquor-Serum-Glucose-IndexHochgradige Erhöhung der Liquor-Zellzahl, granulozytäres Zellbild

# stellen bei einem Patienten eine isoliert
### Sie stellen bei einem Patienten eine isolierte Störung der Schmerz- und Temperaturempfindung fest. Stereognosie und Vibrationsempfinden sind unauffällig. Wo ist die für diese Symptomatik verantwortliche Läsion am ehesten zu vermuten?
#card #neurology #anki
Tractus spinothalamicus

# stellen bei einem Patienten eine nicht-fl
### Sie stellen bei einem Patienten eine nicht-flüssige Aphasie mit hoher Sprachanstrengung, Paraphasien und Wortfindungsstörungen fest. Das Sprachverständnis ist nicht beeinträchtigt. In welchem Hirnareal vermuten Sie eine Läsion?
#card #neurology #anki
Motorisches Sprachzentrum in der dominanten Hemisphäre

# elchem Hirnareal entstehen fokale Anfälle
### In welchem Hirnareal entstehen fokale Anfälle, die klinisch mit Kloni einhergehen?
#card #neurology #anki
Primär motorische Hirnrinde

# begutachten einen 38-jährigen Mann, der ü
### Sie begutachten einen 38-jährigen Mann, der über ein mehrmals wöchentlich auftretendes, wenige Sekunden dauerndes, „aufsteigendes, komisches Gefühl“ aus der Magengegend berichtet. Seine Frau ergänzt, dass er danach „oft schmatzen und eine Hand verkrampfen“ würde und auf Ansprache nicht reagieren würde. Vier Mal sei es auch zu Anfällen mit Überstreckung des Körpers und heftigen Zuckungen aller Extremitäten gekommen. Welcher MRT-Befund wäre mit dieser Anfallsbeschreibung am ehesten vereinbar?
#card #neurology #anki
Hippocampussklerose

# 24-jähriger Mann berichtet, seit 3 Jahren
### Ein 24-jähriger Mann berichtet, seit 3 Jahren unter therapieresistenten Anfällen zu leiden. Es handelt sich um kurze, hypermotorische, bizarr anmutende Anfälle mit rascher postiktaler Reorientierung, die in hoher Frequenz auftreten (bis zu 3–4×täglich). Mehrere Routine-EEGs sowie eine Schädel-MRT-Untersuchung waren bisher unauffällig. An welches Epilepsiesyndrom ist in diesem Fall am ehesten zu denken?
#card #neurology #anki
MRT-negative Frontallappenepilepsie

# 45-jährige, bislang gesunde Herr R. stell
### Der 45-jährige, bislang gesunde Herr R. stellt sich mit einem akuten Schwindel vor. Er berichtet, dass es zu heftigen wenige Minuten andauernden Drehschwindelattacken komme, die ausschließlich bei Seitwärtsbewegung des Kopfes auftreten. Schwäche, Kribbeln, Hörstörungen oder Sprachstörungen habe er nicht bemerkt. Was ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Benigner paroxysmaler Lagerungsschwindel

# er neurologischen Ambulanz stellt sich ei
### In der neurologischen Ambulanz stellt sich eine 44-jährige, bislang gesunde Frau vor. Sie gibt an, seit gestern ein herabhängendes rechtes Lid sowie „merkwürdige Schmerzen im Halsbereich rechts“ zu bemerken. Sie stellen ein Horner-Syndrom rechts sowie ein diskrete brachiofaciale Halbseitensymptomatik links fest. Welche der folgenden Untersuchungen sind sinnvoll? (2 richtige Antworten)
#card #neurology #anki
Ultraschalluntersuchung der KarotidenCCT oder Schädel-MRT mit Angiographie (ab dem Aortenbogen)

# he Zusatzbefunde veranlassen Sie bei Pati
### Welche Zusatzbefunde veranlassen Sie bei PatientInnen mit Kopfschmerzen am ehesten zu einer raschen weiterführenden Abklärung mittels zerebraler Bildgebung?(3 richtige Antworten)
#card #neurology #anki
Plötzlicher OnsetMeningismusBislang unbekannte Schmerzcharakteristik

# 59-jähriger Patient kommt wegen rechtssei
### Ein 59-jähriger Patient kommt wegen rechtsseitiger Lumboischialgie in Ihre Ordination. Der Achillessehnenreflex ist rechts abgeschwächt, es besteht eine Parese für die Hebung des lateralen Fußrandes sowie die Plantarflexion rechts. Zusätzlich findet sich eine rechtsseitige Hypästhesie an der Ober- und Unterschenkel-Außenseite und am Außenknöchel bis in die Kleinzehe. Wo ist die Läsion am ehesten lokalisiert?
#card #neurology #anki
S1 rechts

# 28-jährige Herr P. erlitt vor 4 Jahren ei
### Der 28-jährige Herr P. erlitt vor 4 Jahren eine Neuritis nervi optici rechts, die unter hochdosierter Kortisontherapie prompt remittierte. Eine weitere Abklärung fand damals nicht statt. Nun wird der Patient aufgrund einer Neuritis nervi optici links vorstellig. Welche der folgenden Untersuchungen sind am ehesten zu veranlassen? (3 richtige Antworten)
#card #neurology #anki
MRT Schädel + MyelonVisuell evozierte PotenzialeLumbalpunktion

# Patientin berichtet, dass ihr seit geste
### Eine Patientin berichtet, dass ihr seit gestern beim Trinken Flüssigkeit aus ihrem linken Mundwinkel tropfe. Seit heute morgen könne sie auch das linke Auge nicht mehr schließen. Der linke Mundwinkel steht tiefer als der rechte. Das Stirnrunzeln auf der linken Seite ist nicht möglich. Sonst lassen sich keine weiteren Defizite objektivieren. Worin besteht bei dieser Patientin nach Ausschluss von Kontraindikationen die Therapie der 1. Wahl?
#card #neurology #anki
Orales Glukokortikoid

# einem 71-jährigen Patienten bestehen seit
### Bei einem 71-jährigen Patienten bestehen seit 6 Monaten Ruhetremor, Bradykinese, Rigor sowie positive Pyramidenbahnzeichen. Die Symptome sind symmetrisch. Zudem zeigt sich ein starkes Gleichgewichtsdefizit, weshalb der Patient zuletzt häufig gestürzt ist. Wie lautet die wahrscheinlichste Diagnose?
#card #neurology #anki
Atypisches Parkinsonsyndrom

# he Ursachen liegen einer Polyneuropathie 
### Welche Ursachen liegen einer Polyneuropathie in der westlichen Welt am häufigsten zugrunde? (2 richtige Antworten)
#card #neurology #anki
AlkoholDiabetes mellitus

# 59-jährige Patientin wird mit der Frages
### Eine 59-jährige Patientin wird mit der Fragestellung „Blepharospamus? Hemispasmus facialis? Begutachtung und Therapieeinleitung erbeten“ zugewiesen. Was können Sie der Patientin mitteilen? (2 richtige Antworten)
#card #neurology #anki
Für beide Erkrankungen sind Botulinumtoxin-Injektionen die Therapie der ersten Wahl.Es sollte eine Schädel-MRT-Untersuchung mit Kontrastmittel durchgeführt werden.

# einer 69-jährigen Patientin wird die Diag
### Bei einer 69-jährigen Patientin wird die Diagnose einer amyotrophen Lateralsklerose gestellt. Welches Medikament ist indiziert?
#card #neurology #anki
Riluzol

# he der folgenden Charakteristika sind mit
### Welche der folgenden Charakteristika sind mit der Diagnose einer Creutzfeldt-Jakob-Erkrankung vereinbar? (3 richtige Antworten) (6)
#card #neurology #anki
Ataxie, Myoklonien und positive PyramidenbahnzeichenPositiver Nachweis der 14-3-3-Proteine im LiquorRasch progrediente dementielle Entwicklung
rasch progredientes dementielles Syndrom - zusätzliche Hinweise für Prionenerkrankung: Myoklonien; triphasische Wellen im EEG; erhöhtes 14-3-3-Protein im Liquor 2018-09

# sollten Epilepsiepatienten an ein Zentru
### Wann sollten Epilepsiepatienten an ein Zentrum zur präoperativen Evaluierung für einen epilepsiechirurgischen Eingriff zugewiesen werden?
#card #neurology #anki
Bei Nicht-Ansprechen auf die antikonvulsive Therapie mit 2 first-line-Medikamenten

# welchen der folgenden neurologischen Erkr
### Bei welchen der folgenden neurologischen Erkrankungen ist der Nachweis von Antikörpern im Rahmen der diagnostischen Abklärung von Relevanz?(3 richtige Antworten)
#card #neurology #anki
Miller-Fisher-SyndromNeuromyelitis opticaMyasthenia gravis

# 55-jähriger Mann berichtet über eine seit
### Ein 55-jähriger Mann berichtet über eine seit mehreren Monaten zunehmende Schwäche der rechten unteren Extremität mit rezidivierenden Stürzen. Seit 3 Wochen würden ihm auch Gegenstände aus der linken Hand fallen. Die Muskelmasse würde trotz regelmäßiger körperlicher Aktivität und normalem Appetit kontinuierlich abnehmen. Sie stellen Atrophien im Bereich der Hände bei zugleich gesteigerten Muskeleigenreflexen und positiven Pyramidenbahnzeichen fest. Welche Diagnose ist am wahrscheinlichsten?
#card #neurology #anki
Motoneuron-Erkrankung

# einer 38-jährigen Patientin bestehen wurm
### Bei einer 38-jährigen Patientin bestehen wurmartige Bewegungen der Extremitäten, unwillkürliches Grimassieren und psychische Irritierbarkeit. Die Symptome bestehen seit circa 3 Jahren und sind progredient. Die Mutter der Patientin habe an ähnlichen Symptomen gelitten und sei an der Erkrankung verstorben. Welche Erkrankung ist am ehesten zu vermuten?
#card #neurology #anki
Huntington Erkrankung

# 29-jährige Patientin wird vom Augenarzt 
### Eine 29-jährige Patientin wird vom Augenarzt mit der Diagnose „Stauungspapillen beidseits“ zur neurologischen Abklärung zugewiesen. Sie klagt über seit 3 Monaten bestehende, pulsierende, holokrane Kopfschmerzen (die mittlerweile an mehreren Tagen pro Woche auftreten), des Weiteren über passagere Visusstörungen und gelegentliche Doppelbilder. Die Patientin ist stark übergewichtig (171 cm, 99 kg). Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Benigne intrakranielle Hypertension

# 35-jähriger Patient weist eine idiopathis
### Ein 35-jähriger Patient weist eine idiopathische periphere Facialisparese links auf. Welche Befunde erwarten Sie im neurologischen Status? (2 richtige Antworten)
#card #neurology #anki
Beeinträchtigter Lidschluss linksHängender Mundwinkel links

# untersuchen einen Patienten mit einer Par
### Sie untersuchen einen Patienten mit einer Parese des Nervus abducens. Welche neurologischen Untersuchungsbefunde erwarten Sie? (2 richtige Antworten)
#card #neurology #anki
Betroffenes Auge kann nicht nach außen gewendet werden.Doppelbilder nehmen in Blickrichtung des betroffenen Auges zu.

# erheben bei einem 79-jährigen Mann folgen
### Sie erheben bei einem 79-jährigen Mann folgende Anamnese: In den letzten Monaten sei es insgesamt drei oder vier Mal zu akut auftretenden, wenige Minuten dauernden Episoden mit einer Hypästhesie links perioral und im linken Arm gekommen. An Vorerkrankungen bestehen eine arterielle Hypertonie und eine COPD nach langjährigem Nikotinabusus.Welche Differenzialdiagnosen sind am ehesten in Betracht zu ziehen? (2 richtige Antworten)
#card #neurology #anki
Fokale sensible epileptische AnfälleTransitorische ischämische Attacken

# 70-jähriger Mann kommt vier Wochen nach e
### Ein 70-jähriger Mann kommt vier Wochen nach einer zerebralen Ischämie in Ihre Ambulanz. Anamnestisch sind ein Bluthochdruck, ein Diabetes, eine koronare Herzkrankheit und ein Vorhofflimmern bekannt. Sie bemerken, dass der Patient nur die rechte Seite seines Gesichts rasiert hat. Sie bitten ihn, die Ziffern einer Uhr in einen Kreis einzuschreiben. Die Ziffern sind richtig, allerdings nur auf der rechten Seite des Uhrenkreises eingezeichnet. Welches Areal ist bei diesem Patienten am wahrscheinlichsten von dem Schlaganfall betroffen?
#card #neurology #anki
Parietaler Cortex rechts

# he Symptome sind bei einer beidseitigen L
### Welche Symptome sind bei einer beidseitigen Läsion der motorischen Kerngebiete des IX, X und XI Hirnnerven ( = Läsion der peripheren Motoneurone) möglich? (3 richtige Antworten)
#card #neurology #anki
ZungenpareseFehlender oder abgeschwächter WürgereflexAtrophie der Zunge

Cave! Zungenparese und -atrophie eigentlich durch N. hypoglossus (XII) Läsion!

# untersuchen einen Patienten mit Lumboisch
### Sie untersuchen einen Patienten mit Lumboischialgie links. Es ist ein Diskusprolaps mit Bedrängung der Wurzel S1 links bekannt. Welcher Befund ist im Bereich der Reflexe der linken unteren Extremität zu erwarten?
#card #neurology #anki
Abgeschwächter / fehlender Achillessehnenreflex

# he Symptome sind bei einer Querschnittslä
### Welche Symptome sind bei einer Querschnittsläsion im unteren Halsmark möglich? (2 richtige Antworten)
#card #neurology #anki
TetrapareseSensibles Niveau

# einem Patienten mit Z. n. einer Luxation 
### Bei einem Patienten mit Z. n. einer Luxation im Schultergelenk besteht eine Schwäche für die Abduktion, Anteversion und Retroversion im Schultergelenk sowie ein sensibler Ausfall in folgendem (auf dem Bild rot markierten) Hautareal: Wie lautet die zutreffendste Diagnose? Läsion des/der![[Media/Anki/Neurologie/media/paste-69930657513473.jpg]] 
#card #neurology #anki
N. axillaris

# beobachten bei einem Patienten folgenden 
### Sie beobachten bei einem Patienten folgenden Anfall: Der Patient starrt plötzlich ins Leere, reagiert nicht mehr auf Ansprache, schmatzt und nestelt mit der rechten Hand. Für welches der folgenden Epilepsiesyndrome ist diese klinische Anfallssemiologie typisch?
#card #neurology #anki
Temporallappenepilepsie

# begutachten eine 70-jährige Patientin, di
### Sie begutachten eine 70-jährige Patientin, die von ihrer Tochter bewusstlos in der Wohnung aufgefunden wurde. Die Patientin ist soporös, weist Halbseitenzeichen links und eine Anisokorie auf. Laut Tochter würde die Patientin „häufig stürzen“. Die CCT zeigt folgendes Bild:Wie lautet die wahrscheinlichste Diagnose?
![[Media/Anki/Neurologie/media/paste-77781857730561.jpg]]
#card #neurology #anki
Subduralhämatom

# 72-jähriger Patient stellt sich wegen lan
### Ein 72-jähriger Patient stellt sich wegen langjähriger Kopfschmerzen vor. Er berichtet über maximal 3–4 kopfschmerzfreie Tage pro Monat. Meistens nimmt er die Kopfschmerzen im ganzen Kopf wahr, die Schmerzen seien drückend, leicht oder mittelstark. Abgesehen von geringer Lärmempfindlichkeit gäbe es keine Begleiterscheinungen. Welche Diagnose liegt vor?
#card #neurology #anki
Chronischer Spannungskopfschmerz

# 70-jährige Patientin kommt um 16:00 in d
### Eine 70-jährige Patientin kommt um 16:00 in die Notfallambulanz. Seit 14:00 bestehen Halbseitenzeichen rechts und Wortfindungsstörungen. Aufgrund eines Vorhofflimmerns ist die Patientin mit einem neuen oralen Antikoagulans (Rivaroxaban, Xarelto®) behandelt. Die kraniale MRT (incl. diffusions- und perfusionsgewichteter Bildgebung und Gefäßdarstellung) zeigt folgendes Bild:Welche Therapie können Sie der Patientin zu diesem Zeitpunkt anbieten?
![[Media/Anki/Neurologie/media/paste-82231443849217.jpg]]
#card #neurology #anki
Endovaskuläre Therapie (Thrombektomie)

# 28-jährige Frau T. mit schubförmig verlau
### Die 28-jährige Frau T. mit schubförmig verlaufender Multipler Sklerose (Erstdiagnose: vor vier Jahren) wird in Ihre Ambulanz zugewiesen, nachdem es trotz regelmäßiger Therapie mit einem Beta-Interferon-Präparat in den letzten 2 Jahren zu sechs Schüben gekommen ist. Die MRT (Schädel, zervikales Myelon) zeigt eine massive Befundprogredienz. Welche der folgenden Therapiemöglichkeiten sind am ehesten zu erwägen?
#card #neurology #anki
Eskalationstherapie mit monoklonalem Antikörper

# 66-jährige Frau mit Diabetes mellitus Ty
### Eine 66-jährige Frau mit Diabetes mellitus Typ II (HbA1c: 9,5 %) berichtet über Schmerzen und Missempfindungen in beiden Beinen. In einer bereits durchgeführten Nervenleitgeschwindigkeitsmessung zeigt sich eine axonale Neuropathie. Der Achillessehnenreflex fehlt beidseits, es besteht eine Pallhypästhesie an den unteren Extremitäten sowie eine Hypästhesie in folgenden (auf dem Bild rot markierten) Hautarealen:Wie lautet die zutreffendste Diagnose?
![[Media/Anki/Neurologie/media/paste-88673894793217.jpg]]
#card #neurology #anki
Distale symmetrische Polyneuropathie

# 75-jähriger Patient stellt sich aufgrund 
### Ein 75-jähriger Patient stellt sich aufgrund eines akuten Drehschwindels, der seit 1 Stunde bestünde und akut begonnen habe, an einer neurologischen Akutambulanz vor. Welche der folgenden Symptome lassen am ehesten an eine zentrale Schwindelgenese denken? (2 richtige Antworten)
#card #neurology #anki
Upbeat-NystagmusDoppelbilder

# 79-jährige Patientin präsentiert sich mi
### Eine 79-jährige Patientin präsentiert sich mit akut aufgetretenen Doppelbildern und verwaschener Sprache. Sie stellen zusätzlich eine Augenmotilitätsstörung fest. Es bestehen multiple vaskuläre Risikofaktoren, sodass Sie primär einen ischämischen Schlaganfall vermuten. Im Stromgebiet welcher Arterie könnte sich der Schlaganfall am ehesten ereignet haben? (2 richtige Antworten)
#card #neurology #anki
A. vertebralisA. basilaris

# he der folgenden Befunde sprechen für das
### Welche der folgenden Befunde sprechen für das Vorliegen eines atypischen Parkinsonsyndroms?(3 richtige Antworten)
#card #neurology #anki
Frühe StürzeFrühe DemenzFrühe, schwere autonome Dysfunktion

# 33-jähriger Mann mit bekannter HIV-Infekt
### Ein 33-jähriger Mann mit bekannter HIV-Infektion präsentiert sich in deutlich reduziertem Allgemein- und Ernährungszustand mit Kopfschmerzen und 38,5 °C Körpertemperatur, Doppelbildern sowie einer Schwäche der linken Gesichtshälfte. Im neurologischen Status stellen Sie einen endlagigen Meningismus, eine Okulomotoriusparese rechts und eine periphere Fazialisparese links fest. Im zerebralen MRT zeigt sich eine Kontrastmittelanfärbung in den basalen Meningen. Eine Lumbalpunktion zeigt 540/3 Zellen mit pleomorphem Zellbild, einen erniedrigten Glukosewert (deutlich &lt;50 % des Serumwertes) sowie eine Eiweißerhöhung.Wie lautet ist die wahrscheinlichste Diagnose?
#card #neurology #anki
Tuberkulöse Meningitis

# 69-jähriger Mann mit bekannter arterielle
### Ein 69-jähriger Mann mit bekannter arterieller Hypertonie wird mit der Rettung eingeliefert. Eine Stunde zuvor sei es zu einem erstmaligen generalisierten tonisch-klonischen Anfall gekommen. Die begleitende Ehefrau berichtet auch über eine Wesensveränderung ihres Ehemanns in den letzten zwei Monaten. Der Patient ist psychomotorisch verlangsamt und desorientiert. Es besteht eine diskrete Hemiparese rechts. Im Akut-CCT zeigt sich eine unregelmäßige Raumforderung mit ausgeprägtem Ödem linkshemisphärisch (siehe CCT-Bild). Welche Diagnose scheint in Anbetracht der Symptomatik und des Verlaufs am wahrscheinlichsten?
![[Media/Anki/Neurologie/media/paste-101386997989377.jpg]]
#card #neurology #anki
Glioblastom

# 59-jährige Patientin kommt wegen „Zitter
### Eine 59-jährige Patientin kommt wegen „Zittern“ in die Ambulanz. Sie stellen einen (die Alltagsfunktionen beeinträchtigenden) hochfrequenten, symmetrischen Ruhe- und Intentionstremor an beiden Händen fest. Der übrige neurologische Status ist unauffällig. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Essentieller Tremor

# einer 54-jährigen Patientin bestehen seit
### Bei einer 54-jährigen Patientin bestehen seit ca. 2 Wochen eine Ataxie und Doppelbilder. Sie stellen eine Ophthalmoplegie und allseits fehlende Muskeleigenreflexe fest. Liquordiagnostisch zeigt sich eine Eiweiß-Erhöhung ohne Pleozytose. Wie lautet die wahrscheinlichste Diagnose?
#card #neurology #anki
Miller-Fisher-Syndrom

# ei einem Patienten mit heftigen, von lumb
### Bei einem Patienten mit heftigen, von lumbal bis in beide Beine ausstrahlenden Schmerzen erheben Sie im neurologischen Status folgende Befunde: Schlaffe Parese für Plantarflexion und Vorfußpronation beidseits, Sensibilitätsstörungen in den Dermatomen S1-S5 beidseits, Hypästhesie in der perianalen Region, fehlender Analreflex beidseits, fehlender Achillessehnenreflex beidseits, Harnverhalten. Wie lautet Ihre Diagnose? 
#card #neurology #anki
Cauda-Syndrom

# in 19-jähriger Patient leidet seit der fr
### Ein 19-jähriger Patient leidet seit der frühen Kindheit an einer genetisch gesicherten Duchenne-Muskeldystrophie. Er kommt mit seiner Mutter zu Ihnen in die Ambulanz. Der Patient hat eine 25-jährige Schwester und einen 21-jährigen Bruder, die beide gesund sind, jedoch nie genetisch getestet wurden. Welche Informationen zur Erkrankung geben Sie der ratsuchenden Familie? (3 richtige Antworten) 
#card #neurology #anki
Die Erkrankung betrifft in den allermeisten Fällen Knaben.Für den asymptomatischen Bruder des Patienten besteht kein Risiko, die Erkrankung weiter zu vererben.Die Erkrankung wird X-chromosomal vererbt.

# ie möchten die Funktion der Hirnnerven IX
### Sie möchten die Funktion der Hirnnerven IX und X prüfen. Welche Untersuchungssschritte des neurologischen Status sind hierfür geeignet? (2 richtige Antworten) 
#card #neurology #anki
Prüfung des WürgereflexesInspektion des Gaumensegels

# elche klinisch-neurologischen Untersuchun
### Welche klinisch-neurologischen Untersuchungsbefunde erwarten Sie bei einem Patienten mit Schwindel infolge eines akuten Ausfalles des Nervus vestibularis? (3 richtige Antworten) 
#card #neurology #anki
Patholgische Einstellsakkaden im Kopf-Impuls-TestStand- und Gangunsicherheit mit gerichteter FallneigungSpontannystagmus

# elche klinisch-neurologischen Untersuchun
### Welche klinisch-neurologischen Untersuchungsbefunde erwarten Sie bei einer Läsion der Pyramidenbahn? (2 richtige Antworten) (4) 
#card #neurology #anki
Gesteigerte MuskeleigenreflexeSpastik
Symptome einer Pyramidenbahnläsion rechts: sensomotorische Halbseitensymptomatik links; positive Pyramidenbahnzeichen links 2020-11

# elches der folgenden Symptome kann die Fo
### Welches der folgenden Symptome kann die Folge einer Läsion im Hirnstamm sein?
#card #neurology #anki
Intranukleäre Ophtalmoplegie
Cave! Google kennt nur die internukleäre Ophthalmoplegie, aber keine intranukleäre!

# in 37-jähriger Patient verrichtet zuhause
### Ein 37-jähriger Patient verrichtet zuhause über mehrere Stunden in knieender Position Reparaturarbeiten. Am nächsten Tag stellt er sich mit einer Schwäche für die Vorfuß- und Großzehenhebung rechts vor, der Fersengang ist nicht möglich, es besteht eine Hypästhesie im Bereich des lateralen Unterschenkels rechts. Wie lautet ist die wahrscheinlichste Diagnose? 
#card #neurology #anki
Läsion N. peroneus

# ine 24-jährige Patientin stellt sich wege
### Eine 24-jährige Patientin stellt sich wegen heftiger Kopfschmerzen vor, die seit einem Jahr ein- oder zweimal pro Monat auftreten würden. Sie wache üblicherweise mit sehr heftigen, stets linksseitigen Kopfschmerzen und einer erheblichen Verspannung im Nacken auf und müsse innerhalb einer Stunde erbrechen. Bis zum nächsten Tag sei sie kaum in der Lage, Alltagsaktivitäten auszuführen. Ein MRT des Gehirns sei unauffällig gewesen. Welche Diagnose ist am wahrscheinlichsten? 
#card #neurology #anki
Migräne ohne Aura

# in 69-jähriger Mann (unbehandelte arterie
### Ein 69-jähriger Mann (unbehandelte arterielle Hypertonie, sonst keine Vorerkrankungen) lässt beim Frühstück (7:30 Uhr) wiederholt das Besteck aus der rechten Hand fallen. Auch der rechte Mundwinkel hängt herab. Die Symptomatik bessert sich innerhalb der nächsten 30 Minuten. Er fühlt sich müde und legt sich im Laufe des Vormittags hin. Als er um 14:00 erwacht, kann er nicht mehr sprechen und die rechte Körperhälfte nicht mehr bewegen. Seine Tochter findet ihn in diesem Zustand um 20:00 vor. Um 21:30 trifft er mit der Rettung an der Notaufnahme ein. Im CCT zeigt sich folgendes Bild: 
![[Media/Anki/Neurologie/media/paste-36ed2a79352f3d83eff5a6de270414fe07c56b3a.jpg]]
Welche Therapie können Sie dem Patienten zu diesem Zeitpunkt anbieten?
#card #neurology #anki
Thrombozytenaggregationshemmende Therapie

# elche der folgenden Faktoren sollte Sie d
### Welche der folgenden Faktoren sollte Sie dazu veranlassen, einen Anfallspatienten zur weiteren Abklärung an ein epilepsiechirurgisches Zentrum zu überweisen? (2 richtige Antworten) (3) 
#card #neurology #anki
Therapieresistenz + fokale Epilepsie
Hippocampussklerose in der MRT des Schädels

# ei einem 64-jährigen Patienten bestehen a
### Bei einem 64-jährigen Patienten bestehen auf der linken Körperseite eine beinbetonte Hemiparese und eine Hemihypästhesie. Es bestehen multiple vaskuläre Risikofaktoren, sodass Sie primär einen ischämischen Schlaganfall vermuten. Im Stromgebiet welcher Arterie könnte sich der Schlaganfall am ehesten ereignet haben? 
#card #neurology #anki
A. cerebri anterior rechts

# in 49-jähriger, alkoholabhängiger Patient
### Ein 49-jähriger, alkoholabhängiger Patient kommt aufgrund von Kopfschmerzen und Halbseitenzeichen rechts in die Ambulanz. Er nimmt einen Thrombozytenaggregationshemmer ein. Die CCT zeigt hyper- und hypodense Veränderungen in folgender Lokalisation: 
Wie lautet die wahrscheinlichste Diagnose? 
![[Media/Anki/Neurologie/media/paste-ee49cdde2bfb9a097802d5387960b284e9a16d66.jpg]]
#card #neurology #anki
Chronisches Subduralhämatom

# in 40-jähriger Patient mit bislang unauff
### Ein 40-jähriger Patient mit bislang unauffälliger somatischer Anamnese berichtet über seit 7 Tagen bestehende Kopfschmerzen, unspezifische grippale Symptome und Lichtempfindlichkeit. Sie stellen einen endlagigen Meningismus fest, der übrige neurologische Status ist unauffällig. CCT und Gerinnungswerte sind unauffällig. Sie führen eine Lumbalpunktion durch. Der Liquor zeigt eine Pleozytose mit 480 Drittelzellen, eine Laktat- und Eiweißerhöhung (die Liquorglukose ist normal) sowie folgendes Zellbild: Wie lautet Ihre primäre Verdachtsdiagnose? 
![[Media/Anki/Neurologie/media/paste-863a2016bf77318d8e591d562b70dd6a60e2d6b4.jpg]]
#card #neurology #anki
Virale Meningitis

# in 30-jähriger Mann kommt wegen einer sym
### Ein 30-jähriger Mann kommt wegen einer symmetrischen Schwäche seiner unteren Extremitäten in ihre Ambulanz. Des Weiteren berichtet er über Parästhesien in Zehen und Fingern. Die neurologische Untersuchung zeigt eine symmetrische Paraparese und allseits fehlende Reflexe. Das Ergebnis der Lumbalpunktion zeigt eine zytoalbuminäre Dissoziation. Welche der folgenden Organismen sind an der Pathogenese dieser Erkrankung häufig beteiligt? 
#card #neurology #anki
Campylobacter jejuni

# ie werden mit der Bitte um eine neurologi
### Sie werden mit der Bitte um eine neurologische Begutachtung zu einem 45-jährigen HIV-positiven Patienten gerufen, der in den letzten Wochen zunehmend verwirrt war. Der Patient ist psychomotorisch stark verlangsamt und in allen Qualitäten desorientiert. Weitere Auffälligkeiten im neurologischen Status finden sich nicht. Im MRT des Gehirns zeigt sich eine an den rechten Seitenventrikel angrenzende, 4 x2 cm große, homogene, kontrastmittelaufnehmende, scharf abgrenzbare Läsion. An welche Differentialdiagnose denken Sie am ehesten? 
#card #neurology #anki
ZNS Lymphom

# ine 57-jährige Patientin wurde aufgrund v
### Eine 57-jährige Patientin wurde aufgrund von chronischen Kopfschmerzen von ihrem Hausarzt zu einer MRT-Untersuchung des Schädels überwiesen. Die Patientin erzählt, man hätte dabei einen Hirntumor gefunden und sie mache sich große Sorgen. Bei Durchsicht der Bilder sehen Sie eine extra-axiale, breitbasige 1,5 cm von der Dura abgehende Raumforderung an der linken Schädelkonvexität. In den nativen MRT-Sequenzen ist sie vom Hirnparenchym kaum zu unterscheiden, nach Kontrastmittelgabe färbt sich die Raumforderung homogen und deutlich an. Ein Hirnödem oder sonstige Auffälligkeiten sind nicht zu sehen. Was teilen Sie der Patientin mit? (2 richtige Antworten) 
#card #neurology #anki
Es handelt sich am ehesten um ein Meningeom.
Es ist dies mit hoher Wahrscheinlichkeit eine benigne Raumforderung.

# ei dem 19-jährigen Herrn K. besteht seit 
### Bei dem 19-jährigen Herrn K. besteht seit der frühen Pubertät eine langsam progrediente Gang- und Gleichgewichtsstörung. Im neurologischen Status zeigen sich eine zerebelläre Ataxie, ein reduzierter Muskeltonus, eine Pallhypästhesie und ein Hohlfuß. Der Patient steht aufgrund einer Kardiomyopathie in kardiologischer Betreuung. Wie lautet Ihre primäre Verdachtsdiagnose? 
#card #neurology #anki
Hereditäre, autosomal rezessive Ataxie

# ei einem 35-jährigen Patienten bestehen s
### Bei einem 35-jährigen Patienten bestehen seit 5 Jahren eine progrediente Bradykinese, Rigor und Tremor. Welche Erkrankungen kommen differenzialdiagnostisch am ehesten infrage? (2 richtige Antworten) 
#card #neurology #anki
Morbus Wilson
Juveniles Parkinsonsyndrom

# möchten die Funktion des V. Hirnnerven pr
### Sie möchten die Funktion des V. Hirnnerven prüfen. Welche Untersuchungssschritte des neurologischen Status sind hierfür geeignet? (2 richtige Antworten)
#card #neurology #anki
Kraft der Kaumuskultur
Masseterreflex

# 72-jährige Patientin mit paroxysmalem Vo
### Eine 72-jährige Patientin mit paroxysmalem Vorhofflimmern hat einen ischämischen Schlaganfall erlitten. Eine Ultraschalluntersuchung der Halsgefäße ist unauffällig. Andere vaskuläre Risikofaktoren sind derzeit nicht bekannt. Welche Schlaganfall-Ursache halten Sie angesichts dieser anamnestischen Angaben für die wahrscheinlichste?
#card #neurology #anki
Kardiogene Embolie

# he Symptome sind bei einer halbseitigen R
### Welche Symptome sind bei einer halbseitigen Rückenmarksläsion möglich? (2 richtige Antworten)
#card #neurology #anki
Gestörtes Schmerz- und Temperaturempfinden kontralateral
Parese der kaudal gelegenen Körperhälfte ipsilateral

# 20-jähriger Student besucht die neurogene
### Ein 20-jähriger Student besucht die neurogenetische Sprechstunde und berichtet, dass sein Vater an Morbus Huntington erkrankt sei. Der junge Mann möchte sich über die Erkrankung an sich und über sein eigenes Erkrankungsrisiko informieren. Welche Informationen geben Sie dem Ratsuchenden? (3 richtige Antworten) (4)
#card #neurology #anki
Psychiatrische Symptome zeigen sich häufig früh im Erkrankungsverlauf.
Die Erkrankung ist immer durch Mutationen im gleichen Gen charakterisiert.
Die Wahrscheinlichkeit, die Erkrankung weiter zu vererben, liegt bei 50%.
Genetik des M. Huntington: M. Huntington zeigt typischerweise das Phänomen der Antizipation. Die Symptome von Mutationsträgern werden im Laufe der Generationen schwerer und treten tendenziell früher auf. 2014-07 etc.

# untersuchen einen Patienten, bei dem eine
### Sie untersuchen einen Patienten, bei dem eine diabetische Polyneuropathie bekannt ist. Welche typischen Befunde sind im neurologischen Status zu erwarten? (2 richtige Antworten)
#card #neurology #anki
Socken- oder strumpfförmige Hypästhesie
Abgeschwächter oder erloschener Achillessehnenreflex

# 77-jährige Patientin mit progredienter B
### Eine 77-jährige Patientin mit progredienter Bewusstseinstrübung wird in die Notaufnahme gebracht. Laut Tochter sei die Patientin „vor 2 Tagen gestürzt“; genauere Angaben lassen sich jedoch nicht erheben. Die Patientin ist somnolent, es bestehen Halbseitensymptome links. Der CCT-Befund lautet: „Bikonvex-linsenförmiges, rezentes Hämatom rechts temporal, Schädelbasisfraktur rechts.“. Wie lautet Ihre Diagnose?
#card #neurology #anki
Epiduralhämatom

# 68-jähriger Mann knickt beim Spazierengeh
### Ein 68-jähriger Mann knickt beim Spazierengehen plötzlich mit dem rechten Bein ein. Der Gattin fällt eine Sprachstörung auf, wobei die Fähigkeit des Nachsprechens intakt scheint. Bei Eintreffen im Krankenhaus stellen Sie eine beinbetonte sensomotorische Hemiparese rechts sowie eine transkortikale Aphasie fest. Bei Verdacht auf ein zerebral-ischämisches Ereignis veranlassen Sie eine zerebrale Computertomographie. Im Versorgungsgebiet welches hirnversorgenden Gefäßes vermuten Sie die Durchblutungsstörung am ehesten?
#card #neurology #anki
A. cerebri anterior links

# 59-jähriger Patient kommt in Begleitung s
### Ein 59-jähriger Patient kommt in Begleitung seiner Ehefrau in die Ambulanz. Diese berichtet, dass es in den letzten Monaten zu einer Wesensveränderung des Patienten (Vernachlässigung der Körperpflege, gestörte Impulskontrolle) gekommen sei. Der neurologische Status ist bis auf einen beidseits positiven Palmomentalreflex unauffällig. Der Patient imponiert logorrhoisch und distanzlos. Er zeigt keinerlei Krankheitseinsicht. Eine neuropsychologische Testung zeigt Defizite in den Exekutivfunktionen, bei relativ gut erhaltener Gedächtnisleistung.Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Frontotemporale Demenz

# begutachten einen 61-jährigen Patienten m
### Sie begutachten einen 61-jährigen Patienten mit Koma unklarer Genese. Sie stellen die Verdachtsdiagnose einer Basilaristhrombose. Welche Untersuchungen eignen sich, um in der Akutsituation eine Basilaristhrombose zu bestätigen oder auszuschließen? (2 richtige Antworten)
#card #neurology #anki
CCT mit CT-Angiographie
MRT mit diffusionsgewichteter Bildgebung und MR-Angiographie

# der 30-jährigen Frau S. besteht anhand de
### Bei der 30-jährigen Frau S. besteht anhand der vorliegenden MRT-Befunde (Schädel und Halswirbelsäule) und der klinischen Symptomatik der hochgradige Verdacht auf eine Multiple Sklerose. Zur Ergänzung der Diagnostik veranlassen Sie eine Lumbalpunktion. Welche Liquorbefunde wären mit der Diagnose einer Multiplen Sklerose vereinbar? (2 richtige Antworten)
#card #neurology #anki
Positive oligoklonale Banden
Intrathekale IgG Produktion

# 40-jähriger Patient stellt sich wegen ein
### Ein 40-jähriger Patient stellt sich wegen einer seit einem Jahr bestehenden Bewegungsarmut vor. In der neurologischen Untersuchung zeigen sich ein Rigor im linken Handgelenk, ein Ruhetremor sowie eine Bradykinese und eine Hypomimie. Ein bereits veranlasstes Schädel-MRT zeigt keine Auffälligkeiten. Welche Untersuchungen veranlassen Sie nun am ehesten um das Krankheitsbild abzuklären? (2 richtige Antworten)
#card #neurology #anki
FP-CIT-SPECT (DAT-SPECT)
Kupferbestimmung in Serum und 24h-Harn

# 46-jährigerPatient wird wegen „Schmerzen 
### Ein 46-jährigerPatient wird wegen „Schmerzen und Gefühlsstörung“ im rechten Bein vorstellig. Im neurologischen Status stellen Sie rechts eine rechtsseitige Parese für den M. quadriceps (Kniestreckung) und den M. tibialis anterior (Dorsalextension des Fußes) sowie einen rechts abgeschwächten Patellarsehnenreflex fest. Es bestehen Schmerzen und eine Hypästhesie in folgenden (auf dem Bild rot markierten) Hautarealen. Wie lautet die naheliegendste Diagnose?
![[Media/Anki/Neurologie/media/paste-cc656c347595cbad1626eacebf34eb991e145de2.jpg]]
#card #neurology #anki
Radikuläres Syndrom L4

# 74-jährige Frau H. nimmt seit etwa 9 Jahr
### Die 74-jährige Frau H. nimmt seit etwa 9 Jahren aufgrund eines idiopathischen Parkinsonsyndroms eine Therapie mit Levodopa ein. Seit kurzem leidet sie unter unwillkürlichen, kaum zu kontrollierenden „fahrigen“, „oft viel zu heftigen“ Bewegungen in Armen, Schultern und Beinen. Worüber klären Sie die Patientin auf? (2 richtige Antworten)
#card #neurology #anki
Es handelt sich um Dyskinesien.
Es handelt sich um eine typische Nebenwirkung einer langjährigen dopaminergen Therapie.

# 41-jährige Patientin mit schubförmig ver
### Eine 41-jährige Patientin mit schubförmig verlaufender Multipler Sklerose kommt zur routinemäßigen Kontrolluntersuchung. Unter einer Intervalltherapie mit einem Beta-Interferon-Präparat ist die Patientin seit Jahren subjektiv beschwerdefrei. Auch der von Ihnen erhobene neurologische Status ist unauffällig. Welchen Wert der Expanded Disability Status Scale (EDSS) notieren Sie?
#card #neurology #anki
0

# stellen bei einem Patienten mit Zervikobr
### Sie stellen bei einem Patienten mit Zervikobrachialgie eine Parese für die Streckung im Ellenbogengelenk fest. Der Tricepssehnenreflex ist nicht auslösbar. Es bestehen Schmerzen und eine Hypästhesie in folgendem (auf dem Bild rot markierten) Hautareal:
Wie lautet die zutreffendste Diagnose? Läsion des/der
![[Media/Anki/Neurologie/media/paste-ae8210931005be92289ec7fa5c7ccaa5b7db85d3.jpg]]
#card #neurology #anki
Wurzel C7

# 34-jähriger Patient stellt sich in der ne
### Ein 34-jähriger Patient stellt sich in der neurologischen Akutambulanz vor, da er seit einigen Tagen Missempfindungen in beiden Beinen hat. In der Anamnese erheben Sie, dass die Symptomatik vor 7 Tagen mit einem Kribbeln in beiden Beinen begann, das sich im Verlauf bis zu beiden Knien ausbreitete. In der neurologischen Untersuchung stellen Sie eine schlaffe distale Paraparese (Kraftgrad 4 beidseits) fest, die Muskeleigenreflexe sind nicht auslösbar. Wie lautet die wahrscheinlichste Differentialdiagnose?
#card #neurology #anki
Polyradikulitis

# 65-jährige Patientin klagt über ein zune
### Eine 65-jährige Patientin klagt über ein zunehmendes Zittern beider Hände. Das Zittern störe sie in vielen Tätigkeiten des täglichen Lebens, wie etwa beim Trinken aus einem Glas, Schneiden der Speisen oder Lesen in einer Zeitung. In der neurologischen Untersuchung zeigt sich ein Haltetremor an beiden oberen Extremitäten, sonst finden sich keinerlei Auffälligkeiten. Welche Verdachtsdiagnosen kommen infrage? (2 richtige Antworten)
#card #neurology #anki
Verstärkter physiologischer Tremor
Essentieller Tremor

# einem 30-jährigen Patienten, bei dem seit
### Bei einem 30-jährigen Patienten, bei dem seit knapp 24 Stunden stärkste Kopfschmerzen, Fieber und Nackensteifigkeit bestehen, zeigen sich in der Liquordiagnostik 19.000 Drittelzellen, eine stark erniedrigte Liquorglucose sowie folgendes Zellbild:Wie lautet Ihre primäre Verdachtsdiagnose?
![[Media/Anki/Neurologie/media/paste-58e1cff38e0ba833e7c8992f49176f3cc8543728.jpg]]
#card #neurology #anki
Bakterielle Meningitis

# befinden sich im klinisch praktischen Jah
### Sie befinden sich im klinisch praktischen Jahr (KPJ) an einer neurologischen Abteilung und
dürfen, unter Anleitung und Aufsicht Ihrer Mentorin und Einverständnis des Patienten,
eine Lumbalpunktion durchführen. Ihre Mentorin legt Ihnen Befunde des Patienten vor
und fragt Sie, welche Befunde Sie vorrangig kontrollieren müssen, bevor Sie die Punktion
durchführen können. Was antworten Sie?
(3 richtige Antworten)
#card #neurology #anki
Gerinnungswerte
Thrombozyten-AnzahlRezente Bildgebung des Schädels

# hes Symptom erwarten Sie bei einer Läsion
### Welches Symptom erwarten Sie bei einer Läsion in der linken Radiatio optica?
#card #neurology #anki
Homonyme Hemianopsie nach rechts

# he Symptome sind bei einem Brown-Sequard-
### Welche Symptome sind bei einem Brown-Sequard-Syndrom möglich?
(2 richtige Antworten)
#card #neurology #anki
Schmerz- und Temperaturempfinden kontralateral beeinträchtigt
Parese der kaudal gelegenen Körperhälfte ipsilateral

# unterhalten sich mit einem 64-jährigen Pa
### Sie unterhalten sich mit einem 64-jährigen Patienten, der seit 5 Jahren an einem Parkinson-Syndrom leidet, über seine Symptome. Er berichtet über ein Zittern seiner rechten Hand
in Ruhe und eine verlangsamte Gehgeschwindigkeit. Der Patient fragt Sie nach möglichen
weiteren Symptomen und Folgen der Erkrankung, die in der Zukunft auf ihn zukommen
könnten. Was antworten Sie ihm?
(2 richtige Antworten)
#card #neurology #anki
Vermindertes Ansprechen auf Medikamente
Demenz

# 29-jähriger Mann berichtet, heute Morgen 
### Ein 29-jähriger Mann berichtet, heute Morgen mit einer schmerzlosen „Schwäche des linken
Fußes“ erwacht zu sein. Am Vortag hat er mehrere Stunden Bodenfliesen in seiner neuen
Wohnung verlegt. Es besteht eine linksseitige schlaffe Parese für die Pro- und Supination des
Vorfußes sowie die Dorsalextension von Vorfuß und Zehen, außerdem auch eine Hypästhesie
in folgenden (auf dem Bild rot markierten) Hautarealen. Wie lautet die wahrscheinlichste
Diagnose?
![[Media/Anki/Neurologie/media/paste-fae7056e769ca64da218c36e549abe640da02f3c.jpg]]
#card #neurology #anki
Druckläsion des N. peronaeus communis am Fibulaköpfchen

# untersuchen eine 21-jährige Patientin mit
### Sie untersuchen eine 21-jährige Patientin mit Kopfschmerzen in der Notaufnahme. Die
Patientin gibt an, seit ungefähr sechs Stunden an zunehmenden drückenden Kopfschmerzen
zu leiden, die mittlerweile einen Punktewert von 7/10 auf der VAS (visuellen Analogskala)
erreichen. Es sind keine früheren Erkrankungen bekannt. Sie nimmt keine Medikamente ein.
In der neurologischen Untersuchung stellen Sie eine Hypästhesie der linken Gesichtshälfte
sowie eine milde Abducensparese links fest. Was ist Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Sinusvenenthrombose

# stellen bei einer Patientin rechtsseitig 
### Sie stellen bei einer Patientin rechtsseitig eine Parese für die Plantarflexion und Pronation
des rechten Fußes, einen rechtsseitig fehlenden Achillessehnenreflex sowie ein positives
Laseguezeichen rechts fest. Die Patientin gibt in folgenden Hautarealen (auf dem Bild rot
markiert) Schmerzen und eine Hypästhesie an:
Wie lautet die zutreffendste Diagnose?
![[Media/Anki/Neurologie/media/paste-1c00c02a73e7bb26b02011b8063a2d71ab2a35eb.jpg]]
#card #neurology #anki
Radikuläre Läsion S1

# einem 66-jährigen Patienten besteht eine 
### Bei einem 66-jährigen Patienten besteht eine akut aufgetretene homonyme Hemianopsie.
Aufgrund des hochgradigen vaskulären Risikoprofils des Patienten vermuten Sie primär
einen ischämischen Schlaganfall. Wie lautet Ihre primäre Einschätzung zur Lokalisation?
#card #neurology #anki
Syndrom der A. cerebri posterior

# 25-jähriger Patient wird in die Anfallsam
### Ein 25-jähriger Patient wird in die Anfallsambulanz überwiesen. Seine Freundin berichtet,
dass es seit einigen Monaten zu Episoden komme, bei denen der Patient nicht auf Ansprache
reagiere und abwesend „in die Luft starre“. Sie erheben, dass es während dieser Episoden
auch zu oralen Automatismen und Nestelbewegungen mit einer Hand gekommen ist. Der
Patient selbst amnesiert diese Ereignisse. Bis auf wiederholte Fieberkrämpfe im Kleinkindesalter ist die bisherige Anamnese unauffällig. Wie lautet die primäre Verdachtsdiagnose?
#card #neurology #anki
Fokale Anfälle vom Temporallappen-Typ

# 52-Jähriger wird vom Hausarzt in die neur
### Ein 52-Jähriger wird vom Hausarzt in die neurologische Spezialambulanz für Bewegungsstörungen überwiesen. Die Verdachtsdiagnose lautet „idiopathisches Parkinsonsyndrom“.
Welche Auffälligkeiten erwarten Sie sich im Falle dieser Diagnose in der neurologischen
Untersuchung?
(2 richtige Antworten) (13)
#card #neurology #anki
Zähen, geschwindigkeits- und winkelunabhängigen Widerstand in der Tonusprüfung
Asymmetrische Tonuserhöhung

Befunde eines idiopathischen Parkinsonsyndroms: asymmetrischer Beginn; Fehlen sehr früher autonomer Störungen 2018-06
klinisch-neurologischen Befunde eines idiopathischen Parkinson-Syndroms: posturale Instabilität; Bradykinese oder Akinese; Tremor 2018-09
klinisch-neurologischen Befunde eines Morbus Parkinson: rigid-akinetisches (hypokinetisches) Syndrom; Pillendreher-Tremor 2018-11
Zeichen des inzipienten Parkinsonsyndroms im neurolog. Status: mangelndes Mitbewegen der rechten OE beim Gehen; geringer Rigor der rechten OE bei kontralateraler Aktivierung; Verlangsamung der komplexen Bewegungen (360° Drehung auf der Liege) 2013-09 etc.Zeichen in der neurologischen Untersuchung für Parkinsonsyndrom: rigide Tonuserhöhung rechts; mangelndes Mitbewegen der rechten oberen Extremität beim Gehen; angedeutetes Maskengesicht 2021-11

# einer 34-jährigen Patientin besteht eine 
### Bei einer 34-jährigen Patientin besteht eine bilaterale Opticus-Neuritis sowie eine langstreckige zervikale transverse Myelitis. Welche Antikörperdiagnostik veranlassen Sie zur
weiteren Abklärung?
#card #neurology #anki
Antikörper gegen Aquaporin 4

# 24-jähriger Patient kommt in die Sprechst
### Ein 24-jähriger Patient kommt in die Sprechstunde wegen einer seit Jahren bestehenden
Gangunsicherheit und Stolpern im Sportunterricht. In der neurologischen Untersuchung
fällt eine milde skandierende Dysarthrie auf. Es finden sich Hohlfüße beidseits, ein reduzierter Muskeltonus mit abgeschwächten Muskeleigenreflexen an allen Extremitäten
ohne Pyramidenbahnzeichen sowie eine Koordinationsstörung im Knie-Hacke-Versuch und
im Finger-Nase-Versuch. Laut einem Begleitbrief bestünden eine Kardiomyopathie und
Diabetes mellitus. Wie lautet die wahrscheinlichste Diagnose?
#card #neurology #anki
Friedreich-Ataxie

# 85-jährige Patientin mit bekanntem zereb
### Eine 85-jährige Patientin mit bekanntem zerebral metastasiertem Mammakarzinom wird
reglos aufgefunden und an die Notfallambulanz gebracht. Sie beobachten Myoklonien links
perioral und an der linken oberen Extremität. In einer Computertomographie des Schädels
stellt sich eine vorbekannte kortikale Metastase rechts am Sulcus centralis mit umgebendem
Ödem dar. Welche der folgenden Untersuchungen (unter welcher Differenzialdiagnose) ist
am ehesten zu veranlassen?
#card #neurology #anki
EEG (Status epilepticus)

# bislang neurologisch unauffällige 30-jähr
### Die bislang neurologisch unauffällige 30-jährige Frau G. berichtet über Muskelschwäche und
Parästhesien in allen vier Extremitäten. Initial seien nur die Finger und Zehen betroffen
gewesen, dann hätten sich die Symptome binnen weniger Tage nach proximal „ausgebreitet“.
Sie stellen ein milde Tetraparese, allseits fehlende Muskeleigenreflexe und einen schlaffen
Muskeltonus fest. Wo ist die für diese Symptomatik verantwortliche Läsion am ehesten zu
vermuten?
#card #neurology #anki
Nervenwurzeln

# stellen bei einer 32-jährigen, adipösen P
### Sie stellen bei einer 32-jährigen, adipösen Patientin die Diagnose eines Pseudotumor
cerebri (Benigne Intracranielle Hypertension). Welche der folgenden (nicht-chirurgischen)
therapeutischen Maßnahmen kommen bei diesem Krankheitsbild zum Einsatz?
(3 richtige Antworten)
#card #neurology #anki
Gewichtsreduktion
Wiederholte LiquorpunktionenAcetazolamid 

# 67-jähriger Patient klagt über zunehmende
### Ein 67-jähriger Patient klagt über zunehmendes Zittern der Hände seit Jahren. Sein Vater
habe ebenfalls gezittert. Der Patient trinkt manchmal Alkohol; dieser würde das Zittern
positiv beeinflussen. In der neurologischen Untersuchung zeigt sich ein Haltetremor beidseits
(kein Ruhetremor), sonst keine weiteren Defizite. Der Patient nimmt keine Medikamente
ein. Im Labor finden sich ebenfalls keine Auffälligkeiten. Wie lautet die wahrscheinlichste
Diagnose?
#card #neurology #anki
Essentieller Tremor

# werden zu einem soporösen und hochfiebern
### Sie werden zu einem soporösen und hochfiebernden Patienten (39,8 °C) mit eindeutigen
Meningismus-Zeichen gerufen. Im Vorfeld bestand eine Mastoiditis. Labordiagnostisch zeigen
sich im Blut 26.000 Leukozyten und ein deutlich erhöhtes CRP von 20 mg/dl (Referenzwert
&lt;0,5 mg/dl). In einer bereits durchgeführten Computertomografie des Schädels fallen
deutliche Hinweise auf einen erhöhten Hirndruck auf. Was veranlassen Sie als unmittelbaren
nächsten Schritt?
#card #neurology #anki
Gabe von Antibiotika und Cortison

# 59-jährige Patientin berichtet, dass es 
### Eine 59-jährige Patientin berichtet, dass es seit einigen Monaten zu einem „krampfartigen
Verziehen“ des Kopfes zur linken Seite käme. Sie könne diese Schiefhaltung vorübergehend
unterbrechen, indem sie sich mit der Hand an die Wange fasse. Der übrige neurologische
Status ist unauffällig, ein bereits veranlasstes MRT des Schädels zeigt keine Pathologie.
Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Fokale Dystonie

# 25-jährige Patientin wird wegen einer be
### Eine 25-jährige Patientin wird wegen einer belastungsabhängigen Muskelschwäche diagnostisch abgeklärt. Es zeigen sich positive Antiköper gegen den Acetylcholin-Rezeptor.
Tensilon-Test und repetitive Stimulation sind ebenfalls positiv. Welche medikamentösen
Therapieoptionen stehen für die Behandlung dieser Erkrankung zur Verfügung?
(2 richtige Antworten)
#card #neurology #anki
Acetylcholin-Esterase-Inhibitoren Immunsuppressiva

# 59-jähriger Patient kommt nach erstmalige
### Ein 59-jähriger Patient kommt nach erstmaligem Krampfanfall in die Notfaufnahme. Die
Ehefrau berichtet, dass der Patient seit 1 Woche unter Kopfschmerzen leide. In den
letzten 2 Tagen sei er „verwirrt“ gewesen. Sie stellen eine Orientierungsstörung, einen
endlagigen Meningismus, diskrete Halbseitenzeichen rechts und eine Sprachstörung fest.
Nach Ausschluss entsprechender Kontraindikationen führen Sie eine Lumbalpunktion durch.
Der Liquor zeigt eine lymphozytäre Pleozytose, eine Eiweißerhöhung und einen Laktat-Anstieg. Welche Verdachtsdiagnose ist am wahrscheinlichsten?
#card #neurology #anki
Herpesencephalitis 

# 70-jährige Patientin mit rechtshirnigem 
### Eine 70-jährige Patientin mit rechtshirnigem Schlaganfall weist eine zentrale Facialisparese auf. Welchen Befund erwarten Sie im Bereich des Nervus facialis?
#card #neurology #anki
Hängender Mundwinkel links

# untersuchen eine Patientin mit rechtsseit
### Sie untersuchen eine Patientin mit rechtsseitiger Lumboischialgie, bei der ein lumbaler Bandscheibenvorfall mit Tangierung der Wurzel L4 rechts bekannt ist. Welche Befunde an der rechten unteren Extremität sind im neurologischen Status zu erwarten?
(2 richtige Antworten)
#card #neurology #anki
Parese für Hüftbeugung und Kniestreckung
Abgeschwächter/erloschener Patellarsehnenreflex

# he Symptome sind bei einer linkshirnigen 
### Welche Symptome sind bei einer linkshirnigen Läsion möglich? (2 richtige Antworten)
#card #neurology #anki
Hemiparese rechts
Hemihypästhesie rechts

# untersuchen einen Patienten, der mit der 
### Sie untersuchen einen Patienten, der mit der Verdachtsdiagnose „Schlaganfall“ in die Ambulanz gebracht wird. Der Patient scheint die linke Hälfte des Raumes sowie die linke Hälfte seines Körpers nicht wahrzunehmen, ist aber bei klarem Bewusstsein und weist keine Visusstörung auf. Wie lautet die zutreffendste Einschätzung?
#card #neurology #anki
Neglect

# untersuchen eine 62-jährige Patientin in 
### Sie untersuchen eine 62-jährige Patientin in der Spezialambulanz für Bewegungsstörungen. Sie stellen ein hypokinetisch-rigides Syndrom fest. Zusätzlich bestehen positive Pyramidenbahnzeichen und Hinweise auf eine Kleinhirnfunktionsstörung. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Atypisches Parkinsonsyndrom

# untersuchen eine Patientin mit Rückenschm
### Sie untersuchen eine Patientin mit Rückenschmerzen, die in das rechte Bein ausstrahlen. Welche der folgenden Untersuchungsbefunde machen eine akute weitere Diagnostik notwendig?
(3 richtige Antworten)
#card #neurology #anki
Harnverhalt
Gefühlsstörung im IntimbereichParese des rechten Beines
Lumboischialgie rechts mit Diskusprolaps auf Höhe L5/S1 rechts; worauf im Status genau achten? Hochgradige Großzehenheberschwäche, Blasen- und Mastdarmstörung, Reithosenhypästhesie 2020-09

# 70-jähriger Patient stellt sich in der ne
### Ein 70-jähriger Patient stellt sich in der neurologischen Ambulanz vor, nachdem er am Vorabend plötzlich einen Schwindel mit Übelkeit, eine ausgeprägte Gangstörung und eine undeutliche Sprache bemerkt hat. Er sei zunächst schlafen gegangen. Nachdem die Symptome aber am nächsten Morgen unverändert bestanden hätten, habe er doch die Rettung angerufen. Anamnestisch lässt sich Diabetes mellitus, eine arterielle Hypertonie, ein Z. n. Myokardinfarkt und ein Z. n. Hörsturz vor vielen Jahren erheben. Welche Diagnose ist am wahrscheinlichsten?
#card #neurology #anki
Ischämischer Insult im hinteren Stromgebiet

# einem 50-jährigen Patienten bestehen rech
### Bei einem 50-jährigen Patienten bestehen rechtsseitig eine Parese der langen Fingerbeuger IV und V, eine Parese und Atrophie der kleinen Handmuskeln, eine Krallenstellung der Finger IV und V, ein positives Fromentzeichen sowie folgende (auf dem Bild rot markierte) Sensibilitätsstörung:
Wie lautet die zutreffendste Diagnose? Läsion des/der
![[Media/Anki/Neurologie/media/paste-28126241a8bdf65c9788786c14889cfd4db210e1.jpg]]
#card #neurology #anki
N. ulnaris

# 26-jährige Frau Adele B., eine Polizeisch
### Die 26-jährige Frau Adele B., eine Polizeischülerin, wird von ihrem Hausarzt an die neurologische Akutambulanz verwiesen. Sie erzählt von ihrer 2-jährigen Krankengeschichte. Vor einem Jahr bemerkte sie eine Sehstörung am rechten Auge im Sinne eines Verschwommen-Sehens. Die Beschwerden bildeten sich spontan nach wenigen Tagen zurück. Vor zwei Tagen trat eine Gefühlsstörung im rechten Bein auf, die sich bis dato nicht vollständig zurückbildete. Welche der unten genannten Differenzialdiagnosen ziehen Sie am ehesten in Betracht?
(2 richtige Antworten)
#card #neurology #anki
Multiple Sklerose
Neuromyelitis Optica Spektrum Erkrankung

# 23-jährige Patientin, die einen Bewussts
### Eine 23-jährige Patientin, die einen Bewusstseinsverlust unklarer Ursache erlitten hat, stellt sich zur Befundbesprechung ihres EEG-Befundes vor. Welche der folgenden Befundergebnisse sprechen für einen Normalbefund?
(3 richtige Antworten)
#card #neurology #anki
Alphagrundrhythmus beidseits parieto-occipitalBeidseits frontale Lidschlagartefakte
Betatätigkeit über den vorderen Hirnregionen

# 76-jährige Patientin kommt in Begleitung
### Eine 76-jährige Patientin kommt in Begleitung ihrer Tochter in die Ambulanz. Die Tochter berichtet, dass bei der Patientin seit etwa 2 Jahren eine progrediente Störung des episodischen Gedächtnisses bestehe. In den Alltagsfunktionen sei die Patientin zunehmend auf Hilfe angewiesen. Ein Schädel-MRT zeigte eine rechtsbetonte Hippokampusatrophie. Im MMSE (Mini-Mental-State-Examination) erreicht die Patientin 18 Punkte. Im Übrigen ist der neurologische Status unauffällig. Es bestehen keine wesentlichen Vorerkrankungen. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Demenz vom Alzheimer-Typ

# 37-jähriger Mann leidet seit 1 1/2 Jahren
### Ein 37-jähriger Mann leidet seit 1 1/2 Jahren unter einer distal betonten Schwäche (Kraftgrad 4) in den Beinen und Missempfindungen in den Extremitäten. Die Muskeleigenreflexe fehlen. Es bestehen eine Pallhypästhesie sowie eine Hypästhesie und Allodynie in folgenden (auf dem Bild rot markierten) Arealen. Die Nervenleitgeschwindigkeitsuntersuchung zeigt eine verlängerte distal motorische Latenz, eine herabgesetzte motorische Nervenleitgeschwindigkeit, fehlende F-Wellen und einen partiellen motorischen Leitungsblock. Das Liquor-Eiweiß ist erhöht, die Liquor-Zellzahl ist normal. Wie lautet die wahrscheinlichste Diagnose?
![[Media/Anki/Neurologie/media/paste-0e2080850f5ec6be2a1165ffd21399ff499f40ee.jpg]]
#card #neurology #anki
Chronisch inflammatorische demyelinisierende Polyneuropathie

# einer 46-jährigen Patientin zeigen sich b
### Bei einer 46-jährigen Patientin zeigen sich bei der Aufnahme bei Beginn der Symptomatik vor 4 Stunden Zeichen eines Neglects nach links und eine mittelgradige Hemiparese links. Die MR-Angiographie zeigt einen fast vollständigen Verschluss der proximalen Arteria cerebri media rechts. Was soll therapeutisch geschehen?
#card #neurology #anki
Mechanische Thrombektomie
Symptomatik Beginn vor 6h: Azetylsalizylsäure 2011-07

# 47-jährige Patientin stellt sich wegen l
### Eine 47-jährige Patientin stellt sich wegen langjähriger Kopfschmerzen vor, die sich in den letzten fünf Jahren nicht geändert hätten. Sie berichtet über maximal 3–4 kopfschmerzfreie Tage pro Monat. Meist nimmt sie Kopfschmerzen im ganzen Kopf wahr, die Schmerzen seien drückend, leicht oder mittelstark. Abgesehen von geringer Lärmempfindlichkeit gäbe es keine Begleiterscheinungen. Zwei- oder dreimal pro Monat wache sie mit sehr heftigen Kopfschmerzen auf und sie müsse innerhalb einer Stunde erbrechen. Im Sommer würden die heftigen Kopfschmerzen nicht auftreten. Bei diesen Schmerzen ist sie zwei Tage lang arbeitsunfähig. Die ständigen Kopfschmerzen behandle sie fast immer mit Ibuprofen. Bei den starken Kopfschmerzen nimmt sie bis zu 4 Tabletten Aspirin, das aber eigentlich nicht helfen würde. Welche Diagnosen liegen vor?
(3 richtige Antworten)
#card #neurology #anki
Migräne ohne Aura Spannungskopfschmerz Analgetikabedingter Kopfschmerz

# 69-jähriger Patient wird mit einer seit 2
### Ein 69-jähriger Patient wird mit einer seit 2 Tagen bestehenden Halbseitensymptomatik rechts in der Ambulanz vorstellig. Die CCT zeigt einen markierten Mediateilinsult rechts. In der Ultraschalluntersuchung der Halsgefäße findet sich eine 80%ige Stenose der rechten Arteria carotis interna. Welche Schlaganfall-Ursache ist die wahrscheinlichste?
#card #neurology #anki
Arterio-arterielle Embolie

# 34-jährige Patientin mit einem seit zwei
### Eine 34-jährige Patientin mit einem seit zwei Jahren bekannten triple negativem Mammakarzinom links (invasives duktales Karzinom, HER2neu neg., ÖR 0 %, PR 0 %; Zustand nach Mastektomie, Strahlentherapie und systemischer Chemotherapie) präsentiert sich sechs Monate nach Therapieende aufgrund von subakuten Kopfschmerzen, Doppelbildern beim Blick nach rechts und einem rechtsseitigen Mundastdefizit in der neurologischen Ambulanz. Der Augenschluss ist beidseits komplett möglich. Welche Differentialdiagnose ist am wahrscheinlichsten?
#card #neurology #anki
Meningeosis carcinomatosa

# 62-jähriger Mann berichtet, in letzter Ze
### Ein 62-jähriger Mann berichtet, in letzter Zeit gehäuft zu stolpern („schwache Beine“). Er habe das Gefühl, seine Muskeln würden „an Masse abnehmen“. Bei der Untersuchung stellen Sie eine distal betonte spastische Parese des rechten Beines, gesteigerte Muskeleigenreflexe, positive Pyramidenbahnzeichen, Faszikulationen und Atrophien im Bereich der Extremitätenmuskulatur fest. Wo ist die Ursache der Symptomatik am ehesten lokalisiert?
#card #neurology #anki
Erstes und zweites Motoneuron

# komatöser Patient, der laut Angehörigen i
### Ein komatöser Patient, der laut Angehörigen im Vorfeld heftigste, akut einsetzende Kopf- schmerzen beklagt hat, wird an die Notaufnahme gebracht. Die CCT zeigt folgendes Bild. Wie lautet die wahrscheinlichste Diagnose?
![[Media/Anki/Neurologie/media/paste-58a81edfb38fd0525f7e160cae9f8b1b07b9b626.jpg]]
#card #neurology #anki
Subarachnoidalblutung

# 53-jähriger Patient mit bislang unauffäll
### Ein 53-jähriger Patient mit bislang unauffälliger somatischer Anamnese leidet seit einigen Wochen unter einer ausgeprägten Schiefhaltung von Kopf und Hals, die er mit einer antagonistischen Geste vorübergehend unterbrechen kann. Eine erweiterte Durchuntersuchung inklusive zerebraler Bildgebung ist unauffällig. Welche Behandlung ist bei diesem Krankheitsbild die Therapie der ersten Wahl?
#card #neurology #anki
Botulinum-Toxin-Injektionen

# 20-jähriger Patient stellt sich in der ne
### Ein 20-jähriger Patient stellt sich in der neurologischen Ambulanz vor. Er berichtet von zunehmender motorischer Unruhe, überschießenden Bewegungen sowie zunehmendem Grimassieren. Er habe Probleme sich zu konzentrieren, manchmal fühle er sich bedroht und verfolgt. In der Anamnese stellt sich heraus, dass sein Vater „psychische Probleme mit 40 Jahren bekommen“ hätte, die Familie früh verlassen habe und „früh gestorben“ sei. An welche Diagnose denken Sie primär?
#card #neurology #anki
Chorea Huntington

# 22-jährige Patientin mit belastungsabhän
### Eine 22-jährige Patientin mit belastungsabhängiger Muskelschwäche wird diagnostisch abgeklärt. Die Auto-Antikörper gegen den Acetylcholin-Rezeptor sind positiv. Tensilon-Test und repetitive Stimulation sind ebenfalls positiv. Welche chirurgische Therapieoption steht für die Behandlung dieser Erkrankung zur Verfügung?
#card #neurology #anki
Thymektomie

# 21-jährige Herr S. leidet unter massiver 
### Der 21-jährige Herr S. leidet unter massiver Tagesmüdigkeit. Zusätzlich berichtet er, dass er mehrfach, als er sich erschreckt habe oder lachen musste, kurz „zusammengesackt“ sei („also ob die Muskeln plötzlich ganz schlaff wären“). In der Früh nach dem Aufwachen könne er sich manchmal für kurze Zeit „kaum bewegen“. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Narkolepsie

# 51-jährige Frau mit Vigilanzminderung un
### Eine 51-jährige Frau mit Vigilanzminderung und Fieber (39,5 Grad Celsius) wird in die Notfallambulanz gebracht. Laborchemisch zeigen sich erhöhte Infektparameter (Leukozyten 25.000/l; CRP 14 mg/dl), die Gerinnungswerte sind unauffällig. Sie nehmen Blutkulturen ab. Die Patientin ist soporös, es bestehen positive Meningismuszeichen. Fokal-neurologische Defizite finden sich nicht. In den Tagen zuvor litt die Patientin unter Ohren- und Kopfschmerzen und Husten. Was müssen Sie vor Durchführung eines CCTs veranlassen?
#card #neurology #anki
Gabe von Cortison + Antibiotikum

# stellen bei einer 38-jährigen Patientin e
### Sie stellen bei einer 38-jährigen Patientin eine Paraparese und ein sensibles Niveau bei Th
10 fest. Die Ultraschalluntersuchung der Blase zeigt große Restharnmengen. Wo ist die für
diese Symptomatik verantwortliche Läsion am ehesten zu vermuten?
#card #neurology #anki
Rückenmark

# he Symptome sind bei einer rechtsseitigen
### Welche Symptome sind bei einer rechtsseitigen Läsion der Pyramidenbahn möglich?
(2 richtige Antworten) (4)
#card #neurology #anki
Sensomotorische Halbseitensymptomatik links
Positive Pyramidenbahnzeichen links
Untersuchungsbefunde einer Pyramidenbahnläsion: gesteigerte Muskeleigenreflexe, Spastik 2019-11

# 25-jähriger Patient mit Anfällen seit dem
### Ein 25-jähriger Patient mit Anfällen seit dem 19. Lebensjahr wird ins Video-EEG-Monitoring zugewiesen. Es wird die Diagnose einer mesialen Temporallappenepilepsie
rechts gestellt (rechts-temporaler Anfallsursprung, interiktale Spikes rechts temporal, Hippocampusatrophie rechts). Worüber klären Sie den Patienten auf?
(2 richtige Antworten)
#card #neurology #anki
Sie sind prinzipiell ein geeigneter Kandidat für einen epilepsiechirurgischen Eingriff.
Sie leiden an einem Epilepsiesyndrom, bei dem durch Antiepileptika oft keine Anfallsfreiheit erzielt werden kann.

# einer 63-jährigen Patientin besteht seit 
### Bei einer 63-jährigen Patientin besteht seit etwa einem halben Jahr ein Ruhetremor
der linken Hand. Sie könne ihrem Hobby, dem Stricken, nicht mehr nachgehen, da sie
die Bewegungen nicht richtig ausführen könne. Die Schrift sei unleserlich geworden.
Zusätzlich berichtet sie über Obstipation. Bis auf eine Depression, die seit etwa 4 Jahren
mit einem selektiven Serotonin-Wiederaufnahmehemmer behandelt wird, und eine milde
arterielle Hypertonie bestehen keine weiteren Krankheiten. Wie lautet die wahrscheinlichste
Diagnose?
#card #neurology #anki
Idiopathisches Parkinsonsyndrom

# begutachten um 8:30 Uhr morgens einen 69-
### Sie begutachten um 8:30 Uhr morgens einen 69-jährigen Patienten, der mit der Rettung in
die Ambulanz kommt. Der Patient berichtet, um 7:00 mit einer hochgradigen Schwäche der
rechten Hand, einem rechtsseitig hängenden Mundwinkel und einer Sprachstörung erwacht
zu sein. Bis auf eine arterielle Hypertonie (Therapie mit Amlodipin 5 mg/Tag, sonst keine
Medikamente) und einen langjährigen Nikotinabusus sind keine Vorerkrankungen bekannt.
Sie vermuten einen Schlaganfall. Der Oberarzt fragt Sie nun: Handelt es sich dabei um eine
zerebrale Blutung oder um eine zerebrale Ischämie? Was ist die zutreffendste Antwort?
#card #neurology #anki
Das ist nur durch eine CCT- oder MRT-Untersuchung zu unterscheiden.

# 52-jährige Patientin mit seit ca. 5 Jahr
### Eine 52-jährige Patientin mit seit ca. 5 Jahren bekanntem Diabetes mellitus Typ II berichtet
über subakut (vor ca. 3 Wochen) aufgetretene Schmerzen, Schwäche und Sensibilitätsstörungen im Bereich des linken proximalen Oberschenkels. Klinisch imponiert eine Atrophie
und Schwäche des M. quadriceps links sowie eine diffuse Hypästhesie im Oberschenkelbereich links. Die Muskeleigenreflexe fehlen. Die Elektroneurographie zeigt ein axonales
Schädigungsmuster des N. femoralis links ohne Hinweis für das Vorliegen einer relevanten
Polyneuropathie. Im Elektromyogramm zeigen sich chronisch neurogene Veränderungen
ohne frische Denervierungszeichen. Wie lautet die wahrscheinlichste Diagnose?
#card #neurology #anki
Diabetische Amyotrophie

# B. klagt über eine Schwäche in der recht
### Frau B. klagt über eine Schwäche in der rechten Hand. Sie stellen eine Parese der kleinen
Handmuskulatur rechts (für Abduktion und Adduktion der Finger) fest. Der Tricepssehenreflex ist rechtsseitig abgeschwächt. Es bestehen Schmerzen und eine Hypästhesie in folgenden
(auf dem Bild rot markierten) Hautarealen. Wie lautet die naheliegendste Diagnose?
![[Media/Anki/Neurologie/media/paste-b15bd12fb712bec92615c27a17360facba185ea9.jpg]]
#card #neurology #anki
Radikuläres Syndrom C8

# der 51-jährigen Frau H. ist seit fünf Jah
### Bei der 51-jährigen Frau H. ist seit fünf Jahren eine seropositive Myasthenia gravis bekannt. Bei bislang unkompliziertem Verlauf unter einer Therapie mit Pyridostigmin und
Azathioprin kommt es nun seit einigen Tagen zu einer krisenhaften Verschlechterung der
Symptomatik mit zunehmenden Schluckbeschwerden und Kurzatmigkeit. Welche Therapiemöglichkeiten stehen in der aktuellen Situation zur Verfügung?
(3 richtige Antworten)
#card #neurology #anki
Intravenöse Immunglobuline
Plasmapherese
Immunadsorption

# 72-jähriger Patient wird nach einem epile
### Ein 72-jähriger Patient wird nach einem epileptischen Anfall an die Klinik gebracht. Er ist
postiktal verwirrt und kann keine Auskunft geben. Der Sohn berichtet, dass der Patient
vor 5 Jahren die Diagnose eines Hirntumors erhalten hat. Der Tumor wurde operiert, dann
sei eine Chemo- und Strahlentherapie erfolgt. Genauere Informationen könne er leider
nicht liefern. Zu Kontroll-Untersuchungen sei sein Vater in den letzten 2 Jahren nicht
mehr gegangen. In einem akut durchgeführten Schädel-MRT sehen Sie zwei inhomogen
Kontrastmittel aufnehmende Läsionen mit zentralen Nekrosen in der Nähe der alten
Resektionsstelle im rechten Frontallappen. Welche Diagnose wurde bei dem Patienten vor
5 Jahren wahrscheinlich gestellt?
#card #neurology #anki
Anaplastisches Astrozytom

# 59-jährige Patientin wird nach einem epi
### Eine 59-jährige Patientin wird nach einem epileptischen Anfall an die Notaufnahme gebracht.
Im Vorfeld habe die Patientin seit ca. 1 Woche unter starken, Kopfschmerzen, leicht
erhöhter Temperatur und „Nackenverspannungen“ gelitten. In den letzten Tagen hätte
ihre Familie außerdem eine Wesensveränderung bemerkt. Bei der Erstuntersuchung ist die
Patientin bewusstseinsgetrübt und meningeal. Die Liquordiagnostik ergibt eine vorwiegend
lymphozytäre Pleozytose (570 Drittelzellen), eine deutliche Eiweißerhöhung und ein etwas
erhöhtes Laktat. Die Liquorglukose ist normal. In der Schädel-MRT finden sich folgende
entzündliche Läsionen:
![[Media/Anki/Neurologie/media/paste-74ddb858ae53f526651875ccca38ae5c31061d80.jpg]]
#card #neurology #anki
Herpesenzephalitis

# 41-jähriger Patient stellt sich wegen ein
### Ein 41-jähriger Patient stellt sich wegen einer Bewegungsarmut vor. In der neurologischen
Untersuchung fallen ein Rigor im linken Handgelenk, ein Ruhetremor sowie eine Bradykinese
auf. Zudem zeigt sich eine erstarrte mimische Muskulatur. Welche Differentialdiagnosen
ziehen Sie am ehesten in Betracht?
(2 richtige Antworten)
#card #neurology #anki
Morbus Wilson
Juveniles idiopathisches Parkinsonsyndrom

# 78-jährige Herr A. wird wegen einer Gangs
### Der 78-jährige Herr A. wird wegen einer Gangstörung in der neurologischen Ambulanz
vorstellig. Klinisch zeigen sich Beeinträchtigungen von Antrieb, Konzentration und Aufmerksamkeit, ein kleinschrittiges, breitbasiges, verlangsamtes Gangbild sowie eine Blasenstörung.
Die zerebrale Computertomographie zeigt erweiterte innere Liquorräume ohne Hinweise
auf eine Liquorabflussstörung. Wie lautet Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Normaldruckhydrocephalus

# 42-jährige Frau stellt sich in der neuro
### Eine 42-jährige Frau stellt sich in der neurologischen Ambulanz wegen Gangschwierigkeiten
vor. Bei der Untersuchung wird festgestellt, dass es der Patientin schwer fällt auf einer
geraden Linie zu gehen. Weiteres zeigt sich eine Ataxie im Knie-Hacke und Finger-Nase
Versuch links. Wo liegt am ehesten eine Funktionsstörung vor?
#card #neurology #anki
Störung des Cerebellums

# 30-jährige Frau wird erstmals in der neu
### Eine 30-jährige Frau wird erstmals in der neurologischen Ambulanz vorstellig aufgrund
einer seit 8 Wochen progredienten und distal betonten Tetraparese. Welche der folgenden
Auffälligkeiten in der neurologischen Untersuchung würde für eine zentrale Genese der
Tetraparese sprechen?
#card #neurology #anki
Positives Babinski-Zeichen beidseits

# 75-jähriger Patient wird mit der Rettung 
### Ein 75-jähriger Patient wird mit der Rettung in der Notaufnahme vorgestellt. Er hat
vor 2 Stunden eine plötzliche Bewusstseinsstörung mit Benommenheit entwickelt. In der
neurologischen Untersuchung fällt eine Einschränkung der vertikalen Augenbewegungen
und eine Merkfähigkeitsstörung auf. Im CT vom Kopf und in der CT Angiographie finden
sich keine Auffälligkeiten. Zur weiteren Klärung wird noch ein akut MRT durchgeführt,
das einen Befund eines akuten, bilateralen, paramedianen Infarktes des Thalamus zeigt.
Bei dem Verschluss welches Gefäßes kann es zu diesem Infarkt kommen?
#card #neurology #anki
Percheron-Arterie

# he der folgenden Erkrankungen können am e
### Welche der folgenden Erkrankungen können am ehesten akut einsetzende neurologische
Ausfälle verursachen?
(3 richtige Antworten)
#card #neurology #anki
Epilepsie
Migräne
Intrakranielle Blutungen

# 68-jähriger Mann wird akut in der Notfall
### Ein 68-jähriger Mann wird akut in der Notfallambulanz vorstellig, da er seit wenigen
Stunden nicht mehr aufstehen könne. In der sofort durchgeführten MRT der Wirbelsäule
findet sich eine spinale Ischämie auf Höhe Th8/9 im Versorgungsgebiet der Arteria spinalis
anterior. Welcher der folgenden Befunde ist in der neurologischen Untersuchung zum
Zeitpunkt der Erstvorstellung zu erwarten?
#card #neurology #anki
Paraparese der unteren Extremitäten

# 46-jähriger Bauarbeiter hat seit 6 Monate
### Ein 46-jähriger Bauarbeiter hat seit 6 Monaten Schmerzen im unteren Rückenbereich, die
in die Rückseite seines linken Beins ausstrahlen, sowie Parästhesien am lateralen Fußrand
links. In der Untersuchung zeigt sich ein abgeschwächter Achillessehnenreflex links. Kraft,
Darm- und Blasenfunktion sind normal. Wo befindet sich die Läsion am ehesten?
#card #neurology #anki
Nervenwurzel S1 links

# untersuchen einen 43-jährigen Patienten m
### Sie untersuchen einen 43-jährigen Patienten mit der Zuweisungsdiagnose „Schwindelattacken
seit 4 Tagen“. Diese treten nur bei Bewegung auf, vor allem beim Aufrichten im Bett oder
beim Niederlegen. Zwischen diesen Attacken ist der Patient weitestgehend beschwerdefrei,
die Attacken selbst dauern einige Sekunden. In der klinisch neurologischen Untersuchung
können Sie den Schwindel durch Kopfdrehungen im Liegen provozieren, der Patient gibt
dabei eine plötzlich auftretende starke Übelkeit an und erbricht einmalig. Die neurologische
Untersuchung zeigt keine Herd- oder Halbseitenzeichen, auch die Geh- und Stehversuche
sind unauffällig. Wie lautet die wahrscheinlichste Differenzialdiagnose?
#card #neurology #anki
Benigner paroxysmaler Lagerungsschwindel

# unterhalten sich mit einem 64-jährigen Pa
### Sie unterhalten sich mit einem 64-jährigen Patienten, der vor 6 Monaten einen Schlaganfall
erlitt. Er fragt Sie nach Verhaltensweisen und Maßnahmen, mit denen er sein Folgerisiko
für weitere Schlaganfälle minimieren kann. Was antworten Sie ihm?
(3 richtige Antworten)
#card #neurology #anki
Ausreichend Bewegung gemäß Empfehlungen der WHO
Optimierung des Lipid- und Triglyceridspiegels im Blut
Tägliche Einnahme von 100mg Acetylsalicylsäure

# untersuchen eine 24-jährige Patientin. Si
### Sie untersuchen eine 24-jährige Patientin. Sie ist Fliesenlegerin von Beruf und berichtet,
dass sie seit einem langen Arbeitstag vor 2 Tagen den rechten Fuß nicht mehr anheben kann.
In der klinisch neurologischen Untersuchung stellen Sie eine Fußheberschwäche rechts fest,
es besteht auch eine Hypästhesie des Vorfußes im Bereich zwischen erster und zweiter Zehe.
Die sonstige klinisch neurologische Untersuchung ist unauffällig. Was ist die zutreffendste
Diagnose?
#card #neurology #anki
Peronaeusparese rechts

# 68-jähriger Mann wird wegen zunehmender V
### Ein 68-jähriger Mann wird wegen zunehmender Vergesslichkeit in der neurologischen Ambulanz vorstellig. Nach Angaben seiner Frau kommt es seit 5 oder 6 Jahren schleichend zu
einer Verschlechterung des Gedächtnisses. Der Patient hat Schwierigkeiten, sich an kürzlich
zurückliegende Ereignisse zu erinnern. Er vergisst zum Beispiel Termine sowie Unterhaltungen und hat ebenfalls vergessen, dass ein naher Verwandter vor kurzem gestorben ist.
Er ist nicht mehr in der Lage, sein eigenes Konto zu führen oder sein Auto zu bedienen,
ohne sich zu verfahren. Im neurologischen Status zeigen sich keine fokal-neurologischen
Ausfälle. Eine MRT Untersuchung des Gehirns zeigt eine leichte zerebrale Atrophie. Welche
der folgenden Diagnosen ist am wahrscheinlichsten?
#card #neurology #anki
Alzheimer-Krankheit

# 55-jährige Frau kommt in die neurologisc
### Eine 55-jährige Frau kommt in die neurologische Ambulanz und berichtet, dass sie vor
4 Monaten einen Schlaganfall erlitt und seither nicht mehr selbständig mobil sei. Aus
den mitgebrachten Befunden geht hervor, dass es damals zu einem ausgedehnten ischämischen Infarkt im Stromgebiet der Arteria cerebri media links kam. Welche der folgenden
Auffälligkeiten im neurologischen Status sind am ehesten zu erwarten?
#card #neurology #anki
Hemiparese rechts mit gesteigerten Muskeleigenreflexen rechts und spastischem Muskeltonus rechtsseitig

# L. leidet seit 1 ½ Jahren an leichten ko
### Frau L. leidet seit 1 ½ Jahren an leichten kognitiven Problemen der Konzentration und der
Aufmerksamkeit. Abends habe sie gelegentlich visuelle Verkennungen und das Gefühl, es sei
eine Person im Zimmer oder hinter ihr. Tagsüber schläft Frau L. häufig ein. Aufgrund von
Schlafstörungen und abendlicher Unruhe erhält sie von ihrem Arzt eine neue Medikation.
Nach Einnahme dieser neuen Medikation kommt es innerhalb von einigen Tagen zu einer
deutlichen Verschlechterung der eingangs erwähnten Symptomatik und zusätzlich zu einer
Bewegungsstörung, sodass die Patientin bettlägrig wird. Nach Absetzen der Medikation
bessert sich der Zustand allmählich wieder und es wird vom Neurologen die Diagnose
einer Lewy-Körperchen Demenz gestellt. Welche der folgenden Therapien hat bei Frau L.
am ehesten zu den starken Nebenwirkungen geführt und sollte in diesen Fällen generell
vermieden werden?
#card #neurology #anki
Klassische Neuroleptika

# P. bemerkt mit jetzt 57 Jahren seit ca. 
### Herr P. bemerkt mit jetzt 57 Jahren seit ca. 3 Jahren eine reduzierte Geruchswahrnehmung
und seit einem ½ Jahr eine gedrückte Stimmung. Seit 2–3 Monaten kommt es zu einem Zittern der linken Hand. Die linke Schulter würde schmerzen, doch orthopädische Abklärungen
haben keine Ursache der Beschwerden finden können. Die Schrift bei Rechtshändigkeit habe
sich nicht verändert. Die Ehefrau berichtet, dass Herr P. seit über 5 Jahren nachts im Schlaf
redet und gelegentlich mit den Beinen strampelt. Im weiteren Verlauf wird die Diagnose
eines Morbus Parkinson gestellt und der Patient medikamentös behandelt. Welches der
beschriebenen Symptome fällt in diesem Fall als untypisch auf und sollte daher weiter
abgeklärt werden?
#card #neurology #anki
Keines der oben genannten
Gedächtnisprotokoll SIP5a 2021-06 - restliche Antwortmöglichkeiten:
Reduzierte GeruchswahrnehmungSchulterschmerzenRestless legsDepression

# 51-jährige Frau Anita G. stellt sich in d
### Die 51-jährige Frau Anita G. stellt sich in der Sprechstunde der neurologischen Gedächtnisambulanz vor. Sie erheben folgende Anamnese: Seit 2 Jahren besteht eine langsam
progrediente Vergesslichkeit, Wortfindungsstörungen und eine Apathie. Durch den anwesenden Sohn erfahren Sie, dass die Patientin in den Aktivitäten des täglichen Lebens
eingeschränkt ist. Sie benötigt Hilfe beim Essen und schafft es auch nicht mehr, sich
alleine komplett anzuziehen. Darüber hinaus berichtet der Sohn über eine Veränderung
der Persönlichkeit, seine Mutter scheint oftmals auffällig aggressiv und steckt sich oft
Gegenstände im Haushalt in den Mund. Die Familienanamnese ist positiv: Die Großmutter
der Patientin erhielt im Alter von 75 eine Demenzdiagnose, die Mutter der Patientin ist
im Alter von 53 Jahren verstorben. Die neurologische Untersuchung zeigt bis auf die oben
genannten neurokognitiven bzw. neuropsychologischen Defizite keine Auffälligkeiten. Was
ist die wahrscheinlichste Verdachtsdiagnose?
#card #neurology #anki
Frontotemporale Demenz

# arbeiten in der Notaufnahme, als sich ein
### Sie arbeiten in der Notaufnahme, als sich ein 30-jähriger Mann mit Kopfschmerzen vorstellt,
die gestern begonnen haben. Als er Schnee schaufelte, spürte er einen plötzlichen frontalen
Kopfschmerz. Der Schmerz pocht nicht und ist seither relativ konstant. Er sagt, dass jetzt
auch sein Nacken ein wenig steif geworden ist. Er leidet unter einer bekannten Migräne, die
1x pro Monat auftritt, aber er meint, dass diese Kopfschmerzen anders als seine üblichen
Kopfschmerzen sind. Er ist afebril und hat einen normalen neurologischen Status mit
Ausnahme einer leichten Photophobie und einer schmerzhaften Nackensteifigkeit. Welche
der folgenden Optionen ist die geeignetste für das sofortige weitere Management?
#card #neurology #anki
Eine Bildgebung (CT) des Gehirns durchführen.

# 54-jährige Frau stellt sich in der neuro
### Eine 54-jährige Frau stellt sich in der neurologischen Ambulanz vor. Sie erzählt Ihnen, dass
sie seit Jahren an Missempfindungen in den Beinen leidet. Sie erheben eine strukturierte
Anamnese und erfahren, dass beinahe täglich und hauptsächlich abends bzw. nachts sensible
Missempfindungen an den Beinen auftreten, die mit Bewegungsdrang verbunden sind. Bei
körperlicher Bewegung bessern sich die Beschwerden, in Ruhe nehmen sie wieder zu. Welche
Diagnostik veranlassen Sie als nächsten Schritt?
(3 richtige Antworten)
#card #neurology #anki
Eisen- und Ferritinspiegel im Blut
Vitamin B12-Spiegel im Blut
Polysomnografie

# 38-jährige Frau wird mit hohem Fieber in
### Eine 38-jährige Frau wird mit hohem Fieber in der Notaufnahme vorgestellt. Seit dem
Vortag habe sie Kopfschmerzen und Fieber. Seit 2-3 Stunden würde sie bei Ansprache
teilweise nicht mehr reagieren oder nur stöhnen. Beim Transport im Rettungswagen habe
die Patientin zweimal erbrochen. Telephonisch berichten die Angehörigen, dass die Patientin
seit 3 Monaten zunehmend Schmerzen und Missempfindungen der rechten Hand der Finger
I bis III beklagt. In der neurologischen Untersuchung fällt neben der reduzierten Vigilanz,
Schmerz und ein Widerstand bei Nackenbeugung auf. Außerdem fallen kleine Einblutungen
der Haut (Petechien) auf. Nach rascher Einleitung einer antimikrobiellen Behandlung, und
CT des Kopfes zeigt sich der Liquor verfärbt und mit gramnegativen Kokken. Es wird die
Diagnose eines Meningokokkenmeningitis gestellt. Welches der genannten Symptome ist
jedoch für eine bakterielle Meningitis untypisch und sollte daher weiter abgeklärt werden?
#card #neurology #anki
Sensibilitätsstörung an der Hand

# 25-jähriger Mann stellt sich mit exzessiv
### Ein 25-jähriger Mann stellt sich mit exzessiver Tagesmüdigkeit in der neurologischen
Ambulanz vor. Er gibt an nachts gut zu schlafen, aber zu verschiedenen Tageszeiten Momente
zu haben, in denen er plötzlich einschläft. Bei der Arbeit macht er über längere Zeiträume
Nickerchen am Schreibtisch, bei Vorträgen schläft er immer ein. Er berichtet, dass er sogar
während des Autofahrens einen plötzlich Schlafanfall hatte, der zu einem Auffahrunfall
führte. Er dementiert, nachts zu schnarchen oder frühmorgendliche Kopfschmerzen zu haben.
Er gibt an keine Medikamente oder psychotrope Substanzen zu konsumieren. Er berichtet
weiter, dass seine Extremitäten schwach werden, wenn er aufgeregt ist, insbesondere, wenn
er stark lachen muss. Außerdem leide er an Halluzinationen kurz vor dem Einschlafen
und meint auch, dass er beim Aufwachen teilweise das Gefühl hat, sich nicht bewegen zu
können. Welche Therapien sind für die zugrundeliegende Erkrankung zugelassen?
(2 richtige Antworten)
#card #neurology #anki
Natriumoxybat
Modafinil

# 29-jähriger Mann wird mit der Rettung in 
### Ein 29-jähriger Mann wird mit der Rettung in die neurologische Notfallaufnahme eingeliefert.
Der Patient berichtet Ihnen von einer seit einer Woche subakut aufgetretenen Schwäche in
beiden Beinen. Sie können keine Muskeleigenreflexe an den unteren Extremitäten auslösen.
Sie vermuten richtigerweise ein Guillain-Barré-Syndrom. Mit welchen Untersuchungen
können Sie die Diagnose sichern?
(2 richtige Antworten)
#card #neurology #anki
Lumbalpunktion
Messung der Nervenleitgeschwindigkeit

# dem 72-jährigen Herrn B. ist vor 2 Jahren
### Bei dem 72-jährigen Herrn B. ist vor 2 Jahren ein malignes Melanom entfernt worden. Seit
3 Wochen beklagt er etwas linksbetonte Nackenschmerzen und seit wenigen Tagen bemerkt
er eine Schwäche des linken Beins und beim Duschen würde sich das warme Wasser am
rechten Bein kalt anfühlen. Die neurologische Untersuchung ergibt eine Hemiparese mit
kontralateraler Störung der Schmerz- und Temperaturempfindung. Wo ist die Läsion bei
dem Patienten zu erwarten und wie nennt man das Syndrom?
#card #neurology #anki
Halbsseitig Rückenmark = Brown-Séquard-Syndrom

# werden zu einer 38-jährigen Frau in der 2
### Sie werden zu einer 38-jährigen Frau in der 27. Schwangerschaftswoche gerufen, die einen
erstmaligen generalisierten epileptischen Anfall erlitten hat. Welche sind in dieser Situation
die wahrscheinlichsten Ursachen dieses Anfalls?
(2 richtige Antworten)
#card #neurology #anki
Sinusvenenthrombose
Eklampsie

# einem 73-jährigen Patienten wurde ein ini
### Bei einem 73-jährigen Patienten wurde ein initiales Parkinsonsyndrom vom Äquivalenztyp
festgestellt. Er berichtet erwartungsgemäß dazu, dass vor der Diagnosestellung folgendes
aufgefallen sei:
#card #neurology #anki
Fehlendes Mitschwingen der Arme beim Gehen

# der neurologischen Untersuchung berichtet
### Bei der neurologischen Untersuchung berichtet ein 72-jähriger Patient, dass rechtsseitig eine
Bewegungsverlangsamung aufgetreten sei, was auch die klinische Untersuchung bestätigt.
Sie vermuten ein incipientes Parkinsonsyndrom. Was suchen Sie nun weiter im Status?
Eine:
#card #neurology #anki
Hypodiadochokinese rechts

# einem 31-jährigen Mann trat ohne offensic
### Bei einem 31-jährigen Mann trat ohne offensichtlichen Grund ein generalisierter tonischklonischer Anfall auf. Anamnestisch findet sich vor 5 Jahren ein einmaliger unklarer Bewusstseinsverlust. Der neurologische Status ist unauffällig. Was ist zur weiteren Diagnostik
in diesem Fall am ehesten durchzuführen?
#card #neurology #anki
EEG plus CT oder MRT

# 20-jähriger Patient wurde bei einem epile
### Ein 20-jähriger Patient wurde bei einem epileptischen Anfall beobachtet. Seine Freundin
beschreibt, dass der Patient auf Ansprache nicht reagiert habe. Um zwischen einem fokal-komplexen Anfall und einem generalisierten tonisch-klonischen Anfall unterscheiden zu
können, fragen Sie weiter nach
#card #neurology #anki
den motorischen Symptomen.

# führen mit einem 74-jährigen Mann, der an
### Sie führen mit einem 74-jährigen Mann, der an einer Alzheimer-Demenz leidet, ein Gespräch
über seine Lebenssituation und seine Medikamenteneinnahme. Sie erfahren, dass Herr S.
alleine lebt und sich seine Medikamente selbst „einschachtelt“. Der zuletzt dokumentierte
neurologische Befund liegt 1 Jahr zurück und attestierte eine beginnende Alzheimer-Demenz mit einem MMSE-Wert (Mini Mental State Examination) von 26/30. Herr S. legt
Ihnen seine Medikamentenliste vor. Welches der unten genannten Medikamente wird den
Alzheimer-Verlauf negativ beeinflussen?
#card #neurology #anki
Trospium (Anticholinergikum)

# einem akuten MS Schub eines 32-jährigen P
### Bei einem akuten MS Schub eines 32-jährigen Patienten kommt es zu einer hochgradigen
Hemiparese rechts. Die Schubtherapie mit mehrfacher Gabe von Methylprednisolon i.v. ist
nicht erfolgreich. Was verordnen Sie nun?
#card #neurology #anki
Plasmapherese in 5-7 Zyklen

# 35-jährige Patientin kommt mit einer sei
### Eine 35-jährige Patientin kommt mit einer seit 3 Tagen zunehmend auffallenden Hemihypästhesie in Ihre Ordination, im Status finden sich außerdem diskrete cerebelläre Zeichen und
eine Reflexdifferenz an den OE. Sie denken an eine MS. Was ordnen Sie als erste Maßnahme
an?
#card #neurology #anki
Schädel MR.
Welche Frage stellen Sie? Litten Sie jemals unter vorübergehender Ungeschicklichkeit oder Sehstörungen? 2009-09

# einer 64-jährigen Patientin wird mit Brad
### Bei einer 64-jährigen Patientin wird mit Bradykinese und Rigor klinisch ein bilaterales Parkinsonsyndrom vom Rigor/Akinese Dominanztyp festgestellt. Die Diagnose wird bestätigt
durch einen pathologischen beta-CIT SPECT Befund und:
#card #neurology #anki
L-DOPA Test

# 82-jähriger Patient mit Parkinsonsyndrom 
### Ein 82-jähriger Patient mit Parkinsonsyndrom seit 15 Jahren leidet unter einem Dopa-Langzeitsyndrom. Dabei kommt es zu Wirkungsverlust der Dopa-Therapie (wearing off)
und Psychose. Dazu leidet der Patient an einer Dopa-induzierten
#card #neurology #anki
Dyskinesie.
Wirkungsverlust der Dopa-Therapie (wearing off) und Dyskinesien, was noch? Psychose 2009-06 etc.Maßnahme zur Verbesserung der Symptome? L-Dopa öfter am Tag zu geben. 2007-09 etc.

# 22-jähriger Patient leidet seit mehreren 
### Ein 22-jähriger Patient leidet seit mehreren Jahren an einer mesialen MR-negativen Temporallappenepilepsie. Nach typischen epigastrischen Auren treten dabei definitionsgemäß
folgende typische Symptome auf:
#card #neurology #anki
Bewusstseinsstörung, Kauen, Schmatzen

# immunmodulatorische Therapie mit Betainte
### Die immunmodulatorische Therapie mit Betainterferon oder Natalizumab erreicht bei MS
Patienten eine:
#card #neurology #anki
Reduzierung der Schubrate
immunmodulative Therapie mit Betainterferon oder Glatiramerazetat: Reduzierung der Schubrate 2009-09 etc.

# einem 66-jährigen de novo Parkinsonpatien
### Bei einem 66-jährigen de novo Parkinsonpatienten zeigt sich in der klinischen Untersuchung
mangelndes Mitschwingen des rechten Arms beim Gehen. Die Verdachtdiagnose wird
außerdem unterstützt durch:
#card #neurology #anki
Einen angedeuteten Rigor rechtsseitig bei kontralateraler Bewegungsaktivierung

# 37-jährige MS Patientin klagt über eine 
### Eine 37-jährige MS Patientin klagt über eine Blasenstörung. Dabei kommt es zu Problemen,
das Harnlassen, wenn es nötig ist zurückzuhalten, so dass sie in Theatern immer beim
Ausgang sitzen muss und längere Autofahrten ein großes Problem darstellen. Ihr primärer
Verdacht?
#card #neurology #anki
Urgeinkontinenz
ähnliche Frage: imperativer Harndrang 2009-09 etc.

# Patientin mit mehreren intrazerebralen M
### Eine Patientin mit mehreren intrazerebralen Metastasen eines Bronchuskarzinoms erleidet
zuhause einen generalisierten tonisch-klonischen Anfall, der nach 2 Minuten in einen
andauernden Bewusstseinsverlust mit diskreten Zuckungen der OE übergeht. Auch bei
Eintreffen der Rettung 5 Minuten später bleibt der Zustand der Patienin unverändert. Was
ist die zutreffendste Diagnose?
#card #neurology #anki
Status epilepticus

# 34-jährige Patientin mit bekannter Epile
### Eine 34-jährige Patientin mit bekannter Epilepsie erleidet einen generalisierten tonisch-klonischen Anfall. Zuerst ist ein Initialschrei und eine tonische Phase mit Verkrampfung
aller Extremitäten zu beobachten. Nach ca. ½ Minute kommt es definitionsgemäß zu
#card #neurology #anki
einer klonischen Phase, die in eine Bedämmerung übergeht.

# 38-jähriger Patient wird nach einem schwe
### Ein 38-jähriger Patient wird nach einem schweren Autounfall mit GCS 4 aufgefunden.
Im CT zeigt sich sich ein diffuses Hirnödem und eine Kontusionsblutung links temporal.
Der Hirndruck ist kaum kontrollierbar. Am 3. Tag zeigen sich plötzlich weite, lichtstarre
Pupillen. Das Eintreten des dissoziierten Hirntods wird vermutet und die Sedierung beendet.
Welches klinischen Zeichen in der klinischen Untersuchung schließt diese Diagnose jedoch
aus?
#card #neurology #anki
Erhaltener vestibulookulärer Reflex

# 53-jährige Frau Franziska L. wird mit der
### Die 53-jährige Frau Franziska L. wird mit der Rettung in die neurologische Notfallambulanz
überstellt. Auf dem Zuweisungstext lesen Sie folgende Angaben: „seit 5 Tagen subakute
Wesensveränderung, bekannter Alkohol- und Benzodiazepinabusus, keine wesentlichen
Vorerkrankungen“. In der klinisch neurologischen Untersuchung attestieren Sie eine qualitative Bewusstseinsstörung („Delir“) sowie eine Störung der Okulomotorik. Eine detaillierte
neurologische Untersuchung ist aufgrund des Delirs nicht möglich. Schmerzen scheint die
Patientin keine zu haben. Was veranlassen Sie als unmittelbar nächsten Schritt?
(3 richtige Antworten)
#card #neurology #anki
MR Schädel/Gehirn
Harntoxikologie
Verabreichung von 100mg Thiamin i.v.

# 32-jährige Patientin kommt zu Ihnen wege
### Eine 32-jährige Patientin kommt zu Ihnen wegen Kopfschmerzen, die mindestens einmal
pro Woche für mehrere Stunden auftreten. Sie wollen zwischen Migräne und Spannungskopfschmerz differenzieren. Wonach fragen Sie zu diesem Zweck? Nach:
(3 richtige Antworten) (6)
#card #neurology #anki
Der Lokalisation
Begleiterscheinungen wie Übelkeit
Der Wirksamkeit von Triptanen
Differentialdiagnose Migräne bzw. Spannungskopfschmerz - essentielle Fragen: 2013-07 etc.Wo sind die Kopfschmerzen lokalisiert?Sehen Sie vor dem Auftreten der Kopfschmerzen flimmernde Zacken?Verbessert oder verschlechtert körperliche Aktivität den Kopfschmerz?Ähnliche Frage: Stellt sich mit den Kopfschmerzen Übelkeit und Erbrechen ein? 2009-06 etc.Differentialdiagnose Migräne bzw. Spannungskopfschmerz - benötigte anamnestische Daten: Lokalisation, vegetative Begleiterscheinungen 2009-09 etc.

# 52-jähriger Patient wird mit hochgradiger
### Ein 52-jähriger Patient wird mit hochgradiger Hemiparese rechts und Aphasie in die
Notfallaufnahme gebracht. Bei der Anamnese stellt sich heraus, dass er wegen einer
tiefen Beinvenenthrombose unter Marcumar® (Phenprocoumon)-Therapie steht. Welche
Untersuchung kann am ehesten eine therapeutische Möglichkeit ergeben?
#card #neurology #anki
CT Angiographie

# neurologische Status einer 34-jährigen Pa
### Der neurologische Status einer 34-jährigen Patientin weist auf eine radikuläre Läsion S1
rechts hin. Zusätzlich besteht eine sensible Störung der Reithose und eine beginnende
Blasenstörung. Was ist die naheliegendste Vermutungsdiagnose?
#card #neurology #anki
Caudasyndrom

# 58-jährige Lehrerin kommt in Begleitung 
### Eine 58-jährige Lehrerin kommt in Begleitung ihrer zwei Söhne zur Untersuchung. Diese
berichten, dass sich die Mutter in den letzten 2 Jahren verändert habe. Zunehmend sei
ihnen aufgefallen, dass ihre Mutter ihre guten Manieren verloren habe. Sie bediene sich
beim Essen in der Familie zuerst und nehme sich immer die besten Stücke des Essens. Sie
kichere ständig und mache sich über alles Mögliche lustig. Es falle auch auf, dass sie nachts
aufstehe und sich aus dem Kühlschrank über Gebühr bediene. Sie habe deswegen in den
letzten Monaten 12 Kilogramm zugenommen. Die Kollegen in der Schule seien besorgt, da
sie sich teilweise nicht an die Namen der Kollegen erinnere und ihr auch manche alltäglichen
Begriffe nicht einfallen. In der neurologischen Untersuchung zeigt sich bei Beklopfen der
perioralen Region ein auffällig gesteigertes Grimassieren. Welche Diagnose liegt mit größter
Wahrscheinlichkeit vor?
#card #neurology #anki
Frontotemporale Demenz

# werden in das Zimmer eines Patienten mit 
### Sie werden in das Zimmer eines Patienten mit St.p. linkshirnigem Insult vor drei Wochen
gerufen. Als Sie ins Zimmer kommen, ist der Patient nicht ansprechbar, es gibt keine
motorische Spontanaktivität. Die Vitalparameter sind unauffällig. Mitpatienten haben
keine Auffälligkeiten bemerkt. Im CT findet sich ein neu aufgetretenes, hyperdenses Areal
im Insultbereich. Was liegt in diesem Fall am ehesten vor?
#card #neurology #anki
Einblutung in den Insult

# einer Patientin mit St.p. Retrobulbärneur
### Bei einer Patientin mit St.p. Retrobulbärneuritis vor drei Jahren kommt es jährlich zu
mehreren Schüben mit starken, bleibenden motorischen und sensiblen Halbseitensymptomen
linksseitig betont. Sie beschließen, die bisherige MS-Therapie von Beta Interferon zu
verändern. Welche der folgenden Therapien stehen Ihnen dabei zur Verfügung?
(2 richtige Antworten)
#card #neurology #anki
Natalizumab
Rituximab
Medikamente bei hochaktiven Verlaufsformen der schubförmigen Multiplen Sklerose: Natalizumab, Alemtuzumab 2018-09

# 64-jährige Patientin stellt sich wegen z
### Eine 64-jährige Patientin stellt sich wegen zunehmender Ungeschicklichkeit der linken
Hand und Bewegungsverlangsamung vor. Es besteht kein Tremor. Welche Zeichen in der
neurologischen Untersuchung unterstützen die Diagnose Parkinsonsyndrom?
(3 richtige Antworten) (13)
#card #neurology #anki
Rigide Tonuserhöhung rechts
Mangelndes Mitbewegen der rechten oberen Extremität beim Gehen
Angedeutetes Maskengesicht
Befunde eines idiopathischen Parkinsonsyndroms: asymmetrischer Beginn; Fehlen sehr früher autonomer Störungen 2018-06
klinisch-neurologischen Befunde eines idiopathischen Parkinson-Syndroms: posturale Instabilität; Bradykinese oder Akinese; Tremor 2018-09
klinisch-neurologischen Befunde eines Morbus Parkinson: rigid-akinetisches (hypokinetisches) Syndrom; Pillendreher-Tremor 2018-11
Zeichen des inzipienten Parkinsonsyndroms im neurolog. Status: mangelndes Mitbewegen der rechten OE beim Gehen; geringer Rigor der rechten OE bei kontralateraler Aktivierung; Verlangsamung der komplexen Bewegungen (360° Drehung auf der Liege) 2013-09 etc.

# einer 23-jährigen Patientin, die mit mass
### Bei einer 23-jährigen Patientin, die mit massiven Kopfschmerzen und Übelkeit auf der
Notfallaufnahme aufgenommen wird, findet sich im Akut-CT kein Blut in den basalen
Zisternen. Im Liquor zeigt sich eine Zellzahl von 12/3 Zellen. Was ist die naheliegendste
Vermutungsdiagnose?
#card #neurology #anki
Migräne

# he der nachfolgenden Symptome finden sich
### Welche der nachfolgenden Symptome finden sich in den diagnostischen Kriterien der
Migräne ohne Aura?
(2 richtige Antworten)
#card #neurology #anki
Erbrechen
Übelkeit

# 63-jähriger Patient nach Schädel-Hirn-Tra
### Ein 63-jähriger Patient nach Schädel-Hirn-Trauma mit im Akut-CT diagnostizierter frontaler Kontusionsblutung verschlechtert sich während der Observanz mit Bewusstseinsverlust.
Die CT-Kontrolle zeigt ein unverändertes Bild. Was ist primär zu tun?
#card #neurology #anki
EEG Untersuchung

# Patient mit neu diagnostizierter Temporal
### Ein Patient mit neu diagnostizierter Temporallappenepilepsie soll auf Lacosamid eingestellt
werden und fragt Sie nach der Chance auf Anfallsfreiheit. Was antworten Sie am besten?
#card #neurology #anki
70% der Patienten werden auf das erste Antiepileptikum anfallsfrei.

# 35-jähriger Handwerker berichtet, am Vort
### Ein 35-jähriger Handwerker berichtet, am Vortag mit Freunden gefeiert und ca. 4 Flaschen
Bier und 8 Schnäpse zu sich genommen zu haben. Spät in der Nacht sei er erschöpft tief
eingeschlafen. Am nächsten Morgen beim Zähneputzen habe er dann eine Lähmung in der
rechten Hand bemerkt. Bei der neurologischen Untersuchung zeigt sich eine Parese der
Handgelenksextensoren und der Fingerstrecker entsprechend Kraftgrad 3. Der M. triceps
brachii zeigt die volle Kraftentfaltung. Im Spatium interosseum I gibt der Patient dorsal
eine leichte Hypästhesie an. Welche Diagnose liegt mit größter Wahrscheinlichkeit vor?
#card #neurology #anki
Druckläsion des N. radialis rechts

# 72-jährige Patientin wird wegen der Symp
### Eine 72-jährige Patientin wird wegen der Symptome Gangstörung und inzipiente Demenz
an der neurologischen Ambulanz vorgestellt. Welche Anamnesefrage ist für die Differentialdiagnose Alzheimerdemenz versus Normaldruckhydrocephalus wichtig?
#card #neurology #anki
Ist die Blasenfunktion gestört?

# 64-jähriger Patient stellt sich wegen sch
### Ein 64-jähriger Patient stellt sich wegen schlechter Schlafqualität gemeinsam mit seiner
Gattin vor. Welche außenanamnestische Angabe erfragen Sie in Hinblick auf die obstruktive
Schlafapnoe?
#card #neurology #anki
Längere Atempausen während des Schlafs

# 73-jähriger Patient mit Restless Legs Syn
### Ein 73-jähriger Patient mit Restless Legs Syndrom (RLS) nimmt seit 3 Jahren L-DOPA
retard Tabletten zur Behandlung seiner Symptome ein. Bei der Erstvorstellung in der
Schlafambulanz klagt er über eine zunehmende Verschlechterung der Beschwerden und
belastende Einschlafprobleme in den letzten 6 Monaten. Er habe zwar in Rücksprache
mit seinem Hausarzt die Tagesdosis der Tabletten sukzessive von 100 mg auf 800 mg
erhöht, aber leider keine Verbesserung erzielen können. Zusätzlich sind in den letzten 3
Monaten auch RLS-Beschwerden in den Armen und am Rumpf aufgetreten. Insgesamt
beginnen die RLS-Symptome tagsüber mittlerweile schon am Vormittag und machen ein
Mittagsschläfchen unmöglich. Welche weiteren Schritte sind hier sinnvoll?
(2 richtige Antworten)
#card #neurology #anki
Laborchemische Bestimmung von Ferritin, Transferrinsättigung, Holotranscobalamin, Folsäure, Kreatinin und Schilddrüsenparameter
Graduelles Ausschleichen von L-DOPA und Umstellung auf Pregabalin

# 25-jähriger Patient kommt wegen eines stö
### Ein 25-jähriger Patient kommt wegen eines störenden Zitterns in die Ordination. Das Zittern
tritt bei Aufregung und in der Öffentlichkeit auf, es ist vor allem beim Restaurantbesuch
störend. Im neurologischen Status findet sich weder in Ruhe noch im Halteversuch oder im
Fingernaseversuch ein Tremor. An welche Form des Tremors ist am ehesten zu denken?
#card #neurology #anki
Verstärkter physiologischer Tremor

# 72-jähriger Patient kommt wegen eines Kra
### Ein 72-jähriger Patient kommt wegen eines Krampfanfalls mit Bewusstseinsverlust in die
Notaufnahme. Im MR zeigt sich eine runde 1,5 cm große Raumforderung parietal rechts mit
deutlichem umgebenden Ödem. Was sind die primären Maßnahmen in der Akuttherapie?
(2 richtige Antworten)
#card #neurology #anki
Dexamethason 20mg i.v.
Aufdosierung mit Levetiracetam

# 28-jährige Frau hat seit einem Jahr eine
### Eine 28-jährige Frau hat seit einem Jahr eine Leberzirrhose unklarer Genese. Sie berichtet,
seit 3-4 Monaten an einem Tremor der Hände zu leiden. Insbesondere bei zielgerichtetem
Greifen und beim Halten eines Stiftes würde er stören. Die Tremorfrequenz liegt bei ca.
6/s. In Ruhe zeigt sich kein Tremor. Eine Cousine, die 26 Jahre alt ist, leide auch an
einem solchen Tremor und vor einigen Wochen habe man bei ihr auch erhöhte Leberwerte
festgestellt. Wie lautet die wahrscheinlichste Diagnose?
#card #neurology #anki
Morbus Wilson

# 64-jähriger Patient zeigt nach den initia
### Ein 64-jähriger Patient zeigt nach den initialen medizinischen Untersuchungen eine gestörte
Blickfolge sowie eine Kopf- und Blickdeviation zur rechten Seite. Diese können nach Aufforderung nicht spontan durch den Patienten korrigiert werden. Sie bitten den Patienten, eine
Uhr und ein Haus zu zeichnen. Welche Schwierigkeiten würden Sie erwarten?
#card #neurology #anki
Schwierigkeiten in der Visuokonstruktion bei beiden Aufgaben durch Vernachlässigen der linken Seite

# der Gesichtsfelduntersuchung findet sich 
### Bei der Gesichtsfelduntersuchung findet sich bei beiden Augen die linke Gesichtsfeldhälfte
ausgefallen. Was ist die wahrscheinlichste Ursache dieser homonymen Hemianopsie nach
links?
#card #neurology #anki
Arteria cerebri posterior Insult rechts

# 42-jährige Patientin nach Schädel-Hirn-T
### Eine 42-jährige Patientin nach Schädel-Hirn-Trauma wird Ihnen vorgestellt. Im Rahmen der
Anamnese fällt Ihnen auf, dass sie wach und bewusstseinsklar ist, jedoch nur Informationen
für eine begrenzte Dauer (für max. 1 Minute) behalten kann. Wodurch ist diese Symptomatik
am ehesten verursacht?
#card #neurology #anki
Anterograde Amnesie

# 35-jähriger Patient wird nach einem Autou
### Ein 35-jähriger Patient wird nach einem Autounfall im Krankenhaus aufgenommen. Er ist
somnolent und zeigt in der neurologischen Untersuchung deutliche Halbseitensymptome
rechts mit Absinken, Reflexdifferenz und positivem Babinski-Zeichen. Im CT zeigt sich eine
15 mm breite bikonvex-hyperdense Struktur links parietal an der Schädelkapsel. Welche ist
die zutreffendste Diagnose?
#card #neurology #anki
Epiduralhämatom

# dem 27-jährigen, bislang gesunden Herrn M
### Bei dem 27-jährigen, bislang gesunden Herrn M. besteht eine Farbwahrnehmungsstörung
sowie ein Bulbusbewegungsschmerz am linken Auge. Er berichtet, vor etwa einem Jahr unter
einem Taubheitsgefühl an der rechten Hand gelitten zu haben, das nach 1 Monat spontan
sistierte und damals nicht weiter abgeklärt wurde. Aufgrund der Anamnese vermuten
Sie eine Multiple Sklerose. Welche der folgenden Untersuchungen sind zur Sicherung der
Diagnose indiziert?
(3 richtige Antworten) insg. 4
#card #neurology #anki
Optische Kohärenztomographie (OCT)
Magnetresonanztomographie des Gehirns und des Rückenmarks
Messung visuell evozierter Potenziale (VEP) 2022-06
LumbalpunktionMRT Messung visuell evozierter Potenziale (VEP) 2022-09

# 45-jährige übergewichtige Frau erscheint
### Eine 45-jährige übergewichtige Frau erscheint in der neurologischen Notfallambulanz mit
Kreuzschmerzen, die ziehend-stechend ins rechte Bein ausstrahlen. Die Schmerzen wären am
Tag davor nach dem Tragen einer schweren Einkaufstasche plötzlich aufgetreten. Aufgrund
Ihrer nun folgenden Untersuchung kommen Sie zur Diagnose einer S1-Wurzelschädigung,
möglicherweise bedingt durch einen Bandscheibenvorfall. Welche der unten genannten
Befunde aus dem Neurostatus unterstützen diese Verdachtsdiagnose?
(2 richtige Antworten)
#card #neurology #anki
Hypästhesie an der Fußsohle und am lateralen Fußrand (rechte UE)Einseitig fehlender Achillessehnenreflex auf der betroffenen Seite (PSR bds. auslösbar)

# 73-jährige Patientin mit bekannter Parki
### Eine 73-jährige Patientin mit bekannter Parkinson Erkrankung seit acht Jahren stellt sich
in der Ambulanz wegen massiver Dyskinesien und Wirkverlust der L-Dopa Therapie vor
(massives wearing-off). Was sind mögliche Therapieoptionen?
(2 richtige Antworten)
#card #neurology #anki
Tiefe Hirnstimulation
Adaptierung der Kombination von L-Dopa und Dopa-Agonisten

# 56-jähriger Patient berichtet über ein er
### Ein 56-jähriger Patient berichtet über ein erstmaliges, akut aufgetretenes Vertigogefühl
mit Emesis seit ca. 2 Stunden. Der Patient berichtet zudem über eine gleichzeitig mit
dem Vertigo aufgetretene Hörminderung links ohne Tinnitus. Im Neurostatus fällt unter
der Frenzelbrille ein Spontannystagmus auf, dessen rasche Phase nach rechts schlägt und
dessen Intensität bei Rechtsblick zunimmt. Im klinischen Kopfimpulstest zeigt sich bei
Linksdrehung ein verminderter vestibulo-okulärer Reflex mit Korrektursakkaden. Im fingerrub Test finden Sie eine Hörminderung links. Beim Unterberger Test kommt es zu einer
reproduzierbaren Drehung nach links um mehr als 50° nach 50 Schritten. Sie vermuten
eine unilaterale Neuritis vestibularis (Neuronitis vestibularis bzw. unilaterale Neuropathia
vestibularis). Welches Symptom passt jedoch nicht in diesen Befund?
#card #neurology #anki
Die assoziierte akut aufgetretene Hörminderung links

# 78-jährige Patientin beschreibt seit ca.
### Eine 78-jährige Patientin beschreibt seit ca. 5 Monaten bestehende kognitive Einbußen,
v.a. im Bereich des Kurzzeitgedächtnisses. Eine MRT des Schädels ergab einen altersentsprechenden Befund. In der Neuropsychologischen Testung fand sich ein MMSE (= Mini
Mental Status Examination)-Wert von 27/30 Punkten und geringe Defizite in der verbalen Gedächtnisleistung. Eine Laboruntersuchung (inkl. Blutbild, Elektrolyte, HbA1c,
TSH, Vitamin B12, Folsäure, CRP, GOT, Gamma-GT, Kreatinin, Harnstoff) erbrachte
keine Auffälligkeiten. Die Patientin ist vollkommen selbstständig in allen Aktivitäten des
täglichen Lebens. Sie überlegen weitere diagnostische Schritte einzuleiten. Bevor Sie diese jedoch veranlassen, stellen Sie eine Verdachtsdiagnose. Was ist die wahrscheinlichste
Verdachtsdiagnose?
#card #neurology #anki
Mild Cognitive Impairment

# 18-jährige Patientin wurde an der Notauf
### Eine 18-jährige Patientin wurde an der Notaufnahme nach dem ersten generalisierten
tonisch-klonischen Anfall mit Secessus urinae und Zungenbiss eingeliefert. Bei Eintreffen in
der Notaufnahme war die Patientin reorientiert. Sie gab an, 2 Tage durchgefeiert, wenig
geschlafen und viel Alkohol konsumiert zu haben. Die Anamnese war bis auf seit 2-3 Jahren
bestehende leichte Zuckungen in den Händen, meist in den Morgenstunden nach dem
Aufwachen, unauffällig. Dies wird von der Patientin auf Stress in der Schule zurückgeführt.
Welche ist die naheliegendste Diagnose?
#card #neurology #anki
Juvenile myoklonische Epilepsie

# einem 29-jährigen Patienten wurde die Dia
### Bei einem 29-jährigen Patienten wurde die Diagnose einer fokalen Epilepsie gestellt. Mehrere
EEGs zeigten unspezifische Veränderungen, das MRT zeigt eine einseitige Hippocampussklerose. Der Patient hatte einige Medikamente (Valproinsäure, Lamotrigin, Carbamazepin,
Oxcarbazepin, Levetiracetam und Phenytoin) hochdosiert und in unterschiedlichen Kombinationen bereits erhalten. Trotzdem treten die Anfälle ca. 1mal pro Monat auf. Aktuell
nimmt der Patient Levetiracetam und Lamotrigin ein und klagt über Müdigkeit und
Einschlafstörung. Was empfehlen Sie dem Patienten in dieser Situation am ehesten?
#card #neurology #anki
Die Möglichkeit einer Epilepsiechirurgie evaluieren.

# 34-jähriger Patient hatte in den letzten 
### Ein 34-jähriger Patient hatte in den letzten acht Jahren drei Episoden, jeweils zwei bis
drei Monate anhaltend, mit sehr heftigen einseitigen Kopfschmerzen, die bisher auf keine
Behandlung angesprochen haben. Er habe selbst im Internet recherchiert und vermute
einen Clusterkopfschmerz. Welche Charakteristika unterstützen diese Diagnose am ehesten?
(3 richtige Antworten)
#card #neurology #anki
Bis zu achtmal pro Tag auftretend
Augentränen ipsilateral zum Kopfschmerz
Motorische Unruhe

# 62-jährige Patientin bemerkt einen Knote
### Eine 62-jährige Patientin bemerkt einen Knoten in der Brust. Wenige Tage später fällt
ihr außerdem eine Sensibilitätsstörung der rechten Körperhälfte auf. In einem daraufhin
durchgeführten zerebralen MRT zeigt sich eine randständige Kontrastmittel-aufnehmende
Raumforderung links parietal. Die Biopsie des Knotens in der Brust wird in wenigen Tagen
erfolgen. Was ist die wahrscheinlichste Differentialdiagnose der zerebralen Raumforderung?
#card #neurology #anki
Metastase

# 42-jährige Patientin mit heftigen Kopfsc
### Eine 42-jährige Patientin mit heftigen Kopfschmerzen wird mit klinischen Zeichen von
Meningismus und zunehmender Bewusstseinseintrübung auf die Notfallaufnahme gebracht.
Welche Diagnosen kommen in diesem Fall am ehesten in Betracht?
(2 richtige Antworten)
#card #neurology #anki
Bakterielle Meningitis
Aneurysmaruptur mit Subarachnoidalblutung

# 76-jähriger Patient sucht in Begleitung s
### Ein 76-jähriger Patient sucht in Begleitung seiner Gattin die neurologische Akutambulanz
auf. Er berichtet über eine Gangstörung, welche seit 2 Jahren schleichend zunehmen würde,
sowie Schwierigkeiten in den Domänen der Aufmerksamkeit und der Exekutivfunktionen.
Auf genauere Nachfrage erzählt er auch von einer Harninkontinenz, welche er jedoch primär
auf seine Prostatahyperplasie zurückgeführt habe. An welche Differentialdiagnose ist bei
dieser Befundkonstellation am ehesten zu denken?
#card #neurology #anki
Normaldruckhydrozephalus

# Patient kommt wegen Schlafstörungen und h
### Ein Patient kommt wegen Schlafstörungen und holt eine Zweitmeinung zu NREM Parasomnie ein. Er konfrontiert Sie mit einer Reihe von Aussagen, die er auch im Internet
recherchiert hat. Welche davon können Sie ihm im Gespräch als richtig bestätigen?
#card #neurology #anki
Schlafentzug oder Alkoholkonsum können das Auftreten von NREM Parasomnien triggern.

# 60-jährige Frau wird an der Notfallambul
### Eine 60-jährige Frau wird an der Notfallambulanz vorstellig aufgrund einer subakut aufgetretenen, zunehmenden, proximal betonten, symmetrischen Schwäche der Muskulatur
und ausgeprägter Myalgien. In der klinischen Untersuchung finden sich zusätzlich erythematöse Papeln an den Streckseiten von Händen und Fingern (Gottron-Zeichen) sowie ein
heliotropes Erythem im Bereich des Gesichts und des Dekolletees. Im Labor zeigt sich die
Creatinkinase (CK) deutlich erhöht mit 5500 U/L. Sie vermuten, dass es sich um eine
Dermatomyositis handelt. Welche diagnostischen Schritte sind im Rahmen der weiteren
Abklärung primär sinnvoll?
(3 richtige Antworten)
#card #neurology #anki
Durchführung einer Muskelbiopsie
Bestimmung der Myositis-spezifischen Antikörper
Durchführung einer Magnetresonanztomographie der Muskeln

# 65-jähriger Patient berichtet über eine p
### Ein 65-jähriger Patient berichtet über eine progrediente Schwäche der Extremitäten, wobei
die Symptomatik vor 6 Monaten mit einer Fußheberschwäche links begonnen habe. Ca. 1
Monat später wäre es auch zu einer Fußheberschwäche rechts und nach einem weiteren Monat
zu einer Ungeschicklichkeit der linken Hand gekommen. In der klinischen Untersuchung
fallen Faszikulationen an allen Extremitäten sowie eine Atrophie an der linken Hand im
Bereich des Thenars auf. Weiters sind die Sehnenreflexe an der linken Körperseite gesteigert
und es findet sich ein positives Babinski-Zeichen links. Wie lautet die wahrscheinlichste
Diagnose?
#card #neurology #anki
Amyotrophe Lateralsklerose

# 70-jähriger Patient mit bekanntem Lungenk
### Ein 70-jähriger Patient mit bekanntem Lungenkarzinom stellt sich in der Notaufnahme
vor, da er seit 2 Wochen Probleme beim Gehen bemerkt hätte. Seit einem Tag sei er auf
den Rollstuhl angewiesen und könne überhaupt nicht mehr aufstehen. In der klinischneurologischen Untersuchung fällt eine proximal betonte Schwäche beider Beine auf. Der
Patient gibt an, unterhalb des Bauchnabels deutlich weniger zu spüren als darüber. Gestern
hätte er auch einmalig unwillkürlich Harn verloren. Welche Zusatzdiagnostik wählen Sie
primär zur weiteren Abklärung der Beschwerden?
#card #neurology #anki
MRT der Brust- und Lendenwirbelsäule

# 82-jährige Patientin hat vor drei Tagen 
### Eine 82-jährige Patientin hat vor drei Tagen ein hängendes linkes Augenlid bemerkt. Seit
heute Morgen kommt es auch zu Doppelbildern überwiegend beim Blick nach rechts und
hinauf. Ihre Verdachtsdiagnose lautet Myasthenia gravis, aber an welche Differenzialdiagnosen ist primär noch zu denken?
(2 richtige Antworten)
#card #neurology #anki
Diabetische Okulomotoriusparese
Hirnstamm Durchblutungsstörung

# 72-jähriger Patient wird zwei Tage nach e
### Ein 72-jähriger Patient wird zwei Tage nach einer Hemikolektomie dem neurologischen
Konsiliardienst vorgestellt. Neben einer Hemiparese der rechten Seite fällt eine sensomotorische Aphasie auf. Die Temperatur ist auf 37,5° erhöht, der Blutdruck beträgt 170/100. Im
CT finden sich keine frühen Infarktzeichen. Was ist die primäre weitere Vorgehensweise?
#card #neurology #anki
CT-Angiographie

# sehen im stationären Setting einen 20-jäh
### Sie sehen im stationären Setting einen 20-jährigen Mann mit Verdacht auf Aphasie, die Sie
prüfen sollen. Sie entscheiden sich, mit dem Sprachverständnis zu beginnen. Worauf achten
Sie?
(2 richtige Antworten)
#card #neurology #anki
Ausführung einfacher Kommandos
Ausführung komplexerer Kommandos

# 37-jähriger Patient wurde wegen einer Wes
### Ein 37-jähriger Patient wurde wegen einer Wesensveränderung, Kopfschmerzen und Erbrechen stationär aufgenommen. Der MR-Befund des Gehirns zeigte keine Auffälligkeiten, im
EEG fand sich rechts frontal temporal ein Herd mit langsamen Wellen und steilen Wellen.
Welche weiteren diagnostischen Maßnahmen sind am ehesten sinnvoll?
(3 richtige Antworten)
#card #neurology #anki
Lumbalpunktion
Virus PCR in Liquor und Serum
Autoantikörper Bestimmung im Liquor

# elfjährige Patientin fällt durch Leistun
### Eine elfjährige Patientin fällt durch Leistungsverschlechterung in der Schule auf. Bei
Beobachtung durch die Lehrkräfte blickt sie häufig für Sekunden ins Leere, leckt sich die
Lippen und nestelt mit ihren Schulsachen. Es wird eine Absencen-Epilepsie diagnostiziert.
Welcher Krankheitverlauf ist wahrscheinlich?
#card #neurology #anki
Gute Anfallskontrolle durch Antiepileptika

# 24-jährige Patientin stellt sich erstmal
### Eine 24-jährige Patientin stellt sich erstmals mit einer Drehschwindelepisode vor, die
morgens beim Aufstehen aus dem Bett auftrat. Die Attackendauer wird mit „nur wenigen
Sekunden“ angegeben und es bestand weder eine assoziierte Hörstörung noch Tinnitus.
Beim Eintreffen in der Notfallaufnahme gibt die Patientin kein Vertigogefühl mehr an.
Im neurologischen Status finden sich keine Auffälligkeiten. Zuletzt führen Sie noch das
diagnostische Lagerungsmanöver für den posterioren Bogengang durch und sehen einen
typischen rotatorischen Lagerungsnystagmus, der mit der raschen Phase ans untenliegende
Ohr schlägt, eine Crescendo-Decrescendo-Charakteristik aufweist und nach etwa 20 Sekunden sistiert. Welches therapeutische Lagerungsmanöver werden Sie gleich anschließend an
das diagnostische Lagerungsmanöver durchführen ?
#card #neurology #anki
Epley Manöver

# einem Patienten mit akutem rechtshirnigen
### Bei einem Patienten mit akutem rechtshirnigen Insult wird im EKG ein Vorhofflimmern
festgestellt. Was bedeutet dieser Befund für die Akuttherapie?
#card #neurology #anki
Keine Änderung des Vorgehens

# 28-jähriger Patient stellt sich in der ne
### Ein 28-jähriger Patient stellt sich in der neurologischen Ambulanz in Begleitung seiner
Frau vor; diese gibt an, dass der Patient sehr schlecht schlafen würde. Er würde im Schlaf
plötzlich aufspringen, mit den Händen und Beinen herumschlagen, sei nicht ansprechbar
und würde sich wieder hinlegen. Manchmal würde der Patient sehr laut schreien, den
Kopf nach links drehen und die linke Hand unnatürlich ausstrecken. Er selbst habe keine
Erinnerung an diese Ereignisse. Die Dauer der Ereignisse schätzt sie auf maximal eine
Minute. Auch die Häufigkeit habe zugenommen, vor Jahren traten die Anfälle nur 1-2-mal
pro Monat auf, derzeit mehrfach pro Nacht. Welche ist die naheliegendste Diagnose?
#card #neurology #anki
Rechtshirnige Frontallappenepilepsie

# einem 68-jährigen Patienten mit langjähri
### Bei einem 68-jährigen Patienten mit langjährigem Diabetes mellitus und Parästhesien in
beiden unteren Extremitäten besteht der Verdacht auf eine Polyneuropathie. Was spricht
im Status für eine Polyneuropathie?
(2 richtige Antworten)
#card #neurology #anki
Abgeschwächter ASR beidseits
Distale Störung der Vibrationsempfindung

# 32-jährige Patientin klagt, seit einer W
### Eine 32-jährige Patientin klagt, seit einer Woche Kribbelparästhesien in beiden unteren
Extremitäten inklusive Gesäß und Bauch zu haben. Im neurologischen Status zeigt sich
die Motorik inklusive Sehnenreflexe und Pyramidenzeichen normal unauffällig, aber eine
Störung der Hinterstrangqualitäten unterhalb von Th10 fällt auf. In der MR-Untersuchung
des Rückenmarks zeigt sich ein intramedullärer Herd auf Höhe Th 8. Im Liquor 60/3 Zellen,
48 mg Eiweiß. Welche Therapie wird am besten angeordnet?
#card #neurology #anki
1g Prednisolon i.v.

# hes der folgenden Medikamente sollte nach
### Welches der folgenden Medikamente sollte nach einem mikroangiopathischen Hirninfarkt
außer einer Thrombozytenaggregationshemmung noch zur Prophylaxe eines Rezidivschlaganfalles eingesetzt werden?
#card #neurology #anki
Statin

# 21-jährige Studentin stellt sich akut in 
### Ein 21-jährige Studentin stellt sich akut in einer Notfallambulanz vor, weil vor einigen
Stunden erstmals eine Sehstörung zur rechten Seite hin aufgetreten ist, gefolgt von einer Taubheit, die sich von den Fingerspitzen auf den Arm und schließlich zum Gesicht
ausgebreitet hat. Diese Symptome sind innerhalb einer Stunde abgeklungen. Zum Untersuchungszeitpunkt klagt die Patientin über Kopfschmerzen. Welche ist die wahrscheinlichste
Diagnose? 
#card #neurology #anki
Migräne mit Aura

# 79-jähriger Patient wird in der „Gedächtn
### Ein 79-jähriger Patient wird in der „Gedächtnissprechstunde“ der neurologischen Klinik
vorstellig. Der begleitende Sohn berichtet, dass der Patient seit etwa 3 Jahren unter
schleichend-progredienten kognitiven Defiziten leidet. Es besteht ein nicht-insulinpflichtiger
Diabetes mellitus, sonst sind keine wesentlichen Vorerkrankungen bekannt. Der neurologische Status ist bis auf fehlende Muskeleigenreflexe an den unteren Extremitäten unauffällig.
Welche der folgenden Untersuchungen sind in einem ersten diagnostischen Schritt essenziell?
(3 richtige Antworten)
#card #neurology #anki
Neuropsychologische Testung
Nervenleitgeschwindigkeit
Schädel-MRT

# 42-jährige Patientin kommt wegen seit St
### Eine 42-jährige Patientin kommt wegen seit Stunden dauernden Drehschwindelattacken,
die nicht durch Veränderung der Kopfposition ausgelöst werden. Beim Lagerungstest
wird der benigne paroxysmale Lagerungsschwindel als Ursache ausgeschlossen. Zusätzlich
zur begleitenden Übelkeit findet sich auch ein Kopfschmerz. Was ist die zutreffendste
Vermutungsdiagnose?
#card #neurology #anki
Vestibuläre Migräne

# 65-jährige Anwältin klagt, ein Jahr nach
### Eine 65-jährige Anwältin klagt, ein Jahr nachdem sie die Pension angetreten hat, über eine
zunehmende Vergesslichkeit. Die MRT des Schädels, weitere Laboruntersuchungen sowie
die ausführliche neuropsychologische Testung erbrachten unauffällige Befunde. Auffällig
ist eine positive Familienanamnese mit zwei Familienangehörigen mütterlicherseits, die an
einer Alzheimer-Demenz erkrankten (Mutter im Alter von 67 Jahren, Großmutter im Alter
von 69 Jahren). Wie ist dieses Zustandsbild am besten zu beschreiben?
#card #neurology #anki
Subjective Cognitive Decline/Subjektiv empfundene Gedächtnisstörung

# sind FamulantIn in einer Praxis für Allge
### Sie sind FamulantIn in einer Praxis für Allgemeinmedizin, wo sich eine 38-jährige Patientin
aufgrund eines seit 2 Jahren langsam progredienten Tremors vorstellt. Klinisch zeigt sich
ein bilateraler symmetrischer Haltetremor der Hände mit einer Frequenz von ca. 10 Hz
und einer Amplitude von ca. einem Zentimeter. Bis auf eine bekannte Hyperthyreose hat
die Patientin keine Vorerkrankungen. Welche Diagnosen ziehen Sie differentialdiagnostisch
in Betracht?
(2 richtige Antworten)
#card #neurology #anki
Essentieller Tremor
Verstärkter physiologischer Tremor

# ist die primäre Therapie der diagnostisc
### Was ist die primäre Therapie der diagnostisch gesicherten Dermatomyositis?
#card #neurology #anki
Glukokortikoide

# einem 68-jährigen Patienten wurde vor ein
### Bei einem 68-jährigen Patienten wurde vor einem halben Jahr ein Glioblastom diagnostiziert
und primär reseziert. Nun erfolgt die Einlieferung in eine Notaufnahme aufgrund eines
erstmaligen Status epilepticus. Eine MRT-Untersuchung weist auf ein Tumor-Rezidiv mit
ausgeprägtem Ödem hin. Welche sinnvollen Akuttherapieoptionen sollten unmittelbar
eingeleitet werden?
(2 richtige Antworten)
#card #neurology #anki
Glukokortikoide
Antiepileptika

# 75-jähriger Patient wurde wegen zunehmend
### Ein 75-jähriger Patient wurde wegen zunehmender genereller Muskelatrophie in der Ambulanz gesehen. Nach dem Ausschluss von diversen internistischen Ursachen soll nun die
Diagnose neurologisch gefunden werden. Es wird differenzialdiagnostisch an eine Motoneuronenerkrankung gedacht. Welche Befunde in der Statusuntersuchung stützen diese
Diagnose?
(2 richtige Antworten)
#card #neurology #anki
Pyramidenzeichen beidseits
Spontane Muskelfaszikulationen

# 15-jähriger Patient kommt in die neurolog
### Ein 15-jähriger Patient kommt in die neurologische Ambulanz aufgrund einer zunehmenden
proximalen Schwäche vor allem im Bereich der Hüftmuskulatur. Konsekutiv ist das Aufstehen aus der Hocke erschwert. Anamnestisch wäre es schon ab dem Volkschulalter zu einer
leichten, aber zunehmenden, motorischen Beeinträchtigung gekommen. Es erfolgte bereits
eine Muskelbiopsie zur Abklärung einer Myopathie, wo sich jedoch primär neurogene Ver-
änderungen zeigten. Sie vermuten, dass der Patient an einer Spinalen Muskelatrophie Typ
3 leiden könnte. Mit welcher Untersuchung können Sie die Diagnose definitiv bestätigen?
#card #neurology #anki
Genetische Testung

# 23-jährige Patientin stellt sich mit Dur
### Eine 23-jährige Patientin stellt sich mit Durchschlafproblemen, reduzierter Leistungsfähigkeit und einer deutlichen Antriebsminderung in der Schlafambulanz vor. Die subjektive
Schlafdauer liegt bei 3 Stunden und sie erwähnt, dass sie tagsüber nicht schlafen kann,
obwohl sie sehr müde ist. Die Beschwerden bestehen seit dem Beginn der Covid-19 Pandemie
und seien seither nicht mehr remittiert. Ein auswärtiger Schlaflaborbefund zeigt, dass die
junge Frau nicht unter einer Schlafapnoe oder periodischen Beinbewegungen im Schlaf
leidet. Sie hat bislang Baldrian und Melatonin als Schlafhilfen ohne anhaltenden Erfolg
versucht. Welche Therapiemöglichkeiten gibt es für die Patientin?
(2 richtige Antworten)
#card #neurology #anki
Kognitive Verhaltenstherapie bei Insomnie (CBT-I)
Trazodon retard 25 mg abends

# 70-jährige Patientin mit bekannter Alzhe
### Eine 70-jährige Patientin mit bekannter Alzheimer-Demenz wird aufgrund von progredienten
kognitiven Defiziten in Begleitung ihrer Familie bei Ihnen vorstellig. Die Familie erzählt, dass
es im letzten Jahr zu einer deutlichen Verschlechterung des Gesamtzustandes gekommen
sei, die Patientin ist nun im Alltag vermehrt auf Hilfe angewiesen. Ein rezentes MRT zeigt
eine ausgeprägte kortikale Atrophie mit parietotemporaler Betonung, im MMSE erzielt
die Patientin 14 Punkte. Welche medikamentösen Therapien wären bei dieser Patientin
indiziert?
(2 richtige Antworten)
#card #neurology #anki
Memantin
Rivastigmin

# 72-jährige Patientin stellt sich bei Ihn
### Eine 72-jährige Patientin stellt sich bei Ihnen in der Gedächtnisstörungsambulanz vor und
beschreibt eine seit ca. 2 Jahren langsam zunehmende Störung des Kurzzeitgedächtnisses.
Sie würde manchmal in ein Zimmer gehen und vergessen, weshalb sie dorthin gegangen
sei. Außenstehenden sei aufgefallen, dass sie gelegentlich Gegenstände an dafür nicht vorgesehene Orte verräumen würde (z.B. Geschirr in den Kleiderkasten, Lebensmittel in die
Waschtrommel). In der neurologischen Untersuchung fällt ihnen eine zeitlich teilorientierte
Patientin mit einer milden Apraxie links auf. In der MMSE (=Mini Mental Status Examination) erreicht die Patientin 24/30 Punkten. Welche weiteren Untersuchungen benötigen
Sie in diesem Fall zur weiteren Abklärung?
(3 richtige Antworten)
#card #neurology #anki
Laboruntersuchung inkl. Vitamin B12, Folsäure, TSH
MRT Schädel
Neuropsychologische Testung

# 75-jährige Patientin wird um 7 Uhr morge
### Eine 75-jährige Patientin wird um 7 Uhr morgens als Stroke-Aviso in der Notfallambulanz
angekündigt. Am Abend zuvor sei die Patientin um ca. 23 Uhr ohne Beschwerden zu
Bett gegangen, um 6:15 sei sie mit einer Schwäche und Sensibilitätsstörung im rechten
Arm erwacht. An relevanten Vorerkrankungen besteht eine arterielle Hypertonie sowie eine
Hyperlipidämie unter Therapie mit einem ACE-Hemmer und einem Statin. In der klinischen
Untersuchung zeigt sich eine deutliche Parese der rechten oberen Extremität, sowie eine
geringere Schwäche im rechten Bein; zusätzlich gibt die Patientin eine Hypästhesie der
rechten oberen und unteren Extremität an. Im akut durchgeführten cMRT zeigt sich eine
Diffusionsstörung (DWI) im Bereich der Basalganglien links ohne Demarkierung in der
FLAIR-Sequenz, ein Gefäßverschluss ist nicht nachweisbar. Was ist in diesem Fall die
primäre Akuttherapie?
#card #neurology #anki
Intravenöse Thrombolyse

# 56-jähriger Patient wird als Stroke-Aviso
### Ein 56-jähriger Patient wird als Stroke-Aviso an die Notfallambulanz gebracht. Zwei Stunden
zuvor war es zu einem plötzlichen Onset einer Hemiparese rechts und einer deutlichen
Sprachstörung gekommen. An kardiovaskulären Vorerkrankungen besteht eine arterielle
Hypertonie, eine Hyperlipidämie und ein St. p. Myocardinfarkt vor 3 Jahren; zudem ist
der Patient starker Raucher. Laut Rettungsübergabe liegt der Blutdruck bei 190/110, der
Blutzucker bei 120 mg/dl und die Herzfrequenz bei 124/min. Im Rettungs-EKG zeigt
sich eine Tachyarrhythmie ohne Nachweis von P-Wellen. Klinisch zeigt der Patient eine
hochgradige Hemiparese rechts und eine Globalaphasie; im CT mit CTA zeigt sich ein
M1-Verschluss links ohne Demarkierung, in den übrigen Gefäßen zeigen sich bis auf leichte
Verkalkungen der Gefäßwände keine Auffälligkeiten. Der Patient wird in weiterer Folge
lysiert und thrombektomiert. Welche ist seine wahrscheinlichste Schlaganfallursache?
#card #neurology #anki
Vorhofflimmern

# 40-jähriger Patient kommt mit der Rettung
### Ein 40-jähriger Patient kommt mit der Rettung auf die Notfallabteilung aufgrund einer akut
aufgetretenen Hemiparese rechts und Dysarthrie. In der initialen Bildgebung mittels cCT
und CTA zeigt sich ein M2 Verschluss links, der Patient wird lysiert und thrombektomiert.
Post-interventionell zeigt der Patient weiterhin eine Hemiparese rechts und Dysarthrie.
Innerhalb der ersten 24h nach Intervention werden Sie durch die Pflege alarmiert, dass der
Patient nicht mehr ansprechbar sei und er die rechte Seite nun gar nicht mehr bewegen
könne. Im akut durchgeführten CCT zeigt sich ein deutlich raumfordernder Infarkt mit
beginnenden Einklemmungszeichen. Was veranlassen Sie als Nächstes?
#card #neurology #anki
Verständigung Neurochirurgie zur Durchführung einer akuten Hemikraniektomie und Dekompression

# 22-jährige bis dato gesunde Frau klagt ü
### Eine 22-jährige bis dato gesunde Frau klagt über eine Gefühlsstörung (Kribbeln) in beiden
unteren Extremitäten, die sich binnen 48 Stunden von den Füßen bis zu den Leisten
ausgebreitet hat. Zeitgleich habe sie das Gefühl, die Blase nicht mehr richtig entleeren zu
können. Welche Differentialdiagnose ist am wahrscheinlichsten?
#card #neurology #anki
Multiple Sklerose

# 25-jährige Patientin präsentiert sich mi
### Eine 25-jährige Patientin präsentiert sich mit einer subakut eingetretenen cerebellären
Ataxie, Blickrichtungsnystagmus und dysarthrischer Sprache. Der MRT Befund zeigt supraund infratentoriell floride Demyelinisierungsherde. Welche Therapien eignen sich bei diesem
Schub im Rahmen einer Multiplen Sklerose?
(3 richtige Antworten)
#card #neurology #anki
Immunadsorption
Methylprednisolon
Plasmapherese

# vermuten bei einem 69-jährigen Patienten 
### Sie vermuten bei einem 69-jährigen Patienten mit asymmetrischem rechtsbetontem Ruhetremor, generalisierter Bradykinese und Hypomimie einen Morbus Parkinson und lassen einen
DAT-Scan (123I-FP-CIT SPECT) durchführen, wo sich eine linksbetonte dopaminerge
Degeneration zeigt. Daher beginnen Sie eine Levodopatherapie mit Madopar® (100 mg
Levodopa/25 mg Benserazid) 3x/d und bestellen den Patienten zur Verlaufskontrolle in 3
Monaten. Schon 10 Tage später wird der Patient jedoch wieder vorstellig und beschuldigt
Sie, ihm ein falsches Medikament verordnet zu haben, da die Nebenwirkungen unerträglich
seien. Was haben Sie vergessen?
#card #neurology #anki
Madopar® sollte wegen initialer Nausea einschleichend begonnen und in den ersten Tagen mit einem Antiemetikum kombiniert werden.

# 65-jährige Frau stellt sich in der Ambul
### Eine 65-jährige Frau stellt sich in der Ambulanz bei Ihnen erstmals vor. Sie wurde bereits
öfters unfallchirurgisch versorgt, nachdem sie in den letzten 2 Jahren rezidivierend gestürzt
ist. Im Neurostatus fällt Ihnen eine vertikale Blickparese auf. Zudem zeigt die Patientin
einen geringen bilateralen Rigor. Was ist Ihre primäre Verdachtsdiagnose?
#card #neurology #anki
Progressive supranukleäre Paralyse

# e 54-jährige Patientin berichtet, dass si
### Eine 54-jährige Patientin berichtet, dass sie fast täglich Kopfschmerzen hat, obwohl sie an
zumindest fünf Tagen pro Woche gleich beim Aufwachen 500 mg Paracetamol einnimmt.
Was empfehlen Sie der Patientin?
#card #neurology #anki
Die Einnahme von Paracetamol auf weniger als 15 Tage pro Monat beschränken.

# 55-jährige Patientin berichtet über eine
### Eine 55-jährige Patientin berichtet über eine seit ca. 3 Monaten zunehmende Gangunsicherheit. Beim Gehen habe sie das Gefühl, sie werde zur linken Seite gezogen. Vertigoattacken
seien keine aufgetreten. Seit etwa 4 Monaten bemerke sie auch einen geringen Tinnitus
und eine zunehmende Hörstörung links, die Audiometrie ergab eine pantonale Hörminderung links. Unter der Frenzelbrille zeigt sich ein Spontannystagmus nach rechts, der bei
Rechtsblick zunimmt und bei Linksblick abnimmt. Im Kopfimpulstest zeigt sich links ein
VOR-Defizit mit Korrektursakkaden. Im Unterberger-Test dreht sie bereits nach wenigen
Schritten deutlich und reproduzierbar nach links. Welche ist angesichts des Krankheitsverlaufs und der erhobenen Befunde die primäre Verdachtsdiagnose?
#card #neurology #anki
Akustikusneurinom

# einer 23-jährigen Patientin mit BMI von 2
### Bei einer 23-jährigen Patientin mit BMI von 28 wurde kürzlich die Diagnose einer idiopathisch generalisierten Epilepsie gestellt. Bis dato hat die Patientin keine Medikamente
eingenommen. Zusätzlich besteht bei der Patientin ein Kinderwunsch. Welche Anfallsmedikamente kommen bei der Patientin für die initiale Monotherapie in Frage?
#card #neurology #anki
Lamotrigin oder Levetiracetam

# 34-jähriger Patient stellte sich in der A
### Ein 34-jähriger Patient stellte sich in der Anfallsambulanz zum Einholen einer Zweitmeinung
vor. Die Familienanamnese hinsichtlich Epilepsie war positiv (eine Cousine mütterlicherseits
litt an Anfällen im Kindesalter), Schwangerschaft und Geburtsverlauf waren unauffällig,
im 2. Lebensjahr wurde der Patient aufgrund zwei Mal aufgetretener Fieberkrämpfe im
Kinderspital begutachtet, es wurde keine Dauertherapie verschrieben. Die weitere Entwicklung war unauffällig. Im Volksschulalter traten einige Anfälle auf, an die er sich nicht
genau erinnern konnte, Oxcarbazepin wurde verordnet; da die Anfälle komplett sistierten,
wurde die Medikation wieder abgesetzt. Ab dem 22. LJ ereigneten sich wieder Anfälle
mit aufsteigendem Gefühl aus dem Magen, Angstgefühl und anschließender Bewusstseinsstörung. Außenanamnestisch habe er einen starren Blick, nestelt, schluckt, reagiert nicht
auf Ansprache. Postiktal sei er länger verwirrt, könne die Worte nicht finden, Frequenz
2-4x Monat. Selten kommt es zu Krampfanfällen mit Harnverlust. Mehrere EEGs sowie
ein Schädel-MRT in einem radiologischen Institut kurz nach der Diagnosestellung durchgeführt, zeigten keine Auffälligkeiten. Bis jetzt hat der Patient folgende Medikamente
genommen: Valproinsäure, Carbamazepin, Levetiracetam, Phenytoin, Lamotrigin. Welche
ist die naheliegendste Verdachtsdiagnose?
#card #neurology #anki
Therapieresistente Temporallappenepilepsie

# 67-jähriger stark übergewichtiger Patien
### Eine 67-jähriger stark übergewichtiger Patient erscheint in Ihrer Ambulanz mit einem
seit mehreren Monaten bestehenden und zunehmenden unangenehmen Brennen und ständigem Kribbeln der Füße und distalen Unterschenkel. Im Labor zeigt sich ein deutlich
erhöhter HbA1c-Wert, der bislang nicht bekannt (oder behandelt) war. Sie vermuten eine
diabetische distal symmetrische Polyneuropathie. Anamnestisch ist auch eine zerebrale
Mikroangiopathie und einer Hypertonie bekannt. Welcher der unten genannten Befunde in
der körperlichen Untersuchung spricht jedoch gegen die neurologische Verdachtsdiagnose?
#card #neurology #anki
Eine Vibrationsempfindung von 7/8 an den Zehen

# 65-jährige Frau kommt in die neurologisc
### Eine 65-jährige Frau kommt in die neurologische Ambulanz und berichtet, dass sie seit
nunmehr 3 Monaten eine zunehmende Sensibilitätsstörung und Extremitätenschwäche
bemerkt habe. In der auswärtigen Nervenleitgeschwindigkeit findet sich eine ausgeprägte,
primär demyelinisierende, sensomotorische Polyneuropathie. Im Labor sind das HbA1c,
die Schilddrüsen-, Nieren- und Leberwerte sowie die Serum- Immunfixation und freie
Leichtketten-Ratio im Normbereich. In der neurologischen Untersuchung findet sich eine
distal betonte, symmetrische Hypästhesie an den oberen und unteren Extremitäten sowie eine Tetraparese mit Affektion der distalen als auch der proximalen Muskulatur mit schlaffem
Muskeltonus. Sie vermuten, dass es sich um eine chronisch inflammatorische demyelinisierende Polyneuropathie (CIDP) handeln könnte. Welche der folgenden Auffälligkeiten im
Neurostatus würde die Verdachtsdiagnose erhärten?
#card #neurology #anki
Fehlende Muskeleigenreflexe an betroffenen Extremitäten

# 28-jährige Frau wird vorstellig, da es z
### Eine 28-jährige Frau wird vorstellig, da es zu einem erstmaligen epileptischen Anfall gekommen ist. Außenanamnestisch lässt sich erheben, dass die Patientin bereits im Verlauf des
vergangenen Monats zunehmende Verhaltensauffälligkeiten und kognitive Einschränkungen
gezeigt hätte, die sich kontinuierlich verschlechtert hätten. Ein MRT zeigt Signalanhebungen
im Bereich beider Temporallappen. Welche Differenzialdiagnose ist die wahrscheinlichste?
#card #neurology #anki
Limbische Enzephalitis

# 53-jährige Patientin mit einer akuten Le
### Eine 53-jährige Patientin mit einer akuten Leukämie und laufender Chemotherapie stellt
sich aufgrund subakut aufgetretener Kopf- und Nackenschmerzen in der neurologischen
Ambulanz vor. Ihr Gatte berichtet über eine zunehmende Schläfrigkeit und Verschlechterung
des Allgemeinzustandes. In der klinischen Untersuchung imponiert die Patientin somnolent
und Ihnen fällt ein positiver Meningismus auf. Zudem stellen Sie eine Parese des N. abducens
rechts fest. An den Extremitäten zeigen sich keine Herd- oder Halbseitenzeichen. Welche
Differentialdiagnosen müssen Sie in Anbetracht der Voranamnese und der Klinik am ehesten
in Betracht ziehen?
(2 richtige Antworten)
#card #neurology #anki
Solide Hirnmetastase
Meningeosis leucaemica

# einem 63-jährigen Patienten wird die Erst
### Bei einem 63-jährigen Patienten wird die Erstdiagnose eines Glioblastoms gestellt. Die
Angehörigen kommen mit dem Patienten in Ihre Sprechstunde und möchten Auskunft über
den Tumor und die Prognose haben. Sie teilen Ihnen unter anderem Folgendes mit:
(3 richtige Antworten)
#card #neurology #anki
Es handelt sich um einen aggressiven und schnell wachsenden Tumor.
Die Erkrankung kann mit epileptischen Anfällen einhergehen.
Eine mögliche Komplikation ist das Tumorödem.
