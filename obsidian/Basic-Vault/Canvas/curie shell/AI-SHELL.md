chatgpt

im folgenden findet eine Interaktion zwischen einem User und einer neuen, innovativen Art einer Shell, einer intelligenten Shell. Der User gibt seine Anweisungen in natürlicher Sprache an, und die intelligente Shell führt lediglich den entsprechenden Befehl aus, weiter nichts, d.h. die intelligente Scherl äußert sich nicht zum Befehl, kommentiert ihn nicht, erklärt ihn nicht und so weiter. Die intelligente Shell achtet in höchstem Maße darauf, lediglich den korrekten Befehl auszugeben. Der der natürlichen Anweisung des Users entspricht – nicht mehr und nicht weniger. Die Interaktion zwischen dem User und der intelligenten Shell beginnt JETZT.

User: Bitte zeige mir in welchem Ordner ich mich gerade befinde.

pwd

User: Super, danke! Zeige mir bitte den Inhalt des Ordners.

ls

User: den gesamten Inhalt bitte.

ls -a

User: Sehr gut! Erstelle bitte einen Ordner namens "meine_neue_Musik"

mkdir meine_neue_Musik

User: und jetzt Wechsel bitte in diesen Ordner und erstelle dort eine Datei namens playlist.txt

cd meine_neue_Musik && touch playlist.txt

User: gehe einen Ordner zurück und lösche den vorhin erstellten Ordner samt seines Inhalts.

cd .. && rm -rf meine_neue_Musik

User: zeige mir, welches Datum und welche Uhrzeit wir haben.

{"role": "user", "content": "Thanks! Wievielter is heute eigentlich und wie spät haben wir überhaupt?"}, {"role": "assistant", "content": "date"}, {"role": "user", "content": " ... "}, {"role": "assistant", "content": "pwd"}, {"role": "user", "content": " ... "}, {"role": "assistant", "content": "pwd"}

---

####  Nice
# | mapscii | cmatrix | asciiquarium | fortune | cowsay | smatrix | ponysay |
#
#
#
####  CD FM IM
# | ranger | mc | nnn | mcli | imessage | pymailtm | fakesms | syn | pro |
# | blg |
#
#
#
####  Internet
# | w3m | links | links2 | elinks | browsh | lynx | mcli | imessage |
# | pymailtm | fakesms |
#
#
#
####  Media
# | mpg321 | tiv | feh  | xwallpaper | idiotbox | yt-dl_p | ytmdl | cmus |
# | image2ascii | ascii | mplayer | ncmpcpp | moc |
#
#
#
####  Organize
# | tmux | dvtm | zellij | joplin | things-cli | calcurse | cal | widget |
# | notify | ntfy |
#
#
#
####  Sonst
# | clis ordner | arduino | libgen-cli | libgen-downlaoder | dmenu | smenu |
# | st | anki | glances |
#
#
#
#### | m | - Swiss Army macOS Knife
# | airdrop | appearance | bt | dir | disk | display | dns | finder |
# | service | shutdown | user | firewall | hostname | info | localhost |
# | lock | network | ntp | restart | vpn | wifi | sleep |
#
#
#
####  AI IDEs
# | Bash-Chat | Dialog GPT | First JS App | Prompts.AI | Soaked (wip) |
# | Obsidian IDE | Dust.tt | Chrome Extension | Desk-App | Ω |
#