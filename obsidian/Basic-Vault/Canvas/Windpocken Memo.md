---
PromptInfo:
 promptId: modArtStation
 name: Generate a Trending on ArtStation photo 
 description: This modifier will sample extra training data from the most-liked artwork from the website ArtStation. Images which trend on ArtStation are usually very visually-appealing as it means the ArtStation community enjoys those images, so filtering the data to produce images similar to those will greatly increase the quality of the generated art.
 author: Prompt Engineering Guide
 tags: photo, dalle-2, modifier
 version: 0.0.1
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "1024x1024"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
tags: medicine, ai_ressource
---
ein Bild von einer Gruppe von Schlägertypen, die versuchen, ein Feuer in einem verschneiten Wald zu löschen, Trending on ArtStation ![](https://oaidalleapiprodscus.blob.core.windows.net/private/org-OktF1oCu6UTkdnm9anKvQw8B/user-IISs3JUtt88C6t5SHVfQ6DeF/img-Ou5OMGqZxTaZw4eah18wlhXT.png?st=2023-02-22T14%3A40%3A24Z&se=2023-02-22T16%3A40%3A24Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-02-21T21%3A55%3A17Z&ske=2023-02-22T21%3A55%3A17Z&sks=b&skv=2021-08-06&sig=5%2BJ0dH6trubVTNREQv4BbjRInuOxG1PtQvL8IP4Nbo0%3D)