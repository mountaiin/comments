---
PromptInfo:
 promptId: artModern
 name: 🖼️ Generate a modern art photo 
 description: select a text and photo with the style of modern art will be generated using Dalle-2
 author: Prompt Engineering Guide
 tags: photo, dalle-2,art
 version: 0.0.1
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "1024x1024"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---
full moon, cool night, strong wind, three thugs, trying to put out a fire in a snowy forest painted by artgerm and tom  moon, cool night, strong wind, three thugs, trying to put out a fire in a snowy forest near a church, by katsuhiro otomo, yoshitaka amano and james jean, moebius, artstation, concept art, retro computer graphics, video game, sharp focus, high detail ![](https://oaidalleapiprodscus.blob.core.windows.net/private/org-OktF1oCu6UTkdnm9anKvQw8B/user-IISs3JUtt88C6t5SHVfQ6DeF/img-dK3mrpa9VjuhYyzWUNh36PH8.png?st=2023-02-22T15%3A26%3A11Z&se=2023-02-22T17%3A26%3A11Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-02-21T21%3A54%3A58Z&ske=2023-02-22T21%3A54%3A58Z&sks=b&skv=2021-08-06&sig=cpOO/DIslcy7T%2BugumR87uev6ejOAp5myPkk//N8lqQ%3D)


https://huggingface.co/spaces/huggingface-projects/magic-diffusion