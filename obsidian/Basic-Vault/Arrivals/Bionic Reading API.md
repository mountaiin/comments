
```curl --request POST \
--url https://bionic-reading1.p.rapidapi.com/convert \
--header 'X-RapidAPI-Host: bionic-reading1.p.rapidapi.com' \
--header 'X-RapidAPI-Key: 6cd416d137msh549dd7f03005ad2p15e77fjsn1dc0f25b6c42' \
--header 'content-type: application/x-www-form-urlencoded' \
--data 'content={{INHALT}}' \
--data response_type=html \
--data request_type=html \
--data fixation=1 \
--data saccade=10
```


<div class="bionic-reader bionic-reader-e39f077d-5cbb-436a-83c2-4cd2899b2fb3 origin " style=" " >
    <div class="bionic-reader-content">
        <div class="bionic-reader-container">
        
            
            
            <b class="bionic-b bionic">Lor</b>em <b class="bionic-b bionic">ips</b>um <b class="bionic-b bionic">dol</b>or <b class="bionic-b bionic">si</b>t <b class="bionic-b bionic">ame</b>t, <b class="bionic-b bionic">consetet</b>ur <b class="bionic-b bionic">sadipsci</b>ng <b class="bionic-b bionic">eli</b>tr, <b class="bionic-b bionic">se</b>d <b class="bionic-b bionic">dia</b>m <b class="bionic-b bionic">nonu</b>my <b class="bionic-b bionic">eirm</b>od <b class="bionic-b bionic">temp</b>or <b class="bionic-b bionic">invidu</b>nt <b class="bionic-b bionic">u</b>t <b class="bionic-b bionic">labo</b>re <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">dolo</b>re <b class="bionic-b bionic">mag</b>na <b class="bionic-b bionic">aliquy</b>am <b class="bionic-b bionic">era</b>t, <b class="bionic-b bionic">se</b>d <b class="bionic-b bionic">dia</b>m <b class="bionic-b bionic">volupt</b>ua. <b class="bionic-b bionic">A</b>t <b class="bionic-b bionic">ver</b>o <b class="bionic-b bionic">eo</b>s <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">accus</b>am <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">jus</b>to <b class="bionic-b bionic">du</b>o <b class="bionic-b bionic">dolor</b>es <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">e</b>a <b class="bionic-b bionic">reb</b>um. <b class="bionic-b bionic">Ste</b>t <b class="bionic-b bionic">cli</b>ta <b class="bionic-b bionic">kas</b>d <b class="bionic-b bionic">gubergr</b>en, <b class="bionic-b bionic">n</b>o <b class="bionic-b bionic">se</b>a <b class="bionic-b bionic">takima</b>ta <b class="bionic-b bionic">sanct</b>us <b class="bionic-b bionic">es</b>t <b class="bionic-b bionic">Lor</b>em <b class="bionic-b bionic">ips</b>um <b class="bionic-b bionic">dol</b>or <b class="bionic-b bionic">si</b>t <b class="bionic-b bionic">ame</b>t. <b class="bionic-b bionic">Lor</b>em <b class="bionic-b bionic">ips</b>um <b class="bionic-b bionic">dol</b>or <b class="bionic-b bionic">si</b>t <b class="bionic-b bionic">ame</b>t, <b class="bionic-b bionic">consetet</b>ur <b class="bionic-b bionic">sadipsci</b>ng <b class="bionic-b bionic">eli</b>tr, <b class="bionic-b bionic">se</b>d <b class="bionic-b bionic">dia</b>m <b class="bionic-b bionic">nonu</b>my <b class="bionic-b bionic">eirm</b>od <b class="bionic-b bionic">temp</b>or <b class="bionic-b bionic">invidu</b>nt <b class="bionic-b bionic">u</b>t <b class="bionic-b bionic">labo</b>re <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">dolo</b>re <b class="bionic-b bionic">mag</b>na <b class="bionic-b bionic">aliquy</b>am <b class="bionic-b bionic">era</b>t, <b class="bionic-b bionic">se</b>d <b class="bionic-b bionic">dia</b>m <b class="bionic-b bionic">volupt</b>ua. <b class="bionic-b bionic">A</b>t <b class="bionic-b bionic">ver</b>o <b class="bionic-b bionic">eo</b>s <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">accus</b>am <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">jus</b>to <b class="bionic-b bionic">du</b>o <b class="bionic-b bionic">dolor</b>es <b class="bionic-b bionic">e</b>t <b class="bionic-b bionic">e</b>a <b class="bionic-b bionic">reb</b>um. <b class="bionic-b bionic">Ste</b>t <b class="bionic-b bionic">cli</b>ta <b class="bionic-b bionic">kas</b>d <b class="bionic-b bionic">gubergr</b>en, <b class="bionic-b bionic">n</b>o <b class="bionic-b bionic">se</b>a <b class="bionic-b bionic">takima</b>ta <b class="bionic-b bionic">sanct</b>us <b class="bionic-b bionic">es</b>t <b class="bionic-b bionic">Lor</b>em <b class="bionic-b bionic">ips</b>um <b class="bionic-b bionic">dol</b>or <b class="bionic-b bionic">si</b>t <b class="bionic-b bionic">ame</b>t.
            
                <!-- <div class="br-foot-node">
                    <p style="margin: 32px 0 32px 70px; font-weight: 700; font-size: 26px; line-height: 1.6em;">
                        —
                    </p>
                    <p>
                        Bionic Reading<sup>®</sup><br>
                        A higher dimension of reading.<br>
                        <a href="https://bionic-reading.com">bionic-reading.com</a>
                    </p>
                    <br/>
                    <br/>
                    <p>
                        
                    </p>
                </div> -->
            
        </div>
    </div>
</div>
