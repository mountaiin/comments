### The Waluigi Effect (mega-post) - LessWrong
#card #webclipped
If this Semiotic–Simulation Theory is correct, then RLHF is an irreparably inadequate solution to the AI alignment problem, and RLHF is probably increasing the likelihood of a misalignment catastrophe.
<br>
> 

- - -
[The Waluigi Effect (mega-post) - LessWrong](https://www.lesswrong.com/posts/D7PumeYTDPfBTp3i7/the-waluigi-effect-mega-post#_2__Empirical_evidence_from_Perez_et_al_) – 18/03/2023 15:39### The Waluigi Effect (mega-post) - LessWrong
#card #webclipped
If this Semiotic–Simulation Theory is correct, then RLHF is an irreparably inadequate solution to the AI alignment problem, and RLHF is probably increasing the likelihood of a misalignment catastrophe.
<br>
> 

- - -
[The Waluigi Effect (mega-post) - LessWrong](https://www.lesswrong.com/posts/D7PumeYTDPfBTp3i7/the-waluigi-effect-mega-post#_2__Empirical_evidence_from_Perez_et_al_) – 18/03/2023 15:39