\section*{Zusammenfassung}
The influenza virus causes a very high number of illnesses and deaths annually throughout Europe. Prior to the Covid-19 pandemic, among the 30 most relevant infectious diseases in Europe, influenza had the highest incidence (annual infections/100,000) and at the same time the highest mortality (annual deaths/100,000). The annual influenza virus activity, commonly known as the "wave of influenza," results in hospitalization of a large number of patients each year. According to Suerbaum et al., the most effective preventive measure is vaccination. After vaccination, influenza illness tends to be milder and with fewer complications than in unvaccinated individuals. This retrospective analysis used data from hospitalized patients with influenza between October 1, 2017 and April 30, 2018 in the fourth medical department at Klinik Favoriten. The primary outcome parameter was survival rate at discharge, as well as 90-day mortality. Other important parameters considered were differences in clinic and complication rates between vaccinated and unvaccinated patients. The findings should allow conclusions to be drawn about how much an infection after vaccination is less severe.

There was no significant correlation found when comparing symptoms and complications between vaccinated and unvaccinated patients. Acute kidney failure was the only complication that occurred significantly more frequently in unvaccinated patients. When examining the outcomes of "death in hospital" and 90-day mortality, there was also no significant correlation found. Comparing the number of vaccinated patients (n=40) with the number of non-vaccinated patients (n=164), it can be concluded that vaccinated patients had fewer clinical symptoms, fewer complications, and lower mortality than non-vaccinated patients. However, further research with almost identical cohort sizes is needed to confirm or refute this assumption.

\section*{Abstract in English}
The annual influenza virus activity, commonly known as the "wave of influenza," results in hospitalization of a large number of patients each year. Comparing the number of vaccinated patients (n=40) with the number of non-vaccinated patients (n=164), it can be concluded that vaccinated patients had fewer clinical symptoms, fewer complications, and lower mortality than non-vaccinated patients. However, further research with almost identical cohort sizes is needed to confirm or refute this assumption.

\section*{Hintergrund}
Influenza viruses are RNA viruses of the Orthomyxoviridae family ("myxa", Greek for mucus). Within the Orthomoxyviridae family, there are seven genera, four of which fall under the Influenza A, B, C, and D genera. Different subtypes are distinguished within the Influenza virus genera, of which the subtypes of the Influenza A, B, and C genera are pathogenic to humans. While Influenza B is mainly pathogenic to humans, the Influenza A and C genera pose a threat to a wider range of species. The Influenza D genus is currently known to infect pigs, cattle, and camels.

---

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}

\title{Influenzavirus und Schutzimpfung: Eine retrospektive Analyse}
\author{}
\date{}

\begin{document}

\maketitle

\section*{Zusammenfassung}

Das Influenzavirus verursacht jährlich europaweit eine sehr hohe Anzahl an Krankheits- und Todesfällen. Vor der Covid-19 Pandemie wies die Influenza unter den europaweit 30 relevantesten Infektionskrankheiten die höchste Inzidenz (jährliche Infektionen/100.000) und gleichzeitig die höchste Mortalität (jährliche Todesfälle/100.000) auf~\cite{impact}. 

Die jährliche Influenzavirusaktivität, allgemein als „Grippewelle“ bekannt, führt jedes Jahr zu einer Hospitalisierung einer Vielzahl von PatientInnen.

Die wirksamste Präventionsmaßname ist laut Suerbaum et. al. die Schutzimpfung.\footnote{Suerbaum et. al., Medizinische Mikrobiologie und Infektiologie, 7. Auflage, Springer, 2016}

Nach erfolgter Impfung verlaufe die Influenzaerkrankung milder und mit weniger Komplikationen als bei Ungeimpften.

Diese retrospektive Analyse verwendete die Daten der an Influenza erkrankten, zwischen dem 01.10.2017 und dem 30.04.2018 hospitalisierten PatientInnen der 4. Medizinischen Abteilung an der Klinik Favoriten~\cite{oved}.

Als primärer Zielparameter wurde das Outcome in Form von der Überlebensrate bei Entlassung, sowie der 90 Tages Mortalität festgelegt. Weitere wichtige Parameter, die betrachtet wurden, sind die Unterschiede in der Klinik und in der Komplikationsrate zwischen geimpften und nicht geimpften PatientInnen.

Die Erkenntnisse sollten Rückschlüsse darauf erlauben, in wie fern eine Infektion nach erfolgter Impfung milder verläuft.

\section*{Weiters}

Bezugnehmend auf die erhobenen Symptome und Komplikationen bei geimpften und nicht geimpften PatientInnen ergibt sich kein signifikanter Zusammenhang. Die Komplikation des akuten Nierenversagens trat als einziges signifikant häufiger bei nicht geimpften Patient*innen auf~\cite{ruf}.

Bei der Betrachtung des Outcomes „Tod im Spital“, sowie der 90-Tages-Mortalität ergab sich ebenfalls kein signifikanter Zusammenhang.

\bibliographystyle{unsrt}
\bibliography{references}

\end{document}
```

Und hier ist die Referenzliste `references.bib`:

```bibtex
@article{impact,
    title={Impact of infectious diseases on population health usid incidence-based disability-adjusted life years:},
    author={},
    journal={},
    year={}
}

@article{oved,
    title={A novel host-proteome signature for distinguishing between acute bacterial and viral infections},
    author={Oved, Kfir and Cohen, Asaf and Boico, Orian and Navon, Roy and Friedman, Tal and Etshtein, Liron and Kriger, Ofer and Bamberger, Ellen and Fonar, Yelena and Yacobov, Ronit and Szwarcwort-Cohen, Moran and Averbuch, Diana and Avni, Yuval S. and Eden, Elon and Ben-Ami, Ronen and Kobiler, David and Engelhard, Dan and Glatt, Shlomo and Vardi, Amos and Paret, Gideon and Neuberger, Ami and Moses, Allon E. and Block, Colin and Lewis, Yehezkel E. and Nir-Paz, Ran},
    journal={PLoS ONE},
    volume={10},
    number={3},
    pages={e120012},
    year={2015}
}

@article{ruf,
    title={The burden of seasonal and pandemic influenca in infants and children},
    author={Ruf, Bernhard R. and Knuf, Markus},
    journal={Eur J Pediatr},
    volume={173},
    pages={265-276},
    year={2014}
}
```