#!/usr/bin/env bash



if [ -z "$MODELS" ]; then
    echo "MODELS Umgebungsvariable ist nicht gesetzt. Skript wird beendet."
    exit 1
fi

if [ -z "$PROMPTS" ]; then
    echo "PROMPTS Umgebungsvariable ist nicht gesetzt. Skript wird beendet."
    exit 1
fi

# Get the list of models
model_directory="$MODELS"
models=( $(find $model_directory -type f -name "*.bin" | grep -v '.DS_Store' | xargs -n1 basename) )

# Get the list of prompts
prompt_directory="$PROMPTS"
prompts=( $(find $prompt_directory -type f -name "*.txt" | grep -v '.DS_Store' | xargs -n1 basename) )

# Prompt the user to choose a model
echo "Welches Modell möchtest du gerne auswählen?"
for i in "${!models[@]}"; do
    echo "$((i+1))) ${models[i]}"
done

read -p "Bitte wähle die Nummer des Modells: " model_index
model_index=$((model_index-1))

if [ "$model_index" -ge 0 ] && [ "$model_index" -lt "${#models[@]}" ]; then
    MODEL="-m $(find $model_directory -type f -name "${models[model_index]}") "
else
    echo "Ungültige Auswahl. Skript wird beendet."
    exit 1
fi

# Prompt the user to choose a prompt
echo "Welchen Prompt-Text möchtest du auswählen?"
for i in "${!prompts[@]}"; do
    echo "$((i+1))) ${prompts[i]}"
done

read -p "Bitte wähle die Nummer des Texts: " prompt_index
prompt_index=$((prompt_index-1))

if [ "$prompt_index" -ge 0 ] && [ "$prompt_index" -lt "${#prompts[@]}" ]; then
    PROMPT="--file $(find $prompt_directory -type f -name "${prompts[prompt_index]}") "
else
    echo "Ungültige Auswahl. Skript wird beendet."
    exit 1
fi


llamacpp    $MODEL          $PROMPT             \
            --color         --instruct          \
            -r "User:"      --threads 4         \
            --keep -1       --batch_size 4      \
            --temp 2.33      --top_p 0.38       \
            --top_k 8      --n_predict -1     	\
            --repeat_penalty 1.3                \
            --ctx_size 2048			\
            --repeat_last_n 256

