### Asian carp - Wikipedia
#card #webclipped
A long tradition of Asian carp exists in Chinese culture and literature. A popular lyric circulating as early as 2,000 years ago in the late Han period includes an anecdote which relates how a man far away from home sent back to his wife a pair of carp (Chinese: 鲤鱼; pinyin: Liyu), in which, when the wife opened the fish to cook, she found a silk strip that carried a love note of just two lines: "Eat well to keep fit, missing you and forget me not".

At the Yellow River at Henan (Chinese: 河南; pinyin: Hénán; Wade–Giles: Ho-nan) is a waterfall called the Dragon Gate. It is said that if certain carp called yulong can climb the cataract, they will transform into dragons. Every year in the third month of spring, they swim up from the sea and gather in vast numbers in the pool at the foot of the falls. It used to be said that only 71 could make the climb in any year. When the first succeeded, then the rains would begin to fall. This Dragon Gate was said to have been created after the flood by the god-emperor Yu, who split a mountain blocking the path of the Yellow River. It was so famous that throughout China was a common saying, "a student facing his examinations is like a carp attempting to leap the Dragon Gate."
<br>
> 

- - -
[Asian carp - Wikipedia](https://en.wikipedia.org/wiki/Asian_carp) – 18/03/2023 18:35