---
alias: breast cancer, mamma carcinom
---

Brustkrebs (Brustkrebs) ist eine häufigste Form von Krebs bei Frauen. Es betrifft in der Regel die Drüsen des Brustgewebes, aber es kann auch andere Teile der Brust betreffen. Die Ursache ist noch unklar, aber Risikofaktoren sind Alter, familiäre Vorbelastung, Hormone, Ernährung und Rauchen.

Symptome von Brustkrebs sind:

- Ein harter Knoten in der Brust
- Ein flaches Ödem
- Schmerzen in der Brust
- Veränderung der Form der Brust
- Völlige Verhärtung der Brust
- Verfärbung der Haut
- Dicke Warzen
- Flüssigkeitsausstoß aus der Brust

Früherkennungsmaßnahmen für Brustkrebs sind mammographische Untersuchungen und klinische Brustuntersuchungen. Wenn ein Knoten gefunden wird, kann eine Biopsie durchgeführt werden, um festzustellen, ob es sich um Krebs handelt.

Behandlungsmethoden für Brustkrebs können Chemotherapie, Strahlentherapie, Operation und Hormontherapie sein. Sie müssen immer mit dem behandelnden Arzt besprochen werden. Möglicherweise benötigen Sie anschließend eine Nachsorge, z.B. eine spezielle Ernährung, psychologische Unterstützung oder physiotherapeutische Anwendungen.

---

## Breast cancer
is a type of cancer that develops from breast tissue. Most breast cancers are found in the outer left, right, or central areas of the breast. 

## Risk Factors

There are several known risk factors for developing breast cancer, including:

- Age 
- Genetics 
- A family history of breast cancer 
- Early menstruation 
- Late menopause 
- Having had previous breast biopsies 
- Being overweight 
- Taking certain medications 
- Having dense breast tissue 
- Having had radiation exposure

## Symptoms

The most common symptom of breast cancer is the presence of a lump or mass in the breast tissue. Other symptoms can include:

- Changes in the size or shape of the breast 
- Discharge from the nipples 
- Dimpling of the skin in the breast area 
- A sore or rash on the nipple 
- Swelling or thickening of the breast tissue

## Diagnosis

If you have symptoms of breast cancer or if you have any risk factors for the disease, your doctor may recommend that you have a mammogram. This is a special type of X-ray that can detect changes in the breast tissue. Your doctor may also recommend other tests, including a biopsy, which is a tissue sample that can be examined to determine if cancer cells are present. 

## Treatment 

The treatment for breast cancer depends on the type of cancer and the stage of the disease. Treatments may include surgery, radiation therapy, chemotherapy, and/or hormone therapy. 

It is important to talk to your doctor about your treatment options and what is best for you.