---
---

---
Der Sputnik, der erste künstliche Erdsatellit, wurde von der Sowjetunion im Jahr 1957 als Teil des Kalten Krieges gestartet. Das Sputnik-Programm diente als Vorläufer für die bemannte Raumfahrt, die später mit der Umrundung der Erde und schließlich der Landung auf dem Mond durch die USA fortgesetzt wurde.

---

Die Geschichte der Raumfahrt (1): Der Sputnik.
Der Sputnik | Die Apollo-Mission | Das Space-Shuttle | Die Raumstationen | ISS-Beobachtung | Rückkehr zum Mond.
Der Dichter Jules Verne entwickelte 1865 in seinem Roman "Von der Erde zum Mond" eine Vision von der Zukunft des Menschen außerhalb seines Heimatplaneten:.
"So wie wir heute von Liverpool nach New York spazieren, wird man von der Erde zum Mond spazieren.
Durchquerung des Ozeans, Durchquerung des Weltenraumes - wo liegt da der grundsätzliche Unterschied?".
Der Schriftsteller stellte sich eine hohle Granate als Raumschiff vor, die von einer Kanone mit 900 Fuß (ca.
Rund einhundert Jahre später beginnt das Zeitalter der bemannten Raumfahrt, und ein kleiner Teil von Vernes Science Fiction wird Realität.
Bemerkenswerterweise liegt das Kennedy-Space-Center der USA in Cape Canaveral (Florida) nicht weit entfernt von Tampa, dem Raumfahrtzentrum des Romans.
Die bemannte Raumfahrt beginnt allerdings nicht mit der Fahrt zum Mond, sondern mit der Umrundung der Erde - und im Zeitalter des kalten Krieges, der Konfrontation der Supermächte USA und Sowjetunion, mit einem überraschenden Erfolg des Ostens.
Der Sputnik gewinnt das Rennen und schockiert den Westen.
Nachdem der US-Präsident Dwight D. Eisenhower 1955 die Entwicklung eines Satelliten angekündigt hatte, reagieren die Russen für den Westen unerwartet schnell.
= der Gefährte) die ersten Signale aus dem All zur Erde.
Das Sputnik - Programm ist weniger wissenschaftlich begründet, sondern eine Folge der politischen Rivalität zwischen den Vereinigten Staaten von Amerika und der Sowjetunion.
Sie ist 96 Tage in der Erdumlaufbahn (Orbit) und verglüht beim Wiedereintritt in die Erdatmosphäre.
An Bord der zweiten Sonde befindet sich die Hündin Leika, die aber nicht einmal den Start in der engen und heißen Kapsel überlebt - eine Rückkehr zur Erde war ohnehin nicht vorgesehen.