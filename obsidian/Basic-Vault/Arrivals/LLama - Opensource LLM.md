Ein Durchbruch? Gruselig? Digitale Atomwaffen in den Händen von Menschen, die dieser Verantwortung nicht gewachsen sind? Wir werden es sehen..

Habe ich vorhin gelesen, soll die Einschätzung sein, dass das wohl ein [[magic diffusion|Stable Diffusion]] Moment der LLMs sei.

Ich halte es nicht für abwegig, wenn in einem Jahr jeder in seiner Hosentasche ein SuperSmartphone mit lokaler KI trägt.