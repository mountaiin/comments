# Is the algorithm of intelligence easy?

- GPT-3 kann trotz seiner Größe und Fähigkeiten keine großen Zahlen in einem Schritt teilen
- Dennoch kann es komplexe Aufgaben bewältigen, die menschlicher Intuition nahekommen

# Transformers are not special

- Transformer-Architekturen sind nicht einzigartig leistungsfähig
- Ihre Dominanz ist eher auf ihre frühe Entdeckung und leichtere Parallelisierbarkeit zurückzuführen

# The field of modern machine learning remains immature

- Grundlegendes Verständnis von maschinellem Lernen hinkt hinter den Fortschritten her
- Viele Durchbrüche entstehen aus spontanen Ideen statt tiefgreifenden Theorien

# Scaling walls and data efficiency

- Chinchilla zeigt, dass größere Modelle oft untertrainiert sind
- Optimierung des Dateneinsatzes wird wichtiger werden

# Lessons from biology

- Menschliche Intelligenz ist wahrscheinlich nicht das globale Optimum für künstliche Intelligenz
- Biologische Anker dienen als Grenzen, aber mehr ist möglich

# Physical limits of hardware computation

- Landauer's Prinzip setzt eine Grenze für die Effizienz irreversibler Computerarchitekturen
- Koomey's Gesetz könnte weiterhin gelten, wenn auch mit verlangsamter Verdopplungszeit

**Fazit:** Der Autor glaubt, dass starke allgemeine KI bald kommen wird. Die aktuellen Fortschritte im Bereich maschinelles Lernen deuten darauf hin, dass wir noch weit entfernt von einer Sättigung der Möglichkeiten sind.

**Semi-rapid fire Q&A:**

- **GPT-N**: Wird immer größer und mächtiger
- **Datenbeschränkungen**: Werden zunehmend relevant, aber Lösungen sind absehbar
- **Hardwareverbesserungen**: Können helfen, aber möglicherweise nicht entscheidend sein
- **Biologie als Referenzpunkt**: Nützlich, aber begrenzt in Bezug auf das Potenzial von KI

Quelle: [Why I think strong general AI is coming soon](https://www.lesswrong.com/posts/K4urTDkBbtNuLivJx/why-i-think-strong-general-ai-is-coming-soon)