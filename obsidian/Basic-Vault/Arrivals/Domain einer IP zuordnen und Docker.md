Hallo Jason! Es freut mich, dass du dich an mich wendest. Um deine statische Website mit Docker auf deinem Linode-Server zu veröffentlichen und über yaz.ai.in erreichbar zu machen, musst du die folgenden Schritte ausführen:

1. Erstelle ein `Dockerfile` in dem Verzeichnis, in dem sich deine statische Website befindet (z.B., das `_site`-Verzeichnis, wenn du Eleventy verwendest). Das Dockerfile sollte so aussehen:

```
FROM nginx:alpine
COPY . /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

Dieses Dockerfile verwendet das offizielle Nginx-Image von Docker Hub und kopiert den Inhalt deines Verzeichnisses in das Standardverzeichnis für Nginx (/usr/share/nginx/html).

2. Baue das Docker-Image mit dem folgenden Befehl im selben Verzeichnis wie dein Dockerfile:

```
docker build -t my-static-site .
```

Ersetze "my-static-site" durch einen Namen deiner Wahl.

3. Starte einen neuen Docker-Container basierend auf dem erstellten Image und leite den Port 80 des Containers auf den gewünschten Port deines Hosts weiter (z.B., Port 8080):

```
docker run -d --name my-static-site-container -p 8080:80 my-static-site
```

4. Jetzt musst du einen Reverse Proxy einrichten, um Anfragen von yaz.ai.in an den Docker-Container weiterzuleiten. Du kannst dafür Nginx verwenden. Installiere Nginx auf deinem Linode-Server, falls noch nicht geschehen:

```
sudo apt-get update
sudo apt-get install nginx
```

5. Erstelle eine neue Nginx-Konfigurationsdatei für deine Domain (z.B., `/etc/nginx/sites-available/yaz.ai.in`):

```
server {
    listen 80;
    server_name yaz.ai.in;

    location / {
        proxy_pass http://localhost:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Passe den `proxy_pass`-Eintrag an, um auf den Port zu zeigen, auf dem dein Docker-Container läuft (in diesem Beispiel Port 8080).

6. Erstelle einen symbolischen Link zur Konfigurationsdatei im `sites-enabled`-Verzeichnis:

```
sudo ln -s /etc/nginx/sites-available/yaz.ai.in /etc/nginx/sites-enabled/
```

7. Überprüfe die Nginx-Konfiguration auf Fehler und starte den Dienst neu:

```
sudo nginx -t
sudo service nginx restart
```

8. Stelle sicher, dass die DNS-Einstellungen korrekt sind: Du solltest einen A-Record in deinem DNS-Provider (regery.com) erstellen oder aktualisieren, der yaz.ai.in auf die IP-Adresse 143.42.31.11 zeigt.

9. Warte einige Zeit, bis sich die DNS-Änderungen im Internet verbreitet haben. Dies kann zwischen ein paar Minuten und mehreren Stunden dauern, abhängig von verschiedenen Faktoren wie TTL-Werten und Caching.

Sobald die DNS-Änderungen wirksam geworden sind, sollte deine statische Website unter http://yaz.ai.in erreichbar sein. Wenn du HTTPS verwenden möchtest, empfehle ich dir, Let's Encrypt zu nutzen, um ein kostenloses SSL-Zertifikat zu erhalten und es mit Nginx zu konfigurieren. Dafür kannst du Certbot verwenden, ein Tool, das den Prozess automatisiert.

