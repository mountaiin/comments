import Foundation

let openAI_API_Key = "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"
let jsonString = """
{
    "model": "text-davinci-003",
    "prompt": "Say this is a test",
    "max_tokens": 7,
    "temperature": 0
}
"""

func makeOpenAIRequest(with jsonData: Data, completionHandler: @escaping (Result<Data, Error>) -> Void) {
    guard let url = URL(string: "https://api.openai.com/v1/completions") else {
        completionHandler(.failure(URLError(.badURL)))
        return
    }

    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("Bearer \(openAI_API_Key)", forHTTPHeaderField: "Authorization")
    request.httpBody = jsonData
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        if let error = error {
            completionHandler(.failure(error))
        } else if let data = data {
            completionHandler(.success(data))
        } else {
            completionHandler(.failure(URLError(.unknown)))
        }
    }
    
    task.resume()
}

guard let jsonData = jsonString.data(using: .utf8) else {
    fatalError("Could not convert JSON string to Data.")
}

let sem = DispatchSemaphore(value: 0)
makeOpenAIRequest(with: jsonData) { result in
    switch result {
    case .success(let data):
        if let jsonString = String(data: data, encoding: .utf8) {
            print(jsonString)
        } else {
            print("Failed to convert Data to String")
        }
    case .failure(let error):
        print("Error: \(error.localizedDescription)")
    }
    sem.signal()
}
sem.wait()