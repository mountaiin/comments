Während Chat läuft, sollte es nicht zum Stillstand wenn KI sich "erinnern" muss. Ähnlich wie Menschen, sollte ältere Erinnerung länger dauern. in AI Konzept wäre das Pyramide aus Agent - memory layer einer zusammenfassung | 2 Agenten - je ein memory layer einer zusammenfassung einer zusammenfasung | 4 Agenten - je ein memory layer einer zusammenfassung^3 | 5 Agenten - je ein memory layer einer zusammenfassung^4 | ...

Weiters: je entfernter ein Layer ist, desto eher könnte Vektor gekürzt werden
und je entfernter, desto stärker mit kompression arbeiten.

Folge -> sehr alte Erinnerung brauchen mehr Zeit zur Konsolidierung, da nicht mehr nur eine Frage des Speichers, sondern auch CPU Arbeit.

-> Deshalb sollte Chat weitergehen, bis Mem-&-CPU-Agenten es geschafft haben, Erinnerung zu rekonstruieren.

Wie ist der Zusammenhang zu [[spaced-repetition]]?