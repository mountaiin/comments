### 1960: The Year The Singularity Was Cancelled - LessWrong
#card #webclipped
In the 1950s, an Austrian scientist discovered a series of equations that he claimed could model history. They matched past data with startling accuracy. But when extended into the future, they predicted the world would end on November 13, 2026.

This sounds like the plot of a sci-fi book. But it’s also the story of Heinz von Foerster, a mid-century physicist, cybernetician, cognitive scientist, and philosopher.

His problems started when he became interested in human population dynamics.

(the rest of this section is loosely adapted from his Science paper “Doomsday: Friday, 13 November, A.D. 2026”)
<br>
> 

- - -
[1960: The Year The Singularity Was Cancelled - LessWrong](https://www.lesswrong.com/posts/bYrF8rXFYwPqnfxTp/1960-the-year-the-singularity-was-cancelled) – 18/03/2023 14:42