---
alias: a.i., ai, ki, k.i., künstliche Intelligenz,
---

Map of Concept zum Thema künstliche Intelligenz

- Maschinelles Lernen
- Neuronale Netze
- Deep Learning
- Natural Language Processing (NLP)
- Computer Vision
- Expertensysteme
- Robotik
- Autonome Systeme
- Big Data Analytics
- Predictive Analytics
- Sprachassistenten (z.B. Siri, Alexa)
- Chatbots und virtuelle Assistenten
- Gesichtserkennung und biometrische Identifikation
- Entscheidungsfindungsalgorithmen