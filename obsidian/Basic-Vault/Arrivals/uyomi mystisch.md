---
PromptInfo:
 promptId: ligGoldenHour
 name: 🌅
 description: Goldene Stunde Sonnenlicht
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "512x512"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---
1. Mond 
2. Abendstern 
3. Nachtgestirn 
4. Lichtbringer 
5. Himmelskörper 
6. Satellit 
7. Nachtgespenst  
8. Gespenstermond  								    9. Kugelgestirn  	                10 .Astralleib    11 .Himmelskupfer     12 .Silbermond       13 .Lunar        14 .Sternenmädchen      15 .Lunas        16 .Göttin der Nacht    17 .Nachtheldin     18 Astronautin     19 Hemisphäre      20 Kosmonaut       21 Mondgöttin      22 Mondsüchtig    23 Hellebarde      24 Sternenschweif     25 Glühbirne, Ambient Lighting Goldene Stunde Sonnenlicht
![](https://oaidalleapiprodscus.blob.core.windows.net/private/org-OktF1oCu6UTkdnm9anKvQw8B/user-IISs3JUtt88C6t5SHVfQ6DeF/img-fg3AfO6SeSz9pYdhmcoRSzCZ.png?st=2023-03-19T10%3A56%3A08Z&se=2023-03-19T12%3A56%3A08Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-03-19T11%3A21%3A08Z&ske=2023-03-20T11%3A21%3A08Z&sks=b&skv=2021-08-06&sig=rguZTsKKTkc3ToPy5aIg/NSBMbZAaVjjxdZFNCnFs/E%3D)