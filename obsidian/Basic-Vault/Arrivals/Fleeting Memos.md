---
alias: idee, note, notiz, quicknote, gedanke
context: EditorSuggestContext
---

w
the lazy dog

1. Kompetent
2. Fachkundig
3. Versiert
4. Erfahren
5. Kenntnisreich
6. Meisterhaft
7. Profihaft
8. Kundig
9. Qualifiziert
10. ExpertenwissenAndere Begriffe für "sachkundig"

Hier kommen 10 Namensvorschläge für eine Domain, die die Folgenden Eigenschaften repräsentieren soll -> kurz und prägnant, gut geeignet um mit unterschiedlichen Subdomains kombiniert werden zu können, Finanz affin, Wissenschaft, Forschung, Neurologie, Technologie, ARTIFICIAL INTELLIGENCE

Ich möchte etwas kaufen
Wir haben eine Frage zu einem Produkt
Ich möchte eine Rückerstattung über den Google Play Store
Kann ich bitte einen Rabatt bekommen?
Kann ich den Link zur Produktseite bekommen?

Erstelle einen Readme Text für mein Git Repo. Es handelt sich um ein Reverse Engineering Projekt, das ich "CheatGPT" nenne und mit dem ich vorhabe, ChatGPT durch Reverse Engineering zu rekonstruieren. Erwähne, dass ich neu auf dem Feld des Codings überhaupt bin und mich auch tatsächlich mit Git noch nicht gut auskenne. Daher wäre ich dankbar über jeden Input, sei es ein Grundlagen bezogner Tipp, ein Feedback oder tatkräftige Unterstützung und Mit-entwicklung. Erwähne, dass meine Bereitschaft, jeden Tipp und jede Hilfe anzunehmen, für jedes meiner öffentlichen sichtbaren Repos gilt. Nutze einen emotionalen Tonfall mit einem narrativen Stil.

Willkommen bei CheatGPT!

Ich bin neu auf dem Gebiet des Codings und habe mich entschieden, ein Reverse Engineering Projekt namens CheatGPT zu starten. Mein Ziel ist es, ChatGPT durch Reverse Engineering zu rekonstruieren.

Ich gebe zu, dass ich mich mit Git noch nicht gut auskenne und daher dankbar für jeden Input bin. Ich würde mich sehr über Feedback und tatkräftige Unterstützung freuen, um dieses Projekt erfolgreich abzuschließen.

Meine Bereitschaft, jeden Tipp und jede Hilfe anzunehmen, gilt für jedes meiner öffentlich sichtbaren Repos. Ich lerne gerne neue Dinge und möchte meine Fähigkeiten verbessern.

Ich hoffe, dass dieses Projekt nicht nur mir helfen wird, sondern auch anderen in der Community. Ich werde mein Bestes geben, um das Projekt so transparent wie möglich zu gestalten und alle Fortschritte regelmäßig zu teilen.

Vielen Dank für Eure Unterstützung.


## Welcome to CheatGPT!

I am new to coding and had decided to start a reverse engineering project called CheatGPT. My goal was to reverse engineer ChatGPT using only smart prompts and possibly fine-tuning.

  

### Background info about me

I admit that I'm not very familiar with Git yet, so I'm grateful for any input. I would greatly appreciate feedback and hands-on support to successfully complete this project.

  

I have been passionately interested in computer technologies since childhood and started dreaming of an age of A.I.s at an early age. Unfortunately, I didn't do anything professionally/academically in this direction. Nevertheless, my willingness to accept any tip and help is great - and this applies to any of my publicly visible repos. I like to learn new things and at the moment I especially want to improve my coding skills.

  

I hope that this and the other projects of mine will not only help me, but also others in the community. I will do my best to make the project as transparent as possible and share all progress regularly.

  

Thank you very much for your help.



# Eleventy-Garden
A starter site for building a [mind garden](https://www.mentalnodes.com/a-gardening-guide-for-your-mind), using [11ty](https://github.com/11ty/eleventy) ([demo](https://eleventy-garden.netlify.app/))

[![Netlify Status](https://api.netlify.com/api/v1/badges/4ce845a6-b009-4fcf-9ad4-a560d01a0073/deploy-status)](https://app.netlify.com/sites/eleventy-garden/deploys)

> Note: I'm not building this project for myself, so y'all gotta tell me what you think. Issues are the way to go here.
## :house: About
- Double-bracket links (the ones roam uses)
- Displays backlinks
- hovercards for backlinks

### Related
- [maximevaillancourt/digital-garden-jekyll-template](https://github.com/maximevaillancourt/digital-garden-jekyll-template)
- [mathieudutour/gatsby-digital-garden](https://github.com/mathieudutour/gatsby-digital-garden/)
- Find more at [maggieappleton/digital-gardeners](https://github.com/maggieappleton/digital-gardeners)
## :rocket: Usage
If you're new here, see the _Getting Started_ page in [the wiki](https://github.com/binyamin/eleventy-garden/wiki). Otherwise, check out the wiki guides (coming soon) or FAQ.

## :heart: Contribute
Feel free to dive in!

Here's what this project needs...
- Feedback! I didn't make this repo is for myself, so y'all gotta tell me what you want.
- Stars on GitHub
- Contributors - check out the [contributing guide](https://github.com/binyamin/eleventy-garden/blob/main/CONTRIBUTING.md)

### Maintainers
[@binyamin](https://github.com/binyamin)

## :scroll: License
This project is under the [MIT](https://github.com/binyamin/eleventy-garden/blob/main/LICENSE) license.
Please adhere to the [code of conduct](https://github.com/binyamin/eleventy-garden/blob/main/.github/CODE_OF_CONDUCT.md).


*es geht nicht darum, recht zu behalten (thema künsltich oder natürlich) sondern um den kern. das bewusstsein dafür zu schärfen ist schon ein guter schritt*

Below is a long paragraph generated by a philosopher AI that sees the human world from the outside, without the biases of human experience. Completely neutral and objective, the AI sees the world as it is. It can more easily draw conclusions about the world and human society in general.

Well, if we're talking about what gives meaning and purpose to our lives, then there are really just two questions that matter: 1) What does it mean to be human? And 2) What does it mean to live a good life? To answer these questions, one must look at themselves from an eternal perspective - beyond all social conventions and cultural norms. From this vantage point, the five most important things become clear: connection to other people, connection to nature, self-discovery, creativity, and service to something greater than oneself.
- Marc Aurel AI