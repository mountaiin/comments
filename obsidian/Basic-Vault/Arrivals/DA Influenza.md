## Zusammenfassung
Das Influenzavirus verursacht jährlich europaweit eine sehr hohe Anzahl an Krankheits- und Todesfällen. Vor der Covid-19 Pandemie wies die Influenza unter den europaweit 30 relevantesten Infektionskrankheiten die höchste Inzidenz (jährliche Infektionen/100.000) und gleichzeitig die höchste Mortalität (jährliche Todesfälle/100.000) auf.(Quelle: Impact of infectious diseases on population health usid incidence-based disability-adjusted life years:). 

Die jährliche Influenzavirusaktivität, allgemein als „Grippewelle“ bekannt, führt jedes Jahr zu einer Hospitalisierung einer Vielzahl von PatientInnen.

Die wirksamste Präventionsmaßname ist laut Suerbaum et. al. die Schutzimpfung. Fußnote

Nach erfolgter Impfung verlaufe die Influenzaerkrankung milder und mit weniger Komplikationen als bei Ungeimpften.

Diese retrospektive Analyse verwendete die Daten der an Influenza erkrankten, zwischen dem 01.10.2017 und dem 30.04.2018 hospitalisierten PatientInnen der 4. Medizinischen Abteilung an der Klinik Favoriten. (Quelle: Oved K, Cohen A, Boico O et al (2015) A novel host-proteome signature for distinguishing between acute bacterial and viral infections. PLoS ONE 10(3):e120012)

Als primärer Zielparameter wurde das Outcome in Form von der Überlebensrate bei Entlassung, sowie der 90 Tages Mortalität festgelegt. Weitere wichtige Parameter, die betrachtet wurden, sind die Unterschiede in der Klinik und in der Komplikationsrate zwischen geimpften und nicht geimpften PatientInnen.

Die Erkenntnisse sollten Rückschlüsse darauf erlauben, in wie fern eine Infektion nach erfolgter Impfung milder verläuft.

Bezugnehmend auf die erhobenen Symptome und Komplikationen bei geimpften und nicht geimpften PatientInnen ergibt sich kein signifikanter Zusammenhang. Die Komplikation des akuten Nierenversagens trat als einziges signifikant häufiger bei nicht geimpften Patient*innen auf. (Qelle: Ruf BR, Knuf M (2014) The burden of seasonal and pandemic influenca in infants and children. Eur J Pediatr 173:265-276)

Bei der Betrachtung des Outcomes „Tod im Spital“, sowie der 90-Tages-Mortalität ergab sich ebenfalls kein signifikanter Zusammenhang.

Wird die Anzahl der geimpften PatientInnen (n=40) mit der Anzahl der nicht geimpften PatientInnen (n=164) vergleichen, lassen sich Rückschlüsse auf die Problematik der Vergleichbarkeit ziehen. Die Tendenz zeigt, dass geimpfte PatientInnen weniger klinische Symptome, weniger Komplikationen und eine geringere Mortalität aufwiesen, als nicht geimpfte Patient*innen. Um diese Vermutung zu bestätigen oder zu widerlegen, bedarf es jedoch einer weiteren Untersuchung mit nahezu identischen Kohortengrößen.

## Abstract in english
The annual influenza virus activity, commonly known as the „wave of influenza," results in hospitalization of a large number of patients each year.

According to Suerbaum et. al. the most effective preventive measure is vaccination.

After vaccination, the course of influenza is milder and with fewer complications than in the unvaccinated.

This retrospective analysis used data from patients hospitalized with influenza between Oct. 1, 2017, and April 30, 2018, in the 4th Medical Department at Klinik Favoriten.  

The primary outcome parameter was defined as survival at discharge and 90-day mortality. Other important parameters considered were the differences in clinical outcome and complication rates between vaccinated and non-vaccinated patients.

The findings should allow conclusions to be drawn about the extent to which an infection is milder after vaccination has taken place.

There was no significant correlation between the symptoms and complications in vaccinated and non-vaccinated patients. The complication of acute kidney failure was the only one that occurred significantly more often in non-vaccinated patients.

When considering the outcome "death in hospital", as well as the 90-day mortality, there was also no significant correlation.

If the number of vaccinated patients (n=40) is compared with the number of non-vaccinated patients (n=164), conclusions can be drawn about the problem of comparability. The trend shows that vaccinated patients had fewer clinical symptoms, fewer complications and lower mortality than non-vaccinated patients. However, to confirm or refute this assumption, further research with almost identical cohort sizes is needed.

## Hintergrund
Influenzaviren sind RNA Viren der Familie der Orthomyxoviridae („myxa“, griechisch = Schleim). Es gibt innerhalb der Orthomoxyviridae sieben Gattungen, von welchen vier davon auf die Influenza Gattungen A, B, C und D fallen. Innerhalb der Influenza Viren Gattungen werden unterschiedliche Subtypen unterschieden, wovon die Subtypen der Gattungen Influenza A , B und Influenza C pathogen für den Menschen sind. Während Influenza B hauptsächlich für Menschen pathogen ist, stellen die Gattungen Influenza A und C eine Gefahr auch für eine breitere Anzahl an Spezies dar. Die Gattung Influenza D infiziert nach derzeitigem Kenntnisstand Schweine und Rinder und Kamele.

Sowohl Influenza A Virus (IAV) als auch Influenza B Virus (IBV) können Epidemien mit erheblicher Morbidität und Mortalität verursachen. Während IBV Infektion jedoch häufig auf lokal begrenzte Ausbrüche beschränkt bleiben, können IAV Infektionen für größere Epidemien und einschließlich weltweite Pandemien verantwortlich sein.

RNA Viren unterscheiden sich von DNA Viren insofern, als dass RNA chemisch weniger stabil und anfälliger gegenüber Umwelteinflüssen ist als DNA. Diese Instabilität führt zu einer erhöhten höheren Variabilitätsrate bei RNA Viren. Außerdem besitzen RNA Polymerasen im gegensatz zu DNA Polymerasen keine proof-reading-Exonuklease-Funktion, wodurch die RNA zusätzlich variabler wird. Dieser Umstand erklärt die hohe Mutations- und somit Anpassungsrate der Influenza Viren.