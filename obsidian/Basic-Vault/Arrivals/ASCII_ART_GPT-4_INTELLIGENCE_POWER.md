Generate an ASCII Art that shows a simple circle
```
      *****
  *         *
 *           *
 *           *
  *         *
      *****

```


Now Generate an ASCII Art that shows a bookshelf
```
 _________________________
|                         |
|  ______    ______    ______  |
|  |    |    |    |    |    |    |
|  |    |    |    |    |    |    |
|  |____|    |____|    |____|    |
|_________________________|

```


Now please Generate an ASCII Art that shows the side profile of a car
```
      ______
  ___//   \___\___
 /   [-]-[0]---[0]\
 '-----'-----'-----'

```


This doesnt look like the front of a car for me. Please try another, more accurate one.
```
	   ______
   /              \
 /  | |        | |  \
|___|_|________|_|___|
|_o_|            |_o_|
```


Please create the floor plan of a simple 4 room apartment in ASCII Art.
```
+--------------------------------+
|          Living Room           |
|                                |
|                                |
+----+----------------------+----+
|    |                      |    |
| Be |                      | Be |
| dr |      Kitchen         | dr |
| oo |                      | oo |
| m1 |                      | m2 |
|    |                      |    |
+----+----------------------+----+
|                                |
|          Bathroom              |
|                                |
+--------------------------------+
```


The bedrooms look pretty small, espacially when you compare them to the huge bathroom. Do you think that this makes sense? Or do you want to create a new version?
```
+----------------------+--------+
|       Living Room    |        |
|                      |  Bath  |
|                      |  room  |
+--------+-------+-----+--------+
|        |       |     |        |
| Bedroom|       |Kitchen/Dining|
|   1    |       |     |        |
|        |       |     |   Bed  |
+--------+-------+-----+   room +
|                            2   |
|                                |
+--------------------------------+
```



So here we try it again: 

1) While you are still in the main floor, (while the bathroom 1 is on your left) if you look straight forward, you will see a very big bedroom (bedroom 3).

2) Bathroom 2 is like 'attached' or 'anchored' on the backside of bathroom 1. Maybe this would help you to understand the relations between the rooms better.

3) The wider are of the main floor is set correctly (infront of bathroom 1) and so is the kitchen, but again: betweed bathroom 1 and the kitchen, the side is not just a wall, but actually consists two doors, from which the first leads to bedroom 3 and the second leads to the living-room.

4) You couldnt know it, but bedroom 1 and 2 have the opposite proportions when it comes to width*length compared to your draw. I mean, let assume you have drawed the rooms in the shape of 2*1 but they are actually 1*2

So based on the new information, try to create version 2, which is optimized.