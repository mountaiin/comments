- Das FBI hat umfangreich versucht, Martin Luther King und seine Bewegung zu "neutralisieren".
- Die Bemühungen des FBI umfassten Überwachung, Abhören von Telefonen und Räumen, sowie das Sammeln von Informationen.
- Die Kampagne gegen King war durch persönliche Rachsucht von Hoover und anderen FBI-Agenten geprägt.
- Die Operationen von COINTELPRO wurden 1972 aufgedeckt, als gestohlene Dokumente von der Citizens' Commission to Investigate the FBI an linke Zeitschriften weitergeleitet wurden.
- Das FBI hat auch nach der Aufdeckung von COINTELPRO weiterhin fragwürdige Überwachungspraktiken durchgeführt.

>Through counter-intelligence it should be possible to pinpoint potential trouble-makers and neutralize them.