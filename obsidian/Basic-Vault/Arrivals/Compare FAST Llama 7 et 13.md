| Modell | Framework  | sample ms/run | eval ms/run | ms/token |
| ------ | ---------- | ------------- | ----------- | -------- |
| 13Y    | llama      | 1.32          | 834         | 587      |
| 13Y    | FAST_llama | 0.76          | 608         |          |
| 7Y     | llama      | 1.28          | 253         | 391      |
| 7Y     | FAST_llama | 0.76          | 243         |          |

