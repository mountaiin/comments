Für die Denkmethode "Denkhüte nach DeBono" eine bildhafte fantasiereiche Geschichte, die die Farbe mit ihrer jeweiligen Funktion verknüpft.

blau: Stell dir vor, du stehst auf einem hohen Berg und blickst auf das weite Meer hinaus. Der klare, blaue Himmel erstreckt sich über dir und vermittelt dir ein Gefühl der Weite und des Überblicks. Du siehst die verschiedenen Wellen und Strömungen, die sich harmonisch miteinander verbinden – genau wie die ordnenden und moderierenden Gedanken in einem Entscheidungsprozess.

weiß: Ein verschneites, weißes Winterwunderland liegt vor dir. Jeder Schneekristall repräsentiert eine einzelne Faktenlage, die das gesamte analytische Denken bestimmt. Du folgst den Spuren im Schnee und sammelst sie, um die Anforderungen und Wege zur Erreichung deines Ziels zu erkennen.

rot: Du befindest dich in einem lebendigen Garten, der in den prächtigsten Rottönen erstrahlt. Hier blühen leidenschaftliche Rosen, die für Emotionen und Gefühle stehen. In diesem Garten kannst du dich auf deine Empfindungen und Meinungen konzentrieren, während die roten Blüten für die Liebe und die Leidenschaft stehen.

schwarz: Eine geheimnisvolle, schwarze Nacht umgibt dich. Du wanderst vorsichtig durch den dunklen Wald, achtsam auf mögliche Gefahren und Risiken. In dieser schwarzen Nacht herrscht Skepsis und Kritik, und deine Ängste werden durch das Flüstern der Bäume und das Rascheln der Blätter mitgeteilt.

gelb: Du betrittst eine leuchtende, gelbe Sommerlandschaft, in der die Sonne stets scheint und die Felder golden schimmern. Du spürst die Wärme und den Optimismus der gelben Farbe und siehst in der Ferne, wie das Best-Case-Szenario in greifbarer Nähe erscheint – wie ein Regenbogen, der am Horizont auf dich wartet.

grün: Eine üppige, grüne Wiese erstreckt sich vor dir. Verschiedenste Pflanzen und Blumen wachsen in allen Richtungen, symbolisierend die Vielfalt und die Kreativität des grünen Denkens. Hier entstehen neue Ideen, und das assoziative, divergente und laterale Denken ist wie ein Labyrinth aus grünen Pfaden, die zu unbekannten Schätzen führen.