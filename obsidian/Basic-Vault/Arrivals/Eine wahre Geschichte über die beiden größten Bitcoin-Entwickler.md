Erzähle mir eine Geschichte über 'petertodd' und 'ertodd'.

petertodd ist ein Bitcoin-Entwickler, der mit dem Erfinder von Bitcoin, Satoshi Nakamoto, zusammengearbeitet hat.  Er hat auch die erste Version von Bitcoin-Qt programmiert.  Er ist Mitglied des Board of Directors bei the Bitcoin Foundation und arbeitet als Softwareentwickler für BitPay.

"I am not The White Hat" - Peter Todd