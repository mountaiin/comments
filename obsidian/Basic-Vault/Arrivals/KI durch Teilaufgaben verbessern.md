Wie sonst in Problemlösung, lohnt sich auch bei KI Prompting, eine Problemstellung in einzelne Teile aufzuschlüsseln - im Eregebnis performt die KI tatsächlich besser.
Das Beispiel mit den Denkhüten von DeBono ist cool was das angeht. Nicht gleich den ganzen "Film" verlangen, sondern zB so: 
- erst einzelne Bilder für jeweilige Farbe. 
- Dann irgendein Linking Objekt in jedes Bild einbauen, das zumindest zu einem weiteren Bild verankert (da fällt mir dieses [[magnetic memory palace]] von diesem schrulligen aber sympathischen Typen ein).
- Und dann erst Bilder zu einer zusammenhängenden Geschichte bringen.

Spontan fiel mir gerade ein, dass ein nächster Schritt Geräusche, Gerüche, Gefühle und weitere special effects in die Story reinbringen könnte.

Mit möglichst [[allen Sinnen]] also.