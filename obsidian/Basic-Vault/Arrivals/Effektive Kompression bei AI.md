Idee 1 - Vokale entfernen
Am besten sollten aber die Vokale bei sehr kurzen Wörtern beibehalten werden. Und je länger das Wort, desto weiter steigt Wahrscheinlichkeit, dass Vokale entfernt werden.
Es muss einen cut-off geben, ab dem ganz sicher alle Vokale entfernt werden, weil Wörter dieser Länge bspw nahezu unverwechselbar werden.



Konkretes Beispiel 1, die Kompression wurde von GPT-4 erstellt:

#### Kompression
_Bitte entferne alle Vokale aus diesem Text:_

User: Hi! :) Welche sind eigentlich die 'effektivsten' Komprimierungsmethoden, um reinen Text zu komprimieren? 

Künstliche Intelligenz: Hallo Jason! Um deine Frage bestmöglich zu beantworten, wäre es hilfreich zu wissen, welche Prioritäten du bei der Komprimierung von reinem Text hast. Geht es dir dabei vor allem um möglichst hohe Komprimierungsraten, oder spielen Aspekte wie Geschwindigkeit und die Komplexität der Algorithmen auch eine Rolle? Denn die "effektivsten" Komprimierungsmethoden können je nach Anforderungen und Kontext variieren.

**s r: H! :) Wlch snd gntlich d 'ffktvstn' Kmprmrgngsmthdn, m rnn Txt z kmprmrm?**

**Knstlch Intllgnz: Hll Jsn! M dn Fr bstmglch z btwrtn, wr s hlfrch z wssn, wlc Prorttn d b dr Kmprmrg v rnn Txt hst. Ght s dr db vrm lst hgh Kmprmrgsrtn, dr spln Aspkt w Gschwndgkt nd d Kmplxtt d Algthmn ch n Rll? Dnn d "ffktvstn" Kmprmrgngsmthdn knn j nch Anfrdrngn nd Kn txt vr.**

---

#### GPT-3.5 Rekonstruktion
Ich habe hier einen Text, aus dem ein Großteil der Vokale entfernt wurden. Versuche die richtigen Vokale zu ermitteln und so den ursprünglichen Text zu rekonstruieren. Hier der Text: 

s r: H! :) Wlch snd gntlich d 'ffktvstn' Kmprmrgngsmthdn, m rnn Txt z kmprmrm? Knstlch Intllgnz: Hll Jsn! M dn Fr bstmglch z btwrtn, wr s hlfrch z wssn, wlc Prorttn d b dr Kmprmrg v rnn Txt hst. Ght s dr db vrm lst hgh Kmprmrgsrtn, dr spln Aspkt w Gschwndgkt nd d Kmplxtt d Algthmn ch n Rll? Dnn d "ffktvstn" Kmprmrgngsmthdn knn j nch Anfrdrngn nd Kn txt vr.

_So könnte der Text ursprünglich ausgesehen haben:_

**Es ist so: Hallo! :) Welche sind eigentlich die 'effektivsten' Komprimierungsmethoden, um einen Text zu komprimieren?**

**Künstliche Intelligenz: Hallo Jason! Um den Frage bestmöglich zu beantworten, wäre es hilfreich zu wissen, welche Prioritäten die bei der Komprimierung von einem Text hast. Geht es dir um die Verluste hoch Kompressionsraten, der sparen Aspekt wie Geschwindigkeit und die Komplexität der Algorithmus in Rollen? Denn die "effektivsten" Komprimierungsmethoden können je nach Anforderungen und Text variieren.**

---

#### GPT-4 Rekonstruktion

Ich habe hier einen Text, aus dem ein Großteil der Vokale entfernt wurden. Versuche die richtigen Vokale zu ermitteln und so den ursprünglichen Text zu rekonstruieren. Hier der Text: 

s r: H! :) Wlch snd gntlich d 'ffktvstn' Kmprmrgngsmthdn, m rnn Txt z kmprmrm? Knstlch Intllgnz: Hll Jsn! M dn Fr bstmglch z btwrtn, wr s hlfrch z wssn, wlc Prorttn d b dr Kmprmrg v rnn Txt hst. Ght s dr db vrm lst hgh Kmprmrgsrtn, dr spln Aspkt w Gschwndgkt nd d Kmplxtt d Algthmn ch n Rll? Dnn d "ffktvstn" Kmprmrgngsmthdn knn j nch Anfrdrngn nd Kn txt vr.

_Hier ist der Text mit den fehlenden Vokalen ergänzt:_

**User: Hallo! :) Welche sind eigentlich die 'effektivsten' Komprimierungsmethoden, um einen Text zu komprimieren?**

**Künstliche Intelligenz: Hallo Jason! Um deine Frage bestmöglich zu beantworten, wäre es hilfreich zu wissen, welche Prioritäten du bei der Komprimierung von einem Text hast. Geht es dir dabei vor allem um hohe Komprimierungsraten, oder spielen Aspekte wie Geschwindigkeit und die Komplexität der Algorithmen auch eine Rolle? Denn die "effektivsten" Komprimierungsmethoden können je nach Anforderungen und Kontext variieren.**

---

Beispiel 2

Hmn: Gd mrnng A!
A:  Gd mrnng! It's a btfl dy tdy, isn't it? Hw cn I hlp y?
Hmn: My intrst hr is t explr th ptntl of intgrtng Lrg Lngg Mdls wth extrnl knwldg
A:  Intrstng! Lrg Lngg Mdls ar a typ of artfcl intllgnc tht cn prcss ntrl lngg and gnrt txt. Thy cn b usd t gnrt txt frm a gvn cntxt, or t answr qstns abt a gvn cntxt. Intgrtng thm wth extrnl knwldg cn hlp thm t bttr undrstnd th cntxt and gnrt mr accrt rslts. Is thr anythng els I cn hlp y wth?
Hmn: I jst wnt t anlyz th dffrnt pssblts. Wht cn y thnk of?
A:  Wll, intgrtng Lrg Lngg Mdls wth extrnl knwldg cn opn up a lt of pssblts. Fr exmpl, y cld us thm t gnrt mr accrt and dtld smmrs of txt, or t answr qstns abt a gvn cntxt mr accrtly. Y cld als us thm t gnrt mr accrt trnsltns, or t gnrt mr accrt prdctns abt ftr evnts.
Hmn: Whch dt src typs cld b usd t gv cntxt t th mdl?
A:   Thr ar a vrty of dt srcs tht cld b usd t gv cntxt t a Lrg Lngg Mdl. Ths incld strctrd dt srcs sch as dtbss, unstrctrd dt srcs sch as txt dcmnts, and evn ad and vd dt srcs. Addtnlly, y cld us extrnl knwldg srcs sch as Wkpd or othr onln encyclpds t prvd addtnl cntxt.
Hmn: Wht is my am agn?
A:  Yr am is t explr th ptntl of intgrtng Lrg Lngg Mdls wth extrnl knwldg.

---

Human: Good morning AI!
AI:  Good morning! It's a beautiful day today, isn't it? How can I help you?
Human: My interest here is to explore the potential of integrating Large Language Models with external knowledge
AI:  Interesting! Large Language Models are a type of artificial intelligence that can process natural language and generate text. They can be used to generate text from a given context, or to answer questions about a given context. Integrating them with external knowledge can help them to better understand the context and generate more accurate results. Is there anything else I can help you with?
Human: I just want to analyze the different possibilities. What can you think of?
AI:  Well, integrating Large Language Models with external knowledge can open up a lot of possibilities. For example, you could use them to generate more accurate and detailed summaries of text, or to answer questions about a given context more accurately. You could also use them to generate more accurate translations, or to generate more accurate predictions about future events.
Human: Which data source types could be used to give context to the model?
AI:   There are a variety of data sources that could be used to give context to a Large Language Model. These include structured data sources such as databases, unstructured data sources such as text documents, and even audio and video data sources. Additionally, you could use external knowledge sources such as Wikipedia or other online encyclopedias to provide additional context.
Human: What is my aim again?
AI:  Your aim is to explore the potential of integrating Large Language Models with external knowledge.

#### Rekonstruktion durch 3.5

A: Good morning! It's a beautiful day today, isn't it? How can I help you?

Hmn: My interest here is to explore the potential of integrating Large Language Models with external knowledge.

A: Interesting! Large Language Models are a type of artificial intelligence that can process natural language and generate text. They can be used to generate text from a given context or to answer questions about a given context. Integrating them with external knowledge can help them to better understand the context and generate more accurate results. Is there anything else I can help you with?

Hmn: I just want to analyze the different possibilities. What can you think of?

A: Well, integrating Large Language Models with external knowledge can open up a lot of possibilities. For example, you could use them to generate more accurate and detailed summaries of text or to answer questions about a given context more accurately. You could also use them to generate more accurate translations or to generate more accurate predictions about future events.

Hmn: Which data source types could be used to give context to the model?

A: There are a variety of data sources that could be used to give context to a Large Language Model. These include structured data sources such as databases, unstructured data sources such as text documents, and even audio and video data sources. Additionally, you could use external knowledge sources such as Wikipedia or other online encyclopedias to provide additional context.

Hmn: What is my aim again?

A: Your aim is to explore the potential of integrating Large Language Models with external knowledge.