- - -
## Kompatibilität der Modell-Endpunkte
<br>

| ENDPUNKT                 | MODELLNAME                                                                                                                       |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------------- |
| /v1/**chat/completions** | **gpt-4**, gpt-4-0314, **gpt-4-32k**, gpt-4-32k-0314, **gpt-3.5-turbo**, gpt-3.5-turbo-0301                                      |
| /v1/**completions**      | **text-davinci-003**, text-davinci-002, **text-curie-001**, **text-babbage-001**, text-ada-001, **davinci**, curie, babbage, ada |
| /v1/**edits**            | text-davinci-edit-001, code-davinci-edit-001                                                                                     |
| /v1/audio/transcriptions | whisper-1                                                                                                                        |
| /v1/audio/translations   | whisper-1                                                                                                                        |
| /v1/fine-tunes           | davinci, curie, babbage, ada                                                                                                     |
| /v1/embeddings           | text-embedding-ada-002, text-search-ada-doc-001                                                                                  |
| /v1/moderations          | text-moderation-stable, text-moderation-latest                                                                                   |
| /v1/**completions**      | ==CODE DAVINCI FEHLT??==                                                                                                                                 |

<br>

[Alte Embeddings Modelle](https://platform.openai.com/docs/guides/embeddings/similarity-embeddings) Für Bilder Generierung siehe [[Image Generation - DALL-E|DALL-E]]

- - -

## [Kontinuierliche Modell-Upgrades](https://platform.openai.com/docs/models/continuous-model-upgrades)

Mit der Veröffentlichung von "gpt-3.5-turbo" werden einige unserer Modelle nun kontinuierlich aktualisiert. Um das Risiko zu verringern, dass sich Modelländerungen auf unerwartete Weise auf unsere Nutzer auswirken, bieten wir auch Modellversionen an, die für einen Zeitraum von 3 Monaten statisch bleiben. Mit dem neuen Rhythmus der Modellaktualisierungen bieten wir auch die Möglichkeit, Bewertungen beizusteuern, die uns helfen, das Modell für verschiedene Anwendungsfälle zu verbessern. Bei Interesse [hier](https://github.com/openai/evals) die Evals anschauen.

- - -


## Snapshots

Bei den folgenden Modellen handelt es sich um temporäre Snapshots, die zum angegebenen Datum veraltet sein werden. Wenn du die neueste Modellversion verwenden möchtest, benutze die Standardmodellnamen wie `gpt-4` oder `gpt-3.5-turbo`.


| MODELLNAME         | ABSCHREIBUNGSDATUM |
| ------------------ | ------------------ |
| gpt-3.5-turbo-0301 | 01.06.2023         |
| gpt-4-0314         | 14.06.2023         |
| gpt-4-32k-0314     | 14.06.2023         |



