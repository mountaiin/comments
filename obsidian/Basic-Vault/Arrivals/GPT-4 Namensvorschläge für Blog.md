Sei kreativ! Kreiere 8 episch, poetische und zynisch anmutende Bezeichnungen als Anspielung für "Persönliche Markdown Notizen, die als Homepages dienen". Die Bezeichnungen sollten aber nicht so lang werden wie meine Beschreibung, sondern lieber kürzer. Please write in emotional tone, narrative writing style, German language.

1.  Seelenblatt Skizzen
2.  Trugbilder des Selbst
3.  Sternenstaub-Gedankensplitter
4.  Heimathafen der Zerrissenheit
5.  Flüstern des digitalen Ichs
6.  Schattenlicht-Reflexionen
7.  Entfesselte Tintenträume
8.  Flackernde Seelenfackeln