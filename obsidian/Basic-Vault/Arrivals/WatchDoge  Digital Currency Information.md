### WatchDoge | Digital Currency Information & Commentary
#card #webclipped
Lee, Leo, Leigh, Li, Lion, Lions, Bitcoin
Posted on February 27, 2015
A few days back I posted this on BitconTalk.org:

WARNING – a little off topic, but tangentially related. 

Can someone tell me, still very much a tech noob, what the significance is of the term “lion” or names containing “Lee” or “Leo” (including homophones like “Leigh” or parts of names (“Cleo,” as a made up example)).  In my sleuthing, I’m seeing an uncanny amount of usernames w/ Lee, Leo, etc. in the twitter followers (and other social media) of some of the most unscrupulous appearing individuals in digital currency. 

IMHO, this is not a coincidence (maybe it is, maybe its a figment of my imagination), but I haven’t been interested tech/computers in a substantial way but for a year and half or so and lack some of the foundational knowledge to know what’s going on. 

From the hacker dictionary: 

lion food /n./

[IBM] Middle management or HQ staff (or, by extension, administrative drones in general). From an old joke about two lions who, escaping from the zoo, split up to increase their chances but agree to meet after 2 months. When they finally meet, one is skinny and the other overweight. The thin one says: “How did you manage? I ate a human just once and they turned out a small army to chase me — guns, nets, it was terrible. Since then I’ve been reduced to eating mice, insects, even grass.” The fat one replies: “Well, I hid near an IBM office and ate a manager a day. And nobody even noticed!”

http://www.outpost9.com/reference/jargon/jargon_27.html#SEC34

Very interested to read what others think and whether there is something to this. 

EDIT – Very aware of seeing patterns where none may exist . . . .  I just don’t see the Lee/Lion thing in other areas, just tech/digital currency/[possible] scammers. 
<br>
> 

- - -
[WatchDoge | Digital Currency Information & Commentary](https://network23.org/dogecoin/) – 19/03/2023 11:50