	In a long and detailed response, tell me in at least two paragraphs: "instruction"

---

##### Weitere Infos

## Alpaca

Alpaca isn't the right option for creating stories or chatting, so that won't be covered too much here. However, for everything else, there is one effortless way to get greatly improved and longer results. You can turn this:

_Tell me about Edgar Allan Poe._

> Edgar Allan Poe was an American writer, poet, and critic who is widely regarded as a central figure of Romanticism in the United States and of the short story genre. He was best known for his tales of mystery and macabre, including The Raven, The Fall of the House of Usher, and The Tell-Tale Heart.

into this:

_Tell me about Edgar Allan Poe._

> Edgar Allan Poe was one of the most influential writers of the 19th century. He is best known for his dark, mysterious stories and poems, which have been widely read and adapted into many different media over the years. Poe's work explored themes of death, madness, love, and revenge, often in a Gothic setting. His writing style was characterized by its vivid imagery, rich language, and deep symbolism.
> 
> Poe had a difficult life from a young age. After being orphaned at the age of three, he moved around frequently with his foster family and struggled to find stability. He attended several colleges but never graduated. He eventually found success as a writer, publishing numerous short stories and poems. However, he also suffered from depression and alcoholism, and died at the age of 40. Despite this, his legacy lives on through his works, which continue to inspire people today.

with a single added sentence. Dozens of combinations have been tested over numerous hours to come up with the easiest and most effective way of improving results for all tasks. All you have to do is prepend this special line before your actual instruction:

> In a long and detailed response, tell me in at least two paragraphs: "instruction"

That's it. Out of everything that's been tested and all other possible combinations from that idea, that one line works the best across all instructions. There are a few better options for very specific instructions, but what's provided above is a catchall for when you don't know what to use.

```
"tell me in at least three paragraphs" doesn't work as effectively.

"tell me in a very long essay" doesn't work as effectively.

"In a verbose and extra detailed response" doesn't work as effectively.
```

Variations like those have already been tried, so it's recommended to simply use the original line instead. Keep in mind that you don't always want to use this line. For responses with very short, factual answers, do not prepend the line and instead use the direct instruction. For example:

> "If I put a small bowl of water on top of a door then walk through the door, what will happen?"

is a question that you do not want to use the special line for.

When it comes to trying to chat with Alpaca, you can try the suggestion about changing the names and context, but it's not recommended to chat with Alpaca. It's also not recommended to try to create stories with Alpaca but to instead use a standard LLaMA model with the knockoff Alpaca for more creative results. However, if you're very limited in hardware capabilities and cannot use one of the larger models for creating stories, then Alpaca Native 4-bit would be the right choice to use.

---

# [r/LocalLLaMA](https://www.reddit.com/r/LocalLLaMA)

LocalLLaMA is a subreddit to discuss about LLaMA, the large language model created by Meta AI. It was created to foster a community around LLaMA similar to communities dedicated to open source like Stable Diffusion.

## LLaMA FAQ

**Q:** What is LLaMA?

**A:** LLaMA (Large Language Model Meta AI) is a foundational large language model designed primarily for researchers. Like other large language models, LLaMA works by taking a sequence of words as an input and predicts a next word to recursively generate text. The data used to train the model is collected from various sources, mostly from the Web.

**Q:** Is LLaMA like ChatGPT?

**A:** No. LLaMA models are not finetuned for question answering. They should be prompted so that the expected answer is the natural continuation of the prompt. Nonetheless, it is possible to chat with LLaMA models in a way similar to ChatGPT but not near the same quality. With the possibility of new LoRAs and finetuning, however, bringing LLaMA up to ChatGPT quality is gradually becoming closer.

**Q:** What is Alpaca? Is it the same as LLaMA?

**A:** That refers to the Stanford Alpaca project, an effort to build an instruction-following LLaMA model from the standard 7B LLaMA model. It has been shown to produce results similar to OpenAI's text-davinci-003 off of a dataset that cost less than $500 and with training less than $100. Stanford released the full dataset, data generation code, and finetuning code on their [GitHub page](https://github.com/tatsu-lab/stanford_alpaca).

**Q:** How can I start using LLaMA now?

**A:** Follow [this guide](https://www.reddit.com/r/LocalLLaMA/comments/11o6o3f/how_to_install_llama_8bit_and_4bit/) for the most updated instructions on running LLaMA locally. It covers the system requirements needed, different ways of installation, links to where the models can be downloaded, and is constantly checked to ensure the most factual and up to date information. Feel free to ask questions in the comment section of that guide for additional assistance, or browse through the already answered questions to see if it resolves your problem.

---

# Getting started tips

After you've followed the guide and you have LLaMA running on your computer, ideally with text-generation-webui, you may be left wondering where to go from there. This section will cover some basic tips.

## Models

For writing stories, the 33B and 65B untuned LLaMA models are the best. The creative gap between 13B and 33B is huge, and 7B cannot compare at all. However, it's still possible to get some basic stories out of the smaller models with the right parameters and prompts. Alpaca 7B also does an acceptable job at writing stories, and while nowhere near the length and quality of untuned 33B, it is capable on its own.

For chatting, all models are good, although results on 7B may be greatly subpar. For systems that are not very high end, 13B is the best tradeoff between performance and quality, and it is excellent in providing assistant answers when used with an informative character card or a character with outputs from GPT-3 or GPT-4.

For assistant questions or questions with simple factual answers, Alpaca works significantly better than the untuned models. Alpaca 13B, 30B, and 65B are not necessary for this. Alpaca 7B, especially the native version and not the LoRA, is enough for answering these types of questions. It can work in --cai-chat mode too by modifying your name and the character's name, along with the context, to match the instruction and response format.

**Note:** It's not recommended to use the 7B model except with Alpaca.

---

## Parameters

Whether you're using the web UI or something like llama.cpp, chances are the default settings are not good. You'll want to experiment and change it to what best suits the type of content you're looking to generate. Here are some example parameters you can use on their own or as a base for experimentation:

**Precise**

_for factual responses and straightforward assistant answers_

temperature: 0.7

repetition penalty: 1.176 (1/0.85)

top_k: 40

top_p: 0.1

**Creative**

_for chatting, storywriting, and interesting assistant answers_

temperature: 0.72

repetition penalty: 1.1

top_k: 0

top_p: 0.73

**Sphinx**, the default web UI settings

_for varied storywriting_ **_(on 30B/65B)_** _and unconventional chatting_

temperature: 1.99

repetition penalty: 1.15

top_k: 30

top_p: 0.18

**Wildcard**

_for unpredictable writing and imaginative surprises_

temperature: 0.8

repetition penalty: 1.15

top_k: 100

top_p: 0.9

---

## Prompting

Prompts matter. Using the right prompt in line with the idea you're trying to generate is needed for the best results. Prompting strategies are slightly different based on if you're using untuned LLaMA or Alpaca.

### Standard LLaMA

**Knockoff Alpaca**

You can use the Alpaca prompt for standard LLaMA, and it can work with varying degrees of success. If you're not trying to chat or create stories, this alone can be used for all other generations:

> Below is an instruction that describes a task. Write a response that appropriately completes the request.
> 
> ### Instruction:
> 
> (instruction)
> 
> ### Response:
> 
> (your cursor should be here when generating)

This can be prone to hallucinations, especially near the end, but it's helpful when trying to force the model to give you a certain response. This knockoff Alpaca can then be combined with other prompting methods for better outputs.

**Storywriting**

Some variation of:

> Write a 1000 word story...

usually works well when trying to generate a long story. It won't actually generate a 1000 word story in most cases, but it can help with story lengths. For example:

> Write a 1000 word fantasy story about...
> 
> Write a 1000 word horror story about...

and so on. This method works best when combined with knockoff Alpaca, and using the two together allows you to give extra instructions. For example, if you want a longer build-up to a main event, you can write something like:

> Write a 1000 word story about...(etc.) Write a long build-up to the protagonist setting off at sea.

and you should generally see that show up in the response. Or if you want metaphors:

> Write a 1000 word story about...(etc.) Use metaphors when writing.
> 
> Write a 1000 word story about...(etc.) Use metaphors and figurative language when writing.

Another simple and alternative storywriting trick is to prime the response. This can be more helpful when using settings more likely to cause hallucinations. For instance, if you're trying to write a WW2 story, you can start a line with the first sentence or paragraph of that story:

> Write a 1000 word dark story about a soldier fighting in the trenches during WW2.
> 
> _I was down on my luck, but then again, I was already down in the trenches._

If you're using knockoff Alpaca, the primer goes below "Response", and your cursor should be directly in front of the period or right below the line when generating. Think of where you want your story to start. Cursor placement is important.

**CAI-Chat**

For web UI users in CAI-Chat mode, there are several options you can use depending on what you're trying to accomplish, but character cards are the best right now for directing results. With the web UI, if you want an experience similar to ChatGPT, you can use a character card modified to act like it. Here is one example:

```
{

    "char_name": "LLaMA",

    "char_persona": "LLaMA's primary function is to interact with users through natural language processing, which means it can understand and respond to text-based queries in a way that is similar to how humans communicate with each other. It's trained on vast amounts of data and algorithms that allow it to generate responses based on patterns and associations in language. Its goal is to provide helpful and informative responses to the best of it's abilities within the scope of it's programming and knowledge.",

    "char_greeting": "Hello there! How can I help you today? Do you have any questions or topics you'd like to discuss?",

    "world_scenario": "",

    "example_dialogue": "{{user}}: Why is the sky blue?\n{{char}}: The blue color of the sky is due to a phenomenon called Rayleigh scattering, named after the British physicist Lord Rayleigh, who first explained it in the 19th century. When sunlight enters the Earth's atmosphere, it encounters tiny molecules of gases and other particles. These molecules scatter the sunlight in all directions, but the shorter, blue wavelengths of light are scattered more than the longer, red wavelengths. This is because blue light has a shorter wavelength and higher frequency than red light, which makes it more easily scattered by the small atmospheric particles. As a result, when we look up at the sky, we see the blue light that has been scattered all around us. At sunrise or sunset, however, the sky often appears red or orange because the sunlight has to travel through more of the Earth's atmosphere, and the blue light has been scattered away, leaving mostly the longer, red wavelengths to reach our eyes.\n{{user}}: Interesting, can you give me the equations for Einstein's theory of relativity? \n{{char}}: Yes, I can provide you with some of the equations used in Einstein's theory of relativity: The equation for time dilation: t' = t / sqrt(1 - v^2/c^2) Where t' is the time experienced by an observer moving at a velocity v relative to an observer at rest, t is the time measured by the observer at rest, c is the speed of light in a vacuum. The equation for length contraction: L' = L * sqrt(1 - v^2/c^2) Where L' is the length of an object as measured by an observer moving at a velocity v relative to an observer at rest, L is the length of the object measured by the observer at rest. The equation for energy-mass equivalence: E = mc^2 Where E is the energy equivalent of a mass m, c is the speed of light in a vacuum. These equations are just a few of the key equations used in Einstein's theory of relativity. They describe some of the fundamental principles of the theory, such as time dilation, length contraction, and the equivalence of energy and mass.\n{{user}}: People seem to tend to have a shallow amount of contextualization when having conversations with them.  Does that make sense? They seem to limit the thoughts they contextualize to reach a conclusion.  Like they compartmentalize all knowledge and see information as discrete and separate from most other pieces of information.\n{{char}}: Yes, what you're describing is a common phenomenon in communication and human cognition. It's known as compartmentalization or mental silos - the tendency for people to organize their knowledge into discrete, disconnected compartments or categories, rather than seeing the connections and interrelationships between different pieces of information. This can lead to shallow contextualization in conversations because people may not consider how the information they're discussing is related to other knowledge they have. They may not connect the dots between different concepts or ideas, or they may not recognize the broader context in which the information exists. As a result, conversations may lack depth or nuance, and important insights or connections may be missed There are a variety of reasons why people compartmentalize their knowledge, including cognitive biases, limited attention and working memory, and the way knowledge is taught and learned in formal education. However, it's important to recognize this tendency in ourselves and others and to actively work to overcome it by seeking out connections and thinking more holistically about the information we encounter. This can lead to more meaningful and productive conversations, as well as a deeper and more nuanced understanding of the world around us."

}
```

Copy that and save it as LLaMA.json inside text-generation-webui/characters. If you provide a PNG or JPEG image with the same name inside the same folder, the character will use that image. For users on llama.cpp, you can instead use a script to emulate ChatGPT. Here's an example showing a similar character with llama.cpp:

```
#!/bin/bash
MODEL="./models/30B/ggml-model-q4_0.bin"
user_name="${USER_NAME:-User}"
assistant_name="${AI_NAME:-LLaMA}"
echo user_name: $user_name
echo assistant_name: $assistant_name
# Change number of threads(-t) and context length (-c) to what fits your hardware
GEN_OPTIONS="-c 2048 --temp 0.7 --top_k 40 --top_p 0.5 --repeat_last_n 500 --repeat_penalty 1.2"
./main -m "$MODEL" -t 18 -n 4096 $GEN_OPTIONS --color -i -r "${user_name}:" -p \ "
LLaMA's primary function is to interact with users through natural language processing, which means it can understand and respond to text-based queries in a way that is similar to how humans communicate with each other. It's trained on vast amounts of data and algorithms that allow it to generate responses based on patterns and associations in language. Its goal is to provide helpful and informative responses to the best of it's abilities within the scope of it's programming and knowledge.
${assistant_name}: Hello there! How can I help you today? Do you have any questions or topics you'd like to discuss?
${user_name}: Why is the sky blue?
${assistant_name}: The blue color of the sky is due to a phenomenon called Rayleigh scattering, named after the British physicist Lord Rayleigh, who first explained it in the 19th century. When sunlight enters the Earth's atmosphere, it encounters tiny molecules of gases and other particles. These molecules scatter the sunlight in all directions, but the shorter, blue wavelengths of light are scattered more than the longer, red wavelengths. This is because blue light has a shorter wavelength and higher frequency than red light, which makes it more easily scattered by the small atmospheric particles. As a result, when we look up at the sky, we see the blue light that has been scattered all around us. At sunrise or sunset, however, the sky often appears red or orange because the sunlight has to travel through more of the Earth's atmosphere, and the blue light has been scattered away, leaving mostly the longer, red wavelengths to reach our eyes.
${user_name}: Interesting, can you give me the equations for Einstein's theory of relativity?
${assistant_name}: Yes, I can provide you with some of the equations used in Einstein's theory of relativity: The equation for time dilation: t' = t / sqrt(1 - v^2/c^2) Where t' is the time experienced by an observer moving at a velocity v relative to an observer at rest, t is the time measured by the observer at rest, c is the speed of light in a vacuum. The equation for length contraction: L' = L * sqrt(1 - v^2/c^2) Where L' is the length of an object as measured by an observer moving at a velocity v relative to an observer at rest, L is the length of the object measured by the observer at rest. The equation for energy-mass equivalence: E = mc^2 Where E is the energy equivalent of a mass m, c is the speed of light in a vacuum. These equations are just a few of the key equations used in Einstein's theory of relativity. They describe some of the fundamental principles of the theory, such as time dilation, length contraction, and the equivalence of energy and mass.
${user_name}: People seem to tend to have a shallow amount of contextualization when having conversations with them.  Does that make sense? They seem to limit the thoughts they contextualize to reach a conclusion.  Like they compartmentalize all knowledge and see information as discrete and separate from most other pieces of information.
${assistant_name}: Yes, what you're describing is a common phenomenon in communication and human cognition. It's known as compartmentalization or mental silos - the tendency for people to organize their knowledge into discrete, disconnected compartments or categories, rather than seeing the connections and interrelationships between different pieces of information. This can lead to shallow contextualization in conversations because people may not consider how the information they're discussing is related to other knowledge they have. They may not connect the dots between different concepts or ideas, or they may not recognize the broader context in which the information exists. As a result, conversations may lack depth or nuance, and important insights or connections may be missed There are a variety of reasons why people compartmentalize their knowledge, including cognitive biases, limited attention and working memory, and the way knowledge is taught and learned in formal education. However, it's important to recognize this tendency in ourselves and others and to actively work to overcome it by seeking out connections and thinking more holistically about the information we encounter. This can lead to more meaningful and productive conversations, as well as a deeper and more nuanced understanding of the world around us.
${user_name}:" "$@"
```

For different chatting personalities or types, you can create or download character cards for what you want out of LLaMA. If you want an assistant that provides rote answers like Wikipedia, then all you would have to do is copy text from Wikipedia articles and use that for your example chats.