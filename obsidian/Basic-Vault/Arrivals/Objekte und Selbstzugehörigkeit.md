Was ist an dem folgenden Argument falsch?
"Für jedes Objekt gibt es eine Tatsache, die besagt, ob das Objekt zu sich selbst gehört oder nicht. Dein Computer: Er gehört nicht zu sich selbst. Die Menge aller natürlichen Zahlen: gehört nicht zu sich selbst. Und so weiter. Also gibt es natürlich eine Menge aller solcher Objekte." Sag genau, bei welchem Satz sich der Fehler einschleicht und warum er ein Fehler ist.


Beschreibe informell, wie man die logische Sprache verwenden könnte, um einen Satz Hal t(p, t, r, s) zu schreiben, der nur dann wahr ist, wenn eine Turing-Maschine mit dem Programm p nach einer endlichen Anzahl von Schritten anhalten würde, wenn sie im Zustand s, mit dem Band t und dem Bandleser an der Position r startet.