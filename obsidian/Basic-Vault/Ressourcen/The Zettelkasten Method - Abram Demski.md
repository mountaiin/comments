Early this year, Conor White-Sullivan introduced me to the Zettelkasten method of note-taking. I would say that this significantly increased my research productivity. I’ve been saying “at least 2x”. Naturally, this sort of thing is difficult to quantify. The truth is, I think it may be more like 3x, especially along the dimension of “producing ideas” and also “early-stage development of ideas”. (What I mean by this will become clearer as I describe how I think about research productivity more generally.) However, it is also very possible that the method produces serious _biases_ in the types of ideas produced/developed, which should be considered. (This would be difficult to quantify at the best of times, but also, it should be noted that other factors have dramatically _decreased_ my overall research productivity. So, unfortunately, someone looking in from outside would not see an overall boost. Still, _my_ impression is that it's been very useful.)

I think there are some specific reasons why Zettelkasten has worked so well for me. I’ll try to make those clear, to help readers decide whether it would work for them. However, I honestly didn’t think Zettelkasten sounded like a good idea before I tried it. It only took me about 30 minutes of working with the cards to decide that it was really good. So, if you’re like me, this is a cheap experiment. I think a lot of people should actually try it to see how they like it, even if it sounds terrible.

My plan for this document is to first give a short summary and then an overview of Zettelkasten, so that readers know roughly what I’m talking about, and can possibly experiment with it without reading any further. I’ll then launch into a longer discussion of why it worked well for me, explaining the specific habits which I think contributed, including some descriptions of my previous approaches to keeping research notes. I expect some of this may be useful even if you don’t use Zettelkasten -- if Zettelkasten isn’t for you, maybe these ideas will nonetheless help you to think about optimizing your notes. However, I put it here primarily because I think it will boost the chances of Zettelkasten working for you. It will give you a more concrete picture of how I use Zettelkasten as a thinking tool.

  

## Very Short Summary

## Materials

-   [Staples index-cards-on-a-ring](https://www.staples.com/Staples-Ruled-Index-Cards-on-a-Ring-Blue-Poly-Cover-3-x-5/product_365997?cid=PS:GooglePLAs:365997&ci_src=17588969&ci_sku=365997&KPID=365997&gclid=EAIaIQobChMIlsiXtfn44gIVEv5kCh0rAA8HEAQYAiABEgIcGfD_BwE) or equivalent, possibly with:

-   [plastic rings](https://smile.amazon.com/gp/product/B07DN6LN9C/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1) rather than metal
-   different 3x5 index cards (I recommend blank, but, other patterns may be good for you) as desired
-   some kind of divider

-   I use yellow index cards as dividers, but slightly larger cards, tabbed cards, plastic dividers, etc. might be better

-   quality hole punch (if you’re using different cards than the pre-punched ones)

-   I like [this one](https://smile.amazon.com/gp/product/B06XP9CPWQ/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1).

-   Blank stickers or some other way to label card-binders with the address range stored within.

-   quality writing instrument -- must suit you, but,

-   multi-color click pen recommended
-   hi-tec-c coleto especially recommended

## Technique

-   Number pages with alphanumeric strings, so that pages can be sorted hierarchically rather than linearly -- 11a goes between 11 and 12, 11a1 goes between 11a and 11b, _et cetera_. This allows pages to be easily inserted between other pages without messing up the existing ordering, which makes it much easier to continue topics.
-   Use the alphanumeric page identifiers to “hyperlink” pages. This allows sub-topics and tangents to be easily split off into new pages, and also allows for related ideas to be interlinked.

Before I launch into the proper description of Zettelkasten, here are some other resources on note-taking which I looked at before diving into using Zettelkasten myself. (Feel free to skip this part on a first reading.)

  

## Related Literature

There are other descriptions of Zettelkasten out there. I mainly read _How to Take Smart Notes_, which is the best book on Zettelkasten as far as I know -- it claims to be the best write-up available _in English_, anyway. The book contains a thorough description of the technique, plus a lot of “philosophical” stuff which is intended to help you approach it with the right mindset to actually integrate it into your thinking in a useful way. I am sympathetic to this approach, but some of the content seems like bad science to me (such as the description of growth mindset, which didn’t strike me as at all accurate -- I’ve read some of the original research on growth mindset).

An issue with some other write-ups is that they focus on implementing Zettelkasten-like systems digitally. In fact, Conor White-Sullivan, who I’ve already mentioned, is working on a Workflowy/Dynalist-like digital tool for thinking, inspired partially by Zettelkasten (and also by the idea that a Workflowy/Dynalist style tool which is _designed explicitly to nudge users into good thinking patterns_ with awareness of cognitive biases, good practices for argument mapping, etc. could be very valuable). You can take a look at his tool, Roam, [here](http://roamresearch.com/#/secretinvite). He also wrote up some thoughts about [Zettelkasten in Roam](http://roamresearch.com/#/v8/help/page/1283/1308). However, _**I strongly recommend trying out Zettelkasten on actual note-cards,**_ even if you end up implementing it on a computer. There’s something good about the note-card version that I don’t fully understand. As such, I would advise against trusting other people’s attempts to distill what makes Zettelkasten good into a digital format -- better to try it yourself, so that you can then judge whether alternate versions are improvements for you. The version I will describe here is fairly close to the original.

I don’t strongly recommend my own write-up _over_ what’s said in _How to Take Smart Notes_, particularly the parts which describe the actual technique. I’m writing this up partly just so that there’s an easily linkable document for people to read, and partly because I have some ideas about how to make Zettelkasten work for you (based on my own previous note-taking systems) which are different from the book.

Another source on note-taking which I recommend highly is Lion Kimbro’s _How to Make a Complete Map of Every Thought You Think ([html](https://users.speakeasy.net/~lion/nb/html/),_ _[pdf](https://users.speakeasy.net/~lion/nb/book.pdf))._ This is about a completely different system of note-taking, with different goals. However, it contains a wealth of inspiring ideas about note-taking systems, including valuable tips for the raw physical aspects of keeping paper notes. I recommend reading [this interview with Lion Kimbro](https://gilest.org/lion-kimbro.html) as a “teaser” for the book -- he mentions some things which he didn’t in the actual book, and it serves somewhat as “the missing introduction” to the book. (You can skip the part at the end about wikis if you don’t find it interesting; it is sort of outdated speculation about the future of the web, and it doesn’t get back to talking about the book.) Part of what I love about _How to Make a Complete Map of Every Thought You Think_ is the manic brain-dump writing style -- it is a book which feels very “alive” to me. If you find its style grating rather than engaging, it’s probably not worth you reading through.

I should also mention [another recent post about Zettelkasten here on LW](https://www.lesswrong.com/posts/T382CLwAjsy3fmecf/how-to-take-smart-notes-ahrens-2017).

  

## Zettelkasten, Part 1: The Basics

Zettelkasten is German for ‘slip-box’, IE, a box with slips of paper in it. You keep everything on a bunch of note cards. Niklas Luhmann developed the system to take notes on his reading. He went on to be an incredibly prolific social scientist. It is hard to know whether his productivity was tied to Zettelkasten, but he thinks so, and others have reported large productivity boosts from the technique as well.

  

## Small Pieces of Paper Are Just Modular Large Pieces of Paper

You may be thinking: aren’t small pieces of paper bad? Aren’t [large notebooks just better](https://acritch.com/notepad/)? Won’t small pages make for small ideas?

What I find is that the drive for larger paper is better-served by splitting things off into new note cards. Note-cards relevant to your current thinking can be spread on a table to get the same big-picture overview which you’d get from a large sheet of paper. Writing on an _actual_ large sheet of paper locks things into place.

> When I was learning to write in my teens, it seemed to me that paper was a prison. Four walls, right? And the ideas were constantly trying to escape. What is a parenthesis but an idea trying to escape? What is a footnote but an idea that tried -- that jumped off the cliff? Because paper enforces single sequence -- and there’s no room for digression -- it imposes a particular kind of order in the very nature of the structure.

> \-- Ted Nelson, [demonstration of Xanadu space](https://www.youtube.com/watch?v=En_2T7KH6RA)

I use 3x5 index cards. That’s quite small compared to most notebooks. It may be that this is the right size for me only because I already have very small handwriting. I believe Luhmann used larger cards. However, _I expected it to be too small_. Instead, I found the small cards to be freeing. I strongly recommend trying 3x5 cards before trying with a larger size. In fact, even smaller sizes than this are viable -- one early reader of this write-up decided to use _half_ 3x5 cards, so that they’d fit in mtg deck boxes.

Writing on small cards _forces_ certain habits which would be good even for larger paper, but which I didn’t consider until the small cards made them necessary. It forces ideas to be [broken up into simple pieces](http://www.dansheffler.com/blog/2015-08-05-one-thought-per-note/), which helps to clarify them. Breaking up ideas forces you to link them together explicitly, rather than relying on the linear structure of a notebook to link together chains of thought.

Once you’re forced to adopt a linking system, it becomes natural to use it to “break out of the prison of the page” -- tangents, parentheticals, explanatory remarks, caveats, … everything becomes a new card. This gives your thoughts much more “surface area” to expand upon.

On a computer, this is essentially the wiki-style \[\[magic link\]\] which links to a page if the page exists, or creates the page if it doesn’t yet exist -- a [critical but all-too-rare](http://takingnotenow.blogspot.com/2018/12/it-needs-wiki-like-superpower.html) feature of note-taking software. Again, though, I strongly recommend trying the system on paper before jumping to a computer; putting yourself in a position where you _need_ to link information like crazy will help you to see the value of it.

This brings us to one of the defining features of the Zettelkasten method: the addressing system, which is how links between cards are established.

  

## Paper Hypertext

We want to use card addresses to organize and reference everything. So, when you start a new card, its address should be the first thing you write -- you never want to have a card go without an address. Choose a consistent location for the addresses, such as the upper right corner. If you’re using multi-color pens, like me, you might want to choose one color just for addresses.

Wiki-style links tend to use the title of a page to reference that page, which works very well on a computer. However, for a pen-and-paper hypertext system, we want to optimize several things:

-   Easy lookup: we want to find referenced cards as easily as possible. This entails sorting the cards, so that you don’t have to go digging; finding what you want is as easy as finding a word in the dictionary, or finding a page given the page number.
-   Easy to sort: I don’t know about you, but for me, putting things in alphabetical order isn’t the easiest thing. I find myself reciting the alphabet pretty often. So, I don’t _really_ want to sort cards alphabetically by title.
-   Easy to write: another reason not to sort alphabetically by title is that you want to reference cards really easily. You probably don’t want to write out full titles, unless you can keep the titles really short.
-   Fixed addresses: Whatever we use to reference a card, it must remain fixed. Otherwise, references could break when things change. No one likes broken links!
-   [Related cards should be near each other](https://zettelkasten.de/posts/kinds-of-ties/). Alphabetical order might put closely related cards very far apart, which gets to be cumbersome as the collection of cards grows -- even if look-up is quite convenient, it is nicer if the related cards are already at hand without purposefully deciding to look them up.
-   [No preset categories](https://zettelkasten.de/posts/no-categories/). Creating a system of categories is a common way to place related content together, but, it is too hard to know how you will want to categorize everything ahead of time, and the needs of an addressing system make it too difficult to change your category system later.

One simple solution is to number the cards, and keep them in numerical order. Numbers are easy to sort and find, and are very compact, so that you don’t have the issue of writing out long names. However, although related content will be somewhat nearby (due to the fact that we’re likely to create several cards on a topic at the same time), we can do better.

The essence of the Zettelkasten approach is the use of repeated decimal points, as in “22.3.14” -- cards addressed 2.1, 2.2, 2.2.1 and so on are all thought of as “underneath” the card numbered 2, just as in the familiar subsection-numbering system found in many books and papers. This allows us to insert cards anywhere we want, rather than only at the end, which allows related ideas to be placed near each other much more easily. A card sitting “underneath” another can loosely be thought of as a comment, or a contituation, or an associated thought.

However, for the sake of compactness, Zettelkasten addresses are usually written in an alphanumeric format, so that rather than writing 1.1.1, we would write 1a1; rather than writing 1.2.3, we write 1b3; and so on. This notation allows us to avoid writing so many periods, which grows tiresome.

Alternating between numbers and letters in this way allows us to get to two-digit numbers (and even two-digit letters, if we exhaust the whole alphabet) without needing periods or dashes or any such separators to indicate where one number ends and the next begins.

Let’s say I’m writing linearly -- something which could go in a notebook. I might start with card 11, say. Then I proceed to card 11a, 11b, 11c, 11d, etc. On each card, I make a note somewhere about the previous and next cards in sequence, so that later I know for sure how to follow the chain via addresses.

Later, I might have a different branch-off thought from 11c. This becomes 11c1. That’s the magic of the system, which you can’t accomplish so easily in a linear notebook: you can just come back and add things. These tangents can grow to be larger than the original.

![](https://res.cloudinary.com/lesswrong-2-0/image/upload/v1568584395/Zettelkasten_Svg_1_iuqopd.svg)

_Don’t get too caught up in what address to give a card to put it near relevant material. A card can be put anywhere in the address system._ The point is to make things more convenient for you; nothing else matters. Ideally, the tree would perfectly reflect some kind of conceptual hierarchy; but in practice, card 11c might turn out to be the primary thing, with card 11 just serving as a historical record of what seeded the idea.

Similarly, a linear chain of writing doesn’t have to get a nice linear chain of addresses. I might have a train of thought which goes across cards 11, 11a, 11b, 11b1, 11b1a, 11b1a1, 18, 18a… (I write a lot of “1a1a1a1a”, and it is sometimes better to jump up to a new top-level number to keep the addresses from getting longer.)

Mostly, though, I’ve written less and less in linear chains, and more and more in branching trees. Sometimes a thought just naturally wants to come out linearly. But, this tends to make it more difficult to review later -- the cards aren’t split up into atomic ideas, instead flowing into each other.

If you don’t know where to put something, make it a new top-level card. You can link it to whatever you need via the addressing system, so the cost of putting it in a suboptimal location isn’t worth worrying about too much! You don’t want to be constrained by the ideas you’ve had so far. Or, to put it a different way: it’s like starting a new page in a notebook. Zettelkasten is supposed to be _less_ restrictive than a notebook, not more. Don’t get locked into place by trying to make the addresses perfectly reflect the logical organization.

  

## Physical Issues: Card Storage

Linear notes can be kept in any kind of paper notebook. Nonlinear/modular systems such as Zettelkasten, on the other hand, require some sort of binder-like system where you can insert pages at will. I’ve tried a lot of different things. Binders are typically just _less comfortable_ to write in (because of the rings -- this is another point where the fact that I’m left-handed is very significant, and right-handed readers may have a different experience).

(One thing that’s improved my life is realizing that I can use a binder “backwards” to get essentially the right-hander’s experience -- I write on the “back” of pages, starting from the “end”.)

They’re also bulky; it seems somewhat absurd how much more bulky they are than a notebook of equivalently-sized paper. This is a serious concern if you want to carry them around. (As a general rule, I’ve found that a binder feels roughly equivalent to one-size-larger notebook -- a three-ring binder for 3x5 cards feels like carrying around a deck of 4x6 cards; a binder of A6 paper feels like a notebook of A5 paper; and so on.)

Index cards are often kept in special boxes, which you can get. However, I don’t like this so much? I want a more binder-like thing which I can easily hold in my hands and flip through. Also, boxes are often made to view cards in landscape orientation, but I prefer portrait orientation -- so it’s hard to flip through things and read while they’re still in the box.

Currently, I use the [Staples index-cards-on-a-ring](https://www.staples.com/Staples-Ruled-Index-Cards-on-a-Ring-Blue-Poly-Cover-3-x-5/product_365997?cid=PS:GooglePLAs:365997&ci_src=17588969&ci_sku=365997&KPID=365997&gclid=EAIaIQobChMIlsiXtfn44gIVEv5kCh0rAA8HEAQYAiABEgIcGfD_BwE) which put all the cards on a single ring, and protect them with plastic covers. However, I replace the metal rings (which I find harder to work with) with [plastic rings](https://smile.amazon.com/gp/product/B07DN6LN9C/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1). I also bought a variety of note cards to try -- you can try thicker/thinner paper, colors, line grid, dot grid, etc. If you do this, you’ll need a hole punch, too. I recommend getting a “low force” hole punch; if you just go and buy the cheapest hole punch you can find, it’ll probably be pretty terrible. You want to be fairly consistent with where you punch the holes, but, that wasn’t as important as I expected (it doesn’t matter as much with a one-ring binder in contrast to a three-ring, since you’re not trying to get holes to line up with each other).

I enjoy the ring storage method, because it makes cards really easy to flip through, and I can work on several cards at once by splaying them out (which means I don’t lose my place when I decide to make a new card or make a note on a different one, and don’t have to take things out of sort order to work with them).

**Deck Architecture**

I don’t keep the cards perfectly sorted all the time. Instead, I divide things up into sorted and not-yet-sorted:

![](https://res.cloudinary.com/lesswrong-2-0/image/upload/v1568584395/Zettelkasten_2_svg_blhwed.svg)

(Blue in this image mean “written on” -- they’re all actually white except for the yellow divider, although of course you could use colored cards if you like.)

**Fetch Modi**

As I write on blank cards, I just leave them where they are, rather than immediately putting them into the sort ordering. I sort them in later. (Unsorted cards still have addresses and can be referenced. _The address is always, always the very first thing I write on a card._)

There is an advantage to this approach beyond the efficiency of sorting things all at once. The unsorted cards are a physical record of _what I’m actively working on_. Since cards are so small, working on an idea almost always means creating new cards. So, I can easily jump back into whatever I was thinking about last time I handled the binder of cards.

Unless you have a specific new idea you want to think about (in which case you start a new card, or, go find the most closely related cards in your existing pile), there are basically two ways to enter into your card deck: from the front, and from the back. The front is “top-down” (both literally and figuratively), going from bigger ideas to smaller details. It’s more breadth-first. You’re likely to notice an idea which you’ve been neglecting, and start a new branch from it. Starting from the back, on the other hand, is depth-first. You’re continuing to go deeper into a branch which you’ve already developed some depth in.

**_Don’t sort too often._** The unsorted cards are a valuable record of what you’ve been thinking about. I’ve regretted sorting too frequently -- it feels like I have to start over, find the interesting open questions buried in my stack of cards all over again.

In theory, one could also move cards from sorted to unsorted specifically to remind oneself to work on those cards, but I haven’t really used this tactic.

The advantage of sorting is to make address lookup easier. But, actually, address lookup in my unsorted cards is not that hard! Because the cards remain in creation-order, I know that e.g. card 10a1 must come _somewhere_ after card 10a. It just doesn't need to be _immediately_ after when the cards aren't sorted.

**Splitting & Deck Management**

When a ring feels over-full (after I fill approximately 100 cards), I sort all of the cards, and split the deck into two. (Look for a sensible place to split the tree into two -- you want to avoid a deep branch being split up into two separate decks, as much as you can.) Load up the two new decks with 50ish blank cards each, and stick them on new rings.

Everything is still on one big addressing system, so, it is a good idea to label the two new binders with the address range within. I use blank stickers, which I put on the front of each ring binder. The labels serve both to keep lookup easy (I don’t want to be guessing about which binder certain addresses are in), and also, to remind me to limit the addresses within a given deck.

For example, suppose this is my first deck of cards (so before the split, it holds everything). Let’s say there are 30 cards underneath “1”, 20 cards underneath “2”, and then about 50 more cards total, under the numbers 3 through 14.

I would split this deck into a “1 through 2” deck, and a “3 through \*” deck -- the \* meaning “anything”. You might think it would be “3 through 14”, but, when I make card 15, it would go in that deck. So at any time, you have one deck of cards with no upper bound. On the other hand, when you are working with the “1 - 2” deck, you don’t want to mistakenly make a card 3; you’ve already got a card 3 somewhere. You don’t want duplicate addresses anywhere!

Currently, I have 6 decks: 0 - 1.4, 1.5 - 1.\*, 2 - 2.4, 2.5 - 2.\*, 3, and 4 - 4.\*. (I was foolish when I started my Zettelkasten, and used the decimal system rather than the alphanumeric system. I switched quickly, but all my top-level addresses are still decimal. So, I have a lot of mixed-address cards, such as 1.3a1, 1.5.2a2, 2.6b4a, etc. As for why my numbers start at 0 rather than 1, I’ll discuss that in the “Index & Bibliography” section.)

I like to have the unsorted/blank “short-term memory” section on every single deck, so that I can conveniently start thinking about stuff within that deck without grabbing anything else. However, it might also make sense to have only one “short-term memory” in order to keep yourself more focused (and so that there’s only one place to check when you want to remember what you were recently working on!).

  

## Getting Started: Your First Card

[Your first note](https://zettelkasten.de/posts/your-first-note/) doesn’t need to be anything important -- it isn’t as if every idea you put into your Zettelkasten has to be “underneath” it. Remember, you aren’t trying to invent [a good category system](https://zettelkasten.de/posts/no-categories/). Not every card has to look like a core idea with bullet points which elaborate on that idea, like my example in the previous section. You can just start writing whatever. In fact, it might be good if you make your first cards messy and unimportant, just to make sure you don’t feel like everything has to be nicely organized and highly significant.

On the other hand, it might be important to have a good starting point, if you really want to give Zettelkasten a chance.

I mentioned that I knew I liked Zettelkasten within the first 30 minutes. I think it might be important that when I sat down to try it, I had an idea I was excited to work on. It wasn’t a nice solid mathematical idea -- it was a fuzzy idea, one which had been burning in the back of my brain for a week or so, waiting to be born. It filled the fractal branches of a zettelkasten nicely, expanding in every direction.

So, maybe start with one of _those_ ideas. Something you’ve been struggling to articulate. Something which hasn’t found a place in your linear notebook.

Alright. That’s all I have to say about the basics of Zettelkasten. You can go try it now if you want, or keep reading. The rest of this document is about further ideas in note-taking which have shaped the way I use Zettelkasten. These may or may not be useful to you; I don’t know for sure why Zettelkasten is such a productive system for me personally.

  

## Note-Taking Systems I Have Known and Loved

I’m organizing this section by my previous note-taking systems, but secretly, the main point is to convey a number of note-taking ideas which may have contributed to Zettelkasten working well for me. These ideas have seemed generally useful to me -- maybe they’ll be useful to you, even if you don’t end up using Zettelkasten in particular.

  

## Notebooks

**Developing Ideas**

Firstly, and most importantly, I have been keeping idea books since middle school. I think there’s something very important in the simple idea of writing regularly -- I don’t have the reference, but, I remember reading someone who described the day they first started keeping a diary as the day they first woke up, started reflectively thinking about their relationship with the world. Here’s a somewhat similar quote from a Zettelkasten blog:

> During the time spanning Nov. 2007–Jan. 2010, I filled 11 note books with ideas, to-do lists, ramblings, diary entries, drawings, and worries.

> Looking back, this is about the time I started to live consciously. I guess keeping a journal helped me “wake up” from some kind of teenage slumber.

> [\--Christian](https://zettelkasten.de/posts/idea-index-journal-fiction/)

I never got into autobiographical diary-style writing, personally, instead writing about ideas I was having. Still, things were in a very “narrative” format -- the ideas were a drama, a back-and-forth, a dance of rejoinders. There was some math -- pages filled with equations -- but only after a great deal of (very) informal development of an idea.

As a result, “elaborate on an idea” / “keep going” seems like a primitive operation to me -- and, specifically, a primitive operation _which involves paper_. (I can’t translate the same thinking style to conversation, not completely.) I’m sure that there is a lot to unpack, but for me, it just feels natural to keep developing ideas further.

So, when I say that the Zettelkasten card 1b2 _“elaborates on”_ the card 1b, I’m calling on the long experience I’ve had with idea books. I don’t know if it’ll mean the same thing for you.

Here’s my incomplete attempt to convey some of what it means.

When I’m writing in an idea book, I spend a lot of time trying to clearly explain ideas under the (often false) assumption that I know what I’m talking about. There’s an imaginary audience who knows a lot of what I’m talking about, but I have to explain certain things. I can’t get away with leaving important terms undefined -- I have to establish anything I feel less than fully confident about. For example, the definition of a Bayesian network is something I can assume my “audience” can look up on wikipedia. However, if I’m less than totally confident in the concept of d-separation, I have to explain it; especially if it is important to the argument I hope to make.

Once I’ve established the terms, I try to explain the idea I was having. I spend a lot of time staring off into space, not really knowing what’s going on in my head exactly, but with a sense that there’s a simple point I’m trying to make, if only I could see it. I simultaneously feel like I know what I want to say (if only I could find the words), and like I don’t know what it is -- after all, I haven’t articulated it yet. Generally, I can pick up where I left off with a particular thought, even after several weeks -- I can glance at what I’ve written so far, and get right back to staring at the wall again, trying to articulate the same un-articulated idea.

If I start again in a different notebook (for example, switching to writing my thoughts on a computer), I have to explain everything again. This audience doesn’t _know_ yet! I can’t just pick up on a computer where I left off on paper. It’s like trying to pick up a conversation in the middle, but with a different person. This is sort of annoying, but often good (because re-explaining things may hold surprises, as I notice new details.)

Similarly, if I do a lot of thinking without a notebook (maybe in a conversation), I generally have to “construct” my new position from my old one. This has an unfortunate “freezing” effect on thoughts: there’s a lot of gravity toward the chain of thought wherever it is on the page. I tend to work on whatever line of thought is most recent in my notebook, regardless of any more important or better ideas which have come along -- especially if the line of thought in the notebook isn’t yet at a conclusive place. Sometimes I put a scribble in the notebook after a line of thought, to indicate explicitly that it no longer reflects the state of my thinking, to give myself “permission” to do something else.

Once I’ve articulated some point, then criticisms of the point often become clear, and I’ll start writing about them. I often have a sense that I know how it’s going to go a few steps ahead in this back-and-forth; a few critiques and replies/revisions. Especially if the ideas are flowing faster than I can write them down. However, it is important to actually write things down, because they often don’t go quite as I expect.

If an idea seems to have reached a natural conclusion, including all the critiques/replies which felt important enough to write, I’ll often write a list of “future work”: any open questions I can think of, applications, details which are important but not so important that I want to write about them yet, etc. At this point, it is usually time to write the idea up for a _real_ audience, which will require more detail and refine the idea yet further (possibly destroying it, or changing it significantly, as I often find a critical flaw when I try to write an idea up for consumption by others).

If I don’t have any particular idea I’m developing, I may start fresh with a mental motion like “OK, obviously I know how to solve everything” and write down the grand solution to everything, starting big-picture and continuing until I get stuck. Or, instead, I might make a bulleted list free-associating about what I think the interesting problems are -- the things I don’t know how to do.

  

## Workflowy

The next advance in my idea notes was workflowy. I still love the simplicity of workflowy, even though I have moved on from it.

For those unfamilar, Workflowy is an outlining tool. I was unfamiliar with the idea before Workflowy introduced it to me. Word processors generally _support_ nested bulleted lists, but the page-like format of a word processor limits the depth such lists can go, and it didn’t really occur to me to use these as a primary mode of writing. Workflowy doesn’t let you do anything _but_ this, and it provides enough features to make it extremely convenient and natural.

**Nonlinear Ideas: Branching Development**

Workflowy introduced me to the possibility of nonlinear formats for idea development. I’ve already discussed this to some extent, since it is also one of the main advantages of Zettelkasten over ordinary notebooks.

Suddenly, I could continue a thread _anywhere,_ rather than always picking it up at the end. I could sketch out where I expected things to go, with an outline, rather than keeping all the points I wanted to hit in my head as I wrote. If I got stuck on something, I could write about how I was stuck nested _underneath_ whatever paragraph I was currently writing, but then collapse the meta-thoughts to be invisible later -- so the overall narrative doesn’t feel interrupted.

In contrast, writing in paper notebooks forces you to choose consciously that you’re done for now with a topic if you want to start a new one. Every new paragraph is like choosing a single fork in a twisting maze. Workflowy allowed me to take them all.

**What are Children?**

I’ve seen people hit a block right away when they try to use workflowy, because they don’t know what a “child node” is.

-   Here’s a node. It could be a paragraph, expressing some thought. It could also be a title.

-   Here’s a child node. It could be a comment on the thought -- an aside, a critique, whatever. It could be something which goes under the heading.

-   Here’s a sibling node. It could be the next paragrapt in the “main thrust” of an argument. It could be an unrelated point under the same super-point everything is under.

As with Zettelkasten, my advice is to not get too hung up on this. A child is sort of like a comment; a parenthetical statement or a footnote. You can continue the main thrust of an argument in sibling nodes -- just like writing an ordinary sequence of paragraphs in a word processor.

You can also organize things under headings. This is especially true if you wrote a sketchy outline first and then filled it in, or, if you have a lot of material in Workflowy and had to organize it. The “upper ontology” of my workflowy is mostly title-like, single words or short noun phrases. As you get down in, bullets start to be sentences and paragraphs more often.

Obviously, all of this can be applied to Zettelkasten to some extent. The biggest difference is that “upper-level” cards are less likely to _just_ be category titles; and, you can’t really organize things into nice categories after-the-fact because the addresses in Zettelkasten are fixed -- you can’t change them without breaking links. You can use redirect cards if you want to reorganize things, actually, but I haven’t done that very much in practice. Something which _has_ worked for me to some extent is to reorganize things in the indexes. Once an index is too much of a big flat list, you can cluster entries into subjects. This new listing can be added as a child to the previous index, keeping the historical record; or, possibly, replace the old index outright. I discuss this more in the section on indexing.

**Building Up Ideas over Long Time Periods**

My idea books let me build up ideas over time to a greater extent than my peers who didn’t keep similar journals. However, because the linear format forces you to switch topics in a serial manner and “start over” when you want to resume a subject, you’re mostly restricted to what you can keep in your head. Your notebooks _are_ a form of information storage, and you _can_ go back and re-read things, but only if you remember the relevant item to go back and re-read.

Workflowy allowed me to build up ideas to a greater degree, incrementally adding thoughts until cascades of understanding changed my overall view.

_Placing a New Idea_

Because you’ve got all your ideas in one big outline, you can add in little ideas easily. Workflowy was easy enough to access via my smartphone (though they didn’t have a proper app at the time), so I could jot down an idea as I was walking to class, waiting for the bus, etc. I could easily navigate to the right location, at least, if I had organized the overall structure of the outline well. Writing one little idea would usually get more flowing, and I would add several points in the same location on the tree, or in nearby locations.

This idea of jotting down ideas while you’re out and about is very important. If you feel you don’t have enough ideas (be it for research, for writing fiction, for art -- whatever) my first question would be whether you have a good way to jot down little ideas as they occur to you.

The fact that you’re _forced_ to _somehow_ fit all ideas into one big tree is also important. It makes you organize things in ways that are likely to be useful to you later.

_Organizing Over Time_

The second really nice thing workflowy did was allow me to go back and reorganize all the little ideas I had jotted down. When I sat down at a computer, I could take a look at my tree overall and see how well the categorization fit. This mostly took the form of small improvements to the tree structure over time. Eventually, a cascade of small fixes turned into a major reorganization. At that point, I felt I had really learned something -- all the incremental progress built up into an overall shift in my understanding.

Again, this isn’t really possible in paper-based Zettelkasten -- the address system is fixed. However, as I mentioned before, I’ve had some success doing this kind of reorganization within the indexes. It doesn’t matter that the addresses of the cards are fixed if the way you actually _find_ those addresses is mutable.

**Limitations of Workflowy**

Eventually, I noticed that I had a big pile of ideas which I hadn’t really developed. I was jotting down ideas, sure. I was fitting them into an increasingly cohesive overall picture, sure. But I wasn’t _doing anything_ with them. I wasn’t writing pages and pages of details and critique.

It was around this time that I realized I had gone more than three years without using a paper notebook very significantly. I started writing on paper again. I realized that there were all these habits of thinking which were tied to paper for me, and which I didn’t really access if I didn’t have a nice notebook and a nice pen -- the force of the long-practiced associations. It was like waking up intellectually after having gone to sleep for a long time. I started to _remember highschool._ It was a weird time. Anyway...

  

## Dynalist

The next thing I tried was Dynalist.

The main advantage of Dynalist over Workflowy is that it takes a feature-rich rather than minimalistic approach. I like the clean aesthetics of Workflowy, but… eventually, there’ll be some critical feature Workflowy just doesn’t provide, and you’ll want to make the jump to Dynasilt. I use hardly any of the extra features of Dynalist, but the ones I _do_ use, I _need._ For me, it’s mostly the LaTeX support.

Another thing about Dynalist which felt very different for me was the file system. Workflowy forces you to keep everything in one big outline. Dynalist lets you create many outlines, which it treats as different files; and, you can organize them into folders (recursively). Technically, that’s just another tree structure. In terms of UI, though, it made navigation much easier (because you can easily access a desired file through the file pane). Psychologically, it made me much more willing to start fresh outlines rather than add to one big one. This was both good and bad. It meant my ideas were less anchored in one big tree, but it eventually resulted in a big, disorganized pile of notes.

I did learn my lesson from Workflowy, though, and set things up in my Dynalist such that I actually developed ideas, rather than just collecting scraps forever.

**Temporary Notes vs Organized Notes**

I organized my Dynalist files as follows:

-   A “log” file, in which I could write whatever I was thinking about. This was organized by date, although I would often go back and elaborate on things from previous dates.
-   A “todo” file, where I put links to items inside “log” which I specifically wanted to go back and think more about. I would periodically sort the todo items to reflect my priorities. This gave me a list of important topics to draw from whenever I wasn’t sure what I wanted to think about.
-   A bunch of other disorganized files.

This system wasn’t _great,_ but it was a whole lot better at _actually developing ideas_ than the way I kept things organized in Workflowy. I had realized that locking everything into a unified tree structure, while good for the purpose of slowly improving a large ontology which organized a lot of little thoughts, was keeping me from just writing whatever I was thinking about.

Dan Sheffler (whose essays I’ve already cited several times in this writeup) writes about realizing that his note-taking system was simultaneously trying to implement [two different goals](http://www.dansheffler.com/blog/2014-07-21-two-goals-of-note-taking/): an organized long-term memory store, and “engagement notes” which are written to clarify thinking and have a more stream-of-consciousness style. My “log” file was essentially engagement notes, and my “todo” file was the long-term memory store.

For some people, I think an _essential part_ of Zettelkasten is the distinction between temporary and permanent notes. Temporary notes are the disorganized stream-of-consciousness notes which Sheffler calls engagement notes. Temporary notes can also include all sorts of other things, such as todo lists which you make at the start of the day (and which only apply to that day), shopping lists, etc. Temporary notes can be kept in a linear format, like a notebook. Periodically, you review the temporary notes, putting the important things into Zettelkasten.

In _Taking Smart Notes,_ Luhmann is described as transferring the important thoughts from the day into Zettel every evening. Sheffler, on the other hand, keeps a gap of at least 24 hours between taking down engagement notes and deciding what belongs in the long-term store. A gap of time allows the initial excitement over an idea to pass, so that only the things which still seem important the next day get into long-term notes. He also points out that this system enforces a small amount of spaced repetition, making it more likely that content is recalled later.

As for myself, I mostly write directly into my Zettelkasten, and I think it’s pretty great. However, I do find this to be difficult/impossible when taking quick notes during a conversation or a talk – when I try, then the resulting content in my Zettelkasten seems pretty useless (ie, I don't come back to it and further develop those thoughts). So, I've started to carry a notebook again for those temporary notes.

I currently think of things like this:

![](https://res.cloudinary.com/lesswrong-2-0/image/upload/v1568584395/Zettelkasted_3_svg_ewy4ql.svg)

_Jots_

These are the sort of small pointers to ideas which you can write down while walking, waiting for the bus, etc. The idea is stated very simply -- perhaps in a single word or a short phrase. A sentence at most. You might forget what it means after a week, especially if you don’t record the context well. The first thing to realize about jots is to capture them at all, as already discussed. The second thing is to capture them in a place where you will be able to develop them later. I used to carry around a small pocket notebook for jots, after I stopped using Workflowy regularly. My plan was to review the jots whenever I filled a notebook, putting them in more long-term storage. This never happened: when I filled up a notebook, unpacking all the jots into something meaningful just seemed like too huge a task. It works better for me to jot things into permanent storage directly, as I did with Workflowy. I procrastinate too much on turning temporary notes into long term notes, and the temporary notes become meaningless.

_Glosses_

A gloss is a paragraph explaining the point of a jot. If a jot is the title of a Zettelkasten card, a gloss is the first paragraph (often written in a distinct color). This gives enough of an idea that the thought will not be lost if it is left for a few weeks (perhaps even years, depending). Writing a gloss is usually easy, and doing so is often enough to get the ideas flowing.

_Development_

This is the kind of writing I described in the ‘notebooks’ section. An idea is fleshed out. This kind of writing is often still comprehensible years later, although it isn’t guaranteed to be.

_Refinement_

This is the kind of writing which is publishable. It nails the idea down. There’s not really any end to this -- you can imagine expanding something from a blog post, to an academic paper, to a book, and further, with increasing levels of detail, gentle exposition, formal rigor -- but to a first approximation, anyway, you’ve eliminated all the contradictions, stated the motivating context accurately, etc.

I called the last item “refinement” rather than “communication” because, really, you can communicate your ideas at any of these stages. If someone shares a lot of context with you, they can understand your jots. That’s really difficult, though. More likely, a research partner will understand your glosses. Development will be understandable to someone a little more distant, and so on.

  

## At Long Last, Zettelkasten

I’ve been hammering home the idea of “linear” vs “nonlinear” formats as one of the big advantages of Zettelkasten. But workflowy and dynalist both allow nonlinear writing. Why should you be interested in Zettelkasten? Is it anything more than a way to implement workflowy-like writing for a paper format?

I’ve said that (at least for me) there’s something extra-good about Zettelkasten which I don’t really understand. But, there are a couple of important elements which make Zettelkasten more than just paper workflowy.

-   **Hierarchy Plus Cross-Links:** A repeated theme across knowledge formats, including wikipedia and textbooks, is that you want both a hierarchical organization which makes it easy to get an overview and find things, and also a “cross-reference” type capability which allows related content to be linked -- creating a heterarchical web. I mentioned at the beginning that Zettelkasten forced me to create cross-links much more than I otherwise would, due to the use of small note-cards. Workflowy has “hierarchy” down, but it has somewhat poor “cross-link” capability. It has tags, but a tag system is not as powerful as hypertext. Because you _can_ link to individual nodes, it’s possible to use hypertext cross-links -- but the process is awkward, since you have to get the link to the node you want. Dynalist is significantly better in this respect -- it has an easy way to create a link to anything by searching for it (without leaving the spot you’re at). But it lacks the wiki-style “magic link” capability, creating a new page when you make a link which has no target. Roam, however, provides this feature.
-   **Atomicity:** The idea of creating pages organized around a single idea (again, an idea related to wikis). This is possible in Dynalist, but Zettelkasten practically forces it upon you, which for me was really good. Again, Roam manages to encourage this style.

  

## Zettelkasten, Part 2: Further Advice

## Card Layout

Don't stress about card formatting. You should write however feels natural to you. However, I thought you might like to see an example of what my cards tend to look like:

![](https://res.cloudinary.com/lesswrong-2-0/image/upload/v1568584395/Zettelkasten_4_svg_x0rcja.svg)

I’m left handed, so you may want to flip all of this around if you’re right handed. I use the ring binder “backwards” from the intended configuration (the punched hole would usually be on the left, rather than the right). Also, I prefer portrait rather than landscape. Most people prefer to use 3x5 cards in landscape, I suppose.

Anyway, not every card will look exactly like the above. A card might just contain a bunch of free-writing, with no bulleted list. Or it might only contain a bulleted list, with no blurb at the beginning. Whatever works. I think my layout is close to Luhmann’s and close to common advice -- but if you try to copy it religiously, you’ll probably feel like Zettelkasten is awkward and restrictive.

The only absolutely necessary thing is the address. The address is the first thing you write on a new card. You don’t ever want a card to go without an address. And it should be in a standard location, so that it is really easy to look through a bunch of cards for one with a specific address.

_Don’t feel bad if you start a card and leave it mostly blank forever._ Maybe you thought you were going to elaborate an idea, so you made a new card, but it’s got nothing but an address. That’s ok. Maybe you will fill it later. Maybe you won’t. Don’t worry about it.

Mostly, a thought is continued through elaboration on bullet points. I might write something like “cont. 1.1a1a” at the bottom of the card if there’s another card that’s really a direct continuation, though. (Actually, I don’t write “cont.”; I just write the down arrow, which means the same thing.) If so, I’d write “see 1.1a1” in the upper left hand corner, to indicate that 1.1a1a probably doesn’t make much sense on its own without consulting 1.1a1 -- moreso than usual for child cards. (Actually, I’d write another down arrow rather than “see”, mirroring the down arrow on the previous card -- this indicates the direct-continuation relationship.)

In the illustration, I wrote links \[in square brackets\]. The truth is, I often put them in full rectangular boxes (to make them stand out more), although not always. Sometimes I put them in parentheses when I’m using them more as a noun, as in: “I think pizza (12a) might be relevant to pasta. \[14x5b\]” In that example, “(12a)” is the card for pizza. “\[14x5b\]” is a card continuing the whole thought “pizza might be relevant to pasta”. So parentheses-vs-box is sort of like top-corner-vs-bottom, but for an individual line rather than a whole card.

**Use of Color**

The colors are true to my writing as well. For a long time, I wanted to try writing with multi-color click pens, because I knew some people found them very useful; but, I was unable to find any which satisfied my (exceptionally picky) taste. I don’t generally go for ball-point pens; they aren’t smooth enough. I prefer to write with felt-tip drawing pens or similar. I also prefer very fine tips (as a consequence of preferring my writing to be very small, as I mentioned previously) -- although I’ve also found that the appropriate line width varies with my mental state and with the subject matter. Fine lines are better for fine details, and for energetic mental states; broad lines are better for loose free-association and brainstorming, and for tired mental states.

In any case, a friend recommended the Hi-Tec C Coleto, a multi-color click pen which feels as smooth as felt-tip pens usually do (almost). You can buy whatever colors you want, and they’re available in a variety of line-widths, so you can customize it quite a bit.

At first I just used different colors haphazardly. I figured I would eventually settle on meanings for colors, if I just used whatever felt appropriate and experimented. Mostly, that meant that I switched colors to indicate a change of topic, or used a different color when I went back and [annotated](http://www.dansheffler.com/blog/2014-07-26-notebook-annotation/) something (which really helps readability, by the way -- black writing with a bunch of black annotations scribbled next to it or between lines is hard to read, compared to purple writing with orange annotations, or whatever!). When I switched to Zettelkasten, though, I got more systematic with my use of color.

I roughly follow Lion Kimbro’s advice about colors, from _How to Make a Complete Map of Every Thought you Think_:

> Now lets talk about color.

> Your pen has four colors: Red, Green, Blue, and Black

> You will want to connect meaning with each color.

> Here’s my associations:

> RED: Error, Warning, Correction

> BLUE: Structure, Diagram, Picture, Links, Keys (in key-value pairs)

> GREEN: Meta, Definition, Naming, Brief Annotation, Glyphs

> BLACK: Main Content

> I also use green to clarify sloppy writing later on. Blue is for Keys, Black is for values.

> I hope that’s self-explanatory.

> If you make a correction, put it in red. Page numbers are blue. If you draw a diagram, make it blue. Main content in black.

> Suppose you make a diagram: Start with a big blue box. Put the diagram in the box. (Or the other way around- make the diagram, than the box around it.) Put some highlighted content in black. Want to define a word? Use a green callout. Oops- there’s a problem in the drawing- X it out in red, followed by the correction, in red.

> Some times, I use black and blue to alternate emphasis. Black and blue are the easiest to see.

> If I’m annotating some text in the future, and the text is black, I’ll switch to using blue for content. Or vise versa.

> Some annotations are red, if they are major corrections.

> Always remember: Tolerate errors. If your black has run out, and you don’t want to get up right away to fetch your backup pen, then just switch to blue. When the thoughts out, go get your backup pen.

The only big differences are that I use brown instead of black in my pen, I tend to use red for titles so that they stand out very clearly, and I use green for links rather than blue. (Honestly I think my use of green for links might be a mistake. You want links to stand out more.)

  

## Index & Bibliography?

**Bibliography**

_Taking Smart Notes_ describes two other kinds of cards: indexes, and bibliographical notes. I haven’t made those work for me very effectively, however. Luhmann, the inventor of Zettelkasten, is described inventing Zettelkasten _as a way to organize notes originally made while reading_. I don’t use it like that -- I mainly use it for organizing notes I make while thinking. So bibliography isn’t of primary importance for me.

(Apparently Umberto Eco [similarly advises](http://www.dansheffler.com/blog/2015-08-10-reading-notes/) keeping idea notes and reading notes on separate sets of index cards.)

**Indexing**

So I don’t miss the bibliography cards. (Maybe I will eventually.) On the other hand, I _definitely_ need some sort of index, but I’m not sure about the best way to keep it up to date. I only notice that I need it when I go looking for a particular card and it is difficult to find! When that happens, and I _eventually_ find the card I wanted, I can jot down its address in an index. But, it would be nice to somehow avoid this. So, I’ve experimented with some ideas. Here are [someone else’s thoughts on indexing](https://zettelkasten.de/posts/three-layers-structure-zettelkasten/) (for a digital zettelkasten).

_Listing Assorted Cards_

The first type of index which I tried lists “important” cards (cards which I refer to often). I just have one of these right now. The idea is that you write a card’s name and address on this index if you find that you’ve had difficulty locating a card and wished it had been listed in your index. This _sounds_ like it should be better than a simple list of the top-level numbered cards, since (as I mentioned earlier) cards like 11a often turn out to be more important than cards like 11. Unfortunately, I’ve found this not to be the case. The problem is that this kind of index is too hard to maintain. If I’ve just been struggling to find a card, my working memory is probably already over-taxed with all the stuff I wanted to do after finding that card. So I forget to add it to the index.

_Topic Index_

Sometimes it also makes sense to just make a new top-level card on which you list everything which has to do with a particular category. I have only done this once so far. It seems like this is the main mode of indexing which other people use? But I don’t like the idea that well.

_Listing Sibling Cards_

When a card has enough children that they’re difficult to keep track of, I add a “zero” card before all the other children, and this works as an index. So, for example, card 2a might have children 2a1, 2a2, 2a3, … 2a15. That’s a lot to keep track of. So I add 2a0, which gets an entry for 2a1-2a15, and any new cards added under 2a. It can also get an entry for particularly important descendants; maybe 2a3a1 is extra important and gets an entry.

For cards like 2, whose children are alphabetical, you can’t really use “zero” to go before all the other children. I use “λ” as the “alphabetical zero” -- I sort it as if it comes before all the other letters in the alphabet. So, card “1λ” lists 1a, 1b, etc.

The most important index is the index at 0, ei, the index of all top-level numbered cards. As I describe in the “card layout” section, a card already mostly lists its own children -- meaning that you don’t need to add a new card to serve this purpose until things get unwieldy. However, top-level cards have no parents to keep track of them! So, you probably want an “absolute zero” card right away.

These “zero” cards also make it easier to keep track of whether a card with a particular address has been created yet. Every time you make a card, you add it to the appropriate zero card; so, you can see right away what the next address available is. This isn’t the case otherwise, especially if your cards aren’t currently sorted.

_Kimbro’s Mind Mapping_

I’ve experimented with adapting Lion Kimbro’s system from _How to Make a Complete Map of Every Thought You Think_. After all, a complete map of every thought you think sounds like the perfect index!

In my terminology, Lion Kimbro keeps only _jots_ -- he was focusing on collecting and mapping, rather than developing, ideas. Jots were collected into topics and sub-topics. When an area accumulated enough jots, he would start a [mind map](https://en.wikipedia.org/wiki/Mind_map) for it. I won’t go into all his specific mapping tips (although they’re relevant), but basically, imagine putting the addresses of cards into clusters (on a new blank card) and then writing “anchor words” describing the clusters.

You built your tree in an initially “top-down” fashion, expanding trees by adding increasingly-nested cards. You’re going to build the map “bottom-up”: when a sub-tree you’re interested in feels too large to quickly grasp, start a map. Let’s say you’re mapping card 8b4. You might already have an index of children at 8b4; if that’s the case, you can start with that. Also look through all the descendants of 8b4 and pick out whichever seem most important. (If this is too hard, start by making maps for 8b4’s _children,_ and return to mapping 8b4 later.) Draw a new mind map, and place it at 8b4a -- it is part of the index; you want to find it easily when looking at the index.

Now, the important thing is that _when you make a map for 8b,_ you can take a look at the map for 8b4, as well as any maps possessed by other children of 8b. This means that you don’t have to go through all of the descendents of 8b (which is good, because there could be a lot). You just look at the maps, which already give you an overview. The map for 8b is going to take _the most important-seeming elements_ from all of those sub-maps.

This allows important things to trickle up to the top. When you make a map at 0, you’ll be getting all the most important stuff from deep sub-trees just by looking at the maps for each top-level numbered card.

The categories which emerge from mapping like this can be completely different from the concepts which initially seeded your top-level cards. You can make new top-level cards which correspond to these categories if you want. (I haven’t done this.)

Now, when you’re looking for something, you start at your top-level map. You look at the clusters and likely have some expectation about where it is (if the address isn’t somewhere on your top-level map already). You follow the addresses to further maps, which give further clusters of addresses, until you land in a tree which is small enough to navigate without maps.

I’ve described all of this as if it’s a one-time operation, but of course you keep adding to these maps, and re-draw updated maps when things don’t fit well any more. If a map lives at 8b40a, then the updated maps can be 8b40b, 8b40c, and so on.You can keep the old maps around as a historical record of your shifting conceptual clusters.

  

## Keeping Multiple Zettelkasten

A note system like Zettelkasten (or workflowy, dynalist, evernote, etc) is supposed to stick with you for years, growing with you and becoming a repository for your ideas. It’s a big commitment.

It’s difficult to optimize note-taking if you think of it that way, though. You can’t experiment if you have to look before you leap. I would have never tried Zettelkasten if I thought I was committing to try it as my “next system” -- I didn’t think it would work.

Similarly, I can’t optimize my Zettelkasten very well with that attitude. A Zettelkasten is supposed to be one repository for everything -- you’re not supposed to start a new one for a new project, for example. But, I have several Zettelkasten, to test out different formats: different sizes of card, different binders. It _is_ still difficult to give alternatives a fair shake, because my two main Zettelkasten have built up momentum due to the content I keep in them.

I use a system of capital letters to cross reference between my Zettelkasten. For example, my main 3x5 Zettelkasten is “S” (for “small”). I have another Zettelkasten which is “M”, and also an “L”. When referencing card 1.1a within S, I just call it 1.1a. If I want to refer to it from a card in M, I call it S1.1a instead. And so on.

Apparently Luhmann [did something similar](https://zettelkasten.de/posts/luhmanns-second-zettelkasten/), starting a new Zettelkasten which occasionally referred to his first.

However, keeping multiple Zettelkasten for special topics is not necessarily a good idea. [Beware fixed categories](https://zettelkasten.de/posts/no-categories/). The danger is that categories limit what you write, or, become less appropriate over time. I’ve tried special-topic notebooks in the past, and while it does sometimes work, I often end up conflicted about where to put something. (Granted, I have a similar conflict about where to put things in my several omni-topic Zettelkasten, but mostly the 3x5 system I’ve described here has won out -- for now.)

On the other hand, I suspect it’s fine to create special topic zettelkasten for “very different” things. Creating a new zettelkasten because you’re writing a new book is _probably_ bad -- although it’ll work fine for the goal of organizing material for writing books, it means your next book idea isn’t coming from Zettelkasten. (Zettelkasten should contain/extend the thought process which generates book ideas in the first place, and it can’t do that very well if you have to have a specific book idea in order to start a zettelkasten about it.) On the other hand, I suspect it is OK to keep a separate Zettelkasten for fictional creative writing. Factual ideas can spark ideas for fiction, but, the two are sufficiently different “modes” that it may make sense to keep them in physically separate collections.

The idea of using an extended address system to make references between multiple Zettelkasten can also be applied to address other things, outside of your Zettelkasten. For example, you might want come up with a way of adding addresses to your old notebooks so that you can refer to them easily. (For example, “notebook number: page number” could work.)

  

## Should You Transfer Old Notes Into Zettelkasten?

Relatedly, since Zettelkasten ideally becomes a library of all the things you have been thinking about, it might be tempting to try and transfer everything from your existing notes into Zettelkasten.

(A lot of readers may not even be tempted to do this, given the amount of work it would take. Yet, those more serious about note systems might think this is a good idea -- or, might be too afraid to try Zettelkasten because they think they’d have to do this.)

I think transferring older stuff into Zettelkasten can be useful, but, trying to make it happen right away as one big project is most likely not worth it.

-   It’s true that part of the usefulness of Zettelkasten is the interconnected web of ideas which builds up over time, and the “high-surface-area” format which makes it easy to branch off any part. However, not all the payoff is long-term: it should also be useful _in the moment._ You’re not _only_ writing notes because they may help you develop ideas in the future; the act of writing the notes should be helping you develop ideas now.
-   You should probably only spend time putting ideas into Zettelkasten if you’re excited about further developing those ideas right now. You should not just be copying over ideas into Zettelkasten. You should be improving ideas, thinking about where to place them in your address hierarchy, interlinking them with other ideas in your Zettelkasten via address links, and taking notes on any new ideas sparked by this process. Trying to put all your old notes into Zettelkasten at once will likely make you feel hurried and unwilling to develop things further as you go. This will result in a pile of mediocre notes which will ultimately be less useful.
-   I mentioned the breadth-first vs depth-first distinction earlier. Putting all of your old notes into Zettelkasten is an extremely breadth-first strategy, which likely doesn’t give you enough time to go deep into further developing any one idea.

What about the dream of having all your notes in one beautiful format? Well, it is true that old notes in different formats may be harder to find, since you have to remember what format the note you want was written in, or check all your old note systems to find the note you want. I think it just isn’t worth the cost to fix this problem, though, especially since you should probably try many different systems to find a good one that works for you, and you can’t very well port all your notes to each new system.

Zettelkasten should be _an overall improvement compared to a normal notebook --_ if it isn't, you have no business using it. Adding a huge up-front cost of transferring notes undermines that. Just pick Zettelkasten up when you want to use it to develop ideas further.

  

## Depth-first vs Breadth-first

Speaking of depth-first vs breadth-first, how should you balance those two modes?

Luckily, this problem has some relevant computer science theory behind it. I tend to think of it in terms of iterative-deepening A\* heuristic search (IDA\*).

The basic idea is this: the advantage of depth-first search is that you can minimize memory cost by only maintaining the information related to the path you are currently trying. However, depth-first search can easily get stuck down a fruitless path, while breadth-first search has better guarantees. IDA\* balances the two approaches by going depth-first, but giving up when you get too deep, backing up, and trying a new path. (The A\* aspect is that you define “too deep” in a way which also depends on how promising a path seems, based on an optimistic assessment.) This way, you simulate a breadth-first search by a series of depth-first sprints. This lets you focus your attention on a small set of ideas at one time.

Once you’ve explored all the paths to a certain level, your tolerance defining “too deep” increases, and you start again. You can think of this as becoming increasingly willing to spend a lot of time going down difficult technical paths as you confirm that easier options don’t exist.

Of course, this isn’t a perfect model of what you should do. But, it seems to me that a note-taking system should aspire to support and encourage something resembling this. More generally, I want to get across the idea of thinking of your existing research methodology as an algorithm (possibly a bad one), and trying to think about how it could be improved. Don’t try to force yourself to use any particular algorithm just because you think you should; but, if you can find ways to nudge yourself toward more effective algorithms, that’s probably a good idea.

  

## Inventing Shorthand/Symbology

I don’t think writing speed is a big bottleneck to thinking speed. Even though I “think by writing”, a lot of my time is spent... well... thinking. However, once I know what I want to write, writing _does_ take time. When inspiration really strikes, I might know more or less what I want to say several paragraphs ahead of where I’ve actually written to. At times like that, it seems like every second counts -- the faster I write, the more ideas I get down, the less I forget before I get to it.

So, it seems worth putting _some_ effort into writing faster. (Computer typing is obviously a thing to consider here, too.) Shorthand, and special symbols, are something to try.

There’s also the issue of space. I know I advocate for small cards, which are intentionally limiting space. But you don’t want to waste space if you don’t have to. The point is to comprehend as much as possible as easily as possible. Writing bullet points and using indentation to make outlines is an improvement over traditional paragraphs because it lets you see more at a glance. Similarly, using abbreviations and special symbols will improve this.

I’ve tried several times to learn “proper” shorthand. Maybe I just haven’t tried hard enough, but it seems like basically all shorthand systems work by leaving out information. Once you’re used to them, they’re easy enough to read shortly after you’ve written them -- when you still remember more or less what they said. However, they don’t actually convey enough information to fully recover what was written if you don’t have such a guess. Basically, they don’t improve readability. They compress things down to the point where they’re hard to decipher, for the sake of getting as much speed as possible.

On the other hand, I’ve spent time experimenting with changes to my own handwriting which improve speed without compromising readability. Pay attention to what takes you the most time to write, and think about ways to streamline that.

Lion Kimbro emphasizes that you come up with ways to abbreviate things you commonly repeat. Ho describes using the Japanese symbols for days of the week and other common things in his system. The Bullet Journaling community has created its own symbology. Personally, I’ve experimented with a variety of different reference symbols which mean different sorts of things (beyond the () vs \[\] distinction I’ve mentioned).

The Bullet Journaling community has thought a lot about short-and-fast writing for the purpose of getting things out quickly and leaving more space on the page. They also have their own symbology which may be worth taking a look at. (I don't yet use it, but I may switch to it or something similar eventually.)

Well, that’s all I want to say for now. I may add to this document in the future. For now, best of luck developing ideas!

## Follow-Up Reports

**June 12, 2020.** My index-card Zettelkasten has not continued to grow at the rate it did initially. However, I still find the address system of Zettelkasten almost indispensable for note-taking. (I would say "I can't imagine what I did before" except that I know exactly what I did before; it was just way worse.) I have several different Zettelkasten in different formats, which I switch between.

One of the main ways I now use Zettelkasten is in notebooks with fixed-order pages. This means I **_can never sort the cards_**; pages remain in the order which they're originally written in. However, maintaining the address system is still _extremely_ useful, and allows me to have thoughts which I would not otherwise be capable of having. As I mentioned, I don't sort my index cards that often anyway, and I feel like I lose important recency information when I do so. So losing the ability to rearrange pages was not so bad! Although, I still miss it sometimes and do still use other formats which allow me to rearrange pages appropriately.

I still mainly find Zettelkasten useful on the order of minutes, weeks, and months. After that long, my notes start to become "stale" and I'm better off starting fresh rather than continuing to build on my old note structures about a particular idea. (Old notes are still useful for reference purposes, but I very rarely add to them to further develop the ideas therein, preferring to start fresh with a new tree of ideas.)

This is partly because the Zettelkasten note structure is not very rearrangeable, and so I can't "fix" old structures to reflect new thinking. As I've discussed, I've had better luck with digital note structures continuing to have real use over long time periods. But it's not just that -- I've still _mainly_ experienced note structures "going stale" in digital formats, as well.

There are two possible responses to this that I see. (1) Try to develop better techniques to make long-term useful structures. (2) Embrace the tendency, and optimize for the more immediate usefulness. I've tended toward (2). I worry that optimizing for (1) too much would create only hypothetical value at the cost of real value -- like when you delay gratification indefinitely.

Relatedly, I still haven't made much use of indexing.