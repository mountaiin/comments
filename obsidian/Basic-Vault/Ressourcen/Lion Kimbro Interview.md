Lion Kimbro is a very nice man. I know this because in recent weeks, I have been pestering him with a stream of questions about wiki, about notebooks, and about something Lion calls "The Public Web."

It began when I noticed Lion's book, [How to make a complete map of every thought you think](http://speakeasy.org/~lion/nb/) being added to lots of people's bookmarks lists at [del.icio.us](http://del.icio.us/url/6f8ac63b65f1b8be6749ab1d3027d1bc). I grabbed Lion's etext and read it while munching a lunchtime sandwich.

"This guy," I thought, "has lots to say. I should interview him."

A couple of emails later and Lion had agreed to be interviewed, and was happy to do it by email. Which was a good job, because Lion has a _lot_ of _very detailed_ thoughts about many subjects. My shorthand would never have been able to keep up with him in a telephone conversation.

I owe Lion my thanks for being such a jovial, entertaining and thought-provoking interviewee.

Below are some edited highlights of our email conversations (spanning several weeks and many messages). I apologise if some of it seems out of context - that's the disadvantage with trying to paraphrase a text-based conversation into a single text document. But I hope that despite the loss of context, there's enough of Lion's interesting thoughts and ideas here to provide you with something useful.

Before you start reading, a warning: this is well over 4500 words long - a good-sized Sunday newspaper magazine sort of article. You might prefer to save it for a day when you have time to read it.

___

**I read your book about notebooks and I loved it. But there's a lot I want to ask you about it. For starters, for how long, and when, did you carry around all this paper and write everything down? What on earth made you start doing it?**

I started thinking about notebook systems around 1998. I just kept hoards of little notebooks, and kept them all over the place - car, work, bedroom. I was 20 years old at the time.

The system described in "How to Make a Complete Map..." started about a year ago, and lasted 3-6 months. It's hard to say when it started, exactly, because it just kept evolving, and it evolved out of the previous systems. So, there's no real "starting point." For about 3 months, I was tracking thoughts pretty much all the time. The last month was the most concentrated period- it was more or less constant.

What inspired it was... Well, a lot of things. The book "Getting Things Done," by David Allen. "Wiki" was in the air, though I wasn't thinking of it consciously. The whole notion of "Intelligence," - in the CIA sense- not the IQ sense- was fresh in my mind. Remember, I was running medical records for a while- it makes you think differently about information, when someone's going to die if you can't find their chart. So it's a collection of things.

Anyways: I wanted to see if I could make myself smarter, by strategically placing notes to myself.

Intelligence, as I define it, is: Getting the right information at the right time at the right place, towards whatever end you are going for.

So, to use the "Getting Things Done" analogy: You're at home, and you realize you need to get a Philip's screwdriver. If, the next day, you're walking down the street, and you walk by a hardware store- if you don't think to get the screwdriver, that's an "Intelligence Failure." You're not getting the right information, at the right time, at the right place.

There's deeper concepts to that map- "Intelligence Process," and "Intelligence Database," and things like that. You can get into the nitty-gritty of it.

But, basically, I wanted to see if I could make myself smarter, by strategically placing notes to myself. Adopting an Intelligence Process, and carrying around an Intelligence Database.

For this particular system, it lasted for about 3-6 months.

**When you started, did you envisage it becoming the monster that it did? Did you plan from the start to make it as all-consuming as it did?**

I knew it would be a physical monster- I mean: really big, physically. Bulky.

I knew that already, because I've done a LOT of notebook systems before. One of them covered my walls with notes. One of them consisted of an enormous text file. One of them consisted of hoards of spiral bound notebooks; there's just hoards of ways to do it.

So, I knew it would be a physical monster.

What I _didn't_ antipicate was the freezing effect. That's where your mind re-hashes old things and sort of unconsciously avoids new things. (It's real, I've come to understand why it happens much better, through work in wiki, where it can happen as well.)

And I didn't anticipate just how strong the drive to write notes would become. You sort of re-wire yourself, as part of the Intelligence Process. It can get quite weird."

**Weren't there times when you thought "this is too much" and wanted to give up?**

No.

Well, yes.

I mean: I always wanted to write notes. Always. Nothing was more interesting. I had systems for _every_ situation. I was always working the notes.

But there were times where- I wanted to be able to do other things, but the compulsion to record notes was keeping me from doing them.

However, if you look in my notebook, there's not too many notes on that subject. I just didn't think about it often.

It wasn't until I watched the X-Men movie, and it made me think about a bunch of things, that I realized that the notes had to stop.

Specifically: The movie made me think about a _LOT_ of things, _Very Quickly._ I filled four pages with one line notes about all sorts of things, from metaphysics, to society, to visual language, to identiy- all sorts of stuff.

It would take ages to unpack it all and integrate it with the rest of the notebook system. It just couldn't be done.

**Uh, X-Men? Really? I saw it and thought it was OK, but what was it about that particular film that inspired so much thought?**

Well, I don't know that it was particularly that film.

While I was in the thick of it, I tended to avoid movies, since they made me think so much. It was sort of like getting psychologically bombed.

I have a hard time talking in depth about what's in there. But, some of the general themes are: Appearance, Democracy, Visual Language, Life's Systems, Egalite vs. Self-Awareness, Educational Systems, and 'the end of history.' "

**You say in your book that the price of immobility is worth paying for the clarity of mind you get out of it. What was clarified for you? Can you describe how your thoughts crystallized, and how it felt when they did?**

I should have written the answer to this question in the book- What a GREAT question!

I learned a _LOT_ of things, by doing this. I'll pick one, for example:

I remember the feeling of realizing, for the very first time, that if you wanted to know what your values were, you should look at your goals. And if you want to know what your goals should be, you should look at your values.

How did I learn that? I didn't learn it from a self-help book. Maybe I'd heard it before, maybe not, but I certainly never _realized_ it before.

But it came out, mechanically, just by doing the calculations. (By calculations, I mean: The steps of the notebook system- applying the introspection.)

Things like that would just "open up," spontaneously. Out of the blue. But when I _saw_ things like that, it became apparent to me that that there are these _deep_ systems in our minds. That there's just ages of structure there. It's like finding Atlantis.

I started to understand sections of the Tao Te Ching, things like that. It's actually very mechanical. It reads as if it's some mystic woo-foo; But then when you see the patterns in it matching the patterns you are seeing now, it appears very mechanical, very straight-forward.

Let me explain, concretely, the kinds of things that happen.

You find yourself reaching towards some objective. Your notes are strategy notes, mental puzzling, things like that, to reach the objective. It all goes into the notebook system. Then you have some questions about life. They seem unrelated. It all goes into the notebook system. Then you realize that the objective is important to you. That goes into the notebook system. How do you place "important?" Well, that's connected to your life. But now it's also attached to your objective. A line is literally drawn between the objectives, and the questions about life. You now consider them together.

Then you see this, and _that_ becomes a thought. It goes into the notebook system. You pay attention to this: You realize (because, it's there, graphically, right in front of you) that your notions about life are intricately connected to your objectives. Before, you thought you were just doing it for fun. But afterwards, you realize that it's part of this causal chain.

It feels like a mystery novel, or a fairy tale.

You ask questions. Remember: You have all the time in the world now. When you were in school, you had to whip something out and go to the next topic. But here, you have your whole _life_ ahead of you, to figure things out with. So, it's very nice. You ask questions that you never asked before, like: "_Why_ is this important to me?"

If you hold any question in mind long enough, you eventually find the answer. You read something, and it has the answer to your question. We're just surrounded by information: Just holding a question in mind is enough.

The answer comes, you integrate it into your database. Everything checks out? Good: You're golden.

So, you start to recognize, quite clearly, because it's all in your conscious mind, the structure of your life. Why you do things. What your REAL beliefs are. What things are core, and what things aren't. You hook your ideas up together- ideas about society, communication, personal identity, and systems in general. (At least, that's how it came up in my 2D graph, from a high-level perspective.)

And you see the pieces chink into place.

You become _different._ People can tell. It's kind of weird, at times. Because you know the answers to things that people always wondered about, and even felt, but couldn't quite put their finger on.

This is the _positive_ aspect of the notebook system: Total immobility, but you get a lot of clarity over what is important to you, and how your life is structured, and what your beliefs are.

You don't have to _impress_ anybody. There's nobody to impress. You don't have to _hurry up_\- you have all the time in the world.

So you can- very patiently- figure out what questions are important to you. You can figure out just where exactly your attention is- _what_ you are doing. You can do this all, more or less, in the privacy of your notebook.

You can think about "hidden subjects"- things that are REALLY important, but that people don't have the time to think about. Such as: "How do we communicate?" "How is thinking structured?" "What am I doing?", etc.

And you find answers.

So, basically: It feels like watching Atlantis come up.

**Some people might say that the notebooks plan outlined in your book is self-defeating, because a person putting it into action would spend so much time writing and organising bits of paper that they would miss out on a whole lot of other stuff in life, stuff that might well help them clarify their thoughts and learn more. How do you respond to that?**

Well, I think that most stuff in life confuses us more than clarifies things for us.

But there's a strong element of truth to the criticism as well. You DO miss out on a lot of things. And the stuff out there CAN help you.

So, I think it's best to get the best of both worlds: I would recommend doing the notebook system once or twice in your lifetime. I think I'll do it again, one day.

You don't have to do "Lion Kimbro's Notebook System." You should do your own form of Introspection. I _STRONGLY_ recommend using notes, however. And focusing on _integration._ Make sure your ideas connect to one another- don't let yourself think that things go one way one day, and another way the other day, just because you feel like it. Figure out what's going on inside of you, as best you can.

Gah. You can't really give expression to these kinds of ideas.

_Highest of all is the Truth. Higher still is True Living._ Right?

The goal isn't to become a monk. But be a monk for a little while. Organize your thoughts. Do the labor. If your results are anything like mine, it's worth it.

**Do you know of any people who have tried your system? What feedback have they given you?**

Nope..!

I've read a lot of nice things from people saying that they like the book. And I know people have taken ideas from the book, and used them in their own notekeeping systems.

But I don't know anyone who has tried the complete system, as it is written.

Which, I'm kind of happy about. That's sort of what I wanted. I wanted people to try different things, and take ideas from this.

So people tell me that they've taken parts of the system, or ideas from the system, and implemented them. They now divide their thoughts into lists. Or, they now map their ideas out on paper, like I described. Or they collect notes from strange places, now. Or even, they just say: "It was really inspiring."

But nobody's done the whole thing, as described. I'm kind of happy about that, now. Because there are some pretty major errors in there. It was always being refined.

**Do you know of any notebook-related sites out there? Other people with a hankering for written notes in preference to digital ones?**

In the Getting Things Done forums, there was a debate about whether to use paper or PDA. The answer is obvious to me: Paper. However, in 20 years time, it won't matter. The answer will be "Computer." The technical stuff will all be figured out. It'll be intuitive. Cheap. The batteries will run longer.

There are near-zero notebook sites out there. There are a lot of computer programs, but most are really primitive. I'm astonished by all the notekeeping programs that think you should categorize notes into one bin or another bin. "No!" Multiple-categorization. If you have a computer, you're not constrained to the physical filing system, where a paper can be in only one place at a time. You can store one piece of paper in 20 places, then. It can belong to 20 category trees. In fact, you can use category graphs- you don't need category trees.

Hardly anything on "how to keep a notebook." Most are lab book instructions. "Put your name and date in the top-right corner, and number your pages. Write neat." That sort of thing.

One of these days, I want to set up a personal notekeeping wiki. However, that day will not be soon. Someone else will make that wiki, before I get around to it. I am too busy.

I do think there needs to be a "science of personal notekeeping." I'd put it under the officially recognized "Library Sciences." I'm astonished that people have never thought it's more important. I looked all over the place for books on keeping notes. All I found were lab science notebooks.

There was one thing: There was a mention somewhere that Leonardo Da Vinci wanted to reorganize his notebook system. I think a friend pointed it out to me, in a book called "How to Think Like Leonardo Da Vinci," or something like that. Leonardo was just doing a straight chronology. He mentioned that he would like to group his related thoughts together. I like to pretend that I finished that thought of his.

("Guess what, Leonardo- It was a bad idea!")

**Tell me why you like wikis.**

Holy moley. That's a whole 'nother discussion in itself.

Wiki, briefly, are the hottest thing in Social Software right now. All this "friendster" stuff is important, but it's just a ruse. People don't see the real revolution that wiki represents.

Wiki is _terribly_ important because it's the first public communication system that is DOCUMENT based.

Instant Messaging, E-Mail, Internet Relay Chat (IRC), Online Message Boards, all this stuff-

It's all _MESSAGE_ based.

It's a "quick signal." You send a message to someone, and they do something different than they would have done before. "Someone." Sometimes even a group of people. Maybe you're on a mailing list, or something.

After the message is done, it's _spent._ It's indexed by Google, and someone looking for something can find your message amidst a sea of messages, and try to apply it to their situation.

But wiki is _totally_ different. It's _document_ based. The fundamental element of a wiki is the _document._ It's not a one-off message. (Though, you CAN attach messages to the bottom of a wiki page. Very common.) It's something that people can use, and refer back to, time and time again.

It's made to exist across time.

And so, also, that means that you get a sense of "space." You can point two wiki towards each other. You can't point two _mailing lists_ at each other. To do that, you'd have to have some program that automatically mails, every day, the same message over and over again. People would hate that. You can't network mailing lists, without the aid of static web pages. But then: People can't affect those, unless they are site administrators. So you are really constrained with what you can do.

Wiki is the only document based public system. That means that wiki (or something just like wiki, integrated into another system,) will be the vehicle by which our online conversations all organize _themselves._

It's my notebook system, ALL OVER AGAIN. In the notebook system, related things get pointed at each other, and then "move" on the grand map towards one another.

Let me put this in plain English:

Let's say we live in the year 2020. We haven't killed ourselves off yet, as a species. You're interested in writing for journalism. So you start off by searching the web for "writing."

You find yourself on some wiki somewhere. By now, the public refinery system has implanted itself, and there's wiki for every subject imaginable. You find yourself on some major "writing for science fiction" wiki. You go to a special page, that lists it's relationships with neighbors. You find that there is a "general wiki for writing." You go there, and find that it's a wiki made almost entirely of redirections. You find a link to a wiki about writing for journalism. You find yourself on the writing for journalism wiki. Now you're happy to be here, but you're just curious- what is topically "near" the journalism wiki? You see that there's a link to the journalism _law_ wiki, you see that there's a link to a _plain talk_ writing wiki, you see links to all sorts of topically related wiki.

There's a wiki for everything imaginable. All the information is maintained by the editors. There's a refinery process that takes poor quality writing, half baked ideas, integrates them into wiki that are taken better care of, and then they are taken into wiki that have even higher standards. It's all networked, so you can trace a path from any one subject to any other subject.

You can't network message based systems. Because the messages go away. Documents make it so you can network groups, though. And if a new group appears, it can graft itself into the network. If they do so inappropriately, the communities that reject them just de-link them.

Really, wiki are going to change the world. It's quite clear. The progression is already extraordinary.

Wiki is going to make our society more democratic, because we're going to have incredible clarity about things, and because we're going to have all this free information. It's going to be refined. It's going to be visual.

Now: Wiki is just one PART of "the Public Web." I shouldn't go overboard here. But the public web is going to completely transform society and our institutions.

For better or worse? Who knows. But it'll be a massive change.

I can talk for a long time about this. I'm very involved in wiki.

I hang out on [Community wiki](http://communitywiki.org/), mostly

I coordinate the [WikiFeatures wiki](http://wikifeatures.wiki.taoriver.net/).

Wiki IS the closest digital alternative to the paper notebooks concept. The problems with the notebook system are the same that wiki have faced.

However, my thinking has evolved a lot since I did the notebook system. I've found solutions to the problems."

**Do you still use paper notes at all, or store everything on wiki?**

Yes, I do use paper notes. Absolutely.

My process is so different, it's hard to describe in comparison to "How to make a complete map.

It's like a traditional journal- so, you can used bound notebooks. However, it has specific disciplines attached to it.

Some general _writing_ guidelines:

-   Keep track of progress towards some aim. Monitor focus, results.
-   Write for yourself, not your biographer. Use acronyms, assume your own knowledge.
-   Diagram-heavy. Your Diagrams:Words ratio should vary from 1:1 to 3:1. This is because images are extremely dense. \* Use a 4-color pen. Use one color predominantly.
-   If you're just thinking aloud to yourself, do it on seperate sheets of paper. When you've figured out your thoughts, transcribe the good parts to your notebook. You want the notebook to keep your best thoughts only.
-   Strictly chronological. Can reference previous/ future items, but don't try to maintain integrity.
-   Always keep the notebook contents "high density."

Some No-nos:

-   Never use the paper for storing doodles you used as part of a conversation. Just, don't fill it with anything low-quality.
-   Don't keep "Lab Notes" in the notebook. That is, lengthy notes on projects, details. Don't fill it with raw data, don't fill it with big design specs. It's only for your high level thoughts.
-   Don't use it _substantially_ as an address book. (Of course, you'll use it now and then to store a phone number or address, since you'll always carry it around with you.)

The new system I use actually _minimizes_ items for fixation of attention, and focuses on what's _current_.

I DO intend to write about it. When I do so, I'll be doing so to a wiki. (The "notebooks" wiki. No, it doesn't exist yet.)

**Wiki still needs a keyboard. You can't write and draw like you can on paper.**

Yes- absolutely.

In about 5-10 years, the tablet PCs will be cheap enough, and we'll have integrated SVG editing into the wiki document technology.

We assume that people find value in communicating online, though, even with the constraints of the keyboard.

Remember, too there's a big difference between personal notekeeping, and wiki.

**What practical examples can you think of where wiki will be an important part of "the public web"?**

Practical uses:

-   You've noticed that it takes you a long time to clean up your house. You're wondering if there's a way to speed that along. You find a goldmine resource: A wiki dedicated to home economics, filled with the wisdom of hoards of real life people.
-   You're a student, and can't figure out how to make sense of college life. You go to a wiki on being a studentship. The wisdom of hoards of students and former students is right there for you, hammered out through hundreds, if not thousands, of conversations between people.
-   You've think you've been wronged, but you don't know if you can take your case to court or not. You go onto a wiki about law, and find the wisdom of hoards of people, presented in plain talk.

The power of wiki is the ability to distill the wisdom of common people. It starts with a simple dialog between two people. Then the voices of more people are added. As the issue is explored, it starts to be collected and organized. It is a very messy process, but it yields real gems.

Most of the Internet communication systems are _message-based._ You can't easily cross-reference. You can't replace an old ugly mess of back-and-forth, and replace it with a summary of the key points. Someone may say something wise, but that comment is quickly lost in the river of new comments. When you _search_ the Internet's messages, you find bits and pieces of relevance, few and far between. _Wiki reverses that._ When you touch a page in a wiki, the page is connected to other related pages, relevance determined by humans.

Because wiki entries are written by real people with real problems or sincere observations, the voice of a wiki tends to be _plain._ That means, it's something you can actually read. You get a lot of first-hand exposure to disagreement, as well. When you get a book in a library, or visit a static web page, it's hard to get the perspectives of other people, and round out your vision.

I could...

-   ...write about the problems of the textbook publishing industry- about how the mess of authorities, certificates, distribution chains, professors, review cycles, governments, and colleges have made it near impossible to write a really cheap and innovative textbook. Wiki solves much of this problem.
-   ...write about the limitations of the concept of a "book," and how collaboratively developed hypertexts will help us past those limitations. (Wiki is one technology for doing this.)
-   ...write about how Social Software is going to concentrate the attention of people on the Internet. People will (are already!) get sucked into wiki as they perform google searches. Content in wiki is generally more useful than snippets of un-integrated information scattered about, here and there. As people's attention collectively concentrates, I believe they will learn not only about what they are studying, but I believe they will also learn more about themselves, and the efforts they participate in, whether they knew it or not. (A person who is, say, a Rationalist, remains a Rationalist, whether they know it or not.) I believe the unconscious will become conscious. People will discover how deeply connected they are to one another. At the same time, the other Internet communications technologies will be kicking in as well. Suddenly, it'll be like the lights are all turning on. _Your neighborhood_ will light up like a Christmas tree. It'll be like you have x-ray vision, looking into your neighbors houses, and seeing what they are doing and thinking. Physical community will suddenly re-appear. Aligned people in their region will connect. Community is VERY good for employment, democracy, social justice, and safety, since Trust is key to all of these, and community builds trust.

(Do these things count as "practical?")

What role did wiki play in this? Well, it's sort of like the question, "What role did paper and books have in the history of the world?" I'd say: "A LOT!" Messages are the spoken words of the Internet; Documents are like it's paper. To write on the Internet's paper, you have to own a printing press, and know how to operate it. (That is, own a server, and be an HTML tech, at least.) With wiki, this is no longer the case.

Really, there are so many ways to answer your question, I'm not sure where to begin!

___

_Filed under: [work](https://gilest.org/category-work.html)_  
_(22nd March 2004)_

𐡸