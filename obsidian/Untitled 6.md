Danke für die Informationen, Jason. Es scheint, dass das Bonding-Interface und die Slave-Verbindungen noch immer Probleme bereiten. Es gibt eine alternative Methode, bei der wir das Bonding durch NetworkManager konfigurieren und stattdessen die Konfiguration manuell vornehmen. Hier sind die Schritte:

1.  Deaktiviere zunächst alle Bonding- und Slave-Verbindungen:

```
nmcli connection down bond-bond0
nmcli connection delete bond-bond0
nmcli connection delete bond-slave-eth0
nmcli connection delete bond-slave-wlan0
```

2.  Installiere das ifenslave-Paket:

```
sudo pacman -S ifenslave
```

3.  Erstelle eine neue Konfigurationsdatei `/etc/systemd/network/30-bond0.netdev` mit folgendem Inhalt:

```
[NetDev]
Name=bond0
Kind=bond

[Bond]
Mode=balance-rr
```

4.  Erstelle eine Netzwerk-Konfigurationsdatei `/etc/systemd/network/30-bond0.network`:

```
[Match]
Name=bond0

[Network]
DHCP=yes
```

5.  Erstelle außerdem separate Netzwerkkonfigurationsdateien für `eth0` und `wlan0`:

`/etc/systemd/network/31-eth0.network`:

```
[Match]
Name=eth0

[Network]
Bond=bond0
```

`/etc/systemd/network/32-wlan0.network`:

```
[Match]
Name=wlan0

[Network]
Bond=bond0
```

6.  Aktiviere, starte und überprüfe den systemd-networkd-Service:

```
sudo systemctl enable --now systemd-networkd
sudo systemctl start systemd-networkd
sudo systemctl status systemd-networkd
```

7.  Stelle sicher, dass sowohl `eth0` (Kabelgebundene Verbindung 2) als auch `wlan0` (FRITZ!Box 4040 RI) aktiv sind:

```
nmcli connection up "Kabelgebundene Verbindung 2"
nmcli connection up 'FRITZ!Box 4040 RI'
```

8.  Überprüfe den Status des Bonding-Interface (bond0):

```
cat /proc/net/bonding/bond0
```

Wenn alles erfolgreich ist, sollte bond0 beide Verbindungen verwenden. Bitte beachte, dass dies eine manuelle Konfiguration ist und möglicherweise einige Anpassungen an den Netzwerkeinstellungen deines Systems vornehmen kann. Verwende diese Methode nur, wenn du dich mit der manuellen Netzwerkkonfiguration wohl fühlst.

Gib mir Bescheid, ob du Erfolg hast oder weitere Hilfe benötigst.