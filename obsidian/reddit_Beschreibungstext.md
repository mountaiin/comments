Let's explore together the infinite possibilities of the human mind, neural networks and AI. But there is more: Dive into topics like astronomy, autism, consciousness, learning and memory formation, technological singularity, bash scripting, etymology, or even squids and more fascinating stuff!

AI-generated content as inspiration, join discussions around social change through technology. We are dwarves on the shoulders of giants - come by & discover the adventure of humanity & AI!


<iframe src="https://earnest-bienenstitch-fa40c1.netlify.app/" width="100%" height="1200" frameborder="1"></iframe>

