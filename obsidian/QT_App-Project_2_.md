Ja und zwar ich bin derzeit dabei, meine eigene app zu entwickeln. ich nutze qt creator und möchte dem user ein GUI anbieten, um auf simple weise mit openAI's chatmodellen zu chatten. Meine app sieht shcon gut, aber funktioniert nur grob so wie ich es haben wollte. Es gibt mehrere Probleme: 1) Leider ist es so, dass mein Eingabetext nicht im Hauptfenster erscheint, das heißt, im Hauptfenster (ChatTextEdit) erscheint nur die Antwort von openai, aber nicht das, was ich geschrieben habe. wie hänge ich meinen text oben im hauptfeld an? 2) wenn ich mein request im eingabefeld eingegeben und mit dem button (senden) abgeschickt habe, persistiert der text weiterhin im eingabefeld. dieser sollte aber nach der eingabe aus dem feld verschwinden. was muss ich dazu machen? 3) die Anfragen an openai scheinen auch kontext zu behalten, das heißt, jedes request ist eine neue Anfrage, ohne dass das Sprachmodell seine eigenen vorherigen Antworten mit geschickt bekommt. wie löse ich dieses Preoblem?

Im Folgenden siehst du meinen Code.

Hier erstmal meine mainwindow.h:

```
// mainwindow.h

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void sendRequest(const QString &input);
    void onApiReply(QNetworkReply *reply);
    void appendMessage(const QString &role, const QString &content);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
```

Hier meine main.cpp:

```
#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
```

Hier meine mainwindow.cpp:

```
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->sendButton, &QPushButton::clicked, [this]() {
        QString input = ui->userInput->toPlainText();
        sendRequest(input);
    });
}

void MainWindow::sendRequest(const QString &input) {
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url("https://api.openai.com/v1/chat/completions");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("Authorization", QString("Bearer %1").arg("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").toUtf8());
    QJsonObject data;
    data["model"] = "gpt-3.5-turbo";

    QJsonArray messages;
    messages.append(QJsonObject({{"role", "system"}, {"content", "You are a helpful assistant."}}));
    messages.append(QJsonObject({{"role", "user"}, {"content", input}}));

    data["messages"] = messages;

    QJsonDocument doc(data);
    QByteArray jsonData = doc.toJson();

    connect(manager, &QNetworkAccessManager::finished, this, &MainWindow::onApiReply);

    manager->post(request, jsonData);
}

void MainWindow::appendMessage(const QString &role, const QString &content) {
    ui->chatTextEdit->append(QString("<b>%1:</b> %2").arg(role).arg(content));
}

void MainWindow::onApiReply(QNetworkReply *reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QByteArray responseData = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(responseData);
        QJsonObject obj = doc.object();
        QString result = obj["choices"].toArray()[0].toObject()["message"].toObject()["content"].toString();

        appendMessage("Assistant", result); // Aktualisiere das Chat-Fenster mit der Antwort
    } else {
        // Fehlerbehandlung, z.B. zeige eine Fehlermeldung an
    }

    reply->deleteLater();
}


MainWindow::~MainWindow()
{
    delete ui;
}
```

Und meine mainwindow.ui enthält ein PlainText Edit Element (userInput), ein Text Edit Feld (chatTextEdit), das den Chatverlauf darstellen soll und ein Push Button (sendButton). Bitte sage mir, wie ich die Probleme beheben kann und dafür sorgen kann, dass die nachrichten von mir und die antworten von openai abwechselnd und nacheinander akkumulieren (also dass sie wie ein Chat werden), und wie der Text aus dem Eingabefeld verschwindet, nachdem der User auf den Senden Button geklickt hat.

![Delete](https://chat.yaz.ai.in/src/assets/delete.svg)

Hallo Jason! Danke für die detaillierte Frage und das Teilen deines Codes. Ich werde dir bei jedem deiner Probleme helfen. Hier sind die Lösungen für die genannten Probleme:

1.  Du möchtest, dass der Text, den du eingegeben hast, zusammen mit der Antwort von OpenAI im Chat-Fenster erscheint. Du kannst die Funktion `appendMessage` verwenden, um auch deinen Text hinzuzufügen. Füge einfach die folgende Zeile in der Lambda-Funktion der `sendButton`-Verbindung hinzu:

```
connect(ui->sendButton, &QPushButton::clicked, [this]() {
    QString input = ui->userInput->toPlainText();
    sendRequest(input);
    appendMessage("User", input); // Fügt den Text des Benutzers dem Chat-Fenster hinzu
});
```

2.  Um den Text im Eingabefeld zu löschen, nachdem der Senden-Button gedrückt wurde, füge einfach die folgende Anweisung in der Lambda-Funktion der `sendButton`-Verbindung hinzu:

```
connect(ui->sendButton, &QPushButton::clicked, [this]() {
    QString input = ui->userInput->toPlainText();
    sendRequest(input);
    appendMessage("User", input); // Fügt den Text des Benutzers dem Chat-Fenster hinzu
    ui->userInput->clear(); // Löscht den Text im Eingabefeld
});
```

3.  Um den Kontext zwischen den Anfragen beizubehalten, musst du die vorherigen Nachrichten einschließlich der Antwort des Chat-Modells speichern und in der `messages`-Liste erneut senden. Hierfür sollten wir eine QList<QPair<QString, QString>> erstellen, die die Paare von Rolle und Inhalt der Nachrichten enthält. Füge dieses Attribut im Hauptfenster hinzu, aktualisiere es nach jeder Antwort und verwende es, um den richtigen Kontext bei jeder Anfrage zu behalten.

Zuerst fügen wir das Attribut `chatHistory` zur mainwindow.h hinzu:

```
// mainwindow.h

private:
    Ui::MainWindow *ui;
    QList<QPair<QString, QString>> chatHistory; // Hinzufügung des Attributs zur mainwindow.h
```

Dann aktualisieren wir die `sendRequest`-Funktion, um den richtigen Kontext zu nutzen:

```
void MainWindow::sendRequest(const QString &input) {
    // ...
    chatHistory.append(QPair<QString, QString>("user", input)); // Aktualisiere Chat-Verlauf

    // Hier fügen wir anstelle der alten Nachrichtenliste die chatHistory zur data JSON-Struktur hinzu
    QJsonArray messages;
    for (const QPair<QString, QString> &pair : chatHistory) {
        messages.append(QJsonObject({{"role", pair.first}, {"content", pair.second}}));
    }
    data["messages"] = messages;
    // ...
}
```

Und schließlich aktualisieren wir die `onApiReply` Funktion, um die Antwort des Chat-Modells in der Liste der vorherigen Nachrichten zu speichern:

```
void MainWindow::onApiReply(QNetworkReply *reply) {
    if (reply->error() == QNetworkReply::NoError) {
        // ...

        appendMessage("Assistant", result); // Aktualisiere das Chat-Fenster mit der Antwort
        chatHistory.append(QPair<QString, QString>("assistant", result)); // Aktualisiere Chat-Verlauf
    } else {
        // Fehlerbehandlung, z.B. zeige eine Fehlermeldung an
    }

    reply->deleteLater();
}
```

Jetzt sollten deine Probleme behoben sein: Dein eingegebener Text erscheint im Hauptfenster, der Text im Eingabefeld wird nach dem Senden gelöscht und jede Anfrage sollte den Kontext der vorherigen Antworten beibehalten. Letztendlich wird alles wie ein Chat akkumuliert. Wenn du noch weitere Fragen oder Probleme hast, zögere bitte nicht, mich erneut zu kontaktieren. Viel Erfolg mit deiner App!