
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 04:04:11
```

---


**User:**
Wie kann ich mir in der cli meine ip addresse anzeigen lassen?


---

**ChatGPT:**
Um deine IP-Adresse in der Befehlszeile (CLI) anzuzeigen, gibt es verschiedene Möglichkeiten, je nachdem welches Betriebssystem du verwendest. Hier sind einige Beispiele:
Für Linux und MacOS:
<li>Öffne ein Terminal-Fenster.</li><li>Gib den Befehl <code>ifconfig</code> oder <code>ip addr show</code> ein.</li><li>Suche nach der Zeile mit "inet" oder "inet6". Dort findest du deine IP-Adresse.</li>
Alternativ kannst du auch den Befehl <code>hostname -I</code> verwenden, um nur die IP-Adresse anzuzeigen.
Für Windows:
<li>Öffne die Eingabeaufforderung (cmd.exe).</li><li>Gib den Befehl <code>ipconfig</code> ein.</li><li>Suche nach der Zeile mit "IPv4-Adresse" oder "IPv6-Adresse". Dort findest du deine IP-Adresse.</li>
Alternativ kannst du auch den Befehl <code>ipconfig | findstr /i "ipv4"</code> verwenden, um nur die IPv4-Adresse anzuzeigen.