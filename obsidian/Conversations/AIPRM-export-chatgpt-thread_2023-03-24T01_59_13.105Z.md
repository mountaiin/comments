
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:59:13
```

---


**User:**
Ich bräuchte deine Hilfe beim Erstellen eines Apple Kurzbefehls(Shortcuts). Ich einen Kurzbefehl, der vom User eine Frage entegegennimmt, diese Frage wird über eine API request an openAI's sprachmodell gpt-3 verschickt und daraufhin erhält der User die Antwort. So weit so gut. Ich habe nun eine loop funktion eingebaut, sodass nach der antwort der user wieder nach einer Frage gefragt wird. Aber das Problem, das ich habe ist, dass in der neuen/zweiten Frage, der Kontext von der vorherigen Frage und Antwort nicht mehr dabei ist. Wie mache ich es, dass Frage und Antwort bei jeder "Runde" an die nächste Runde angehängt werden, und sodass Anfrage an OpenAI quasi von Runde zu Runde an Umfang wächst?

Please write in informal tone, instructive writing style, German language.

---

**ChatGPT:**
Du kannst den Kontext der vorherigen Fragen und Antworten speichern und bei jeder neuen Anfrage an OpenAI anhängen. Hier sind die Schritte, um dies in einem Apple Kurzbefehl zu erreichen:
<li><p>Erstelle eine neue Variable, z.B. <code>gesamtKontext</code>, zu Beginn des Kurzbefehls, um den gesamten Kontext zu speichern. Du kannst die Variable als leeren Text initialisieren: <code>""</code>.</p></li><li><p>Nachdem du die erste Frage vom Benutzer erhalten hast, füge sie zu der <code>gesamtKontext</code> Variable hinzu, indem du die "Text zusammenfügen" Aktion verwendest. Beispiel:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>makefile</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-makefile">gesamtKontext = gesamtKontext + <span class="hljs-string">"Frage: "</span> + ersteFrage
</code></div></div></pre></li><li><p>Wenn du die Antwort von GPT-3 erhältst, füge sie ebenfalls zu der <code>gesamtKontext</code> Variable hinzu:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>makefile</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-makefile">gesamtKontext = gesamtKontext + <span class="hljs-string">" Antwort: "</span> + gpt3Antwort
</code></div></div></pre></li><li><p>In der Loop-Funktion, wenn du die nächste Frage vom Benutzer erhältst, füge sie wiederum zu der <code>gesamtKontext</code> Variable hinzu:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>makefile</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-makefile">gesamtKontext = gesamtKontext + <span class="hljs-string">" Frage: "</span> + naechsteFrage
</code></div></div></pre></li><li><p>Bevor du die Anfrage an OpenAI sendest, füge die <code>gesamtKontext</code> Variable in den "Eingabeparameter" für die API-Anfrage ein. So wird der gesamte bisherige Kontext bei jeder Anfrage mitgesendet.</p></li><li><p>Nachdem du die Antwort für die aktuelle Runde erhalten hast, füge sie ebenfalls zu der <code>gesamtKontext</code> Variable hinzu:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>makefile</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-makefile">gesamtKontext = gesamtKontext + <span class="hljs-string">" Antwort: "</span> + aktuelleAntwort
</code></div></div></pre></li>
Dadurch wird der gesamte Kontext der Fragen und Antworten bei jeder Runde an die nächste Runde weitergegeben und die Anfrage an OpenAI wächst von Runde zu Runde an Umfang. Achte darauf, dass du bei der Verwendung von GPT-3 auf das Token-Limit achtest. Wenn der Kontext zu groß wird, musst du eventuell ältere Teile des Kontexts entfernen oder kürzen.

---

**User:**
Also ich habe mir bereits nach Punkt 2 ein "Ergebnis einblenden" eingebaut und es zeigt mir leider nichts an.. ich verstehe diesen Fehler nicht. Ich habe alles nach ANleitung gemacht:
Leerer Text -> zu Variable "gesamtKontext" hinzugefügt, Loop fängt an, User wird nach Frage gefragt, User Input wird in der Variable "anliegen" gespeichert, Text kombinieren -> Text:variable "gesamtKontext" mit variable "anliegen" kombinieren. Dann folgt Anzeigen von "kombinierter Text" und es erscheint leider nur ein leerer Inhalt, ohne die Frage des Users. Was mache ich da falsch?

Please write in informal tone, instructive writing style, German language.

---

**ChatGPT:**
Es sieht so aus, als ob der "kombinierte Text" nicht korrekt erstellt wird. Ich gehe davon aus, dass du die "Text kombinieren" Aktion verwendet hast. Hier ist eine alternative Möglichkeit, den Text zu kombinieren und das Problem zu beheben:
<li><p>Statt der "Text kombinieren" Aktion zu verwenden, erstelle eine neue Aktion "Text" unter der Aktion, die den Benutzer nach der Frage fragt.</p></li><li><p>In der neuen "Text" Aktion, platziere zwei Textplatzhalter (<code>%VariableName%</code>), einen für die <code>gesamtKontext</code> Variable und einen für die <code>anliegen</code> Variable. Zwischen den beiden Platzhaltern kannst du den Text "Frage: " einfügen. Zum Beispiel:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>makefile</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-makefile"><span class="hljs-section">%gesamtKontext%Frage: %anliegen%</span>
</code></div></div></pre></li><li><p>Füge die neue "Text" Aktion zur <code>gesamtKontext</code> Variable hinzu, indem du die Aktion "Variable festlegen" verwendest. Wähle die <code>gesamtKontext</code> Variable aus und setze sie auf die neue "Text" Aktion. Beispiel:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>makefile</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-makefile">gesamtKontext = %Text%
</code></div></div></pre></li><li><p>Jetzt kannst du das Ergebnis einblenden, indem du die Aktion "Ergebnis einblenden" verwendest und die <code>gesamtKontext</code> Variable auswählst.</p></li>
Das sollte das Problem beheben und den kombinierten Text, der die Frage des Benutzers enthält, anzeigen. Vergewissere dich, dass die Reihenfolge der Aktionen korrekt ist, um sicherzustellen, dass der Kurzbefehl wie erwartet funktioniert.

---

**User:**
Ich erhalte aktuell als antwort von openai folgendes:

{"error":{"param":null,"message":"We could not parse the JSON body of your request. (HINT: This likely means you aren't using your HTTP library correctly. The OpenAI API expects a JSON payload, but what was sent was not valid JSON. If you have trouble figuring out how to fix this, please send an email to support@openai.com and include any relevant code you'd like help with.)","code":null,"type":"invalid_request_error"}}

Dabei hat es vorher funktioniert, bevor ich texte zusammengefügt hatte usw

Ich nutze aktuell als Endpoint v1/chat/completion und als model "gpt-3.5-turbo"
Dort wird die Anfrage folgenderweise formatiert:

curl https://api.openai.com/v1/chat/completions \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer YOUR_API_KEY' \
  -d '{
  "model": "gpt-3.5-turbo",
  "messages": [{"role": "user", "content": "Hallo meine Freundin!"}]
}'

Wäre es hier möglicherweise sinnvoller, die Texte (Fragen und Antworten) als Wörterbücher aneinander zu verketten?

Please write in informal tone, instructive writing style, German language.

---

**ChatGPT:**
Das Problem scheint bei der Formatierung der JSON-Anfrage zu liegen. Bei Verwendung der OpenAI Chat API und des <code>v1/chat/completion</code> Endpunkts musst du die Fragen und Antworten in Form von Nachrichten (Messages) in einem Array senden.
Du kannst die Fragen und Antworten als Wörterbuch (Dictionary) verketten, um sicherzustellen, dass die Anfrage korrekt formatiert ist. Hier sind die Schritte, um dies zu tun:
<li><p>Erstelle eine leere Liste namens <code>nachrichtenListe</code> zu Beginn des Kurzbefehls.</p></li><li><p>Nachdem du die erste Frage vom Benutzer erhalten hast, füge sie der Liste als neues Wörterbuch hinzu:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>go</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-go">nachrichtenListe.<span class="hljs-built_in">append</span>({<span class="hljs-string">"role"</span>: <span class="hljs-string">"user"</span>, <span class="hljs-string">"content"</span>: ersteFrage})
</code></div></div></pre></li><li><p>Wenn du die Antwort von GPT-3 erhältst, füge sie ebenfalls der Liste hinzu:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>go</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-go">nachrichtenListe.<span class="hljs-built_in">append</span>({<span class="hljs-string">"role"</span>: <span class="hljs-string">"assistant"</span>, <span class="hljs-string">"content"</span>: gpt3Antwort})
</code></div></div></pre></li><li><p>In der Loop-Funktion, wenn du die nächste Frage vom Benutzer erhältst, füge sie wiederum der Liste hinzu:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>go</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-go">nachrichtenListe.<span class="hljs-built_in">append</span>({<span class="hljs-string">"role"</span>: <span class="hljs-string">"user"</span>, <span class="hljs-string">"content"</span>: naechsteFrage})
</code></div></div></pre></li><li><p>Bevor du die Anfrage an OpenAI sendest, konvertiere die <code>nachrichtenListe</code> in JSON und füge sie in den "Eingabeparameter" für die API-Anfrage ein. Du kannst dies mit der "Dictionary in JSON Text konvertieren" Aktion tun.</p></li><li><p>Achte darauf, dass die API-Anfrage korrekt formatiert ist, indem du die JSON-Struktur beibehältst und die Liste der Nachrichten (Messages) als Wert des Schlüssels "messages" in der API-Anfrage einfügst.</p></li>
Der Fehler sollte jetzt behoben sein, und du solltest in der Lage sein, den Kontext der vorherigen Fragen und Antworten in der API-Anfrage zu behalten. Achte darauf, dass die Reihenfolge der Aktionen korrekt ist und die API-Anfrage korrekt formatiert ist, um sicherzustellen, dass der Kurzbefehl wie erwartet funktioniert.