

**User:**
Ich habe eine Streamlit App lokal laufen, die auf Basis eines ML Modells Bildunterschriften zu entsprechenden Bildern erzeugt. Am Ende ist das Ergebnis "caption" auf Englisch. Kann ich es innerhalb meines Streamlit Codes so verändern, dass Caption erst mithilfe meiner DeepL API auf deutsch übersetzt wird und dann das ergebnis beispielsweise als "caption_de" ausgegeben wird? So sieht der Code aus:

import streamlit as st
import requests
import io


# Designing the interface
st.title("AI basierte Bildbeschreibungen")

st.sidebar.markdown(
    """
    Ein Bilduntertitelmodell durch Kombination des ViT-Modells mit dem GPT2-Modell\n
    """
)

with st.spinner('Laden und Kompilieren des ViT-GPT2-Modells ...'):
    from model import *

random_image_id = get_random_image_id()

st.sidebar.title("Wähle eine Beispieldatei aus")
sample_image_id = st.sidebar.selectbox(
    "Wähle aus dem Dropdown Menü aus",
    sample_image_ids
)

if st.sidebar.button("Random COCO 2017 (val) images"):
#if st.sidebar.button("Zufälliges Bild"):
    random_image_id = get_random_image_id()
    sample_image_id = "None"

bytes_data = None
with st.sidebar.form("file-uploader-form", clear_on_submit=True):
    uploaded_file = st.file_uploader("Wähle eine lokale Datei aus")
    submitted = st.form_submit_button("Upload")
    if submitted and uploaded_file is not None:
        bytes_data = io.BytesIO(uploaded_file.getvalue())

if (bytes_data is None) and submitted:

    st.write("Es wurde noch keine Datei ausgewählt, die hochgeladen werden könnte")

else:

    image_id = random_image_id
    if sample_image_id != "Kein Bild ausgewählt":
        assert type(sample_image_id) == int
        image_id = sample_image_id

    sample_name = f"COCO_val2017_{str(image_id).zfill(12)}.jpg"
    sample_path = os.path.join(sample_dir, sample_name)

    if bytes_data is not None:
        image = Image.open(bytes_data)
    elif os.path.isfile(sample_path):
        image = Image.open(sample_path)
    else:
        url = f"http://images.cocodataset.org/val2017/{str(image_id).zfill(12)}.jpg"
        image = Image.open(requests.get(url, stream=True).raw)

    width, height = image.size
    resized = image.resize(size=(width, height))
    if height > 384:
        width = int(width / height * 384)
        height = 384
        resized = resized.resize(size=(width, height))
    width, height = resized.size
    if width > 512:
        width = 512
        height = int(height / width * 512)
        resized = resized.resize(size=(width, height))

    if bytes_data is None:
        st.markdown(f"[{str(image_id).zfill(12)}.jpg](http://images.cocodataset.org/val2017/{str(image_id).zfill(12)}.jpg)")
    show = st.image(resized)
    show.image(resized, '\n\nAusgewähltes Bild')
    resized.close()

    # For newline
    st.sidebar.write('\n')

    with st.spinner('Bildunterschrift generieren ...'):

        caption = predict(image)

        caption_en = caption
        st.subheader(caption_en)

    st.sidebar.write("ViT-GPT2's Einschätzung:")
    st.sidebar.header(f"{caption}")

    image.close()

Please write in confident tone, conversational writing style, German language.

---

**ChatGPT:**
Ja, Sie können Ihren Code so modifizieren, dass die Übersetzung mithilfe der DeepL API vorgenommen wird. Hier ist ein Beispiel dafür:
```python
python
Copy code
import streamlit as st
import requests
import io

# Fügen Sie diese Zeile hinzu, um die DeepL API zu importieren
import deepl

# Designing the interface
st.title("AI basierte Bildbeschreibungen")

st.sidebar.markdown(
    """
    Ein Bilduntertitelmodell durch Kombination des ViT-Modells mit dem GPT2-Modell\n
    """
)

with st.spinner('Laden und Kompilieren des ViT-GPT2-Modells ...'):
    from model import *

random_image_id = get_random_image_id()

st.sidebar.title("Wähle eine Beispieldatei aus")
sample_image_id = st.sidebar.selectbox(
    "Wähle aus dem Dropdown Menü aus",
    sample_image_ids
)

if st.sidebar.button("Random COCO 2017 (val) images"):
#if st.sidebar.button("Zufälliges Bild"):
    random_image_id = get_random_image_id()
    sample_image_id = "None"

bytes_data = None
with st.sidebar.form("file-uploader-form", clear_on_submit=True):
    uploaded_file = st.file_uploader("Wähle eine lokale Datei aus")
    submitted = st.form_submit_button("Upload")
    if submitted and uploaded_file is not None:
        bytes_data = io.BytesIO(uploaded_file.getvalue())

if (bytes_data is None) and submitted:

    st.write("Es wurde noch keine Datei ausgewählt, die hochgeladen werden könnte")

else:

    image_id = random_image_id
    if sample_image_id != "Kein Bild ausgewählt":
        assert type(sample_image_id) == int
        image_id = sample_image_id

    sample_name = f"COCO_val2017_{str(image_id).zfill(12)}.jpg"
    sample_path = os.path.join(sample_dir, sample_name)

    if bytes_data is not None:
        image = Image.open(bytes_data)
    elif os.path.isfile(sample_path):
        image = Image.open(sample_path)
    else:
        url = f"http://images.cocodataset.org/val2017/{str(image_id).zfill(12)}.jpg"
        image = Image.open(requests.get(url, stream=True).raw)

    width, height = image.size
    resized = image.resize(size=(width, height))
    if height > 384:
        width = int(width
```

---

**User:**
Du hast abgebrochen. Bitte führe den Code weiter aus, aber schreibe nur das, was nach dem hier kommt:     width, height = image.size
    resized = image.resize(size=(width, height))
    if height > 384:

Please write in confident tone, conversational writing style, German language.

---

**ChatGPT:**
Ja, kein Problem. Hier ist der restliche Code:
```python
python
Copy code
if height > 384:
    width = int(width / height * 384)
    height = 384
    resized = resized.resize(size=(width, height))
width, height = resized.size
if width > 512:
    width = 512
    height = int(height / width * 512)
    resized = resized.resize(size=(width, height))

if bytes_data is None:
    st.markdown(f"[{str(image_id).zfill(12)}.jpg](http://images.cocodataset.org/val2017/{str(image_id).zfill(12)}.jpg)")
show = st.image(resized)
show.image(resized, '\n\nAusgewähltes Bild')
resized.close()

# For newline
st.sidebar.write('\n')

with st.spinner('Bildunterschrift generieren ...'):
    caption = predict(image)

    caption_en = caption
    st.subheader(caption_en)

# Fügen Sie diesen Code hinzu, um die Übersetzung durchzuführen
caption_de = deepl.translate(caption_en, target_language='DE')

st.sidebar.write("ViT-GPT2's Einschätzung:")
st.sidebar.header(f"{caption_en}")

# Fügen Sie diese Zeile hinzu, um die Übersetzung auszugeben
st.header(f"Übersetzung: {caption_de}")

image.close()
```
Sie können die Übersetzung in einer neuen Variablen <code>caption_de</code> speichern und dann mithilfe von <code>st.header</code> ausgeben.