
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:47:49
```

---


**User:**
Ich habe über mein cli über curl eine Anfrage an dich geschickt. Hier siehst du den Code dazu:

```
curl https://api.openai.com/v1/chat/completions \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer sk-GgA5[...]6M9' \
  -d '{
  "model": "gpt-3.5-turbo",
  "messages": [{"role": "user", "content": "Hallo! Magst du chatten?"}]
}'
```
Und bekam das Folgende als Response:

```
{"id":"chatcmpl-6scEaaGT2ChjhD7iErGNZN3M9G8Wq","object":"chat.completion","created":1678474704,"model":"gpt-3.5-turbo-0301","usage":{"prompt_tokens":15,"completion_tokens":19,"total_tokens":34},"choices":[{"message":{"role":"assistant","content":"\n\nJa, ich stehe gerne zur Verfügung. Was benötigst du Hilfe bei?"},"finish_reason":"stop","index":0}]}
```

Wie lautet der richtige jq Befehl, um das Response so zu parsen, dass nur die Antwort aus "content" heraus kommt und nicht die Informationen drum herum noch?

---

**ChatGPT:**
Um nur den Inhalt aus dem "content"-Feld zu extrahieren, kannst du den folgenden jq-Befehl verwenden:
```arduino
arduino
Copy code
jq '.choices[0].message.content'
```
Wenn du die Response-Daten in einer Datei namens "response.json" gespeichert hast, kannst du den Befehl wie folgt ausführen:
```bash
bash
Copy code
cat response.json | jq '.choices[0].message.content'
```
Dies sollte das folgende Ergebnis liefern:
```swift
swift
Copy code
"\n\nJa, ich stehe gerne zur Verfügung. Was benötigst du Hilfe bei?"
```