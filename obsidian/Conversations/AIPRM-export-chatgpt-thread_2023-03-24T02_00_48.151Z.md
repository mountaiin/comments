
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 03:00:48
```

---


**User:**
wie entferne ich ein submodule wieder? ich habe es bisher nur über git submodule add und über git submodule update --init --recursive eingerichtet, aber noch kein staging oder committing gehabt. ich möchte es wieder entfernen, wie geht das?

Please write in informal tone, instructive writing style, German language.

---

**ChatGPT:**
Kein Problem, um ein Git-Submodul zu entfernen, folge einfach diesen Schritten:
<li><p>Öffne die <code>.gitmodules</code>-Datei in deinem Projektordner und lösche die Zeilen, die das Submodul betreffen. Die Zeilen sollten in etwa so aussehen:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>bash</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-bash">[submodule <span class="hljs-string">"Name-des-Submoduls"</span>]
path = Pfad/zum/Submodul
url = https://github.com/Beispiel/Beispiel-Repository.git
</code></div></div></pre></li><li><p>Öffne die <code>.git/config</code>-Datei in deinem Projektordner und lösche auch hier die entsprechenden Zeilen, die das Submodul betreffen. Die Zeilen sollten so ähnlich aussehen wie die in der <code>.gitmodules</code>-Datei.</p></li><li><p>Führe diesen Befehl in deinem Terminal aus, um den Eintrag des Submoduls aus dem Git-Index zu entfernen:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>bash</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-bash">git <span class="hljs-built_in">rm</span> --cached Pfad/zum/Submodul
</code></div></div></pre><p>Achte darauf, dass du "Pfad/zum/Submodul" durch den tatsächlichen Pfad zu dem Submodul ersetzt, das du entfernen möchtest.</p></li><li><p>Lösche nun den Ordner des Submoduls aus deinem Projekt:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>bash</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-bash"><span class="hljs-built_in">rm</span> -rf Pfad/zum/Submodul
</code></div></div></pre></li><li><p>Zum Schluss führst du noch einen Commit aus, um die Änderungen zu speichern:</p><pre><div class="bg-black rounded-md mb-4"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>sql</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-sql">git <span class="hljs-keyword">commit</span> <span class="hljs-operator">-</span>m "Submodul entfernt"
</code></div></div></pre></li>
Das war's! Du hast jetzt das Submodul aus deinem Projekt entfernt.