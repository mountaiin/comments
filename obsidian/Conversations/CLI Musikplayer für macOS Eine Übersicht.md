
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:48:08
```

---


**User:**
Was gibt es für cli musik player für macos?

---

**ChatGPT:**
Für macOS gibt es einige Befehlszeilen-basierte Musikplayer, darunter:
<li><p>CMUS - Ein vollständiger Musikplayer mit einer benutzerfreundlichen Befehlszeile.</p></li><li><p>Moc (Music On Console) - Ein Musikplayer, der einfach zu bedienen ist und eine große Auswahl an Formaten unterstützt.</p></li><li><p>ncmpcpp - Ein ncurses-basierter Musikplayer, der eine benutzerfreundliche Oberfläche bietet und mit einer Vielzahl von Musik-Backends wie MPD kompatibel ist.</p></li><li><p>mplayer - Ein universeller Medienplayer mit einer Befehlszeile, der eine große Auswahl an Audio- und Videoformaten unterstützt.</p></li>
Dies sind nur einige der vielen Befehlszeilen-basierten Musikplayer für macOS. Es hängt von Ihren Anforderungen und Vorlieben ab, welcher am besten für Sie geeignet ist.

siehe auch [[ocp]]