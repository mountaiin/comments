
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:48:03
```

---


**User:**
Kann ich text über meine clo an aws Polly senden und die audio ausgabe als mp3 erhalten, ganz ohne, dass ich aws-cli installiert habe?

---

**ChatGPT:**
Ja, Sie können Text an Amazon Polly senden und die Audiausgabe als MP3-Datei erhalten, ohne dass Sie die AWS-CLI installieren müssen. Eine Möglichkeit besteht darin, die AWS SDKs zu verwenden, um eine Anwendung zu erstellen, die Polly-Anfragen sendet und die Antworten empfängt. Es gibt AWS SDKs für eine Vielzahl von Programmiersprachen, einschließlich Java, .NET, PHP, Python und Ruby.
Eine andere Möglichkeit besteht darin, die Polly-APIs direkt über HTTP-Anfragen aufzurufen. Hierfür benötigen Sie jedoch Zugriff auf Ihre AWS-Sicherheitsschlüssel und müssen sicherstellen, dass Ihre Anfragen den Anforderungen von AWS entsprechen.
In beiden Fällen sollten Sie die Polly-Dokumentation sorgfältig lesen, um zu verstehen, wie Sie Polly verwenden können, und um sicherzustellen, dass Sie die Anforderungen an die Verwendung von AWS-Diensten erfüllen.