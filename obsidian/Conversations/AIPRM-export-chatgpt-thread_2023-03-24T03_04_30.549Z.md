
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 04:04:30
```

---


**User:**
Wie startet man in vim eine Suche nach einem bestimmten Wort?

---

**ChatGPT:**
Um in Vim nach einem bestimmten Wort zu suchen, können Sie den Befehl "/" (ohne Anführungszeichen) verwenden, gefolgt vom Wort, nach dem Sie suchen möchten. Hier ist ein Beispiel:
<li>Öffnen Sie eine Datei in Vim.</li><li>Drücken Sie "/" (ohne Anführungszeichen).</li><li>Geben Sie das Wort ein, nach dem Sie suchen möchten, und drücken Sie die Eingabetaste.</li><li>Vim springt zum ersten Vorkommen des Wortes. Wenn Sie ein weiteres Vorkommen suchen möchten, drücken Sie "n" für das nächste Vorkommen oder "N" für das vorherige Vorkommen.</li>
Beachten Sie, dass die Suche in Vim standardmäßig zwischen Groß- und Kleinschreibung unterscheidet. Wenn Sie nach einem Wort suchen möchten, ohne zwischen Groß- und Kleinschreibung zu unterscheiden, fügen Sie einen "i" (ohne Anführungszeichen) hinzu, nachdem Sie "/" eingegeben haben. Zum Beispiel:
/:&lt;word&gt;/i
Dies wird nach dem Wort suchen, ohne zwischen Groß- und Kleinschreibung zu unterscheiden.