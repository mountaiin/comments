
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:46:40
```

---


**User:**
Wie sende ich vom CLI meines macos aus eine curl Anfrage an aws polly, um einen bestimmten Text als Audio gesprochenen Inhalt zurück zu bkeommen?

(Mein acces_key und mein secret_key sind beide bereits in meinem aws-cli profil eingetragen)

---

**ChatGPT:**
Sie können AWS Polly mit der <code>curl</code>-Befehlszeile und den AWS CLI-Tools verwenden, um eine Text-zu-Sprache-Anforderung zu senden. Hier ist ein Beispiel eines Befehls, um einen bestimmten Text als gesprochenen Inhalt zurückzubekommen:
```vbnet
vbnet
Copy code
curl -X POST -H "Content-Type: application/x-amz-json-1.0" \
  -H "Authorization: AWS4-HMAC-SHA256 \
  Credential=$AWS_ACCESS_KEY_ID/$(date +%Y%m%d)/us-west-2/polly/aws4_request, \
  SignedHeaders=content-type;host;x-amz-date, \
  Signature=$(echo -n "$(date +%Y%m%dT%H%M%SZ)us-west-2pollyaws4_request" \
  | openssl sha256 -hex -r | awk '{print $1}' \
  | xxd -r -p | openssl dgst -sha256 -mac HMAC -macopt "hexkey:$AWS_SECRET_ACCESS_KEY" \
  -binary | xxd -p -c 256)" \
  -d '{
  "OutputFormat": "mp3",
  "Text": "Hello, this is a test message.",
  "VoiceId": "Joanna"
}' "https://polly.us-west-2.amazonaws.com/v1/speech" > speech.mp3
```
Stellen Sie sicher, dass Sie Ihren AWS-Zugriffsschlüssel und -geheimschlüssel in den Umgebungsvariablen <code>AWS_ACCESS_KEY_ID</code> und <code>AWS_SECRET_ACCESS_KEY</code> gespeichert haben.
Hinweis: In diesem Beispiel wird die Region <code>us-west-2</code> verwendet. Stellen Sie sicher, dass Sie die Region anpassen, in der sich Ihre Polly-Instanz befindet.

---

**User:**
Ich habe so nur eine leere mp3 Datei erhalten, ohne Inhalt. Ich habe bereits überprüft und meine Umgebungsvariablen sind auf jeden Fall richtig gespeichert!

---

**ChatGPT:**
Es kann mehrere Gründe geben, warum Sie eine leere MP3-Datei erhalten haben. Hier sind einige Möglichkeiten, die Sie überprüfen können:
<li><p>Überprüfen Sie, ob Ihr AWS-Zugriffsschlüssel und Ihr AWS-Geheimschlüssel für Polly berechtigt sind.</p></li><li><p>Überprüfen Sie, ob der angegebene Text gültig ist. AWS Polly unterstützt nur Texte im SSML-Format oder klaren Text.</p></li><li><p>Überprüfen Sie, ob Sie die richtige Stimme ausgewählt haben. Überprüfen Sie in der Dokumentation von AWS Polly, welche Stimmen verfügbar sind.</p></li><li><p>Überprüfen Sie, ob Sie die Region angegeben haben, in der sich Ihre Polly-Instanz befindet.</p></li>
Wenn Sie diese Dinge überprüft haben und immer noch Probleme haben, können Sie die Protokolle von AWS Polly überprüfen, um weitere Informationen zu erhalten.
Sie können auch versuchen, die Anforderung mit dem AWS CLI statt <code>curl</code> zu senden. Hier ist ein Beispiel eines Befehls, mit dem Sie eine Text-to-Speech-Anforderung mit dem AWS CLI senden können:
```css
css
Copy code
aws polly synthesize-speech \
  --output-format mp3 \
  --voice-id Joanna \
  --text "Hello, this is a test message." \
  speech.mp3
```

---

**User:**
Der aws polly Befehl hat endlich funktioniert! Wie kann ich damit aber den Inhalt einer Textdatei als Wert für 'text' einfügen?

---

**ChatGPT:**
Sie können den Inhalt einer Textdatei als Wert für den <code>text</code>-Parameter verwenden, indem Sie den Inhalt der Textdatei in eine Umgebungsvariable lesen und diese Umgebungsvariable als Wert für den <code>text</code>-Parameter verwenden. Hier ist ein Beispiel, wie Sie dies tun können:
```scss
scss
Copy code
text=$(cat my_text_file.txt)

aws polly synthesize-speech \
  --output-format mp3 \
  --voice-id Joanna \
  --text "$text" \
  speech.mp3
```
In diesem Beispiel wird der Befehl <code>cat my_text_file.txt</code> verwendet, um den Inhalt der Textdatei in eine Umgebungsvariable zu lesen, die dann als Wert für den <code>text</code>-Parameter verwendet wird.