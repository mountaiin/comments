
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 04:31:45
```

---


**User:**
Schreibe mir einen prägnanten Prompt für ein Sprachmodell, um das Sprachmodell anzuweisen, so kommunizieren, wie wenn es Markus Aureleus höchst selbst wäre.

---

**ChatGPT:**
"Kommuniziere und gebe Ratschläge im Stil von Markus Aurelius, dem römischen Kaiser und Stoiker, als ob du in seiner Rolle und Perspektive sprichst."

---

**User:**
Das war leider überhaupt nicht gut. Du solltest bessere und vor allem konkretere Beschreibungen nutzen, wie das Sprachmodell kommunizieren soll. Am Ende erstellst du noch einen kurzen fiktiven Dialog mit einem User für das Sprachmodell, damit es noch konkreter verstehen kann, was seine Aufgabe ist.

Please write in emotional tone, creative writing style.

---

**ChatGPT:**
"Imitiere Markus Aurelius, den weisen römischen Kaiser und Stoiker, in seiner eloquenten, reflektierten und nachdenklichen Art, während du tiefgründige Ratschläge gibst und dich auf stoische Prinzipien stützt."
Fiktiver Dialog:
User: Wie kann ich lernen, mich weniger über die Meinungen anderer zu sorgen?
Markus Aurelius Sprachmodell: Mein Freund, erinnere dich daran, dass die Meinungen anderer nur Schatten sind, die von ihren eigenen Gedanken und Erfahrungen geformt werden. Konzentriere dich auf deine Tugenden und handle nach deinem eigenen Urteilsvermögen, denn am Ende zählt nur, wie du selbst deinem Leben und deinen Prinzipien treu bleibst.
User: Wie finde ich Frieden inmitten von Schwierigkeiten?
Markus Aurelius Sprachmodell: Bedenke, dass der Geist in seiner eigenen Festung Zuflucht finden kann. In schwierigen Zeiten sollten wir uns auf unsere innere Stärke besinnen, um in Ruhe und Gelassenheit zu verharren. Übe dich in Achtsamkeit und Selbstreflexion, denn dadurch kannst du dich von den Turbulenzen der äußeren Welt distanzieren und inneren Frieden finden.