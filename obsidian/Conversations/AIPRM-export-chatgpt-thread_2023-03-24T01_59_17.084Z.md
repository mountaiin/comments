
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:59:17
```

---


**User:**
Ich bräuchte deine Hilfe beim Erstellen eines Apple Kurzbefehls(Shortcuts). Ich einen Kurzbefehl, der vom User eine Frage entegegennimmt, diese Frage wird über eine API request an openAI's sprachmodell gpt-3 verschickt und daraufhin erhält der User die Antwort. So weit so gut. Ich habe nun eine loop funktion eingebaut, sodass nach der antwort der user wieder nach einer Frage gefragt wird. Aber das Problem, das ich habe ist, dass in der neuen/zweiten Frage, der Kontext von der vorherigen Frage und Antwort nicht mehr dabei ist. Wie mache ich es, dass Frage und Antwort bei jeder "Runde" an die nächste Runde angehängt werden, und sodass Anfrage an OpenAI quasi von Runde zu Runde an Umfang wächst?

---

**ChatGPT:**
Um den Kontext der vorherigen Frage und Antwort in jeder Runde beizubehalten, kannst du eine Variable erstellen, in der du den gesamten Textverlauf speicherst. Du aktualisierst diese Variable nach jeder Antwort und fügst sie dem nächsten API-Aufruf hinzu. Hier ist eine Schritt-für-Schritt-Anleitung, um dies in Apple Shortcuts zu tun:
<li>Erstelle eine Text-Variable namens <code>textVerlauf</code> und initialisiere sie mit einem leeren Text (oder einer Einführung, falls gewünscht).</li><li>Beginne deine Loop-Funktion (Wiederholen mit Jeder oder "Repeat with Each").</li><li>Verwende die "Fragen" Aktion, um eine Frage vom Benutzer zu erhalten, und speichere diese Frage in einer Variable namens <code>userFrage</code>.</li><li>Aktualisiere die <code>textVerlauf</code> Variable, indem du die neue Frage an den vorhandenen Text anhängst. Du kannst die "Text" Aktion verwenden, um dies zu tun:</li>
```csharp
csharp
Copy code
[textVerlauf]
[userFrage]
```
<li>Füge die aktualisierte <code>textVerlauf</code> Variable im OpenAI API-Aufruf ein. Achte darauf, dass der <code>prompt</code> Parameter den Wert der <code>textVerlauf</code> Variable enthält. Du könntest den API-Aufruf wie folgt anpassen:</li>
```json
json
Copy code
{
  "model": "text-davinci-002",
  "prompt": "[textVerlauf]",
  "temperature": 0.7,
  "max_tokens": 150
}
```
<li>Empfange die Antwort von GPT-3 und speichere sie in einer Variable namens <code>gpt3Antwort</code>.</li><li>Aktualisiere erneut die <code>textVerlauf</code> Variable, indem du die Antwort von GPT-3 hinzufügst:</li>
```csharp
csharp
Copy code
[textVerlauf]
[gpt3Antwort]
```
<li>Zeige die Antwort mit der Aktion "Benachrichtigung anzeigen" oder einer ähnlichen Aktion an.</li><li>Wiederhole Schritte 3 bis 8 für jede neue Runde in der Loop-Funktion.</li>
Auf diese Weise behältst du den Kontext der vorherigen Fragen und Antworten bei und der Umfang der Anfrage an OpenAI wächst von Runde zu Runde.