
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 04:04:04
```

---


**User:**
Gibt es vergleichbare tools wie sourcetree, die aber im cli, also als TUI laufen?

---

**ChatGPT:**
Ja, es gibt mehrere Tools, die ähnliche Funktionen wie Sourcetree haben, aber als CLI-Tools (TUI) laufen. Einige der bekanntesten Tools sind:
<li><p>GitKraken Glo Boards: GitKraken Glo Boards ist ein kostenloses Projektmanagement-Tool, das auf der Kommandozeile läuft. Es ist eine TUI-Version des webbasierten Projektmanagement-Tools GitKraken Glo.</p></li><li><p>tig: Tig ist ein Open-Source-Tool, das auf der Kommandozeile läuft. Es bietet eine TUI-Schnittstelle zur Verwaltung von Git-Repositories.</p></li><li><p>lazygit: Lazygit ist ein Open-Source-Tool, das auf der Kommandozeile läuft. Es bietet eine TUI-Schnittstelle zur Verwaltung von Git-Repositories. Lazygit ist besonders nützlich für Git-Anfänger, da es einfache und intuitive Befehle bietet.</p></li><li><p>Magit: Magit ist ein Emacs-Plugin, das eine TUI-Schnittstelle zur Verwaltung von Git-Repositories bietet. Es bietet viele Funktionen, einschließlich der Möglichkeit, Git-Operationen wie das Staging von Änderungen und das Erstellen von Branches direkt aus Emacs heraus auszuführen.</p></li>
Diese Tools bieten ähnliche Funktionen wie Sourcetree, aber sie laufen auf der Kommandozeile und können somit nützlich sein, wenn man sich mit der Arbeit auf der Kommandozeile vertrauter fühlt oder sich auf die Automatisierung von Aufgaben durch die Verwendung von Skripten und Tools konzentrieren möchte.

---

**User:**
Ich habe inzwischen sehr viele git repos auf meinem computer und verliere langsam den überblick, ich brauche daher ein tool, das vor allem gut darin ist, einen überblick über mehrere git repos gleichzeztig anzuzeigen. Welches deiner vorher genannten würdest du für diesen zweck am ehesten empfehlen?

---

**ChatGPT:**
Für den Überblick über mehrere Git-Repositories würde ich dir das Tool "tig" empfehlen. Tig bietet eine interaktive TUI-Schnittstelle, die es ermöglicht, eine Übersicht über alle lokal vorhandenen Git-Repositories zu erhalten und durch diese zu navigieren.
Sobald du tig installiert hast, kannst du einfach in das Hauptverzeichnis deines Projektordners navigieren und das Tool starten. Von dort aus kannst du durch alle vorhandenen Repositories navigieren und dir eine Übersicht über alle Commits, Branches und Tags anzeigen lassen. Du kannst auch Änderungen in den Repositories vergleichen, Commits zusammenführen oder Änderungen rückgängig machen.
Tig ist sehr leistungsfähig und benutzerfreundlich, so dass du schnell und einfach eine Übersicht über all deine Git-Repositories erhalten und verwalten kannst.