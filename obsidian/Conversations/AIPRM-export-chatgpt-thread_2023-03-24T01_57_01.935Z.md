
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:57:01
```

---


**User:**
wie isntalliere ich zsh auf ubuntu? bitte kurze antworten

Please write in informal tone, instructive writing style, German language.

---

**ChatGPT:**
Um Zsh auf Ubuntu zu installieren, musst du zunächst das Paket "zsh" aus den Ubuntu-Repositories installieren. Das kannst du mit dem folgenden Befehl in der Terminal-App tun:
```arduino
arduino
Copy code
sudo apt-get install zsh
```
Nachdem die Installation abgeschlossen ist, musst du Zsh als dein Standard-Shellprogramm festlegen. Dazu kannst du den folgenden Befehl ausführen:
```bash
bash
Copy code
chsh -s $(which zsh)
```
Anschließend musst du deinen Computer neu starten oder sich aus- und wieder einloggen, damit die Änderungen wirksam werden.
Nach dem Neustart wird Zsh als deine Shell verwendet und du kannst mit dem Programm arbeiten.