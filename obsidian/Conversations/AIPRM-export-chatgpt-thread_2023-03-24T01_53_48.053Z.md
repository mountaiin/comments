
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:53:48
```

---


**User:**
Ich versuche über ein Apple shortcut eine api Anfrage an OpenAI zusenden. Allerdings erhalte ich folgende Fehlermeldung. Was bedeutet diese und wie löse ich sie? {{error":{"param":null,"message":"'content: How old are elephants?' is not of type 'array' - 'messages'","code":null,"type":"invalid_request_error"}}

---

**ChatGPT:**
Die Fehlermeldung deutet darauf hin, dass die API-Anfrage, die Sie an OpenAI senden, ein Problem mit dem Format der "messages" hat. Die Fehlermeldung sagt, dass der Wert für 'messages' nicht vom Typ "array" ist, wie es erwartet wird.
In der Fehlermeldung wird angegeben: <code>'content: How old are elephants?' is not of type 'array' - 'messages'</code>. Das bedeutet, dass der Teil <code>'content: How old are elephants?'</code> im Anfrage-JSON als Array formatiert sein sollte.
Hier ist ein Beispiel für eine korrekt formatierte Anfrage an die Chat-API von OpenAI:
```json
json
Copy code
{
  "messages": [
    {"role": "system", "content": "You are ChatGPT, a large language model trained by OpenAI."},
    {"role": "user", "content": "How old are elephants?"}
  ],
  "max_tokens": 50
}
```
In diesem Beispiel ist "messages" ein Array mit zwei Objekten, die jeweils eine "role" und einen "content" enthalten. Achten Sie darauf, dass Ihre Anfrage in Ihrem Apple Shortcut auf ähnliche Weise formatiert ist.
Um das Problem zu beheben, stellen Sie sicher, dass Ihre API-Anfrage die erforderlichen Parameter in der richtigen Formatierung enthält. Im Fall von Apple Shortcuts können Sie die "URL" und "Get Contents of URL" Aktionen verwenden, um die Anfrage richtig zu formatieren. Achten Sie darauf, dass der "messages" Parameter als Array mit den erforderlichen Objekten gesendet wird.