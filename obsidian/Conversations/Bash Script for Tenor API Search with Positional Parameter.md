---
tags: technologie
---

**User:**
Ich möchte, dass aus dieser ANfrage ein Bash SKript wird und annstatt dem Wirt "excited" möchte ich ein positional paramter angeben können:

curl "https://tenor.googleapis.com/v2/search?q=excited&key=AIzaSyBUBQRYo7NNWEmo18TXH4KsFwfY2lNGn3o&client_key=my_test_app&limit=1"

Please write in serious tone, analytical writing style, German language.

---

**ChatGPT:**
Sehr geehrter Benutzer,
nach Ihrem Wunsch habe ich ein Bash-Skript erstellt, das einen Positionsparameter akzeptiert, um den gewünschten Suchbegriff an die Tenor-API zu übermitteln und die Ergebnisse zu begrenzen.
#!/bin/bash
Positional parameter 1: Suchbegriff
Positional parameter 2: Ergebnisbegrenzung
API-Parameter: client_key, key
SEARCH_TERM=$1
RESULT_LIMIT=$2
CLIENT_KEY=my_test_app
API_KEY=AIzaSyBUBQRYo7NNWEmo18TXH4KsFwfY2lNGn3o
URL erstellen
URL="<a href="https://tenor.googleapis.com/v2/search?q=$SEARCH_TERM&amp;key=$API_KEY&amp;client_key=$CLIENT_KEY&amp;limit=$RESULT_LIMIT" target="_new">https://tenor.googleapis.com/v2/search?q=$SEARCH_TERM&amp;key=$API_KEY&amp;client_key=$CLIENT_KEY&amp;limit=$RESULT_LIMIT</a>"
API-Aufruf
curl $URL
Ich hoffe, dass dieses Skript Ihren Anforderungen entspricht und Ihnen bei Ihrer Arbeit hilft.
Mit freundlichen Grüßen,
ChatGPT