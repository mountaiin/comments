
**User:**
Ich möchte aus folgendem response nur den von „content“ erhalten. Wie löse ich das mit jq? Hier der Code:

{
  "id": "chatcmpl-123",
  "object": "chat.completion",
  "created": 1677652288,
  "choices": [{
    "index": 0,
    "message": {
      "role": "assistant",
      "content": "\n\nHello there, how may I assist you today?",
    },
    "finish_reason": "stop"
  }],
  "usage": {
    "prompt_tokens": 9,
    "completion_tokens": 12,
    "total_tokens": 21
  }
}


---

**ChatGPT:**
Um nur den Wert von "content" zu erhalten, können Sie folgenden Befehl verwenden:
```swift
swift
Copy code
echo '{
  "id": "chatcmpl-123",
  "object": "chat.completion",
  "created": 1677652288,
  "choices": [{
    "index": 0,
    "message": {
      "role": "assistant",
      "content": "\n\nHello there, how may I assist you today?",
    },
    "finish_reason": "stop"
  }],
  "usage": {
    "prompt_tokens": 9,
    "completion_tokens": 12,
    "total_tokens": 21
  }
}' | jq '.choices[0].message.content'
```
Das Ergebnis sollte folgendes sein:
```swift
swift
Copy code
"\n\nHello there, how may I assist you today?"
```