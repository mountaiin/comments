
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 04:31:07
```

---


**User:**
Ich möchte gerne auf meinem MacBook Pro ein multiboot System haben. Mein MacBook ist vom Jahr 2016 und hat einen Intel Prozessor. Ich würde gerne meine interne Festplatte (500gb) aufteilen und sowohl Linux, als auch Windows, als auch macOS drauf haben. Wie kann ich das machen? 

---

**ChatGPT:**
Um ein Multiboot-System mit macOS, Linux und Windows auf deinem MacBook Pro (2016) mit Intel-Prozessor zu erstellen, kannst du die folgenden Schritte befolgen:
<li><p>Sichere deine Daten: Bevor du mit dem Einrichten eines Multiboot-Systems beginnst, solltest du ein Backup deiner wichtigen Daten erstellen, falls während des Prozesses etwas schief geht.</p></li><li><p>macOS bereits installiert: Da macOS bereits auf deinem MacBook Pro vorinstalliert ist, musst du es nicht erneut installieren. Stelle jedoch sicher, dass du das neueste verfügbare macOS-Update installiert hast.</p></li><li><p>Windows installieren:</p></li>
a. Lade das Windows 10 ISO-Abbild von der Microsoft-Website herunter: <a href="https://www.microsoft.com/de-de/software-download/windows10ISO" target="_new">https://www.microsoft.com/de-de/software-download/windows10ISO</a>
b. Öffne die Boot Camp-Assistent-App, die auf deinem MacBook vorinstalliert ist (zu finden in /Applications/Utilities).
c. Folge den Anweisungen im Boot Camp-Assistenten, um eine Partition für Windows zu erstellen und das heruntergeladene ISO-Abbild auszuwählen. Der Assistent wird dir helfen, die Windows-Partition zu erstellen und die notwendigen Treiber zu installieren.
d. Nach Abschluss des Boot Camp-Assistenten wird dein MacBook neu gestartet und der Windows-Setup-Assistent gestartet. Folge den Anweisungen auf dem Bildschirm, um Windows zu installieren.
<li>Linux installieren:</li>
a. Lade das ISO-Abbild der gewünschten Linux-Distribution herunter (z.B. Ubuntu: <a href="https://ubuntu.com/download/desktop" target="_new">https://ubuntu.com/download/desktop</a>).
b. Erstelle einen bootfähigen USB-Stick mit dem heruntergeladenen ISO-Abbild. Du kannst dafür Programme wie Etcher (<a href="https://www.balena.io/etcher/" target="_new">https://www.balena.io/etcher/</a>) oder UNetbootin (<a href="https://unetbootin.github.io/" target="_new">https://unetbootin.github.io/</a>) auf deinem MacBook verwenden.
c. Starte dein MacBook neu und halte dabei die "Option" (⌥)-Taste gedrückt, um den Start-Manager aufzurufen. Wähle den USB-Stick aus, um von ihm zu starten.
d. Folge den Anweisungen auf dem Bildschirm, um Linux zu installieren. Achte darauf, während der Installation die benutzerdefinierte Partitionierungsoption zu wählen, um eine neue Partition für Linux zu erstellen, ohne die bestehenden macOS- und Windows-Partitionen zu beeinträchtigen.
e. Installiere den Bootloader (z.B. GRUB) auf der Linux-Partition, nicht auf dem Hauptlaufwerk. Dies verhindert Konflikte mit dem macOS-Bootloader.
<li>Nachdem du alle Betriebssysteme installiert hast, kannst du dein MacBook Pro neu starten und die "Option" (⌥)-Taste gedrückt halten, um den Start-Manager aufzurufen. Hier kannst du das Betriebssystem auswählen, das du starten möchtest.</li>
Das war's! Du solltest jetzt ein funktionierendes Multiboot-System mit macOS, Linux und Windows auf deinem MacBook Pro haben.

---

**User:**
Und in welchem Schritt unterteile ich die Festplatte in verschiedene Partitionen?

---

**ChatGPT:**
Entschuldigung für das Missverständnis. Hier ist eine klarere Anleitung zum Erstellen von Partitionen für jedes Betriebssystem:
<li>Partitionieren der Festplatte:</li>
a. Öffne die App "Festplattendienstprogramm" auf deinem MacBook (zu finden in /Applications/Utilities).
b. Wähle dein internes Laufwerk (meistens "APPLE SSD" oder "Macintosh HD") in der linken Seitenleiste und klicke auf die Schaltfläche "Partitionieren" oben in der App.
c. Du solltest jetzt eine schematische Darstellung deiner aktuellen Partition(en) sehen. Klicke auf das Pluszeichen (+), um eine neue Partition für Windows zu erstellen. Wähle als Format "ExFAT" und weise die gewünschte Größe zu (z.B. 100 GB).
d. Füge auf die gleiche Weise eine weitere Partition für Linux hinzu. Wähle als Format "MS-DOS (FAT)" und weise die gewünschte Größe zu (z.B. 100 GB).
e. Klicke auf "Anwenden", um die Partitionen zu erstellen. Das Festplattendienstprogramm erstellt nun die neuen Partitionen, ohne deine bestehenden macOS-Daten zu beeinträchtigen.
<li><p>Windows installieren: Befolge die Schritte 3a-3d aus meiner vorherigen Antwort, um Windows mit Boot Camp-Assistenten zu installieren. Stelle sicher, dass du während des Windows-Setups die zuvor erstellte ExFAT-Partition auswählst und formatierst (in NTFS).</p></li><li><p>Linux installieren: Befolge die Schritte 4a-4e aus meiner vorherigen Antwort, um Linux von einem bootfähigen USB-Stick zu installieren. Wähle während der Linux-Installation die benutzerdefinierte Partitionierungsoption und verwende die zuvor erstellte FAT-Partition. Du musst diese Partition noch in zwei Teile aufteilen: eine für das Linux-Dateisystem (z.B. ext4) und eine für den Swap-Bereich. Die Größe des Swap-Bereichs hängt von deinen Anforderungen ab, aber als Faustregel kann die Größe deines RAMs verwendet werden.</p></li>
Nachdem du alle Betriebssysteme installiert hast, kannst du wie zuvor erklärt zwischen ihnen wechseln, indem du beim Start die "Option" (⌥)-Taste gedrückt hältst.