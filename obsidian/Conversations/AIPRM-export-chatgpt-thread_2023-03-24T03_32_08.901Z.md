
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 04:32:08
```

---


**User:**
Was bedeuten die folgenden Fehler?

Launching Web UI with arguments: --upcast-sampling --no-half-vae --use-cpu interrogate
Warning: caught exception 'Torch not compiled with CUDA enabled', memory monitor disabled
No module 'xformers'. Proceeding without it.
Error loading script: quickcss.py
Traceback (most recent call last):
  File "/Users/yazan_mk/stable-diffusion-webui/modules/scripts.py", line 248, in load_scripts
    script_module = script_loading.load_module(scriptfile.path)
  File "/Users/yazan_mk/stable-diffusion-webui/modules/script_loading.py", line 11, in load_module
    module_spec.loader.exec_module(module)
  File "<frozen importlib._bootstrap_external>", line 883, in exec_module
  File "<frozen importlib._bootstrap>", line 241, in _call_with_frames_removed
  File "/Users/yazan_mk/stable-diffusion-webui/extensions/sd-web-ui-radius-theme/scripts/quickcss.py", line 386, in <module>
    tab = MyTab(basedir)
  File "/Users/yazan_mk/stable-diffusion-webui/extensions/sd-web-ui-radius-theme/scripts/quickcss.py", line 31, in __init__
    self.styles_list = self.get_files(self.style_folder)
  File "/Users/yazan_mk/stable-diffusion-webui/extensions/sd-web-ui-radius-theme/scripts/quickcss.py", line 330, in get_files
    focus_list = [file_name if not split else os.path.splitext(file_name)[0] for file_name in os.listdir(folder) if os.path.isfile(os.path.join(folder, file_name)) and file_name not in file_filter]
FileNotFoundError: [Errno 2] No such file or directory: '/Users/yazan_mk/stable-diffusion-webui/extensions/sd-web-ui-radius-theme/style_choices'

Loading weights [6ce0161689] from /Users/yazan_mk/stable-diffusion-webui/models/Stable-diffusion/v1-5-pruned-emaonly.safetensors
Creating model from config: /Users/yazan_mk/stable-diffusion-webui/configs/v1-inference.yaml
LatentDiffusion: Running in eps-prediction mode
DiffusionWrapper has 859.52 M params.

---

**ChatGPT:**
Die Fehler- und Warnmeldungen, die in diesem Auszug angezeigt werden, lassen sich wie folgt interpretieren:
<li><p><code>Warning: caught exception 'Torch not compiled with CUDA enabled', memory monitor disabled</code>: Diese Warnung weist darauf hin, dass die PyTorch-Bibliothek ohne CUDA-Unterstützung kompiliert wurde. Das bedeutet, dass die GPU-Beschleunigung nicht verfügbar ist und die Berechnungen nur auf der CPU ausgeführt werden. Das Memory-Monitoring wurde aufgrund dieser Ausnahme deaktiviert.</p></li><li><p><code>No module 'xformers'. Proceeding without it.</code>: Diese Meldung zeigt an, dass das Modul "xformers" nicht gefunden wurde. Das Programm wird ohne dieses Modul fortgesetzt. Abhängig von der Rolle des Moduls in Ihrem Programm kann dies zu eingeschränkter Funktionalität oder weiteren Fehlern führen.</p></li><li><p><code>Error loading script: quickcss.py</code>: Dieser Fehler weist darauf hin, dass es Probleme beim Laden des Skripts "quickcss.py" gibt. Der folgende Traceback gibt genauere Informationen über das zugrunde liegende Problem.</p></li><li><p><code>FileNotFoundError: [Errno 2] No such file or directory: '/Users/yazan_mk/stable-diffusion-webui/extensions/sd-web-ui-radius-theme/style_choices'</code>: Dieser Fehler zeigt an, dass das Programm versucht hat, auf einen Ordner zuzugreifen, der nicht vorhanden ist. Der angegebene Pfad "/Users/yazan_mk/stable-diffusion-webui/extensions/sd-web-ui-radius-theme/style_choices" existiert nicht. Um diesen Fehler zu</p></li>