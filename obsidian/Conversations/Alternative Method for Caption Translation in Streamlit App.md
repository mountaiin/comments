

**User:**
Ich habe in meiner streamlit app eine deepl funktion eingefügt, die `caption` auf deutsch übersetzen soll. ich möchte das aber ändern und da, wo deepl vorkommt:

```# For newline
st.sidebar.write('\n')

with st.spinner('Bildunterschrift generieren ...'):
    caption = predict(image)

    caption_en = caption
    st.subheader(caption_en)

# Fügen Sie diesen Code hinzu, um die Übersetzung durchzuführen
caption_de = deepl.translate(caption_en, target_language='DE')

st.sidebar.write("ViT-GPT2's Einschätzung:")
st.sidebar.header(f"{caption_en}")

# Fügen Sie diese Zeile hinzu, um die Übersetzung auszugeben
st.header(f"Übersetzung: {caption_de}")

image.close()
``` 
soll stattdessen die folgende google translate anfrage geschehen, aber text soll als data 'q gelten. schreibe den code bitte um, sodass google statt deepl den text übersetzt:

```curl --silent --request POST \
	--url https://google-translate1.p.rapidapi.com/language/translate/v2 \
	--header 'Accept-Encoding: application/gzip' \
	--header 'X-RapidAPI-Host: google-translate1.p.rapidapi.com' \
	--header 'X-RapidAPI-Key: 6cd416d137msh549dd7f03005ad2p15e77fjsn1dc0f25b6c42' \
	--header 'content-type: application/x-www-form-urlencoded' \
	--data 'q=HIER SOLL DER CAPTION TEXT HIN' \
	--data target=de \
	--data source=en | jq '.data.translations[0].translatedText'
```

Please write in confident tone, conversational writing style, German language.

---

**ChatGPT:**
Klar, ich kann dir helfen, den Code entsprechend zu ändern, damit Google Translate den Text übersetzt. Hier ist der umgeschriebene Code:
```python
python
Copy code
import requests
import json
import streamlit as st

# For newline
st.sidebar.write('\n')

with st.spinner('Bildunterschrift generieren ...'):
    caption = predict(image)

    caption_en = caption
    st.subheader(caption_en)

# Übersetzung mit Google Translate
url = "https://google-translate1.p.rapidapi.com/language/translate/v2"

payload = f"q={caption_en}&target=de&source=en"
headers = {
    "X-RapidAPI-Host": "google-translate1.p.rapidapi.com",
    "X-RapidAPI-Key": "6cd416d137msh549dd7f03005ad2p15e77fjsn1dc0f25b6c42",
    "content-type": "application/x-www-form-urlencoded"
}

response = requests.post(url, data=payload, headers=headers)

# Ausgabe der Übersetzung
if response.status_code == 200:
    caption_de = json.loads(response.text)['data']['translations'][0]['translatedText']
    st.header(f"Übersetzung: {caption_de}")
else:
    st.error("Fehler beim Übersetzen.")

st.sidebar.write("ViT-GPT2's Einschätzung:")
st.sidebar.header(f"{caption_en}")

image.close()
```
Ich habe den <code>deepl.translate()</code>-Aufruf durch eine HTTP-Anfrage an die Google Translate API ersetzt, die den Text in der Variable <code>caption_en</code> übersetzt und das Ergebnis in der Variable <code>caption_de</code> speichert. Beachte, dass ich auch eine Fehlerbehandlung eingebaut habe, falls es Probleme bei der Übersetzung gibt.
Ich hoffe, das hilft dir weiter! Lass es mich wissen, wenn du weitere Fragen hast.