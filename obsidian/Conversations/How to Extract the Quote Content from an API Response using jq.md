**User:**
Ich möchte aus dem folgenden API response nur den Inhalt von Quote erhalten. Wie mache ich das mit jq?

 curl --request GET \                                                                                      9:11
        --url https://random-quote-generator2.p.rapidapi.com/randomQuote \
        --header 'X-RapidAPI-Host: random-quote-generator2.p.rapidapi.com' \
        --header 'X-RapidAPI-Key: 6cd416d137msh549dd7f03005ad2p15e77fjsn1dc0f25b6c42'
[{"_id": {"$oid": "6191751f02b6c0a61ed8e934"}, "Quote": "If fretting was an Olympic sport, I\u2019d own the gold medal", "Author": "Lori Hatcher", "Tags": "anxiety, fretting, prayer, worry"}]%       

Please write in serious tone, analytical writing style, German language.

---

**ChatGPT:**
Um den Inhalt des Feldes "Quote" aus der JSON-Antwort der API mit Hilfe von jq zu extrahieren, können Sie die folgende jq-Abfrage verwenden:
```bash
bash
Copy code
curl --request GET \
    --url https://random-quote-generator2.p.rapidapi.com/randomQuote \
    --header 'X-RapidAPI-Host: random-quote-generator2.p.rapidapi.com' \
    --header 'X-RapidAPI-Key: 6cd416d137msh549dd7f03005ad2p15e77fjsn1dc0f25b6c42' \
    | jq -r '.[0].Quote'
```
Die Option <code>-r</code> wird verwendet, um das Ergebnis als Rohtext zu formatieren, ohne Anführungszeichen um den Text. Die Abfrage <code>. [0] .Quote</code> wählt das erste Element des Arrays aus und gibt den Wert des Feldes "Quote" zurück.
Die obige Abfrage gibt nur den Inhalt des Feldes "Quote" zurück, der Inhalt aller anderen Felder wird ignoriert. Wenn Sie mehrere Felder benötigen, können Sie die Abfrage entsprechend anpassen. Zum Beispiel können Sie folgende Abfrage verwenden, um den Inhalt der Felder "Quote" und "Author" zurückzugeben:
```bash
bash
Copy code
curl --request GET \
    --url https://random-quote-generator2.p.rapidapi.com/randomQuote \
    --header 'X-RapidAPI-Host: random-quote-generator2.p.rapidapi.com' \
    --header 'X-RapidAPI-Key: 6cd416d137msh549dd7f03005ad2p15e77fjsn1dc0f25b6c42' \
    | jq -r '.[0] | {quote: .Quote, author: .Author}'
```
Diese Abfrage gibt den Inhalt der Felder "Quote" und "Author" zurück und formatiert sie als JSON-Objekt mit den Feldnamen "quote" und "author".