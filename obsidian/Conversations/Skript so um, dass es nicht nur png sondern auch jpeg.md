


**User:**
Wie schreibe ich dieses Skript so um, dass es nicht nur png sondern auch jpeg/jpg nimmt und weiter sendet?
you.


if [ "$#" -ne 1 ]; then
  echo "Usage: $0 image_file"
  exit 1
fi

image_file="$1"
base64_image=$(base64 -i "$image_file" | tr -d '\n')

response=$(curl -X POST "https://phi0112358-google-vi.hf.space/run/predict" \
-H "Content-Type: application/json" \
-d '{"data":["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACklEQVR4nGMAAQAABQABDQottAAAAABJRU5ErkJggg=="]}')

echo "$response"

---

**ChatGPT:**
Dieses Skript kann so angepasst werden, dass es sowohl PNG- als auch JPEG-/JPG-Bilddateien akzeptiert:
```bash
bash
Copy code
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 image_file"
  exit 1
fi

image_file="$1"
extension="${image_file##*.}"

if [ "$extension" == "png" ]; then
  content_type="image/png"
elif [ "$extension" == "jpg" ] || [ "$extension" == "jpeg" ]; then
  content_type="image/jpeg"
else
  echo "Error: Unsupported image format. Only PNG and JPEG are supported."
  exit 1
fi

base64_image=$(base64 -i "$image_file" | tr -d '\n')

response=$(curl -X POST "https://phi0112358-google-vi.hf.space/run/predict" \
-H "Content-Type: application/json" \
-d "{\"data\":[\"data:$content_type;base64,$base64_image\"]}")

echo "$response"
```
Diese Änderungen prüfen die Dateierweiterung und passen den <code>Content-Type</code> im Header und im JSON-Datenkörper entsprechend an.