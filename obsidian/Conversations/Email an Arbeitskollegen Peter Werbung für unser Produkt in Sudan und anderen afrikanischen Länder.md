


**User:**
Ich muss eine email an meinen Arbeiteskollegen Peter schreiben. Der Inhalt ist der folgene und schreibe den Text bitte um, dass eine angemessene email wird:
Ich habe für unser Produkt im Sudan und weitere afrikanische Länder geworben. Es kann sein dass du eine E-Mail bekommst ohne mein Wissen. sag mir Bescheid falls jemand versucht mit dir Kontakt Kontakt aufzunehmen. Nutze bitte die „Du“ Form und nicht die Höflichkeitsform „Sie“.

---

**ChatGPT:**
Sehr geehrter Peter,
ich wollte dir mitteilen, dass ich mich kürzlich dafür eingesetzt habe, unser Produkt in Sudan und anderen afrikanischen Ländern zu bewerben. Es könnte sein, dass du unerwartet eine E-Mail von einem potenziellen Kunden erhältst, ohne dass ich davon weiß. Falls das der Fall ist, lass es mich bitte wissen, damit ich den Kontakt koordinieren kann.
Bitte verwende in unseren E-Mails die informelle Anrede "Du", anstatt der höflichen Anrede "Sie".
Beste Grüße,
[Dein Name]