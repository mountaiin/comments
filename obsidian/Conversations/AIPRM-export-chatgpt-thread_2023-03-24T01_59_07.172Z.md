
```
Exported with AIPRM https://www.aiprm.com by schaedling.monatlich_0w@icloud.com on 24.3.2023, 02:59:07
```

---


**User:**
Ich bräuchte deine Hilfe beim Erstellen eines Apple Kurzbefehls(Shortcuts). Ich einen Kurzbefehl, der vom User eine Frage entegegennimmt, diese Frage wird über eine API request an openAI's sprachmodell gpt-3 verschickt und daraufhin erhält der User die Antwort. So weit so gut. Ich habe nun eine loop funktion eingebaut, sodass nach der antwort der user wieder nach einer Frage gefragt wird. Aber das Problem, das ich habe ist, dass in der neuen/zweiten Frage, der Kontext von der vorherigen Frage und Antwort nicht mehr dabei ist. Wie mache ich es, dass Frage und Antwort bei jeder "Runde" an die nächste Runde angehängt werden, und sodass Anfrage an OpenAI quasi von Runde zu Runde an Umfang wächst?

Please write in informal tone, instructive writing style, German language.

---

**ChatGPT:**
Klar, ich helfe gerne weiter!
Um sicherzustellen, dass der Kontext der vorherigen Frage und Antwort auch in der nächsten Runde vorhanden ist, musst du den Text aus der vorherigen Runde speichern und bei jeder neuen Runde an den neuen Text anhängen. Du kannst dafür eine Variable erstellen, die du bei jeder Runde aktualisierst.
Hier ist ein Beispiel, wie du das umsetzen könntest:
<li><p>Erstelle eine Variable mit dem Namen "text" und setze sie auf einen leeren String:</p><ul><li>"text" = ""</li></ul></li><li><p>Wenn der Nutzer eine Frage stellt, schickst du diese Frage zusammen mit dem aktuellen "text" an die OpenAI-API. Du speicherst die Antwort in einer Variable mit dem Namen "antwort".</p></li><li><p>Danach aktualisierst du die "text"-Variable, indem du die Frage und die Antwort an den aktuellen Text anhängst.</p><ul><li>"text" = "Vorheriger Text. Frage: " + frage + " Antwort: " + antwort</li></ul></li><li><p>Anschließend stellst du die nächste Frage und die Schleife beginnt von vorne.</p></li>
Hier ist der gesamte Code mit diesen Änderungen:
```perl
perl
Copy code
Setze Variable "text" auf ""

Wiederhole die folgenden Schritte:

    Frage Nutzer nach der Frage
    Setze Variable "frage" auf die Antwort des Nutzers
    
    Setze Variable "antwort" auf den Rückgabewert von OpenAI mit "frage" und "text" als Eingabe
    
    Setze Variable "text" auf "Vorheriger Text. Frage: " + frage + " Antwort: " + antwort
    
    Gib die "antwort" aus
```
Durch diese Änderungen wird der vorherige Text, einschließlich der Frage und der Antwort, bei jeder neuen Runde an den neuen Text angehängt. Auf diese Weise wächst der Umfang der Anfrage an die OpenAI-API von Runde zu Runde.
Ich hoffe, das hilft dir weiter!