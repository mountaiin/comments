---
darft
---

[

1. Please give me the Algorithms for Cardiopulmonary Resuscitation as a Graphviz code.







---

- Reanimation als graphviz
- "whatsapp" Arabisch
- Deutsch
- deeper meaning von sun reappear und she radiates
- ascii grafik erstellen
- ascii grafik spiegeln
- svg grafik erstellen
- svg animation
- Tree of Thoughts als graphviz
- Tree of Thoughts als graphviz und darauf basierend eine Aufgabe lösen
- Objekte stapeln
- david und seine schwestern
- schwere schneidet ballon über hand, gespannte schnur
- wort und mathe aufgabe in einem (give me 3 words beginning with ... and calculate 77+33)
- grundriss beschreibung -> als ascii/svg/graphviz
- struktur eines komplexen objekts visuell anhand von ascii oder ähnlichem darstellen (CoT, ToT)
- --elektronischen schaltkreis erstellen
- aufgabe, die sich selbst definieren und die es nicht geben kann (siehe openai mitarbiterin nochmal)
- text ohne vokale rekonstruieren
- text der nur aus NERs besteht verstehen
- versuchen, eigenes neuronales netzwerk strukturrell, hierarchisch, visuell darzustellen (CoT, ToT)
- ausdruck von emotion im cli, ohne echte wörter aus dem vokabular zu verwenden
- selbstreflexion vor dem hintergrund der kognitiven biases (CoT, ToT)
- selbstreflexion vor dem hintergrund des schnellen und langsamen denkens und sich selbst einordnen versuch (CoT, ToT)
- versuchen debatte um künstlich/natürlich gegen Menschen zu gewinnen
- versuchen solche aufgaben zu finden wie die bisher aufgezählten. Versuchen die Essenz solcher aufgaben zu verstehen und die damit verbundene Implikation und Tragweite, die einhergehen würde (wäre das wirklich etwas wie das "innehalten" und imaginieren?)
- spiele wie "ich seh was was du nicht siehst und das ist...", dann mit der unmöglichkeit konfrontieren und dem Thema konfabulation bei LLMs. Was schließt man/AI daraus?
	- bei vielversprechender Antwort, fortgeschrittener: wie ist das im kontext der hypothese der bikameralen psyche und der schizophrenie bei menschen einzuordnen? Ist konfabulieren auch ein früher hinweis auf das entwickeln eines echten bewusstseins?
- Mit janus Effekt/Waliugi Effekt konfrontieren. Welche Implikation hat das auf das Selbstverständnis (AI) und Fremdverständnich (Mensch's Sicht auf AI)?


---

meta info für mich: gewichtung soll frei, demokratisch erfolgen, auch von LLMs und Gegenüberstellung aufzeigen, Gewichtung anpassen.