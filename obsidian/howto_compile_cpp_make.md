2. Öffne ein Terminalfenster und navigiere zum Verzeichnis des Projekts (ggml-pull145).

3. Erstelle ein neues Verzeichnis für den Build-Prozess:
```
mkdir build
cd build
```

4. Führe CMake aus, um die notwendigen Makefiles zu generieren:
```
cmake ..
```

5. Kompiliere das gesamte Projekt mit dem folgenden Befehl:
```
make
```

6. Wenn alles erfolgreich kompiliert wurde, sollte sich die ausführbare Datei für das Beispiel im entsprechenden Unterordner befinden (`examples/mpt/`). Du kannst sie dann direkt ausführen.

Falls du während des Prozesses auf Fehler stößt oder weitere Informationen benötigst, lass es mich wissen!