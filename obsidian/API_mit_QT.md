1) Um eine API-Anfrage mit Qt zu machen, kannst du die Qt Network-Bibliothek verwenden. Hier ist ein Beispiel, wie du das in C++ umsetzen kannst:

Zuerst musst du die notwendigen Module in deinem Projekt hinzufügen. Öffne die `.pro`-Datei deines Projekts und füge folgende Zeile hinzu:
```
QT += network
```

Dann füge die erforderlichen Header-Dateien in deine Haupt-C++-Datei (z.B. `mainwindow.cpp`) ein:
```cpp
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
```

Erstelle eine Funktion, um die API-Anfrage durchzuführen und die Antwort zu verarbeiten:
```cpp
void MainWindow::sendRequest(const QString &input) {
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url("https://api.openai.com/v1/chat/completions");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("Authorization", QString("Bearer %1").arg(OPENAI_API_KEY).toUtf8());

    QJsonObject data;
    data["model"] = "gpt-3.5-turbo";

    QJsonArray messages;
    messages.append(QJsonObject({{"role", "system"}, {"content", "You are a helpful assistant."}}));
    messages.append(QJsonObject({{"role", "user"}, {"content", input}}));

    data["messages"] = messages;

    QJsonDocument doc(data);
    QByteArray jsonData = doc.toJson();

    connect(manager, &QNetworkAccessManager::finished, this, &MainWindow::onApiReply);

    manager->post(request, jsonData);
}

void MainWindow::onApiReply(QNetworkReply *reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QByteArray responseData = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(responseData);
        QJsonObject obj = doc.object();
        QString result = obj["choices"].toArray()[0].toObject()["message"].toObject()["content"].toString();

        // Verarbeite das Ergebnis hier, z.B. zeige es in einem Textfeld an
    } else {
        // Fehlerbehandlung, z.B. zeige eine Fehlermeldung an
    }

    reply->deleteLater();
}
```

Vergiss nicht, den `OPENAI_API_KEY` durch deinen tatsächlichen API-Schlüssel zu ersetzen.

Du kannst die `sendRequest`-Funktion aufrufen, wenn der Benutzer auf einen Button klickt oder eine Nachricht eingibt. Zum Beispiel:
```cpp
connect(ui->sendButton, &QPushButton::clicked, [this]() {
    QString input = ui->inputLineEdit->text();
    sendRequest(input);
});
```

2) Wenn keine Beispiele im Qt Creator angezeigt werden, kann es sein, dass die entsprechenden Pakete nicht installiert sind. Um die Qt-Beispiele für Arch Linux zu installieren, führe folgenden Befehl aus:
```
sudo pacman -S qt5-examples
```

Nach der Installation sollte der Qt Creator die Beispiele anzeigen, sobald du ihn neu startest. Du kannst auch direkt auf die Beispiele zugreifen, indem du zum Installationsverzeichnis navigierst, normalerweise unter `/usr/share/doc/qt/examples`.