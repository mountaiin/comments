<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Disqus Iframe</title>
</head>
<body>
<div id="disqus_thread"></div>
<script>
    var disqus_config = function () {
        this.page.url = 'https://publish.obsidian.md/mountaiin/kreativer_Tanz'; // Ersetze PAGE_URL mit der tatsächlichen URL deiner Artikel-Seite
        this.page.identifier = 'kreativertanz'; // Ersetze PAGE_IDENTIFIER mit einem eindeutigen Bezeichner für deine Artikel-Seite
    };

    (function() {
        var d = document, s = d.createElement('script');
        s.src = 'https://mountaiin.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Bitte aktiviere JavaScript, um die <a href="https://disqus.com/?ref_noscript">Kommentare von Disqus anzuzeigen.</a></noscript>
</body>
</html>